package red.yancloud.www.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.socialize.weixin.view.WXCallbackActivity;
import red.yancloud.www.common.Constants;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/30 18:50
 * E_Mail：yh131412hys@163.com
 * Describe：微信登录、分享回调
 * Change：
 * @Version：V1.0
 */
public class WXEntryActivity extends WXCallbackActivity {

//    private IWXAPI mWxApi;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        mWxApi = WXAPIFactory.createWXAPI(this, Constants.PlatformParams.WX_APP_ID);
//        mWxApi.handleIntent(getIntent(), this);
//    }
//
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        setIntent(intent);
//        mWxApi.handleIntent(intent, this);
//    }
//
//    @Override
//    public void onReq(BaseReq baseReq) {
//
//    }
//
//    private static final String TAG = "WXEntryActivity";
//
//    @Override
//    public void onResp(BaseResp baseResp) {
//        int errCode = baseResp.errCode;
//        Log.d(TAG, "onResp: "+errCode);
////        微信登录授权回调
//        if(baseResp.getType() == ConstantsAPI.COMMAND_SENDAUTH){
//            SendAuth.Resp sendResp = (SendAuth.Resp) baseResp;
//            sendLoginResultBroadcast(errCode,sendResp.code);
//        }
////        微信分享回调
//        else if(baseResp.getType() ==ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX){
//            sendShareResultBroad(baseResp.errCode);
//        }
//        finish();
//    }
//
//
//    /**
//     * 发起微信登录授权结果本地广播
//     * @param resultCode
//     * @param authCode
//     */
//    private void sendLoginResultBroadcast(int resultCode,String authCode){
////        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
////        Intent loginAuthResult = new Intent();
////        loginAuthResult.setAction(WE_CHAT_LOGIN_RESULT_ACTION);
////        loginAuthResult.putExtra(WE_CHAT_LOGIN_RESULT_EXTRA,resultCode);
////        loginAuthResult.putExtra(WE_CHAT_LOGIN_RESULT_AUTH_CODE,authCode);
////        broadcastManager.sendBroadcast(loginAuthResult);
//    }
//
//
//    /**
//     * 发起微信分享结果本地广播
//     * @param resultCode
//     */
//    private void sendShareResultBroad(int resultCode){
////        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
////        Intent shareResult = new Intent();
////        shareResult.setAction(WE_CHAT_SHARE_RESULT_ACTION);
////        shareResult.putExtra(WE_CHAT_SHARE_RESULT_EXTRA,resultCode);
////        broadcastManager.sendBroadcast(shareResult);
//    }
}
