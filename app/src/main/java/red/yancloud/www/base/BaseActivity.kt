package red.yancloud.www.base

import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dyhdyh.widget.loading.bar.LoadingBar
import com.dyhdyh.widget.loading.dialog.LoadingDialog
import com.gyf.immersionbar.ImmersionBar
import com.gyf.immersionbar.OSUtils
import com.umeng.analytics.MobclickAgent
import red.yancloud.www.R
import red.yancloud.www.ui.dialog.LoadingCallback

import java.util.Objects

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/12 17:35
 * E_Mail：yh131412hys@163.com
 * Describe：基类Activity
 * Change：
 * @Version：V1.0
 */
abstract class BaseActivity : AppCompatActivity() {

    protected lateinit var mContext: Context

    protected lateinit var loadingDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        this.mContext = this
        //        ButterKnife.bind(this);
        //友盟统计应用启动数据
//        PushAgent.getInstance(mContext).onAppStart()
        loadingDialog = LoadingDialog.make(this, LoadingCallback()).create()
        loadingDialog.setCancelable(true)
        loadingDialog.setCanceledOnTouchOutside(true)
        initData()
        configViews()

        // 所有子类都将继承这些相同的属性,请在设置界面之后设置
        ImmersionBar.with(this)
            .fitsSystemWindows(true)  //使用该属性,必须指定状态栏颜色(处理布局与状态栏重叠问题)
            .statusBarColor(R.color.colorPrimaryDark)
            .init()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // 非必加
        // 如果你的app可以横竖屏切换，适配了4.4或者华为emui3.1系统手机，并且navigationBarWithKitkatEnable为true，
        // 请务必在onConfigurationChanged方法里添加如下代码（同时满足这三个条件才需要加上代码哦：1、横竖屏可以切换；2、android4.4或者华为emui3.1系统手机；3、navigationBarWithKitkatEnable为true）
        // 否则请忽略
        ImmersionBar.with(this).init()
    }

    override fun onResume() {
        super.onResume()
        MobclickAgent.onResume(this)
        // 非必加
        // 如果你的app可以横竖屏切换，适配了华为emui3系列系统手机，并且navigationBarWithEMUI3Enable为true，
        // 请在onResume方法里添加这句代码（同时满足这三个条件才需要加上代码哦：1、横竖屏可以切换；2、华为emui3系列系统手机；3、navigationBarWithEMUI3Enable为true）
        // 否则请忽略
        if (OSUtils.isEMUI3_x()) {
            ImmersionBar.with(this).init()
        }
    }

    override fun onPause() {
        super.onPause()
        MobclickAgent.onPause(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        LoadingBar.release()
    }

//    abstract fun getLayoutId(): Int
    abstract val layoutId: Int

    abstract fun initData()

    abstract fun configViews()

    /**
     * 隐藏控件
     * @param views
     */
    protected fun gone(vararg views: View) {
        if (views.isNotEmpty()) {

            for (view in views) {
                view.visibility = View.GONE
            }
        }
    }

    /**
     * 显示控件
     * @param views
     */
    protected fun visible(vararg views: View) {
        if (null != views && views.size > 0) {

            for (view in views) {
                if (null != view) {
                    view.visibility = View.VISIBLE
                }
            }
        }
    }

    /**
     * 判断传入控件的显示状态
     * @param view
     * @return
     */
    protected fun isVisible(view: View): Boolean {
        return view.visibility == View.VISIBLE
    }
}
