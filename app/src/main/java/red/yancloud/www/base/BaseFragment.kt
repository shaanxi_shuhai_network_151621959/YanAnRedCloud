package red.yancloud.www.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.dyhdyh.widget.loading.dialog.LoadingDialog
import com.umeng.analytics.MobclickAgent
import red.yancloud.www.ui.dialog.LoadingCallback

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/13 10:19
 * E_Mail：yh131412hys@163.com
 * Describe：基类Fragment
 * Change：
 * @Version：V1.0
 */
abstract class BaseFragment : Fragment() {

    protected lateinit var parentView: View

    protected var mContext: Context? = null

    protected lateinit var loadingDialog: Dialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        parentView = inflater.inflate(layoutId, container, false)
        mContext = activity
        loadingDialog = LoadingDialog.make(mContext, LoadingCallback()).create()
        loadingDialog.setCancelable(true)
        loadingDialog.setCanceledOnTouchOutside(true)
        return parentView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        attachView()
        initData()
        configView()
    }

    override fun onResume() {
        super.onResume()
        MobclickAgent.onPageStart("MainActivity")
    }

    override fun onPause() {
        super.onPause()
        MobclickAgent.onPageEnd("MainActivity")
    }

    abstract val layoutId: Int

    abstract fun attachView()

    abstract fun initData()

    abstract fun configView()
}
