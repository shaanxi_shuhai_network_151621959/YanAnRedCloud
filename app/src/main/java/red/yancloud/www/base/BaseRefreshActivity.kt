package red.yancloud.www.base

import android.util.Log
import android.view.View
import com.scwang.smartrefresh.layout.constant.SpinnerStyle
import com.scwang.smartrefresh.layout.header.ClassicsHeader
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import kotlinx.android.synthetic.main.activity_my_collect.*
import red.yancloud.www.R
import red.yancloud.www.util.NetworkUtils
import red.yancloud.www.util.TimeFormatUtil
import red.yancloud.www.util.ToastUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 10:49
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
abstract class BaseRefreshActivity : BaseActivity(){

    private val TAG = "BaseRefreshActivity"

    protected var page: Int = 1
    protected var limit: String = "10"
    protected var totalPage: Int = 0

    override fun initData() {

        getRefreshDataList()
    }

    override fun configViews(){

//        initView()
        initRecyclerView()
        initSmartRefreshLayout()
    }

    abstract fun initRecyclerView()

//    abstract fun initView()

    abstract fun getRefreshDataList()

    private fun initSmartRefreshLayout() {

        val mClassicsHeader = smartRefreshLayout.refreshHeader as ClassicsHeader
        mClassicsHeader.setLastUpdateTime(Date(System.currentTimeMillis()))
        mClassicsHeader.setTimeFormat(SimpleDateFormat("更新于 MM-dd HH:mm", Locale.CHINA))
        mClassicsHeader.setTimeFormat(TimeFormatUtil("更新于 %s"))

//        smartRefreshLayout.setEnableAutoLoadMore(true)//开启自动加载功能（非必须）
        smartRefreshLayout.setOnRefreshListener(OnRefreshListener {

            if (!NetworkUtils.isConnected(this)) {
                errorView.visibility = View.VISIBLE
                ToastUtils.toastNetErrorMsg()
                smartRefreshLayout.finishRefresh()
                return@OnRefreshListener
            }
            page=1
            getRefreshDataList()
        })
        smartRefreshLayout.setOnLoadMoreListener(OnLoadMoreListener { refreshLayout ->

            if (totalPage!=0&&page>=totalPage){

                ToastUtils.showToast(getString(R.string.data_all_load))
                smartRefreshLayout.finishLoadMoreWithNoMoreData()//将不会再次触发加载更多事件
                return@OnLoadMoreListener
            }

            if (!NetworkUtils.isConnected(this)) {
                errorView.visibility = View.VISIBLE
                ToastUtils.toastNetErrorMsg()
                smartRefreshLayout.finishLoadMore()
                return@OnLoadMoreListener
            }
            page++
            getRefreshDataList()
        })
    }
}
