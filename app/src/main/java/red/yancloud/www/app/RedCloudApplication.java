package red.yancloud.www.app;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.tencent.smtt.sdk.QbSdk;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.zzhoujay.richtext.RichText;
import red.yancloud.www.common.Constants;
import red.yancloud.www.util.Utils;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/31 11:47
 * E_Mail：yh131412hys@163.com
 * Describe：Application
 * Change：
 * @Version：V1.0
 */
public class RedCloudApplication extends MultiDexApplication {

    private static final String TAG = "RedCloudApplication";

    private static RedCloudApplication sInstance;
//    private LocationService locationService;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

//        富文本初始化
        RichText.initCacheDir(this);
        //搜集本地tbs内核信息并上报服务器，服务器返回结果决定使用哪个内核。
        QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

            @Override
            public void onViewInitFinished(boolean arg0) {
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Log.d("app", " onViewInitFinished is " + arg0);
            }

            @Override
            public void onCoreInitFinished() {
            }
        };
        //x5内核初始化接口
        QbSdk.initX5Environment(getApplicationContext(),  cb);

        /*获取当前系统的android版本号*/
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion < Build.VERSION_CODES.LOLLIPOP) {//svg适配android5.0以下 21
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
//        科大讯飞语音
        initSpeech();
        Utils.init(this);
        initUMConfigure();
//        推送
        initPush();
        initLocationService();
//        调试数据库
//        SqlScoutServer.create(this, getPackageName());
    }

    private void initPush() {

//        注册成功后会在tag：MiPushBroadcastReceiver下面打印log：
//        onCommandResult is called. regid= xxxxxxxxxxxxxxxxxxxxxxx接收到小米消息则会打印log： onReceiveMessage,msg= xxxxxxxxxxxxxxxxxxxxxxx
//        MiPushRegistar.register(this,Constants.PlatformParams.XIAOMI_APP_ID,Constants.PlatformParams.XIAOMI_APP_KEY);
//                注册成功后会在tag：HuaWeiReceiver下面打印log：
//                获取token成功，token= xxxxxxxxxxxxxxxxxxxxxxx接收到华为消息则会打印log： HuaWeiReceiver,content= xxxxxxxxxxxxxxxxxxxxxxx
//        HuaWeiRegister.register(this);
    }

    private void initSpeech() {

        //初始化即创建语音配置对象，只有初始化后才可以使用MSC的各项服务。建议将初始化放在程序入口处
        // 应用程序入口处调用，避免手机内存过小，杀死后台进程后通过历史intent进入Activity造成SpeechUtility对象为null
        // 如在Application中调用初始化，需要在Mainifest中注册该Applicaiton
        // 注意：此接口在非主进程调用会返回null对象，如需在非主进程使用语音功能，请增加参数：SpeechConstant.FORCE_LOGIN+"=true"
        // 参数间使用半角“,”分隔。
        // 设置你申请的应用appid,请勿在'='与appid之间添加空格及空转义符
        // 注意： appid 必须和下载的SDK保持一致，否则会出现10407错误
//        SpeechUtility.createUtility(this, "appid="+Constants.PlatformParams.SPEECH_APP_ID);
        // 以下语句用于设置日志开关（默认开启），设置成false时关闭语音云SDK日志打印
        // Setting.setShowLog(false);
    }

    private void initLocationService() {

        /***
         * 初始化定位sdk，建议在Application中创建
         * 主Application，所有百度定位SDK的接口说明请参考线上文档：http://developer.baidu.com/map/loc_refer/index.html
         * 百度定位SDK官方网站：http://developer.baidu.com/map/index.php?title=android-locsdk
         */
//        locationService = new LocationService(getApplicationContext());
//        mVibrator =(Vibrator)getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
//        SDKInitializer.initialize(getApplicationContext());
    }

    public static RedCloudApplication getInstance(){
        return sInstance;
    }

//    public LocationService getLocationService() {
//        return locationService;
//    }

    /**
     * @Author：风不会停息 on 2019/1/10 10:29
     * @param base
     * @Description：添加dex分包
     * @Change：
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initUMConfigure() {

        /**
         * 初始化时可以通过该方法设置组件化的Log是否输出，默认关闭Log输出。
         * 设置组件化的Log开关
         * 参数: boolean 默认为false，如需查看LOG设置为true
         */
        UMConfigure.setLogEnabled(true);
        /**
         * 设置日志加密初始化时可以通过该方法设置日志加密。
         * 设置日志加密
         * 参数：boolean 默认为false（不加密）
         */
        UMConfigure.setEncryptEnabled(false);
        //场景类型设置接口
        // EScenarioType.E_UM_NORMAL 普通统计场景，如果您在埋点过程中没有使用到
        //U-Game统计接口，请使用普通统计场景
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
//        MobclickAgent.setSessionContinueMillis(1000);

        // 在此处调用基础组件包提供的初始化函数 相应信息可在应用管理 -> 应用信息 中找到 http://message.umeng.com/list/apps
        // 参数一：当前上下文context；
        // 参数二：应用申请的Appkey（需替换）；
        // 参数三：渠道名称；
        // 参数四：设备类型，必须参数，传参数为UMConfigure.DEVICE_TYPE_PHONE则表示手机；传参数为UMConfigure.DEVICE_TYPE_BOX则表示盒子；默认为手机；
        // 参数5:Push推送业务的secret，需要集成Push功能时必须传入Push的secret，否则传空。
        UMConfigure.init(this, Constants.PlatformParams.UMENG_APPKEY, "Umeng", UMConfigure.DEVICE_TYPE_PHONE, Constants.PlatformParams.UMENG_MASTER_SECRET);
        //获取消息推送代理示例
//        PushAgent mPushAgent = PushAgent.getInstance(this);
//        //注册推送服务，每次调用register方法都会回调该接口
//        mPushAgent.register(new IUmengRegisterCallback() {
//
//            @Override
//            public void onSuccess(String deviceToken) {
//                //注册成功会返回deviceToken deviceToken是推送消息的唯一标志
//                Log.i(TAG,"注册成功：deviceToken：-------->  " + deviceToken);
//            }
//
//            @Override
//            public void onFailure(String s, String s1) {
//                Log.e(TAG,"注册失败：-------->  " + "s:" + s + ",s1:" + s1);
//            }
//        });
//
//        UmengMessageHandler messageHandler = new UmengMessageHandler() {
//
//            /**
//             * 自定义通知栏样式的回调方法
//             */
//            @Override
//            public Notification getNotification(Context context, UMessage msg) {
//                switch (msg.builder_id) {
//                    case 1:
//                        Notification.Builder builder = new Notification.Builder(context);
//                        RemoteViews myNotificationView = new RemoteViews(context.getPackageName(),
//                                R.layout.notification_view);
//                        myNotificationView.setTextViewText(R.id.notification_title, msg.title);
//                        myNotificationView.setTextViewText(R.id.notification_text, msg.text);
//                        myNotificationView.setImageViewBitmap(R.id.notification_large_icon, getLargeIcon(context, msg));
//                        myNotificationView.setImageViewResource(R.id.notification_small_icon,
//                                getSmallIconId(context, msg));
//                        builder.setContent(myNotificationView)
//                                .setSmallIcon(getSmallIconId(context, msg))
//                                .setTicker(msg.ticker)
//                                .setAutoCancel(true);
//
//                        return builder.getNotification();
//                    default:
//                        //默认为0，若填写的builder_id并不存在，也使用默认。
//                        return super.getNotification(context, msg);
//                }
//            }
//        };
//        mPushAgent.setMessageHandler(messageHandler);
//
//        UmengNotificationClickHandler notificationClickHandler = new UmengNotificationClickHandler(){
//
//            @Override
//            public void dealWithCustomAction(Context context, UMessage msg){
//                Log.e(TAG,"click");
//            }
//
//        };
//
//        mPushAgent.setNotificationClickHandler(notificationClickHandler);
        PlatformConfig.setWeixin(Constants.PlatformParams.WX_APP_ID, Constants.PlatformParams.APP_SECRET);
//        https://open.weibo.com/wiki/授权机制说明#.E5.8F.96.E6.B6.88.E6.8E.88.E6.9D.83.E5.9B.9E.E8.B0.83.E9.A1.B5
//        "http://www.yancloud.red/" 回调地址
        PlatformConfig.setSinaWeibo(Constants.PlatformParams.WEIBO_APP_ID,Constants.PlatformParams.WEIBO_APP_SECRET,Constants.PlatformParams.WEIBO_REDIRECT_URL);
//        https://connect.qq.com/manage.html#/
        PlatformConfig.setQQZone(Constants.PlatformParams.QQ_APP_ID, Constants.PlatformParams.QQ_APP_SECRET);
    }
}
