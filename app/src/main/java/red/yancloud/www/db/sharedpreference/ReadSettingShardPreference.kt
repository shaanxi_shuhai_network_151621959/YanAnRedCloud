package red.yancloud.www.db.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import red.yancloud.www.R
import red.yancloud.www.manager.ThemeManager
import red.yancloud.www.util.Utils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/10 11:01
 * E_Mail：yh131412hys@163.com
 * Describe：阅读设置存储
 * Change：
 * @Version：V1.0
 */
class ReadSettingShardPreference private constructor() {

    private val sp: SharedPreferences
    private val ed: SharedPreferences.Editor

    init {
        sp = Utils.getAppContext().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
        ed = sp.edit()
    }

    companion object {

        private val FILE_NAME = "redCloud_readSetting_shardPreferences"

        // 亮度
        private val READ_LIGHT = 70

        // 阅读图书的位置
        private val BEGIN = 1

        // 行间距
        val linespace_len = 10

        // 简体繁体切换
        @JvmField
        val LANGUAGE_SIMPLIFIED = 0// 简体
        @JvmField
        val LANGUAGE_TRADITIONAL = 1// 繁体

        // 音量键翻页
        val SOUND_SWITCH_ON = true
        val SOUND_SWITCH_OFF = false

        // 翻页效果
        val FLIP_PAGE_NONE = 0 // 无翻页效果
        val FLIP_PAGE_MOVE = 1// 平滑翻页
        val FLIP_PAGE_REAL = 2// 仿真翻页
        val FLIP_PAGE_SCROLL = 3// 上下翻页

        // 首航缩进
        val INDENTATION_0 = 0// 首行缩进 0
        val INDENTATION_1 = 1// 首行缩进 1
        val INDENTATION_2 = 2// 首行缩进 2

        // 阅读风格
        val READ_STYLE_DAY = 0 // 阅读风格:白天
        val READ_STYLE_BLUE = 1 // 阅读风格:蓝色
        val READ_STYLE_EYE = 2 // 阅读风格:护眼
        val READ_STYLE_PAPER = 3 // 阅读风格:羊皮纸
        val READ_STYLE_PAINK = 4 // 阅读风格:粉色
        val READ_STYLE_NIGHT = 5 // 阅读风格:夜间

        // 默认风格
        private val DEFAULT_STYLE = ThemeManager.READ_STYLE_THEME_DEFAULT

        private val WELCOME_URL = ""

        // 阅读指导页
        private val READ_GUIDE_SHOW = true

        val instance: ReadSettingShardPreference
            get() = ReadSettingShardPreferenceHolder.INSTANCE
    }

    private object ReadSettingShardPreferenceHolder {
        internal val INSTANCE = ReadSettingShardPreference()
    }

    /**
     * 获取段间距（本地）
     * @return
     */
    /**
     * 设置段间距（本地）
     *
     * @param length
     */
    var paragraphSpaceLocal: Int
        get() = sp.getInt("paragraph_space_local", 0)
        set(length) {
            ed.putInt("paragraph_space_local", length)
            ed.commit()
        }

    /**
     * 获取是否显示阅读指导页
     *
     * @return
     */
    /**
     * 设置是否显示阅读指导页
     *
     * @param isRun
     */
    var readGuide: Boolean
        get() = sp.getBoolean("read_guide", READ_GUIDE_SHOW)
        set(isRun) {
            ed.putBoolean("read_guide", isRun)
            ed.commit()
        }

    /**
     * 存储欢迎页URL
     * @return
     */
    /**
     * 存储欢迎页URL
     *
     * @param url
     */
    var welcomeUrl: String?
        get() = sp.getString("welcome_settring_url", WELCOME_URL)
        set(url) {
            ed.putString("welcome_settring_url", url)
            ed.commit()
        }

    /**
     * 获取亮度
     */
    /**
     * 设置亮度
     *
     * @param light
     */
    var light: Int
        get() = sp.getInt("read_setting_light", READ_LIGHT)
        set(light) {
            ed.putInt("read_setting_light", light)
            ed.commit()
        }

    /**
     * 获取系统亮度
     *
     * @return
     */
    /**
     * 设置系统亮度
     *
     * @param flag
     */
    var systemLight: Boolean
        get() = sp.getBoolean("read_system_light", true)
        set(flag) {
            ed.putBoolean("read_system_light", flag)
            ed.commit()
        }

    /**
     * 获取最后阅读书籍id
     * @return
     */
    /**
     * 设置最后阅读书籍id
     * @param articleId
     */
    var lastReadArticleId: String?
        get() = sp.getString("last.read.articleid", "")
        set(articleId) {
            ed.putString("last.read.articleid", articleId)
            ed.commit()
        }

    /*var systemLight: Boolean
        get() = sp.getBoolean("read_system_light", true)
        set(flag) {
            ed.putBoolean("read_system_light", flag)
            ed.commit()
        }*/

    /**
     * 设置阅读风格
     *
     * @param readStyle
     */
    var readStyle: Int
        get() = sp.getInt("read_style", DEFAULT_STYLE)
        set(readStyle) {
            ed.putInt("read_style", readStyle)
            ed.commit()
        }

    /**
     * 设置翻页模式
     * @param model
     */
    var flipModel: Int
        /**
         * 获取翻页模式
         * @return
         */
        get() = sp.getInt("read_flip_mode", FLIP_PAGE_REAL)
        set(model) {
            ed.putInt("read_flip_mode", model)
            ed.commit()
        }

    /**
     * 获得语言
     *
     * @return
     */
    /**
     * 设置语言
     *
     * @param language
     */
    var language: Int
        get() = sp.getInt("read_language", LANGUAGE_SIMPLIFIED)
        set(language) {
            ed.putInt("read_language", language)
            ed.commit()
        }

    /**
     * 获取护眼模式
     * @return
     */
    /**
     * 设置护眼模式
     * @param eye
     */
    var eyeMode: Boolean
        get() = sp.getBoolean("protect.eye.mode", true)
        set(eye) {
            ed.putBoolean("protect.eye.mode", eye)
            ed.commit()
        }

    /**
     * 设置音量键翻页模式
     * @return
     */
    var soundSwitch: Boolean
        get() = sp.getBoolean("soundSwitch", true)
        set(soundSwitch){
            ed.putBoolean("soundSwitch", soundSwitch)
            ed.commit()
        }

    /**
     *
     * @return
     */
    /**
     *
     * @param status
     */
    var protectionEye: Boolean
        get() = sp.getBoolean("protection.eye", false)
        set(status) {
            ed.putBoolean("protection.eye", status)
            ed.commit()
        }

    // 获取语言类型
    // 设置语言类型
    var voicer: Int
        get() = sp.getInt("speech.voicer", 0)
        set(index) {
            ed.putInt("speech.voicer", index)
            ed.commit()
        }

    var fristLoginTime: Long
        get() = sp.getLong("frist.login.time", 0)
        set(time) {
            ed.putLong("frist.login.time", time)
            ed.commit()
        }

    /**
     * 获取字体大小
     */
    /**
     * 设置字体大小
     *
     * @param size
     */
    //{return sp.getInt("read_text_size",34);
    var textSize: Int
        get() = sp.getInt(
            "read_setting_size", Utils.getAppContext().resources
                .getDimension(R.dimen.chapter_font_mix_size).toInt()
        )
        set(size) {
            ed.putInt("read_setting_size", size)
            ed.commit()
        }

    /**
     * 获取左右边距
     */
    /**
     * 左右边距
     *
     * @param length
     */
    var rMargins: Int
        get() = sp.getInt(
            "rMargins", Utils.getAppContext().resources
                .getDimension(R.dimen.read_base_right_margins).toInt()
        )
        set(length) {
            ed.putInt("rMargins", length)
            ed.commit()
        }

    /**
     * 获取上下边距
     */
    /**
     * 上下边距
     *
     * @param length
     */
    var tMargins: Int
        get() = sp.getInt(
            "tMargins", Utils.getAppContext().resources
                .getDimension(R.dimen.read_base_top_margins).toInt()
        )
        set(length) {
            ed.putInt("tMargins", length)
            ed.commit()
        }

    /**
     * 获取行间距大小
     */
    /**
     * 设置行间距大小
     *
     * @param length
     */
    var lineSpace: Float
        get() = sp.getFloat(
            "linespace_length", /*Utils.getAppContext().resources
                .getDimension(R.dimen.read_base_line_space).toInt()*/1.3f
        )
        set(length) {
            ed.putFloat("linespace_length", length)
            ed.commit()
        }

    /**
     * 获取段间距(书城)
     * @return
     */
    /**
     * 设置段间距（书城）
     *
     * @param length
     */
    var paragraphSpaceBkshop: Int
        get() = sp.getInt(
            "paragraph_space", Utils.getAppContext().resources
                .getDimension(R.dimen.read_base_paragraph_space).toInt()
        )
        set(length) {
            ed.putInt("paragraph_space", length)
            ed.commit()
        }

    /**
     * 获取最后开机时间
     *
     * @return
     */
    /**
     * 设置最后开机时间
     *
     * @param time
     */
    var lastReadTime: Long
        get() = sp.getLong("last.power.timer", 0)
        set(time) {
            ed.putLong("last.power.timer", time)
            ed.commit()
        }

    /**
     * 获取书架同步时间
     * @return
     */
    /**
     * 记录书架同步时间
     * @param time
     */
    var backupTime: String?
        get() = sp.getString("last.backup.timer", "0")
        set(time) {
            ed.putString("last.backup.timer", time)
            ed.commit()
        }

    /**
     * 获取最新版本号
     * @return
     */
    /**
     * 设置最新版本号
     * @param code
     */
    var latestVersionCode: Int
        get() = sp.getInt("app.latest.version.code", 0)
        set(code) {
            ed.putInt("app.latest.version.code", code)
            ed.commit()
        }


    /**
     * 获取夜间阅读模式
     * @return
     */
    /**
     * 设置夜间阅读模式
     * @param status
     */
    var readerDayOrNight: Boolean
        get() = sp.getBoolean("read_theme_day_or_night", true)
        set(status) {
            ed.putBoolean("read_theme_day_or_night", status)
            ed.commit()
        }

    /**
     * 设置书籍读取位置
     *
     * @param begin
     */

    fun setBegin(bookid: String, begin: Int) {
        ed.putInt(bookid + "begin", begin)
        ed.commit()
    }

    /**
     * 获取书籍读取位置(本章第第几页)
     */
    fun getBegin(bookid: String): Int {
        return sp.getInt(bookid + "begin", BEGIN)
    }

    /**
     * 设置最后阅读章节
     *
     * @param articleid
     * @param vchpid
     */

    fun setLastReadChp(articleid: String, vchpid: Int) {
        ed.putInt(articleid + "last.read.chpid", vchpid)
        ed.commit()
    }

    /**
     * 获取阅读最后章节
     *
     * @param articleid
     * @return
     */

    fun getLastReadChp(articleid: String): Int {
        return sp.getInt(articleid + "last.read.chpid", -1)
    }

    /**
     * 设置最后阅读章节排序id
     *
     * @param articleid
     * @param vchporder
     */
    fun setLastReadOrd(articleid: String, vchporder: Int) {
        ed.putInt(articleid + "last.read.chpord", vchporder)
        ed.commit()
    }

    /**
     * 获取最后阅读章节排序id
     *
     * @return
     */
    fun getLastReadOrd(articleid: String): Int {
        return sp.getInt(articleid + "last.read.chpord", 0)
    }

    /**
     * 设置书籍读取章节
     * @param bookid
     * @param chp
     */
    fun setChp(bookid: String, chp: Int) {
        ed.putInt(bookid + "chp", chp)
        ed.commit()
    }

    /**
     * 获取本地阅读百分比
     *
     * @param bookid
     * @return
     */
    fun getReadPercent(bookid: String): String? {
        return sp.getString(bookid + "percent", "0.0")
    }

    /**
     * 设置本地阅读百分比
     *
     * @param bookid
     * @param percent
     */
    fun setReadPercent(bookid: String, percent: String) {
        ed.putString(bookid + "percent", percent)
        ed.commit()
    }

    /**
     * 设置书城书籍章节阅读进度
     * @param book
     * @param chp
     * @param pro
     */
    fun setShopBookReadPro(book: String, chp: Int, pro: Int) {
        ed.putInt(book + chp, pro)
        ed.commit()
    }

    /**
     * 获取书城书籍章节阅读进度
     * @param book
     * @param chp
     * @return
     */
    fun getShopBookReadPro(book: String, chp: Int): Int {
        return sp.getInt(book + chp, 0)
    }

    /**
     * 清除该书阅读记录
     *
     * @param bookid
     */
    fun removeBook(bookid: String) {
        ed.remove(bookid + "chp")
        ed.remove(bookid + "free")
        ed.remove(bookid + "begin")
    }

    /**
     * 设置签到时间
     * @param time
     */
    fun setSignInTime(userName: String, time: Long) {
        ed.putLong(userName + "sign.in.show.time", time)
        ed.commit()
    }

    // 获取语速
    var voiceSpeed: Int
        get() = sp.getInt("speech.voicer.speed", 50)
        set(speed) {
            ed.putInt("speech.voicer.speed", speed)
            ed.commit()
        }
}
