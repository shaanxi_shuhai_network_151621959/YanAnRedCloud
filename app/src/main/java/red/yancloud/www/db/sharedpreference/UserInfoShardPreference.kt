package red.yancloud.www.db.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.Login
import red.yancloud.www.util.Utils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/31 16:09
 * E_Mail：yh131412hys@163.com
 * Describe：用户信息存储
 * Change：
 * @Version：V1.0
 */
class UserInfoShardPreference private constructor() {

    private val sp: SharedPreferences
    private val ed: SharedPreferences.Editor

    init {
        sp = Utils.getAppContext().getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE)
        ed = sp.edit()
    }

    private object UserInfoShardPreferenceHolder {
        internal val INSTANCE = UserInfoShardPreference()
    }

    companion object {
        private const val FILE_KEY = "redCloud_userInfo_shardPreferences"

        val instance: UserInfoShardPreference
            get() = UserInfoShardPreferenceHolder.INSTANCE
    }

    /**
     * 保存登录信息
     *
     */
    fun saveUserInfo(user: Login.DataBean) {
        ed.putBoolean("user.login.state", true)
        ed.putString("user.id", user.id)
        ed.putString("user.username", user.username)
        ed.putString("user.name", user.name)
        ed.putString("user.pwd", user.pwd)
        ed.putString("user.sex", user.sex)
        ed.putString("user.headimgurl", user.headimgurl)

        ed.commit()
    }

    /**
     * 清除缓存
     */
    fun clearUserInfo() {
        ed.remove("user.login.state")
        ed.remove("user.id")
        ed.remove("user.username")
        ed.remove("user.name")
        ed.remove("user.pwd")
        ed.remove("user.sex")
        ed.remove("user.headimgurl")
        ed.remove("editPwd")
        ed.commit()
    }

//    /**
//     * 储存输入的pwd
//     */
//    fun saveEditPwd(passWord: String) {
//
//    }

    /**
     * 获取输入的pwd
     *
     * @return
     */
    var editPwd: String?
        get() = sp.getString("editPwd", "")
        set(editPwd){
        ed.putString("editPwd", editPwd).commit()
        }

    /**
     * 检测登录状态
     *
     * @return
     */
    val isLogin: Boolean
        get() = sp.getBoolean("user.login.state", false)

    /**
     * 获取用户id
     *
     * @return
     */
    val uid: String?
        get() = sp.getString("user.id", "0")

    /**
     * 获取用户名
     *
     * @return
     */
    val userName: String?
        get() = sp.getString("user.username", "")

    /**
     * 获取用户真实名字
     * @return
     */
    val name: String?
        get() = sp.getString("user.name", "")

    /**
     * 获取密码
     *
     * @return
     */
    val pwd: String?
        get() = sp.getString("user.pwd", "")

    /**
     * 获取用户头像
     * @return
     */
    val headImgUrl: String?
        get() = sp.getString("user.headimgurl", "")

    /**
     * 欢迎页最后更新时间
     * @param time
     */
    var splashTime: Int
        get() = sp.getInt("splash_time_end", 0)
        set(time) {
            ed.putInt("splash_time_end", time).commit()
        }

    /**
     * 是否显示用户引导页
     *
     * @param versionCode
     * @param b
     */
    fun setISShowLead(versionCode: Int, b: Boolean) {
        if (Constants.IS_SHOW_LEAD) {
            ed.putBoolean(versionCode.toString() + "app.isshow.lead", b)
        } else {
            ed.putBoolean("app.isshow.lead", b)
        }
        ed.commit()
    }

    /**
     * 获取是否显示用户引导页
     *
     * @param versionCode
     * @return
     */
    fun getISShowLead(versionCode: Int): Boolean {
        return if (Constants.IS_SHOW_LEAD) {
            sp.getBoolean(versionCode.toString() + "app.isshow.lead", true)
        } else {
            sp.getBoolean("app.isshow.lead", true)
        }
    }

    /**
     * 储存输入的pwd
     */
    fun setLastLocation(lastLocation: Int) {
        ed.putInt("lastLocation", lastLocation).commit()
    }

    /**
     * 获取输入的pwd
     *
     * @return
     */
    val lastLocation: Int
        get() = sp.getInt("lastLocation", 0)
}
