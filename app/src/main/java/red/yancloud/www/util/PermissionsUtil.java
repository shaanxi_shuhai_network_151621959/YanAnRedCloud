package red.yancloud.www.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/2/18 15:55
 * E_Mail：yh131412hys@163.com
 * Describe：权限工具类
 * Change：
 * @Version：V1.0
 */
public class PermissionsUtil {

    private static final String TAG = "PermissionsUtil";

    private final Context mContext;

    public PermissionsUtil(Context context){
        this.mContext = context;
    }


    /**
     * 判断权限集合
     * @param permissions
     * @return
     */
    public boolean lacksPermissions(String... permissions){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getTargetSDKVersionCode() >= Build.VERSION_CODES.M) {
                for (String permission : permissions) {
                    if(lacksPermissionM(permission)){
                        return true;
                    }
                }
            } else {
                for (String permission : permissions) {
                    if(lacksPermissionN(permission)){
                        return true;
                    }
                }
            }
        }
        return false;
    }




    /**
     * 判断时候缺少权限
     * @param permission
     * @return
     */
    private boolean lacksPermissionM(String permission){
        return ContextCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_DENIED;
    }

    private boolean lacksPermissionN(String permission){
        return PermissionChecker.checkSelfPermission(mContext, permission) == PermissionChecker.PERMISSION_DENIED;
    }


    private int getTargetSDKVersionCode(){
        try {
            final PackageInfo info = mContext.getPackageManager().getPackageInfo(
                    mContext.getPackageName(), 0);
            return  info.applicationInfo.targetSdkVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return 0;
    }

    /**
     * @Author：风不会停息 on 2019/2/18 15:53
     * E_Mail：yh131412hys@163.com
     * @param context
     * @param permission
     * @Description：判断是否缺少权限
     * @Change：
     * @Version：V1.0
     */
    public static boolean selfPermissionGranted(Context context, String permission) {

        int targetSdkVersion = 0;
        boolean ret;

        try {
            final PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            targetSdkVersion = info.applicationInfo.targetSdkVersion;
            Log.d(TAG,"selfPermissionGranted targetSdkVersion="+targetSdkVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (targetSdkVersion >= Build.VERSION_CODES.M) {
                ret = context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
            } else {
                ret = PermissionChecker.checkSelfPermission(context, permission) == PermissionChecker.PERMISSION_GRANTED;
            }
        }else{
            return true;
        }
        Log.d(TAG,"selfPermissionGranted permission:" + permission +" grant:" + ret);
        return ret;
    }

}
