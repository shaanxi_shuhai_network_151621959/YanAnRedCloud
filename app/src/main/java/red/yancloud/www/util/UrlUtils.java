package red.yancloud.www.util;

import android.net.Uri;
import android.text.TextUtils;
import red.yancloud.www.common.Constants;
import red.yancloud.www.db.sharedpreference.UserInfoShardPreference;

import java.net.URLEncoder;
import java.util.Set;

/**
 * @author 55345364
 * @date 2017/11/3.
 */
public final  class UrlUtils {

    /**
     * 拼接加验证码的URL
     * @param vStr
     * @return
     */
    public static String markSignUrl(String vStr){
        try {
            Uri uri = Uri.parse(vStr);
            Set<String> collection  = uri.getQueryParameterNames();
            if(!collection.contains("packagename") || TextUtils.isEmpty(uri.getQueryParameter("packagename"))){
                vStr += "&packagename=" +  /*AppUtils.getPackageName()*/"red.yancloud.www";
            }
            if(!collection.contains("uuid") || TextUtils.isEmpty(uri.getQueryParameter("uuid"))){
                vStr += "&uuid=" +  PhoneUtils.getUUID();
            }
            if(!collection.contains("timestamp")||TextUtils.isEmpty(uri.getQueryParameter("timestamp"))){
                vStr += "&timestamp=" +  TimeFormatUtil.getCurrentDateTimeSeconds();
            }else{
                vStr = vStr.replaceAll("&timestamp=" + uri.getQueryParameter("timestamp"), "&timestamp=" +  TimeFormatUtil.getCurrentDateTimeSeconds());
            }
            if(!collection.contains("sign") ||TextUtils.isEmpty(uri.getQueryParameter("sign"))){
                vStr += "&sign=" + AppUtils.getSign("");
            }else{
                vStr = vStr.replaceAll("&sign=" + uri.getQueryParameter("sign"), "&sign=" +  AppUtils.getSign(""));
            }
            if(!collection.contains("version") || TextUtils.isEmpty(uri.getQueryParameter("version"))){
                vStr += "&version=" + AppUtils.getAppVersionCode();
            }
            if(!collection.contains("uid") || TextUtils.isEmpty(uri.getQueryParameter("uid"))){
                vStr += "&uid=" + UserInfoShardPreference.Companion.getInstance().getUid();
            }
            if(!collection.contains("pass") || TextUtils.isEmpty(uri.getQueryParameter("pass"))){
                vStr += "&pass=" + UserInfoShardPreference.Companion.getInstance().getPwd();
            }
            if(!collection.contains("username") || TextUtils.isEmpty(uri.getQueryParameter("username"))){
                vStr += "&username=" + URLEncoder.encode(URLEncoder.encode(UserInfoShardPreference.Companion.getInstance().getUserName(), "UTF-8"), "UTF-8");
            }
            if(!collection.contains("source") || TextUtils.isEmpty(uri.getQueryParameter("source"))){
                vStr += "&source=" + AppUtils.getChannel("UMENG_CHANNEL");
            }
        }catch (Exception e){
            return vStr;
        }
        return vStr;
    }

    /**
     * 所有网页访问
     * @param vstr
     * @return
     */
    public static String makeWebUrl(String vstr) {

        String url;
        try {
            url = vstr
                    + "&version=" + AppUtils.getAppVersionCode()
                    + "&source="
                    +  AppUtils.getChannel("UMENG_CHANNEL")
                    + "&packagename=" + AppUtils.getPackageName()
                    + "&timestamp=" + TimeFormatUtil.getCurrentDateTimeSeconds()
                    + "&uuid=" + AppUtils.getMyUUID(Utils.getAppContext())
                    + "&sign=" + MD5Utils.string2MD5(AppUtils.getPackageName() + TimeFormatUtil.getCurrentDateTimeSeconds() + Constants.KEY_SIGN + AppUtils.getMyUUID(Utils.getAppContext()));
        } catch (Exception e) {
            url = vstr + "&version=" + AppUtils.getAppVersionCode()
                    + "&source="
                    + AppUtils.getChannel("UMENG_CHANNEL")
                    + "&packagename=" + AppUtils.getPackageName();
        }
        return url;
    }


    /**
     * 仅限用户和目录页面使用
     * @param
     * @param vstr
     * @return
     */
    public static String makeUserUrl(String vstr) {
        String url = "";
        try {
            url = vstr
                    + "&uid="
                    + UserInfoShardPreference.Companion.getInstance().getUid()
                    + "&pass="
                    + UserInfoShardPreference.Companion.getInstance().getPwd()
                    + "&username="
                    + URLEncoder.encode(URLEncoder.encode(UserInfoShardPreference.Companion.getInstance().getUserName(), "UTF-8"), "UTF-8")
                    + "&version=" + AppUtils.getAppVersionCode()
                    + "&source="
                    + AppUtils.getChannel("UMENG_CHANNEL")
                    + "&packagename=" + AppUtils.getPackageName();
        } catch (Exception e) {
            url = vstr + "&uid=" + UserInfoShardPreference.Companion.getInstance().getUid() + "&username=" + ""+"&pass=" + ""
                    + "&version=" + AppUtils.getAppVersionCode()
                    + "&source="
                    + AppUtils.getChannel("UMENG_CHANNEL")
                    + "&packagename=" + AppUtils.getPackageName();
        }
        return url;
    }


    /**
     * webview 初始化加载 ，将app信息发送服务端
     * @return
     */
    public static  String makeJsonText(){
        String systemTime = String.valueOf(System.currentTimeMillis());

        /*"{\"username\":\"wangxu\",\"nickname\":\"wangxu\",\"imei\":\"356261050497138\",\"uuid\":\"00000000-22a7-504a-39da-21960033c587\",\"source\":\"com.shuhai\",\n" +
                "                \"version\":\"64\",\"uid\":\"173434\",\"pass\":\"e10adc3949ba59abbe56e057f20f883e\",\"packagename\":\"com.com.shuhai.bookos\",\n" +
                "                \"timestamp\":\"1512612838925\",\"siteid\":\"300\",\"sign\":\"42d8569c57011efb8576b4b93b692d6f\"}"*/


//        String str = "{" + "\"username\":\"wangxu\","
//                + "" + "\"nickname\":\"wangxu\","
//                + "" + "\"imei\":\"356261050497138\","
//                + "\"uuid\":\"00000000-22a7-504a-39da-21960033c587\","
//                + "\"source\":\"shuhai\","
//                + "\"version\":\"64\","
//                + "\"uid\":\"173434\","
//                + "\"pass\":\"e10adc3949ba59abbe56e057f20f883e\","
//                + "\"packagename\":\"com.shuhai.bookos\","
////                + "\"packagename\":\""+ AppUtils.getPackageInfo().packageName + "\","
//                + "\"timestamp\":\"" + systemTime + "\","
//                + "\"siteid\":\"" + Constants.SITEID + "\","
//                + "\"sign\":\""+ MD5Utils.string2MD5(Constants.KEY_SIGN + systemTime) + "\"" + "}";
        return "{"
                + "\"version\":\"" + AppUtils.getAppVersionCode() + "\","
                + "\"packagename\":\"red.yancloud.www\","
//                + "\"packagename\":\""+ AppUtils.getPackageInfo().packageName + "\","
                + "\"username\":\"" + UserInfoShardPreference.Companion.getInstance().getUserName() + "\","
                + "\"source\":\""+AppUtils.getChannel("UMENG_CHANNEL")+ "\","
                + "\"uid\":\"" + UserInfoShardPreference.Companion.getInstance().getUid() + "\","
                + "\"pass\":\"" + UserInfoShardPreference.Companion.getInstance().getPwd() + "\","
                + "" + "\"name\":\"" + UserInfoShardPreference.Companion.getInstance().getName() + "\","

                + "" + "\"imei\":\"" + PhoneUtils.getIMEI() + "\","
                + "\"uuid\":\"" + PhoneUtils.getUUID() + "\","
                + "\"timestamp\":\"" + systemTime + "\","
                + "\"siteid\":\"" + Constants.SITEID + "\","
                + "\"sign\":\""+ MD5Utils.string2MD5(Constants.KEY_SIGN + systemTime) + "\""
                + "}";
    }

    /***
     * 获取url 指定name的value;
     * @param url
     * @param name
     * @return
     */
    public static String getValueByName(String url, String name) {
        String result = "";
        int index = url.indexOf("?");
        String temp = url.substring(index + 1);
        String[] keyValue = temp.split("&");
        for (String str : keyValue) {
            if (str.contains(name)) {
                result = str.replace(name + "=", "");
                break;
            }
        }
        return result;
    }
}
