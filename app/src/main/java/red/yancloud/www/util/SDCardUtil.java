package red.yancloud.www.util;

import android.os.Environment;
import android.os.StatFs;

import java.io.File;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/1/11 11:15
 * E_Mail：yh131412hys@163.com
 * Describe：SD卡工具类
 * Change：
 * @Version：V1.0
 */
public class SDCardUtil {

	/**
	 * 判断sdcard是否存在或可用
	 * 
	 * @return
	 */
	public static boolean checkSDCard() {
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		return sdCardExist;
	}

	public static boolean checkSDCardSize() {
		if (SDCardUtil.getAvailableSize() < 5) {
			return true;
		}
		return false;
	}

	/**
	 * 获取sd卡路径
	 * 
	 * @return
	 */
	public static String getSDPath() {
		File sdDir;
		sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
		return sdDir.toString();
	}

	@SuppressWarnings("deprecation")
	public static int getAvailableSize() {
		File path = Environment.getExternalStorageDirectory(); // 取得sdcard文件路径
		StatFs stat = new StatFs(path.getPath());
		// long totalBlocks = stat.getBlockCount();// 获取block数量
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		// long total = (totalBlocks * blockSize) / 1024 / 1024;
		long available = (availableBlocks * blockSize) / 1024 / 1024;
		// double point = ((double)available / total) * 100;
		return (int) available;
	}
}
