package red.yancloud.www.common

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/13 18:21
 * E_Mail：yh131412hys@163.com
 * Describe：EventBus
 * Change：
 * @Version：V1.0
 */
class MessageEvent(var type: Int?,var result: Any?) {

    override fun toString(): String {
        return "MessageEvent(type=$type, result=$result)"
    }
}
