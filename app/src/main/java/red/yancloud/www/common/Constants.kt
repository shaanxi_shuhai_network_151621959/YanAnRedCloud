package red.yancloud.www.common

import android.os.Environment
import java.io.File

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/31 11:29
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
class Constants {

    companion object {

        //        红云
        //        http://www.yancloud.red/Yancloudapp/API/?ac=login&username=yh131412&pwd=yh123456
        const val BASE_RED_CLOUD_HOST_URL = "https://www.yancloud.red"

        const val API_BASE_RED_CLOUD_URL = "$BASE_RED_CLOUD_HOST_URL/Yancloudapp/"

        //        知库
        //        http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/zhilist&cid=1
        const val BASE_REPOSITORY_HOST_URL = "http://zhi.yancloud.red"

        const val API_BASE_REPOSITORY_URL = "$BASE_REPOSITORY_HOST_URL/index.php/"

        const val KEY_SIGN = "yqP6SZMMRCchJAjPg0WtvOSxlpu0Hy" // 系统秘钥

        const val DEV_KEY = "yananhongyun" // 系统秘钥
        const val DEV_SECRET = "cyhaywjgzdyh" // 系统秘钥

        // 系统返回码
        const val SYSTEM_OK = "0000" // 成功

        @JvmField
        var DEFAULT_TIMEOUT: Long = 30

        const val SITEID = "300"//ADNROID 300；单本（302）；winPhone 304；winphone单本 305

        val ROOT_PATH = Environment.getExternalStorageDirectory().toString() + File.separator + "OSREDCLOUD"

        @JvmField
        val WEB_IMAGE_PATH = ROOT_PATH + File.separator + "image"

        //false升级用户不显示引导页  true升级用户显示引导页       此参数针对新安装用户显示引导页无效
        const val IS_SHOW_LEAD = false

//        刷新我的页面
        const val REFRESH_MINE_FRAGMENT=0x001
//        刷新我的收藏列表
        const val REFRESH_MY_COLLECT=0x002

    }

    /**
     * 红云主界面类型
     */
    object RedCloudMainType {
        const val MAIN_HOME_VIEW = 0//首页
        const val MAIN_SORT_VIEW = 1//分类
        const val MAIN_REPOSITORY_VIEW = 2//知库
        const val MAIN_READ_VIEW = 3//阅读
        const val MAIN_MINE_VIEW = 4//我的
    }

    /**
     * 书籍类型 -1为pdf格式，否则是ePup书籍的第一章id
     */
    object BookType{
        const val E_PUP = "0"
        const val PDF = "-1"
    }

    /**
     * 延云知库(model标识：zhiku) 延云阅读(model标识：book) 延云视听(model标识：media)
     * 漫绘延安(model标识：painting) 红色文旅(model标识：travel) 去伪求真(model标识：really)
     * 红书包(model标识：redsbag) 红云专题(model标识：subject) 红云活动(model标识：news) 红云时政(model标识：news)
     */
    object CollectModel{
        const val ZHIKU = "zhiku"
        const val ZHIKUMJ = "zhikumj"
        const val BOOK = "book"
        const val MEDIA = "media"
        const val PAINTING = "painting"
        const val TRAVEL = "travel"
        const val REALLY = "really"
        const val REDSBAG = "redsbag"
        const val SUBJECT = "subject"
        const val NEWS = "news"
//        const val BOOK = "news"
    }

    /**
     * 收藏状态
     */
    object CollectType{
        const val NOT_COLLECT = 0
        const val COLLECT = 1
    }

    /**
     * 第三方登录类型 1:QQ;2:微博;3:微信
     */
    object ThirdPartyLoginType{
        const val QQ = "1"
        const val SINA = "2"
        const val WEIXIN = "3"
    }

    /**
     * 性别 0未知 1男 2女
     */
    object Sex{
        const val BOY = "0"
        const val GIRL = "1"
        const val UNKNOWN = "2"
    }

    /**
     * 第三方平台所需参数
     */
    object PlatformParams {

        const val UMENG_APPKEY = "5d5255f80cafb2ca260006fc"
//        App Master Secret dc93f3a2551158b4a0e6033ae0af76b1
        /**umeng需要 */
        const val UMENG_SECRET = "dc93f3a2551158b4a0e6033ae0af76b1"
//        Umeng Message Secret dftaf3ilpwo3f97gyhyijw5vrdg0ae4w
        /**umeng推送 */
        const val UMENG_MASTER_SECRET = "dftaf3ilpwo3f97gyhyijw5vrdg0ae4w"

        /**xiaomi推送 */
        val XIAOMI_APP_ID = ""
        val XIAOMI_APP_KEY = ""
        val XIAOMI_APP_SECRET = ""

        /**huawei推送 */
        val HUAWEI_APP_ID = ""
        val HUAWEI_APP_SECRET = ""

        /**QQ网页登录需要 */
        val QQ_WAP_ID = ""
        val QQ_WAP_SECRET = ""
        val QQ_WAP_redirect_uri = ""

        /**微信需要 */
        const val WX_APP_ID = "wxf2b4b0ca7923bdbd"
        const val APP_SECRET = "43b2af03094c7e7daf0553bf3d0a926c"
        val MCH_ID = "" //商户号
        val API_KEY = "" //微信支付商户秘钥 api_key
        val WE_CHAT_CODE = "snsapi_userinfo"
        val WE_CHAT_STATUE = "wechat_sdk_demo"

        /**QQ 原生SDK需要 */
        const val QQ_APP_ID = "101751011"
        const val QQ_APP_SECRET = "facbf802438dc6493681d14e72900beb"

        /**讯飞语音 */
        val SPEECH_APP_ID = ""

        /**微博 原生SDK需要 */
        const val WEIBO_APP_ID = "3908023719"
        const val WEIBO_APP_SECRET = "2bca7022b0e95a8e758afb8d4b4c113c"
//        val WEIBO_REDIRECT_URL = "https://api.weibo.com/oauth2/default.html"
        const val WEIBO_REDIRECT_URL = "http://www.yancloud.red/"
        val WEIBO_SCOPE = ("email,direct_messages_read,direct_messages_write," + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
                    + "follow_app_official_microblog," + "invitation_write")
    }

    object ReadDialogSetting {
        @JvmField
        val FINISH_ACTIVITY = 0X0020//销毁当前面
        @JvmField
        val REFRESH_ACTIVITY = 0X0021//刷新当前页面
        const val READ_PROGRESS = 0X0022//阅读进度
        @JvmField
        val OVERTURN = 0X0023//翻转屏幕
        val DOWN_LOAD_BOOK = 0X0024//下载书籍
    }

    /**
     * 页面跳转请求码
     */
    object REQUEST_CODE {
        val READ_SEARCH = 2
        const val BOOKMARKS = 3
        const val READ_SETTING = 5
        val BUY_CHAPTER = 6
    }

    object ReadStatus {
        val SHOW_DIALOG = 1 // 显示加载dialog
        const val LOAD_PRE_PAGE = 2// 加载上一章
        const val LOAD_NEXT_PAGE = 3// 加载下一章
        val NEED_BUY = 4// 需要购买
        val NO_DATA = 5// 没有内容
        val LOAD_PAGE = 6// 加载指定的一张
        val WIDGET_REFRESH = 7// 刷新页面
        val FIRST_PAGE_LOAD = 8// 第一页加载完毕
        val NETWORK_ERROR = 9// 网络连接失败
        val SDCADE_FLOW = 10// 内存空间已满
        val CLEAR_CACHE = 14// 清理缓存成功
        val OPEN_BOOK = 15// 清理缓存成功
        val ERROR_MESSAGE = 16// 错误信息
        val FIRST_PAGE = 17// 已是第一页
        val CANCEL_BUY = 18// 取消购买

        val READ_LANGUANG = 19// 设置语言
        val SPEED = 20// 语速设置
        val BACK_SPEECH = 21 // 退出朗读
        val BEGIN_SPEECH = 22 // 开始朗读
        val READER_OVER = 23 // 阅读完成
        const val LIGHT_LEVEL = 24 //设置屏幕亮度值
        const val LIGHT_SYSTEM = 25 //系统亮度
        const val LIGHT_SYSTEM_OPEN = 26 //系统亮度
        const val LIGHT_SYSTEM_CLOSE = 27 //系统亮度

        val READ_LOG = 28 //阅读记录
        const val READ_STYLE_THEME = 29 //系统亮度
        const val READ_FONT_SIZE = 30 //字体大小
        const val READ_LINESPACE = 31 //行间距
        const val SHARE = 32 //行间距
    }
}