package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/31 11:30
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题内容
 * Change：
 * @Version：V1.0
 */
public class SpecialTopicContent {

    /**
     * code : 0000
     * data : {"sort":{"id":"11027","name":"specialList","title":"重大事件","status":"1","remark":null,"sort":"0","pid":"2900","level":"3","parentid":"2900","icon":null,"isshow":"1","frontfolder":null},"tanbensuyuan":{"title":"探本溯源","typeid":1,"data":[{"title":"1945年8月15日的延安：军民通宵庆祝日本投降","id":"12349","addtime":"2019-08-09 15:38:57","thumbnail":"","introduce":""},{"title":"日本投降过程全记录：从最后的疯狂到溃败","id":"12348","addtime":"2019-08-09 14:29:03","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565335702_1074447182.png","introduce":""},{"title":"九月三，中国人民扬眉吐气的日子","id":"12347","addtime":"2019-08-09 14:07:33","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565330964_517588502.png","introduce":""}]},"mingjiajiexi":{"title":"名家解析","typeid":2,"data":[{"title":"金一南：从空前觉醒到伟大复兴","id":"12346","addtime":"2019-08-09 11:22:12","thumbnail":"","introduce":""},{"title":"荣维木：原子弹不是日本投降根本原因","id":"12345","addtime":"2019-08-09 11:02:20","thumbnail":"","introduce":""},{"title":"彭玉龙：抗日战争中的人民选择和历史必然","id":"12344","addtime":"2019-08-09 10:29:02","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565318451_1248808160.png","introduce":""}]},"wenxiancaokan":{"title":"文献参考","typeid":3,"data":[{"title":"日本投降前夕毛泽东的布局","id":"12351","addtime":"2019-08-09 15:56:55","thumbnail":"","introduce":""},{"title":"日本投降的前前后后","id":"12350","addtime":"2019-08-09 15:53:29","thumbnail":"","introduce":""}]}}
     * msg : success
     */

    private String code;
    private DataBeanXXX data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBeanXXX getData() {
        return data;
    }

    public void setData(DataBeanXXX data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBeanXXX {
        /**
         * sort : {"id":"11027","name":"specialList","title":"重大事件","status":"1","remark":null,"sort":"0","pid":"2900","level":"3","parentid":"2900","icon":null,"isshow":"1","frontfolder":null}
         * tanbensuyuan : {"title":"探本溯源","typeid":1,"data":[{"title":"1945年8月15日的延安：军民通宵庆祝日本投降","id":"12349","addtime":"2019-08-09 15:38:57","thumbnail":"","introduce":""},{"title":"日本投降过程全记录：从最后的疯狂到溃败","id":"12348","addtime":"2019-08-09 14:29:03","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565335702_1074447182.png","introduce":""},{"title":"九月三，中国人民扬眉吐气的日子","id":"12347","addtime":"2019-08-09 14:07:33","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565330964_517588502.png","introduce":""}]}
         * mingjiajiexi : {"title":"名家解析","typeid":2,"data":[{"title":"金一南：从空前觉醒到伟大复兴","id":"12346","addtime":"2019-08-09 11:22:12","thumbnail":"","introduce":""},{"title":"荣维木：原子弹不是日本投降根本原因","id":"12345","addtime":"2019-08-09 11:02:20","thumbnail":"","introduce":""},{"title":"彭玉龙：抗日战争中的人民选择和历史必然","id":"12344","addtime":"2019-08-09 10:29:02","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565318451_1248808160.png","introduce":""}]}
         * wenxiancaokan : {"title":"文献参考","typeid":3,"data":[{"title":"日本投降前夕毛泽东的布局","id":"12351","addtime":"2019-08-09 15:56:55","thumbnail":"","introduce":""},{"title":"日本投降的前前后后","id":"12350","addtime":"2019-08-09 15:53:29","thumbnail":"","introduce":""}]}
         */

        private SortBean sort;
        private TanbensuyuanBean tanbensuyuan;
        private MingjiajiexiBean mingjiajiexi;
        private WenxiancaokanBean wenxiancaokan;

        public SortBean getSort() {
            return sort;
        }

        public void setSort(SortBean sort) {
            this.sort = sort;
        }

        public TanbensuyuanBean getTanbensuyuan() {
            return tanbensuyuan;
        }

        public void setTanbensuyuan(TanbensuyuanBean tanbensuyuan) {
            this.tanbensuyuan = tanbensuyuan;
        }

        public MingjiajiexiBean getMingjiajiexi() {
            return mingjiajiexi;
        }

        public void setMingjiajiexi(MingjiajiexiBean mingjiajiexi) {
            this.mingjiajiexi = mingjiajiexi;
        }

        public WenxiancaokanBean getWenxiancaokan() {
            return wenxiancaokan;
        }

        public void setWenxiancaokan(WenxiancaokanBean wenxiancaokan) {
            this.wenxiancaokan = wenxiancaokan;
        }

        public static class SortBean {
            /**
             * id : 11027
             * name : specialList
             * title : 重大事件
             * status : 1
             * remark : null
             * sort : 0
             * pid : 2900
             * level : 3
             * parentid : 2900
             * icon : null
             * isshow : 1
             * frontfolder : null
             */

            private String id;
            private String name;
            private String title;
            private String status;
            private Object remark;
            private String sort;
            private String pid;
            private String level;
            private String parentid;
            private Object icon;
            private String isshow;
            private Object frontfolder;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getPid() {
                return pid;
            }

            public void setPid(String pid) {
                this.pid = pid;
            }

            public String getLevel() {
                return level;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public Object getIcon() {
                return icon;
            }

            public void setIcon(Object icon) {
                this.icon = icon;
            }

            public String getIsshow() {
                return isshow;
            }

            public void setIsshow(String isshow) {
                this.isshow = isshow;
            }

            public Object getFrontfolder() {
                return frontfolder;
            }

            public void setFrontfolder(Object frontfolder) {
                this.frontfolder = frontfolder;
            }
        }

        public static class TanbensuyuanBean {
            /**
             * title : 探本溯源
             * typeid : 1
             * data : [{"title":"1945年8月15日的延安：军民通宵庆祝日本投降","id":"12349","addtime":"2019-08-09 15:38:57","thumbnail":"","introduce":""},{"title":"日本投降过程全记录：从最后的疯狂到溃败","id":"12348","addtime":"2019-08-09 14:29:03","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565335702_1074447182.png","introduce":""},{"title":"九月三，中国人民扬眉吐气的日子","id":"12347","addtime":"2019-08-09 14:07:33","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565330964_517588502.png","introduce":""}]
             */

            private String title;
            private int typeid;
            private List<DataBean> data;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getTypeid() {
                return typeid;
            }

            public void setTypeid(int typeid) {
                this.typeid = typeid;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class DataBean {
                /**
                 * title : 1945年8月15日的延安：军民通宵庆祝日本投降
                 * id : 12349
                 * addtime : 2019-08-09 15:38:57
                 * thumbnail :
                 * introduce :
                 */

                private String title;
                private String id;
                private String addtime;
                private String thumbnail;
                private String introduce;

                public DataBean() {
                }

                public DataBean(String title, String id, String addtime, String thumbnail, String introduce) {
                    this.title = title;
                    this.id = id;
                    this.addtime = addtime;
                    this.thumbnail = thumbnail;
                    this.introduce = introduce;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getAddtime() {
                    return addtime;
                }

                public void setAddtime(String addtime) {
                    this.addtime = addtime;
                }

                public String getThumbnail() {
                    return thumbnail;
                }

                public void setThumbnail(String thumbnail) {
                    this.thumbnail = thumbnail;
                }

                public String getIntroduce() {
                    return introduce;
                }

                public void setIntroduce(String introduce) {
                    this.introduce = introduce;
                }
            }
        }

        public static class MingjiajiexiBean {
            /**
             * title : 名家解析
             * typeid : 2
             * data : [{"title":"金一南：从空前觉醒到伟大复兴","id":"12346","addtime":"2019-08-09 11:22:12","thumbnail":"","introduce":""},{"title":"荣维木：原子弹不是日本投降根本原因","id":"12345","addtime":"2019-08-09 11:02:20","thumbnail":"","introduce":""},{"title":"彭玉龙：抗日战争中的人民选择和历史必然","id":"12344","addtime":"2019-08-09 10:29:02","thumbnail":"/Uploads/newsThumbnail/2019-08-09/1565318451_1248808160.png","introduce":""}]
             */

            private String title;
            private int typeid;
            private List<DataBeanX> data;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getTypeid() {
                return typeid;
            }

            public void setTypeid(int typeid) {
                this.typeid = typeid;
            }

            public List<DataBeanX> getData() {
                return data;
            }

            public void setData(List<DataBeanX> data) {
                this.data = data;
            }

            public static class DataBeanX {
                /**
                 * title : 金一南：从空前觉醒到伟大复兴
                 * id : 12346
                 * addtime : 2019-08-09 11:22:12
                 * thumbnail :
                 * introduce :
                 */

                private String title;
                private String id;
                private String addtime;
                private String thumbnail;
                private String introduce;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getAddtime() {
                    return addtime;
                }

                public void setAddtime(String addtime) {
                    this.addtime = addtime;
                }

                public String getThumbnail() {
                    return thumbnail;
                }

                public void setThumbnail(String thumbnail) {
                    this.thumbnail = thumbnail;
                }

                public String getIntroduce() {
                    return introduce;
                }

                public void setIntroduce(String introduce) {
                    this.introduce = introduce;
                }
            }
        }

        public static class WenxiancaokanBean {
            /**
             * title : 文献参考
             * typeid : 3
             * data : [{"title":"日本投降前夕毛泽东的布局","id":"12351","addtime":"2019-08-09 15:56:55","thumbnail":"","introduce":""},{"title":"日本投降的前前后后","id":"12350","addtime":"2019-08-09 15:53:29","thumbnail":"","introduce":""}]
             */

            private String title;
            private int typeid;
            private List<DataBeanXX> data;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getTypeid() {
                return typeid;
            }

            public void setTypeid(int typeid) {
                this.typeid = typeid;
            }

            public List<DataBeanXX> getData() {
                return data;
            }

            public void setData(List<DataBeanXX> data) {
                this.data = data;
            }

            public static class DataBeanXX {
                /**
                 * title : 日本投降前夕毛泽东的布局
                 * id : 12351
                 * addtime : 2019-08-09 15:56:55
                 * thumbnail :
                 * introduce :
                 */

                private String title;
                private String id;
                private String addtime;
                private String thumbnail;
                private String introduce;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getAddtime() {
                    return addtime;
                }

                public void setAddtime(String addtime) {
                    this.addtime = addtime;
                }

                public String getThumbnail() {
                    return thumbnail;
                }

                public void setThumbnail(String thumbnail) {
                    this.thumbnail = thumbnail;
                }

                public String getIntroduce() {
                    return introduce;
                }

                public void setIntroduce(String introduce) {
                    this.introduce = introduce;
                }
            }
        }
    }
}
