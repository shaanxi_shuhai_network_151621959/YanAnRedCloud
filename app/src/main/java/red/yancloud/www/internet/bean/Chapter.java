package red.yancloud.www.internet.bean;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/25 9:26
 * E_Mail：yh131412hys@163.com
 * Describe：章节内容
 * Change：
 * @Version：V1.0
 */
public class Chapter {

    /**
     * code : 0000
     * data : {"chapter":{"id":"16678","bookid":"767","level":"1","parentid":"0","title":"后记","path":"/book/15559125726362/OEBPS/Text/chapter014.html","isfree":"0","deleteflg":"1","dot":"9","preCid":"16677","nextCid":"0","content":"<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\r\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\r\n  \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\r\n\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"zh-CN\">\r\n<head>\r\n  <link href=\"http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Styles/main.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n\r\n  <title><\/title>\r\n<\/head>\r\n\r\n<body>\r\n  <h1 class=\"chapterTitle\" id=\"heading_id_2\">后记<\/h1>\r\n\r\n  <p class=\"content\">中铁二院,经过几代人的艰苦拼搏、开拓创新,不仅为祖国的铁道、地铁、公路、市政、建筑、电子工程建设事业建立了不朽功勋;而且在开拓世界铁路、公路工程建设事业中为祖国赢得了荣誉;现正以豪迈的英雄气概,沐浴\u201c科技是第一生产力\u201d的真理光辉,秉承成昆、南昆精神,向未来知识领域探索,不断向现代科技高峰登攀,打造企业知名品牌,为建设国际化第一流工程公司书写新的辉煌篇章!<\/p>\r\n\r\n  <p class=\"content\">作为中铁二院人,深感自豪!<\/p>\r\n\r\n  <p class=\"content\">我为中铁二院服务一生,几十年征战的难忘峥嵘岁月在脑海里翻腾,如在眼前,总想在有生之年能通过我的成长道路、亲身经历和对事业追求的回顾,从一个侧面展现中铁二院人为伟大共和国社会主义建设事业勇于拼搏、勇于创新、攻坚克难、铸造辉煌篇章的豪迈气派和亮丽风采,以了却中铁二院对我的培养之恩的心愿!在改革开放三十周年之际,我意兴盎然提笔疾书了铁二院《在改革开放大潮中拼搏前行》一文;在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d<\/p>\r\n\r\n  <p class=\"content\">在撰写和集录这本小册子过程中,始终得到各方面的帮助。中国华西设计公司黄贞蓉董事长、周华总经理,给予了极大关心、鼓励、支持,并创造条件,使我有充裕的时间聚精会神地写作;中铁二院工程集团公司党委书记兼董事长漆宝瑞,挤出宝贵的时间审阅了书稿;中铁二院工程集团公司朱颖总经理,在百忙中为本小册子写了\u201c代序\u201d,给予了过高评价;中铁二院工程集团公司党办马有举主任,从我写作开始就给予了大力帮助,在繁忙的工作中抽出时间,对文稿从内容上进行了全面审修,对书稿的版面、刊印、发行都做了精细安排;《铁道勘设报》原副总编舒光懋,以饱满的热情,自愿为本小册全部文稿从文字上进行了斧正和刊印校核;中铁二院南宁分院李俊杰院长及武汉谦诚岩土工程有限公司董事长郭克城对本小册子的刊印出版给予了热情支持;成都圣立文化传播有限公司以尽其力承接了本书的出版;原铁二院赵暑生院长、陈宏模副院长、叶祥昌总工程师,企管处廖作铨副处长,经营部李锡延部长,计财处文启明副处长、刘文柱科长等老领导、老同事得知我在撰写对铁二院的一些回忆,都十分支持,鼓励我尽快写出来;张大明兄还为《忆往昔峥嵘岁月》写了感言诗;还得到信息产业电子第十一设计研究院赵振元院长及宁钦海、冯森林、胡波、赵泽永、张勇浩、刘军、孙静、周晓玲等同志的关心、帮助,笔者在这里一一表示衷心的感谢!<\/p>\r\n\r\n  <p class=\"reference-r\">中国华西工程设计建设有限公司名誉董事长何光全<\/p>\r\n\r\n  <p class=\"reference-r\">2010年9月8日<\/p>\r\n\r\n  <p class=\"backcover\"><img alt=\"\" src=\"http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Images/backcover.jpg\" /><\/p>\r\n<\/body>\r\n<\/html>\r\n"},"book":{"id":"767","creater":"admin","bookpath":"15559125726362","tid":"2210","title":"难忘峥嵘岁月","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg","seriesname":"","vicetitle":"","author":"何光权","authordes":"","isbn":"227-04587-8-1","publicationdate":"2010-11-08","publisher":"宁夏人民出版社","price":"280","content":"在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在\n铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d","addtime":"2019-04-22 13:56:12","status":"2","type":null,"deleteflg":"1","settop":"2","dot":"225","bookcon":"小说","bookniandai":"","bookredian":""}}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * chapter : {"id":"16678","bookid":"767","level":"1","parentid":"0","title":"后记","path":"/book/15559125726362/OEBPS/Text/chapter014.html","isfree":"0","deleteflg":"1","dot":"9","preCid":"16677","nextCid":"0","content":"<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\r\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\r\n  \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\r\n\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"zh-CN\">\r\n<head>\r\n  <link href=\"http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Styles/main.css\" rel=\"stylesheet\" type=\"text/css\" />\r\n\r\n  <title><\/title>\r\n<\/head>\r\n\r\n<body>\r\n  <h1 class=\"chapterTitle\" id=\"heading_id_2\">后记<\/h1>\r\n\r\n  <p class=\"content\">中铁二院,经过几代人的艰苦拼搏、开拓创新,不仅为祖国的铁道、地铁、公路、市政、建筑、电子工程建设事业建立了不朽功勋;而且在开拓世界铁路、公路工程建设事业中为祖国赢得了荣誉;现正以豪迈的英雄气概,沐浴\u201c科技是第一生产力\u201d的真理光辉,秉承成昆、南昆精神,向未来知识领域探索,不断向现代科技高峰登攀,打造企业知名品牌,为建设国际化第一流工程公司书写新的辉煌篇章!<\/p>\r\n\r\n  <p class=\"content\">作为中铁二院人,深感自豪!<\/p>\r\n\r\n  <p class=\"content\">我为中铁二院服务一生,几十年征战的难忘峥嵘岁月在脑海里翻腾,如在眼前,总想在有生之年能通过我的成长道路、亲身经历和对事业追求的回顾,从一个侧面展现中铁二院人为伟大共和国社会主义建设事业勇于拼搏、勇于创新、攻坚克难、铸造辉煌篇章的豪迈气派和亮丽风采,以了却中铁二院对我的培养之恩的心愿!在改革开放三十周年之际,我意兴盎然提笔疾书了铁二院《在改革开放大潮中拼搏前行》一文;在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d<\/p>\r\n\r\n  <p class=\"content\">在撰写和集录这本小册子过程中,始终得到各方面的帮助。中国华西设计公司黄贞蓉董事长、周华总经理,给予了极大关心、鼓励、支持,并创造条件,使我有充裕的时间聚精会神地写作;中铁二院工程集团公司党委书记兼董事长漆宝瑞,挤出宝贵的时间审阅了书稿;中铁二院工程集团公司朱颖总经理,在百忙中为本小册子写了\u201c代序\u201d,给予了过高评价;中铁二院工程集团公司党办马有举主任,从我写作开始就给予了大力帮助,在繁忙的工作中抽出时间,对文稿从内容上进行了全面审修,对书稿的版面、刊印、发行都做了精细安排;《铁道勘设报》原副总编舒光懋,以饱满的热情,自愿为本小册全部文稿从文字上进行了斧正和刊印校核;中铁二院南宁分院李俊杰院长及武汉谦诚岩土工程有限公司董事长郭克城对本小册子的刊印出版给予了热情支持;成都圣立文化传播有限公司以尽其力承接了本书的出版;原铁二院赵暑生院长、陈宏模副院长、叶祥昌总工程师,企管处廖作铨副处长,经营部李锡延部长,计财处文启明副处长、刘文柱科长等老领导、老同事得知我在撰写对铁二院的一些回忆,都十分支持,鼓励我尽快写出来;张大明兄还为《忆往昔峥嵘岁月》写了感言诗;还得到信息产业电子第十一设计研究院赵振元院长及宁钦海、冯森林、胡波、赵泽永、张勇浩、刘军、孙静、周晓玲等同志的关心、帮助,笔者在这里一一表示衷心的感谢!<\/p>\r\n\r\n  <p class=\"reference-r\">中国华西工程设计建设有限公司名誉董事长何光全<\/p>\r\n\r\n  <p class=\"reference-r\">2010年9月8日<\/p>\r\n\r\n  <p class=\"backcover\"><img alt=\"\" src=\"http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Images/backcover.jpg\" /><\/p>\r\n<\/body>\r\n<\/html>\r\n"}
         * book : {"id":"767","creater":"admin","bookpath":"15559125726362","tid":"2210","title":"难忘峥嵘岁月","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg","seriesname":"","vicetitle":"","author":"何光权","authordes":"","isbn":"227-04587-8-1","publicationdate":"2010-11-08","publisher":"宁夏人民出版社","price":"280","content":"在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在\n铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d","addtime":"2019-04-22 13:56:12","status":"2","type":null,"deleteflg":"1","settop":"2","dot":"225","bookcon":"小说","bookniandai":"","bookredian":""}
         */

        private ChapterBean chapter;
        private BookBean book;

        public ChapterBean getChapter() {
            return chapter;
        }

        public void setChapter(ChapterBean chapter) {
            this.chapter = chapter;
        }

        public BookBean getBook() {
            return book;
        }

        public void setBook(BookBean book) {
            this.book = book;
        }

        public static class ChapterBean {
            /**
             * id : 16678
             * bookid : 767
             * level : 1
             * parentid : 0
             * title : 后记
             * path : /book/15559125726362/OEBPS/Text/chapter014.html
             * isfree : 0
             * deleteflg : 1
             * dot : 9
             * preCid : 16677
             * nextCid : 0
             * content : <?xml version="1.0" encoding="utf-8" standalone="no"?>
             <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
             "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

             <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN">
             <head>
             <link href="http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Styles/main.css" rel="stylesheet" type="text/css" />

             <title></title>
             </head>

             <body>
             <h1 class="chapterTitle" id="heading_id_2">后记</h1>

             <p class="content">中铁二院,经过几代人的艰苦拼搏、开拓创新,不仅为祖国的铁道、地铁、公路、市政、建筑、电子工程建设事业建立了不朽功勋;而且在开拓世界铁路、公路工程建设事业中为祖国赢得了荣誉;现正以豪迈的英雄气概,沐浴“科技是第一生产力”的真理光辉,秉承成昆、南昆精神,向未来知识领域探索,不断向现代科技高峰登攀,打造企业知名品牌,为建设国际化第一流工程公司书写新的辉煌篇章!</p>

             <p class="content">作为中铁二院人,深感自豪!</p>

             <p class="content">我为中铁二院服务一生,几十年征战的难忘峥嵘岁月在脑海里翻腾,如在眼前,总想在有生之年能通过我的成长道路、亲身经历和对事业追求的回顾,从一个侧面展现中铁二院人为伟大共和国社会主义建设事业勇于拼搏、勇于创新、攻坚克难、铸造辉煌篇章的豪迈气派和亮丽风采,以了却中铁二院对我的培养之恩的心愿!在改革开放三十周年之际,我意兴盎然提笔疾书了铁二院《在改革开放大潮中拼搏前行》一文;在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念“中国华西设计”创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,“莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。”</p>

             <p class="content">在撰写和集录这本小册子过程中,始终得到各方面的帮助。中国华西设计公司黄贞蓉董事长、周华总经理,给予了极大关心、鼓励、支持,并创造条件,使我有充裕的时间聚精会神地写作;中铁二院工程集团公司党委书记兼董事长漆宝瑞,挤出宝贵的时间审阅了书稿;中铁二院工程集团公司朱颖总经理,在百忙中为本小册子写了“代序”,给予了过高评价;中铁二院工程集团公司党办马有举主任,从我写作开始就给予了大力帮助,在繁忙的工作中抽出时间,对文稿从内容上进行了全面审修,对书稿的版面、刊印、发行都做了精细安排;《铁道勘设报》原副总编舒光懋,以饱满的热情,自愿为本小册全部文稿从文字上进行了斧正和刊印校核;中铁二院南宁分院李俊杰院长及武汉谦诚岩土工程有限公司董事长郭克城对本小册子的刊印出版给予了热情支持;成都圣立文化传播有限公司以尽其力承接了本书的出版;原铁二院赵暑生院长、陈宏模副院长、叶祥昌总工程师,企管处廖作铨副处长,经营部李锡延部长,计财处文启明副处长、刘文柱科长等老领导、老同事得知我在撰写对铁二院的一些回忆,都十分支持,鼓励我尽快写出来;张大明兄还为《忆往昔峥嵘岁月》写了感言诗;还得到信息产业电子第十一设计研究院赵振元院长及宁钦海、冯森林、胡波、赵泽永、张勇浩、刘军、孙静、周晓玲等同志的关心、帮助,笔者在这里一一表示衷心的感谢!</p>

             <p class="reference-r">中国华西工程设计建设有限公司名誉董事长何光全</p>

             <p class="reference-r">2010年9月8日</p>

             <p class="backcover"><img alt="" src="http://www.yancloud.red/Uploads/book/15559125726362/OEBPS/Images/backcover.jpg" /></p>
             </body>
             </html>

             */

            private String id;
            private String bookid;
            private String level;
            private String parentid;
            private String title;
            private String path;
            private String isfree;
            private String deleteflg;
            private String dot;
            private String preCid;
            private String nextCid;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getBookid() {
                return bookid;
            }

            public void setBookid(String bookid) {
                this.bookid = bookid;
            }

            public String getLevel() {
                return level;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getIsfree() {
                return isfree;
            }

            public void setIsfree(String isfree) {
                this.isfree = isfree;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getDot() {
                return dot;
            }

            public void setDot(String dot) {
                this.dot = dot;
            }

            public String getPreCid() {
                return preCid;
            }

            public void setPreCid(String preCid) {
                this.preCid = preCid;
            }

            public String getNextCid() {
                return nextCid;
            }

            public void setNextCid(String nextCid) {
                this.nextCid = nextCid;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            @Override
            public String toString() {
                return "ChapterBean{" +
                        "id='" + id + '\'' +
                        ", bookid='" + bookid + '\'' +
                        ", level='" + level + '\'' +
                        ", parentid='" + parentid + '\'' +
                        ", title='" + title + '\'' +
                        ", path='" + path + '\'' +
                        ", isfree='" + isfree + '\'' +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", dot='" + dot + '\'' +
                        ", preCid='" + preCid + '\'' +
                        ", nextCid='" + nextCid + '\'' +
                        ", content='" + content + '\'' +
                        '}';
            }
        }

        public static class BookBean {
            /**
             * id : 767
             * creater : admin
             * bookpath : 15559125726362
             * tid : 2210
             * title : 难忘峥嵘岁月
             * thumbnail : /Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg
             * seriesname :
             * vicetitle :
             * author : 何光权
             * authordes :
             * isbn : 227-04587-8-1
             * publicationdate : 2010-11-08
             * publisher : 宁夏人民出版社
             * price : 280
             * content : 在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在
             铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念“中国华西设计”创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,“莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。”
             * addtime : 2019-04-22 13:56:12
             * status : 2
             * type : null
             * deleteflg : 1
             * settop : 2
             * dot : 225
             * bookcon : 小说
             * bookniandai :
             * bookredian :
             */

            private String id;
            private String creater;
            private String bookpath;
            private String tid;
            private String title;
            private String thumbnail;
            private String seriesname;
            private String vicetitle;
            private String author;
            private String authordes;
            private String isbn;
            private String publicationdate;
            private String publisher;
            private String price;
            private String content;
            private String addtime;
            private String status;
            private Object type;
            private String deleteflg;
            private String settop;
            private String dot;
            private String bookcon;
            private String bookniandai;
            private String bookredian;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getBookpath() {
                return bookpath;
            }

            public void setBookpath(String bookpath) {
                this.bookpath = bookpath;
            }

            public String getTid() {
                return tid;
            }

            public void setTid(String tid) {
                this.tid = tid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getSeriesname() {
                return seriesname;
            }

            public void setSeriesname(String seriesname) {
                this.seriesname = seriesname;
            }

            public String getVicetitle() {
                return vicetitle;
            }

            public void setVicetitle(String vicetitle) {
                this.vicetitle = vicetitle;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getAuthordes() {
                return authordes;
            }

            public void setAuthordes(String authordes) {
                this.authordes = authordes;
            }

            public String getIsbn() {
                return isbn;
            }

            public void setIsbn(String isbn) {
                this.isbn = isbn;
            }

            public String getPublicationdate() {
                return publicationdate;
            }

            public void setPublicationdate(String publicationdate) {
                this.publicationdate = publicationdate;
            }

            public String getPublisher() {
                return publisher;
            }

            public void setPublisher(String publisher) {
                this.publisher = publisher;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getType() {
                return type;
            }

            public void setType(Object type) {
                this.type = type;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getDot() {
                return dot;
            }

            public void setDot(String dot) {
                this.dot = dot;
            }

            public String getBookcon() {
                return bookcon;
            }

            public void setBookcon(String bookcon) {
                this.bookcon = bookcon;
            }

            public String getBookniandai() {
                return bookniandai;
            }

            public void setBookniandai(String bookniandai) {
                this.bookniandai = bookniandai;
            }

            public String getBookredian() {
                return bookredian;
            }

            public void setBookredian(String bookredian) {
                this.bookredian = bookredian;
            }

            @Override
            public String toString() {
                return "BookBean{" +
                        "id='" + id + '\'' +
                        ", creater='" + creater + '\'' +
                        ", bookpath='" + bookpath + '\'' +
                        ", tid='" + tid + '\'' +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", seriesname='" + seriesname + '\'' +
                        ", vicetitle='" + vicetitle + '\'' +
                        ", author='" + author + '\'' +
                        ", authordes='" + authordes + '\'' +
                        ", isbn='" + isbn + '\'' +
                        ", publicationdate='" + publicationdate + '\'' +
                        ", publisher='" + publisher + '\'' +
                        ", price='" + price + '\'' +
                        ", content='" + content + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", status='" + status + '\'' +
                        ", type=" + type +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", settop='" + settop + '\'' +
                        ", dot='" + dot + '\'' +
                        ", bookcon='" + bookcon + '\'' +
                        ", bookniandai='" + bookniandai + '\'' +
                        ", bookredian='" + bookredian + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "chapter=" + chapter +
                    ", book=" + book +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
