package red.yancloud.www.internet.api;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import red.yancloud.www.common.Constants;
import red.yancloud.www.db.sharedpreference.UserInfoShardPreference;
import red.yancloud.www.internet.bean.*;
import red.yancloud.www.util.AppUtils;
import red.yancloud.www.util.NetworkUtils;
import red.yancloud.www.util.PhoneUtils;
import red.yancloud.www.util.TimeFormatUtil;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/31 15:44
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
public class RedCloudApis {

    private final Retrofit retrofit;
//    private final Retrofit repositoryRetrofit;
    public final OkHttpClient okHttpClient;

    private RedCloudApis() {

          /*cookieJar(new CookiesManager())： 设置一个自动管理cookies的管理器
        addInterceptor(new MyIntercepter())：添加拦截器
        addNetworkInterceptor(new
        CookiesInterceptor(MyApplication.getInstance().getApplicationContext()))：添加网络连接器
        connectTimeout(30, TimeUnit.SECONDS):请求超时时间
        writeTimeout(30,TimeUnit.SECONDS):写入超时时间
        readTimeout(30, TimeUnit.SECONDS):读取超时时间*/

        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        retrofit = new Retrofit.Builder()
                .client(okHttpClient)//设置使用okhttp网络请求
                .baseUrl(Constants.API_BASE_RED_CLOUD_URL)//设置服务器路径
                .addConverterFactory(GsonConverterFactory.create())//添加转化库，默认是Gson
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//添加回调库，采用RxJava
                .build();
//        repositoryRetrofit = new Retrofit.Builder()
//                .client(okHttpClient)//设置使用okhttp网络请求
//                .baseUrl(Constants.API_BASE_REPOSITORY_URL)//设置服务器路径
//                .addConverterFactory(GsonConverterFactory.create())//添加转化库，默认是Gson
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//添加回调库，采用RxJava
//                .build();
    }

    public static RedCloudApis getInstance(){
        return RedCloudApisHolder.INSTANCE;
    }

    private static class RedCloudApisHolder{
        private static final RedCloudApis INSTANCE = new RedCloudApis();
    }

    private <T> T create(Class<T> service) {
        return retrofit.create(service);
    }

//    private <T> T repositoryCreate(Class<T> service) {
//        return repositoryRetrofit.create(service);
//    }

    private static ApiService apiService = RedCloudApis.getInstance().create(ApiService.class);
//    private static ApiService repositoryApiService = RedCloudApis.getInstance().repositoryCreate(ApiService.class);

    private RequestBody formatParams(Object params){
        return RequestBody.create(String.valueOf(params), MediaType.parse("application/json; charset=utf-8"));
    }

    private static <T> void setSubscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    /**
     * @Author：风不会停息 on 2019/2/14 16:50
     * E_Mail：yh131412hys@163.com
     * @param
     * @Description：公共请求参数
     * @Change：
     * @Version：V1.0
     */
    private Map<String, RequestBody> getCommonRequestParamsMap(String interfaceName) {

        String systemTime = String.valueOf(TimeFormatUtil.getCurrentDateTimeSeconds());
        Map<String, RequestBody> params = new HashMap<>();
        params.put("packagename", formatParams(AppUtils.getPackageName()));
        params.put("sign", formatParams(AppUtils.getSign(interfaceName)));
        params.put("source", formatParams(AppUtils.getChannel("UMENG_CHANNEL")));
        params.put("uuid", formatParams(PhoneUtils.getUUID()));
        params.put("siteid", formatParams(Constants.SITEID));
        params.put("timestamp", formatParams(systemTime));
        params.put("username", formatParams(UserInfoShardPreference.Companion.getInstance().getUserName()));
        params.put("imei", formatParams(PhoneUtils.getIMEI()));
        params.put("version", formatParams(AppUtils.getAppVersionCode()));
        params.put("ip", formatParams(NetworkUtils.getLocalIpAddress()));
        params.put("uid", formatParams(UserInfoShardPreference.Companion.getInstance().getUid()));
        return params;
    }

    private Map<String, String> getCommonRequestParamsStringMap(String interfaceName) {

        String systemTime = String.valueOf(TimeFormatUtil.getCurrentDateTimeSeconds());
        Map<String, String> params = new HashMap<>();
//        params.put("packagename", AppUtils.getPackageName());
        params.put("sign", AppUtils.getSign(interfaceName));
//        params.put("source", AppUtils.getChannel("UMENG_CHANNEL"));
//        params.put("uuid", PhoneUtils.getUUID());
//        params.put("siteid", Constants.SITEID);
//        params.put("timestamp", systemTime);
//        params.put("username", UserInfoShardPreference.getInstance().getUserName());
//        params.put("imei", PhoneUtils.getIMEI());
//        params.put("version",Integer.toString(AppUtils.getAppVersionCode()));
//        params.put("ip", NetworkUtils.getLocalIpAddress());
//        params.put("uid", UserInfoShardPreference.getInstance().getUid());
        return params;
    }

    public void post(String username,String pwd,Observer<Login> observer) {
//        Map<String, RequestBody> params = getCommonRequestParamsMap("ac=login");
        Map<String, String> params = getCommonRequestParamsStringMap("ac=login");
        params.put("ac", "login");
        params.put("username", username);
        params.put("pwd", pwd);
        setSubscribe(apiService.postMultipart(params), observer);
    }

    public void get(String username,String pwd,Observer<Login> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=login");
        params.put("ac", "login");
        params.put("username", username);
        params.put("pwd", pwd);
        setSubscribe(apiService.getMultipart(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/10 11:29
     * E_Mail：yh131412hys@163.com
     * @param username
     * @param pwd
     * @param observer
     * @Description：登录
     * @Change：
     * @Version：V1.0
     */
    public void login(String username,String pwd,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=login");
//        Map<String, String> params =new HashMap<>();
        params.put("ac", "login");
        params.put("username", username);
        params.put("pwd", pwd);
        setSubscribe(apiService.login(params), observer);
    }

//    pwd=123456&timestamp=1560250730&imei=352621065424043&username=yh1111
//    "username":"qqyyuuuuu","pwd":"74a0c18637d1c7585a37b331c78d71a8",
    /**
     * @Author：风不会停息 on 2019/6/11 19:00
     * E_Mail：yh131412hys@163.com
     * @param username
     * @param pwd
     * @param email
     * @param uCode
     * @param observer
     * @Description：注册
     * @Change：
     * @Version：V1.0
     */
    public void register(String username,String pwd,String email,String uCode,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=register");
        params.put("ac", "register");
        params.put("username", username);
        params.put("pwd", pwd);
        params.put("email", email);
        params.put("uCode", uCode);
        setSubscribe(apiService.register(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/17 20:01
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param page
     * @param limit
     * @param observer
     * @Description：我的书架列表
     * @Change：
     * @Version：V1.0
     */
    public void bookcase(String uid,String page,String limit,Observer<MyBookShelf> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=bookcase");
        params.put("ac", "bookcase");
        params.put("uid", uid);
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.bookcase(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/17 20:01
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param page
     * @param limit
     * @param observer
     * @Description：首页阅读
     * @Change：
     * @Version：V1.0
     */
    public void bookList(String uid,String page,String limit,String tid,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=booklist");
        params.put("ac", "booklist");
        params.put("uid", uid);
        params.put("page", page);
        params.put("limit", limit);
        params.put("tid", tid);
        setSubscribe(apiService.booklist(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/17 20:54
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param page
     * @param limit
     * @param observer
     * @Description：我的收藏（目前只查书，过滤掉了新闻）
     * @Change：
     * @Version：V1.0
     */
    public void myFavorite(String uid,String page,String limit,Observer<MyCollect> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=myFavorite");
        params.put("ac", "myFavorite");
        params.put("uid", uid);
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.myFavorite(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/25 9:13
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param cid 章节ID
     * @param bookId
     * @param observer
     * @Description：获取章节内容
     * @Change：
     * @Version：V1.0
     */
    public void chapter(String uid,String cid,String bookId,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=chapter");
        params.put("ac", "chapter");
        params.put("uid", uid);
        params.put("cid", cid);
        params.put("bookId", bookId);
        setSubscribe(apiService.chapter(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/25 20:42
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @param observer
     * @Description：获取书籍信息
     * @Change：
     * @Version：V1.0
     */
    public void bookInfo(String uid,String bookId,Observer<BookInfo> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=infoByBookId");
        params.put("ac", "infoByBookId");
        params.put("uid", uid);
        params.put("bid", bookId);
        setSubscribe(apiService.bookInfo(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/25 21:11
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @param observer
     * @Description：书籍目录
     * @Change：
     * @Version：V1.0
     */
    public void catalog(String bookId,String page,String limit,Observer<BookCatalog> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=catalog");
        params.put("ac", "catalog");
        params.put("bid", bookId);
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.catalog(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/1 18:57
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id  分类id，必填，默认3001（伟人故居）
     * @param pid   父类id，二级分类必填
     * @param keyword
     * @param observer
     * @Description：红色文旅(没有二级分类则id对应id，有二级分类则id对应pid)
     * @Change：
     * @Version：V1.0
     */
    public void getTravel(String page,String limit,String id,String pid,String keyword,Observer<RedTravel> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getTravel");
        params.put("ac", "getTravel");
        params.put("page", page);
        params.put("limit", limit);
        params.put("id", id);
        params.put("pid", pid);
        params.put("keyword", keyword);
        setSubscribe(apiService.getTravel(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/27 19:48
     * E_Mail：yh131412hys@163.com
     * @param observer
     * @Description：革命纪念馆地区列表
     * @Change：
     * @Version：V1.0
     */
    public void travelType(Observer<RevolutionMemorialType> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=travelType");
        params.put("ac", "travelType");
        setSubscribe(apiService.travelType(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/6/27 20:08
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param area  区域，可选，默认：全部地区
     * @param word  搜索词，可选
     * @param observer
     * @Description：纪念馆数据列表
     * @Change：
     * @Version：V1.0
     */
    public void memorialList(String page,String limit,String area,String word,Observer<RevolutionMemorial> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=memorialList");
        params.put("ac", "memorialList");
        params.put("page", page);
        params.put("limit", limit);
        params.put("area", area);
        params.put("word", word);
        setSubscribe(apiService.memorialList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/3 18:38
     * E_Mail：yh131412hys@163.com
     * @param observer
     * @Description：延云阅读的搜索条件
     * @Change：
     * @Version：V1.0
     */
    public void bookConfig(Observer<ReadBookType> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=bookConfig");
        params.put("ac", "bookConfig");
        setSubscribe(apiService.bookConfig(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/3 18:39
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param id
     * @param observer
     * @Description：我的收藏删除
     * @Change：
     * @Version：V1.0
     */
    public void myCollectDelete(String uid,String id,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=myfavdel");
        params.put("ac", "myfavdel");
        params.put("uid", uid);
        params.put("id", id);
        setSubscribe(apiService.myCollectDelete(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/3 18:39
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param bookId
     * @param observer
     * @Description：我的书架删除
     * @Change：
     * @Version：V1.0
     */
    public void myBookDelte(String uid,String bookId,Observer<ReadBookType> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=mybookdel");
        params.put("ac", "mybookdel");
        params.put("uid", uid);
        params.put("bookId", bookId);
        setSubscribe(apiService.myBookDelte(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/8 20:16
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param page
     * @param limit
     * @param observer
     * @Description：用户书籍阅读记录
     * @Change：
     * @Version：V1.0
     */
    public void bookRecord(String uid,String page,String limit,Observer<BookRecord> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=bookRecord");
        params.put("ac", "bookRecord");
        params.put("uid", uid);
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.bookRecord(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/9 9:31
     * E_Mail：yh131412hys@163.com
     * @param uid
     * @param bookId
     * @param firstbookchapterid 默认第一章的ID，pdf传-1，epub传章节ID
     * @param observer
     * @Description：加入书架
     * @Change：
     * @Version：V1.0
     */
    public void addBookcase(String uid,String bookId,String firstbookchapterid,Observer<Default> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=addBookcase");
        params.put("ac", "addBookcase");
        params.put("uid", uid);
        params.put("bookid", bookId);
        params.put("firstbookchapterid", firstbookchapterid);
        setSubscribe(apiService.addBookcase(params), observer);
    }

    /**
     * 修改密码
     * @param uid
     * @param oldpass 老密码，不加密
     * @param newpass 新密码，不加密   长度大于等于6
     * @param isnewpass 重复新密码，不加密  长度大于等于6
     * @param observer
     */
    public void editPwd(String uid,String oldpass,String newpass,String isnewpass,Observer<Default> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=editPwd");
        params.put("ac", "editPwd");
        params.put("uid", uid);
        params.put("oldpass", oldpass);
        params.put("newpass", newpass);
        params.put("isnewpass", isnewpass);
        setSubscribe(apiService.editPwd(params), observer);
    }

    /**
     * 修改用户信息
     * @param uid  必填，用户id
     * @param name   必填，姓名
     * @param province 必填，省
     * @param city 必填，市
     * @param district 必填，区
     * @param organization 必填，所属党支部
     * @param observer
     */
    public void editUser(String uid,String name,String province,String city,String district,String organization,Observer<Login> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=editUser");
        params.put("ac", "editUser");
        params.put("uid", uid);
        params.put("name", name);
        params.put("province", province);
        params.put("city", city);
        params.put("district", district);
        params.put("organization", organization);
        setSubscribe(apiService.editUser(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/26 16:55
     * E_Mail：yh131412hys@163.com
     * @param uid 用户id
     * @param observer
     * @Description：获取用户信息
     * @Change：
     * @Version：V1.0
     */
    public void userInfo(String uid,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=user");
        params.put("ac", "user");
        params.put("uid", uid);
        setSubscribe(apiService.user(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/16 21:09
     * E_Mail：yh131412hys@163.com
     * @param observer
     * @Description：获取知库的所有分类，二维父子关系
     * @Change：
     * @Version：V1.0
     */
    public void getZhiKuAllSortData(Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getZhikuData");
        params.put("ac", "getZhikuData");
//        http%3a%2f%2fzhi.yancloud.red%2findex.php%3fr%3dyancloudapp%2fyancloudapp%2fallcatalog
        params.put("url", "http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/allcatalog");
        setSubscribe(apiService.getZhiKuAllSortData(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/16 21:17
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param cid 一级分类id，获取一级分类下所有的数据
     * @param observer
     * @Description：获取一级分类下所有的数据
     * @Change：
     * @Version：V1.0
     */
    public void getZhiKuFirstLevelData(String page, String pageSize, String cid, Observer<RepositoryFirstLevelData> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getZhikuData");
        params.put("ac", "getZhikuData");
        params.put("r", "yancloudapp/yancloudapp/zhilist");
        //        http%3a%2f%2fzhi.yancloud.red%2findex.php%3fr%3dyancloudapp%2fyancloudapp%2fzhilist%26cid%3d1
        params.put("url", "http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/zhilist&cid="+cid+"&page="+page+"&pagesize="+pageSize);
//        params.put("page", page);
//        params.put("pagesize", pageSize);
//        params.put("cid", cid);
        setSubscribe(apiService.getZhiKuFirstLevelData(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/17 10:55
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param id 二级分类id
     * @param observer
     * @Description：获取二级分类下的词条
     * @Change：
     * @Version：V1.0
     */
    public void getZhiKuSecondLevelData(String page, String pageSize, String id, Observer<RepositoryFirstLevelData> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getZhikuData");
        params.put("ac", "getZhikuData");
        params.put("r", "yancloudapp/yancloudapp/zhilist");
        //      http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/allcatalog
        params.put("url", "http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/zhilist&id="+id+"&page="+page+"&pagesize="+pageSize);
//        params.put("page", page);
//        params.put("pagesize", pageSize);
//        params.put("id",id);
        setSubscribe(apiService.getZhiKuSecondLevelData(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/17 11:32
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param cid cid一级分类ID
     * @param observer
     * @Description：获取知库二级分类
     * @Change：
     * @Version：V1.0
     */
    public void getZhiKuSecondLevelSortData(String page, String pageSize, String cid, Observer<RepositorySort> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getZhikuData");
        params.put("ac", "getZhikuData");
        params.put("url", "http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/secondcatalog&cid="+cid);
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getZhiKuSecondLevelSortData(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/24 19:19
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param observer
     * @Description：首页广播消息列表
     * @Change：
     * @Version：V1.0
     */
    public void getEventNews(String page,String pageSize,Observer<BroadcastMessage> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getEventNews");
        params.put("ac", "getEventNews");
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getEventNews(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/25 20:55
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param tid 分类ID，可以空，空查全部分类
     * @param keyword  搜索关键字，可以空， 空查全部
     * @param observer
     * @Description：阅读搜索列表接口
     * @Change：
     * @Version：V1.0
     */
    public void getSearchBookList(String tid,String keyword,String page,String limit,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=booklist");
        params.put("ac", "booklist");
        params.put("tid", tid);
        params.put("keyword", keyword);
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.getSearchBookList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/25 21:02
     * E_Mail：yh131412hys@163.com
     * @param id  分类ID，可空，默认9999（史海钩沉）
     * @param keyword 搜索关键词，可空
     * @param page
     * @param limit
     * @param observer
     * @Description：去伪求真
     * @Change：
     * @Version：V1.0
     */
    public void getReallyList(String id,String keyword,String page,String limit,Observer<Really> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getReally");
        params.put("id", id);
        params.put("keyword", keyword);
        params.put("ac", "getReally");
        params.put("page", page);
        params.put("limit", limit);
        setSubscribe(apiService.getReallyList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/26 18:58
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param keywords
     * @param observer
     * @Description：知库搜索
     * @Change：
     * @Version：V1.0
     */
    public void getZhiKuSearch(String page,String pageSize,String keywords,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getZhikuData");
        params.put("ac", "getZhikuData");
        params.put("page", page);
        params.put("pagesize", pageSize);
//        http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/search&keywords=%E6%AF%9B%E6%B3%BD%E4%B8%9C
        params.put("url", "http://zhi.yancloud.red/index.php?r=yancloudapp/yancloudapp/search&keywords="+keywords);
        setSubscribe(apiService.getZhiKuSearch(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/26 19:13
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id 分类ID，可空，默认2705（党建生活）
     * @param keyword 分类ID，可空，默认2705（党建生活）
     * @param observer
     * @Description：红书包
     * @Change：
     * @Version：V1.0
     */
    public void getRedBagList(String page,String limit,String id,String keyword,Observer<RedBag> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getRedsbag");
        params.put("ac", "getRedsbag");
        params.put("page", page);
        params.put("limit", limit);
        params.put("id", id);
        params.put("keyword", keyword);
        setSubscribe(apiService.getRedBagList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/28 12:04
     * E_Mail：yh131412hys@163.com
     * @param page 这个功能不支持分页 只支持查询多少条
     * @param pageSize
     * @param observer
     * @Description：知库热搜更多
     * @Change：
     * @Version：V1.0
     */
    public void getHotEntryList(String page,String pageSize,Observer<HotSearchEntryMore> observer) {

        Map<String, String> params = getCommonRequestParamsStringMap("ac=getHotEntry");
        params.put("ac", "getHotEntry");
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getHotEntry(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/28 12:05
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param observer
     * @Description：知库词条更多
     * @Change：
     * @Version：V1.0
     */
    public void getMoreEntryList(String page,String pageSize,Observer<RepositoryFirstLevelData> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getMoreEntryList");
        params.put("ac", "getMoreEntry");
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getMoreEntry(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/28 12:05
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param observer
     * @Description：知库专家团队更多
     * @Change：
     * @Version：V1.0
     */
    public void getExpertTeamList(String page,String pageSize,Observer<ExpertTeamMore> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getMjList");
        params.put("ac", "getMjList");
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getExpertTeamList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/28 12:05
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param pageSize
     * @param observer
     * @Description：知库作者团队更多
     * @Change：
     * @Version：V1.0
     */
    public void getAuthorList(String page,String pageSize,Observer<ExpertTeamMore> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getAuthorList");
        params.put("ac", "getAuthorList");
        params.put("page", page);
        params.put("pagesize", pageSize);
        setSubscribe(apiService.getAuthorList(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/29 20:15
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id 分类ID，可空，默认11027（重大事件）
     * @param keyword  搜索关键词，可空
     * @param observer
     * @Description：红云专题列表
     * @Change：
     * @Version：V1.0
     */
    public void getSubject(String page,String limit,String id,String keyword,Observer<SpecialTopic> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getSubject");
        params.put("ac", "getSubject");
        params.put("page", page);
        params.put("limit", limit);
        params.put("id", id);
        params.put("keyword", keyword);
        setSubscribe(apiService.getSubject(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/30 19:47
     * E_Mail：yh131412hys@163.com
     * @param id 主题ID，必填
     * @param observer
     * @Description：红云专题内容（暂无分页）
     * @Change：
     * @Version：V1.0
     */
    public void getSubjectTheme(String id,Observer<SpecialTopicContent> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getSubjectTheme");
        params.put("ac", "getSubjectTheme");
//        params.put("page", page);
//        params.put("pagesize", pageSize);
        params.put("id", id);
        setSubscribe(apiService.getSubjectTheme(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/7/31 11:42
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id 专题ID，必填
     * @param typeId 专题分组ID，必填
     * @param observer
     * @Description：红云专题更多
     * @Change：
     * @Version：V1.0
     */
    public void getSubjectMore(String page,String limit,String id,String typeId,Observer<SpecialTopicMore> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getSubjectMore");
        params.put("ac", "getSubjectMore");
        params.put("page", page);
        params.put("limit", limit);
        params.put("id", id);
        params.put("typeid", typeId);
        setSubscribe(apiService.getSubjectMore(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/6 20:26
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id   分类id，可选，区分影片类型，暂时不传
     * @param keyword
     * @param observer
     * @Description：延云视听
     * @Change：
     * @Version：V1.0
     */
    public void getMedia(String uid,String page,String limit,String id,String keyword,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getMedia");
        params.put("ac", "getMedia");
        params.put("uid", uid);
        params.put("page", page);
        params.put("limit", limit);
        params.put("type", id);
        params.put("keyword", keyword);
        setSubscribe(apiService.getMedia(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/6 20:30
     * E_Mail：yh131412hys@163.com
     * @param id  视频id，必填
     * @param observer
     * @Description：延云视听详情
     * @Change：
     * @Version：V1.0
     */
    public void getVideo(String id,Observer<VideoInfo> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getVideo");
        params.put("ac", "getVideo");
        params.put("id", id);
        setSubscribe(apiService.getVideo(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/7 19:04
     * E_Mail：yh131412hys@163.com
     * @param page
     * @param limit
     * @param id 分类id，可选，默认：9994（历史淘）
     * @param keyword 搜索词
     * @param observer
     * @Description：漫绘延安
     * @Change：
     * @Version：V1.0
     */
    public void getPaint(String page,String limit,String id,String keyword,Observer<DiffuseYanAn> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getPaint");
        params.put("ac", "getPaint");
        params.put("page", page);
        params.put("limit", limit);
        params.put("id", id);
        params.put("keyword", keyword);
        setSubscribe(apiService.getPaint(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/12 15:01
     * E_Mail：yh131412hys@163.com
     * @param uid  必填 ,登录用户id
     * @param id  栏目id，用来生成pc的URL
     * @param model  分类
     *               延云知库(model标识：zhiku) 延云阅读(model标识：book) 延云视听(model标识：media)
     *               漫绘延安(model标识：painting) 红色文旅(model标识：travel) 去伪求真(model标识：really)
     *               红书包(model标识：redsbag) 红云专题(model标识：subject) 红云活动(model标识：news) 红云时政(model标识：news)
     * @param observer
     * @Description：收藏状态
     * @Change：
     * @Version：V1.0
     */
    public void getFavoriteState(String uid,String id,String model,Observer<FavoriteState> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getFavoriteState");
        params.put("ac", "getFavoriteState");
        params.put("uid", uid);
        params.put("id", id);
        params.put("model", model);
        setSubscribe(apiService.getFavoriteState(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/12 20:41
     * E_Mail：yh131412hys@163.com
     * @param uid 必填 ,登录用户id
     * @param title 必填，标题
     * @param id  必填，栏目id，用来生成pc的URL
     * @param model 分类
     * @param observer
     * @Description：添加收藏
     * @Change：
     * @Version：V1.0
     */
    public void addFavorite(String uid,String title,String id,String model,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=addFavorite");
        params.put("ac", "addFavorite");
        params.put("uid", uid);
        params.put("title", title);
        params.put("id", id);
        params.put("model", model);
        setSubscribe(apiService.addFavorite(params), observer);
    }

    /**
     * @Author：风不会停息 on 2019/8/13 20:13
     * E_Mail：yh131412hys@163.com
     * @param username
     * @param openid 第三方平台用户唯一标识
     * @param headImgUrl 用户头像
     * @param loginType 1:QQ;2:微博;3:微信
     * @param province 省
     * @param city 市
     * @param area 区
     * @param sex  0未知 1男 2女
     * @param observer
     * @Description：第三方登录
     * @Change：
     * @Version：V1.0
     */
    public void getThirdLogin(String username,String openid,String headImgUrl,String loginType,
                              String province,String city,String area,String sex,Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getThirdLogin");
        params.put("ac", "getThirdLogin");
        params.put("username", username);
        params.put("openid", openid);
        params.put("headimgurl", headImgUrl);
        params.put("third_login_type", loginType);
        params.put("province", province);
        params.put("city", city);
        params.put("area", area);
        params.put("sex", sex);
        setSubscribe(apiService.getThirdLogin(params), observer);
    }

    public void getAbout(Observer<ResponseBody> observer) {
        Map<String, String> params = getCommonRequestParamsStringMap("ac=getAbout");
        params.put("ac", "getAbout");
        setSubscribe(apiService.getAbout(params), observer);
    }
}
