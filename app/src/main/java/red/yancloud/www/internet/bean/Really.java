package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 21:09
 * E_Mail：yh131412hys@163.com
 * Describe：去伪求真
 * Change：
 * @Version：V1.0
 */
public class Really {

    /**
     * code : 0000
     * data : {"menus":[{"id":"9999","name":"史海钩沉"},{"id":"10001","name":"刨根问底"},{"id":"10002","name":"网文纠错"},{"id":"10000","name":"释疑解惑"}],"page":{"count":"2","totalPage":1,"page":"1","limit":"10","firstRow":0},"reallys":[{"id":"11857","title":"刘少奇作政治报告","subtitle":"（一九五六年九月十五日）","thumbnail":"/Uploads/newsThumbnail/2019-07-04/1562205289_1473232143.png","introduce":"在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。","content":"<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了合理地布置生产力，使工业企业接近自然资源，使工业和整个国民经济得到平衡的发展，我国在第一个五年计划期间已经把工业重点逐步移向内地，正在改变着解放前百分之七十以上的工业集中在沿海各省的畸形现象。但是这决不是说可以否认或者忽视沿海各省工业的作用。我们应当充分利用沿海各省的有利条件，继续适当地发展那里的工业，以帮助内地工业的发展，加速全国的工业化。在第一个五年计划期间，辽宁、上海、天津等工业区已经发挥了显著的作用。在第二个五年内，除开充分利用东北和华东的工业基地以外，还必须合理地发挥河北、山东地区和华南地区在发展工业上的作用。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在第二个五年内，为了配合大型企业的建设和生产，为了加速工业的发展，加强工业的协作，增加产品的品种，为了便于充分利用资源，充分利用原有企业，特别是大量的公私合营企业，必须在建设大型企业的同时，有计划地新建和改建中小型企业。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;必须注意把中央各经济部门的积极性和地方经济组织的积极性正确地结合起来。在过去，一方面，有些中央部门对于地方工业的发展和统一安排注意得不够，以致地方工业不能够合理地发挥潜在力量；另一方面，有些地方领导机关也曾经不顾全国生产设备是否有余，不顾当地的资源条件和其他经济条件，盲目地新建和扩建一些工业，因而也造成了国家的损失。这两种偏向都必须纠正。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了完成国家的生产计划，无论轻工业或者重工业，无论地方国营企业或者中央国营企业，都必须努力提高产品的质量。同样，为了完成国家的建设计划，工业、运输业以及其他一切部门的基本建设单位，都必须努力提高工程的质量。这是我国社会主义建设事业中最迫切的问题之一。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社会主义的优越性，不但要表现在我们的经济成就的数量和进度上面，还必须表现在它的质量上面。我们已经生产了许多质量优良的重工业产品和轻工业产品，建成了许多质量优良的工程。但是由于一部分企业的设备落后和技术水平不高，一部分企业缺乏产品标准和工艺规程，一部分企业没有建立严格的质量检查和技术监督的制度，特别是一部分企业的领导机关没有充分地重视保证产品和工程的质量，只是片面地重视保证数量和进度，所以有许多产品和工程的质量是不好的，某些产品不合规格，成为次货。商业方面统购包销制度所产生的副作用和执行这个制度所发生的缺点，以及在私营工商业改造过程中曾经一度发生的某些混乱现象，也降低了许多轻工业企业对于产品质量的责任心，甚至造成了许多产品质量下降的严重情况。这些情况已经给国家和人民带来损失，必须迅速地加以扭转。一切技术水平不高和设备落后的企业必须采取有效措施，争取在短期内熟练地掌握有关的技术，并且逐步地改善设备落后的状况。一切企业都要定出合理的产品标准和工艺规程。一切检查制度不严的厂矿和工地，必须迅速建立质量检查和技术监督的机构和制度。对于不合标准的产品和不合规格的工程应当定出适当的处理办法，应当积极地改善原料、材料的质量和原料、材料的供应工作。对于轻工业产品，应当严格地执行按质分等论价的政策，并且在一部分产品中逐步地推行选购制度。尤其重要的，必须在一切有关的工人和职员中进行关于保证质量、提高质量的思想教育，彻底地纠正那些对于质量不负责任的错误观点。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在发展生产的基础上逐步地改善职工生活，对于提高广大职工群众的积极性，具有重大的作用。为了改善职工生活，需要解决那些问题呢？首先，应当保证职工的工资收入在生产发展的基础上逐步增加，并且继续贯彻执行按劳取酬的原则，改进工资制度和奖励制度。第二，应当切实加强对于生产的安全措施和劳动保护工作。第三，应当努力保证和改善副食品的供应工作。第四，应当逐步增加职工的福利设施，积极设法解决职工急需的住宅和其他的困难。第五，应当保证职工有时间料理家庭的生活，得到应有的休息。<br/>职工群众有许多困难不是短时间内可以解决的，只有在社会主义建设得到更大的发展以后才能解决。我们还需要艰苦奋斗，不应当只管个人利益和目前利益，而忽视全国的和长远的利益。这一点必须向职工群众说明。但是，在另一方面，片面地强调全国的和长远的利益，而忽视职工的个人利益和目前利益，也是不对的。目前职工生活中的有些问题是必须解决而且是可能解决的，其所以没有解决，只是因为企业的领导者、工会组织和有关主管部门没有积极努力。我们必须坚决反对这种不关心群众痛痒的官僚主义态度。<br/>处理职工生活问题的上述原则，适用于一切企业的职工和国家机关的公务人员。<\/p>","addtime":"2019-07-02 08:50:34","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"9999","orign":"中国共产党新闻网","filetype":"","filesize":"0","fileaddress":"","price":"0","clicknumber":"26","downnumber":"0","collectionnumber":null,"tag":"","mp3file":null,"timelength":null,"supportnumber":"0"},{"id":"11856","title":"史海钩沉第二章","subtitle":"地址测试2","thumbnail":"/Uploads/newsThumbnail/2019-07-02/1562037442_1040285515.png","introduce":"简介测试2","content":"<p>详情测试2<\/p>","addtime":"2019-07-02 08:48:59","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"9999","orign":"来源测试2","filetype":"","filesize":"0","fileaddress":"","price":"0","clicknumber":"5","downnumber":"0","collectionnumber":null,"tag":"","mp3file":null,"timelength":null,"supportnumber":"0"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * menus : [{"id":"9999","name":"史海钩沉"},{"id":"10001","name":"刨根问底"},{"id":"10002","name":"网文纠错"},{"id":"10000","name":"释疑解惑"}]
         * page : {"count":"2","totalPage":1,"page":"1","limit":"10","firstRow":0}
         * reallys : [{"id":"11857","title":"刘少奇作政治报告","subtitle":"（一九五六年九月十五日）","thumbnail":"/Uploads/newsThumbnail/2019-07-04/1562205289_1473232143.png","introduce":"在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。","content":"<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了合理地布置生产力，使工业企业接近自然资源，使工业和整个国民经济得到平衡的发展，我国在第一个五年计划期间已经把工业重点逐步移向内地，正在改变着解放前百分之七十以上的工业集中在沿海各省的畸形现象。但是这决不是说可以否认或者忽视沿海各省工业的作用。我们应当充分利用沿海各省的有利条件，继续适当地发展那里的工业，以帮助内地工业的发展，加速全国的工业化。在第一个五年计划期间，辽宁、上海、天津等工业区已经发挥了显著的作用。在第二个五年内，除开充分利用东北和华东的工业基地以外，还必须合理地发挥河北、山东地区和华南地区在发展工业上的作用。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在第二个五年内，为了配合大型企业的建设和生产，为了加速工业的发展，加强工业的协作，增加产品的品种，为了便于充分利用资源，充分利用原有企业，特别是大量的公私合营企业，必须在建设大型企业的同时，有计划地新建和改建中小型企业。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;必须注意把中央各经济部门的积极性和地方经济组织的积极性正确地结合起来。在过去，一方面，有些中央部门对于地方工业的发展和统一安排注意得不够，以致地方工业不能够合理地发挥潜在力量；另一方面，有些地方领导机关也曾经不顾全国生产设备是否有余，不顾当地的资源条件和其他经济条件，盲目地新建和扩建一些工业，因而也造成了国家的损失。这两种偏向都必须纠正。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了完成国家的生产计划，无论轻工业或者重工业，无论地方国营企业或者中央国营企业，都必须努力提高产品的质量。同样，为了完成国家的建设计划，工业、运输业以及其他一切部门的基本建设单位，都必须努力提高工程的质量。这是我国社会主义建设事业中最迫切的问题之一。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社会主义的优越性，不但要表现在我们的经济成就的数量和进度上面，还必须表现在它的质量上面。我们已经生产了许多质量优良的重工业产品和轻工业产品，建成了许多质量优良的工程。但是由于一部分企业的设备落后和技术水平不高，一部分企业缺乏产品标准和工艺规程，一部分企业没有建立严格的质量检查和技术监督的制度，特别是一部分企业的领导机关没有充分地重视保证产品和工程的质量，只是片面地重视保证数量和进度，所以有许多产品和工程的质量是不好的，某些产品不合规格，成为次货。商业方面统购包销制度所产生的副作用和执行这个制度所发生的缺点，以及在私营工商业改造过程中曾经一度发生的某些混乱现象，也降低了许多轻工业企业对于产品质量的责任心，甚至造成了许多产品质量下降的严重情况。这些情况已经给国家和人民带来损失，必须迅速地加以扭转。一切技术水平不高和设备落后的企业必须采取有效措施，争取在短期内熟练地掌握有关的技术，并且逐步地改善设备落后的状况。一切企业都要定出合理的产品标准和工艺规程。一切检查制度不严的厂矿和工地，必须迅速建立质量检查和技术监督的机构和制度。对于不合标准的产品和不合规格的工程应当定出适当的处理办法，应当积极地改善原料、材料的质量和原料、材料的供应工作。对于轻工业产品，应当严格地执行按质分等论价的政策，并且在一部分产品中逐步地推行选购制度。尤其重要的，必须在一切有关的工人和职员中进行关于保证质量、提高质量的思想教育，彻底地纠正那些对于质量不负责任的错误观点。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在发展生产的基础上逐步地改善职工生活，对于提高广大职工群众的积极性，具有重大的作用。为了改善职工生活，需要解决那些问题呢？首先，应当保证职工的工资收入在生产发展的基础上逐步增加，并且继续贯彻执行按劳取酬的原则，改进工资制度和奖励制度。第二，应当切实加强对于生产的安全措施和劳动保护工作。第三，应当努力保证和改善副食品的供应工作。第四，应当逐步增加职工的福利设施，积极设法解决职工急需的住宅和其他的困难。第五，应当保证职工有时间料理家庭的生活，得到应有的休息。<br/>职工群众有许多困难不是短时间内可以解决的，只有在社会主义建设得到更大的发展以后才能解决。我们还需要艰苦奋斗，不应当只管个人利益和目前利益，而忽视全国的和长远的利益。这一点必须向职工群众说明。但是，在另一方面，片面地强调全国的和长远的利益，而忽视职工的个人利益和目前利益，也是不对的。目前职工生活中的有些问题是必须解决而且是可能解决的，其所以没有解决，只是因为企业的领导者、工会组织和有关主管部门没有积极努力。我们必须坚决反对这种不关心群众痛痒的官僚主义态度。<br/>处理职工生活问题的上述原则，适用于一切企业的职工和国家机关的公务人员。<\/p>","addtime":"2019-07-02 08:50:34","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"9999","orign":"中国共产党新闻网","filetype":"","filesize":"0","fileaddress":"","price":"0","clicknumber":"26","downnumber":"0","collectionnumber":null,"tag":"","mp3file":null,"timelength":null,"supportnumber":"0"},{"id":"11856","title":"史海钩沉第二章","subtitle":"地址测试2","thumbnail":"/Uploads/newsThumbnail/2019-07-02/1562037442_1040285515.png","introduce":"简介测试2","content":"<p>详情测试2<\/p>","addtime":"2019-07-02 08:48:59","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"9999","orign":"来源测试2","filetype":"","filesize":"0","fileaddress":"","price":"0","clicknumber":"5","downnumber":"0","collectionnumber":null,"tag":"","mp3file":null,"timelength":null,"supportnumber":"0"}]
         */

        private PageBean page;
        private List<MenusBean> menus;
        private List<ReallysBean> reallys;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<MenusBean> getMenus() {
            return menus;
        }

        public void setMenus(List<MenusBean> menus) {
            this.menus = menus;
        }

        public List<ReallysBean> getReallys() {
            return reallys;
        }

        public void setReallys(List<ReallysBean> reallys) {
            this.reallys = reallys;
        }

        public static class PageBean {
            /**
             * count : 2
             * totalPage : 1
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class MenusBean {
            /**
             * id : 9999
             * title : 史海钩沉
             */

            private String id;
            private String title;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String name) {
                this.title = name;
            }

            @Override
            public String toString() {
                return "MenusBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        '}';
            }
        }

        public static class ReallysBean {
            /**
             * id : 11857
             * title : 刘少奇作政治报告
             * subtitle : （一九五六年九月十五日）
             * thumbnail : /Uploads/newsThumbnail/2019-07-04/1562205289_1473232143.png
             * introduce : 在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。
             * content : <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在工业的布局问题上，目前需要注意的是沿海和内地的配合，大型企业和中小型企业的配合，中央国营企业和地方国营企业的配合。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了合理地布置生产力，使工业企业接近自然资源，使工业和整个国民经济得到平衡的发展，我国在第一个五年计划期间已经把工业重点逐步移向内地，正在改变着解放前百分之七十以上的工业集中在沿海各省的畸形现象。但是这决不是说可以否认或者忽视沿海各省工业的作用。我们应当充分利用沿海各省的有利条件，继续适当地发展那里的工业，以帮助内地工业的发展，加速全国的工业化。在第一个五年计划期间，辽宁、上海、天津等工业区已经发挥了显著的作用。在第二个五年内，除开充分利用东北和华东的工业基地以外，还必须合理地发挥河北、山东地区和华南地区在发展工业上的作用。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在第二个五年内，为了配合大型企业的建设和生产，为了加速工业的发展，加强工业的协作，增加产品的品种，为了便于充分利用资源，充分利用原有企业，特别是大量的公私合营企业，必须在建设大型企业的同时，有计划地新建和改建中小型企业。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;必须注意把中央各经济部门的积极性和地方经济组织的积极性正确地结合起来。在过去，一方面，有些中央部门对于地方工业的发展和统一安排注意得不够，以致地方工业不能够合理地发挥潜在力量；另一方面，有些地方领导机关也曾经不顾全国生产设备是否有余，不顾当地的资源条件和其他经济条件，盲目地新建和扩建一些工业，因而也造成了国家的损失。这两种偏向都必须纠正。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;为了完成国家的生产计划，无论轻工业或者重工业，无论地方国营企业或者中央国营企业，都必须努力提高产品的质量。同样，为了完成国家的建设计划，工业、运输业以及其他一切部门的基本建设单位，都必须努力提高工程的质量。这是我国社会主义建设事业中最迫切的问题之一。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;社会主义的优越性，不但要表现在我们的经济成就的数量和进度上面，还必须表现在它的质量上面。我们已经生产了许多质量优良的重工业产品和轻工业产品，建成了许多质量优良的工程。但是由于一部分企业的设备落后和技术水平不高，一部分企业缺乏产品标准和工艺规程，一部分企业没有建立严格的质量检查和技术监督的制度，特别是一部分企业的领导机关没有充分地重视保证产品和工程的质量，只是片面地重视保证数量和进度，所以有许多产品和工程的质量是不好的，某些产品不合规格，成为次货。商业方面统购包销制度所产生的副作用和执行这个制度所发生的缺点，以及在私营工商业改造过程中曾经一度发生的某些混乱现象，也降低了许多轻工业企业对于产品质量的责任心，甚至造成了许多产品质量下降的严重情况。这些情况已经给国家和人民带来损失，必须迅速地加以扭转。一切技术水平不高和设备落后的企业必须采取有效措施，争取在短期内熟练地掌握有关的技术，并且逐步地改善设备落后的状况。一切企业都要定出合理的产品标准和工艺规程。一切检查制度不严的厂矿和工地，必须迅速建立质量检查和技术监督的机构和制度。对于不合标准的产品和不合规格的工程应当定出适当的处理办法，应当积极地改善原料、材料的质量和原料、材料的供应工作。对于轻工业产品，应当严格地执行按质分等论价的政策，并且在一部分产品中逐步地推行选购制度。尤其重要的，必须在一切有关的工人和职员中进行关于保证质量、提高质量的思想教育，彻底地纠正那些对于质量不负责任的错误观点。<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在发展生产的基础上逐步地改善职工生活，对于提高广大职工群众的积极性，具有重大的作用。为了改善职工生活，需要解决那些问题呢？首先，应当保证职工的工资收入在生产发展的基础上逐步增加，并且继续贯彻执行按劳取酬的原则，改进工资制度和奖励制度。第二，应当切实加强对于生产的安全措施和劳动保护工作。第三，应当努力保证和改善副食品的供应工作。第四，应当逐步增加职工的福利设施，积极设法解决职工急需的住宅和其他的困难。第五，应当保证职工有时间料理家庭的生活，得到应有的休息。<br/>职工群众有许多困难不是短时间内可以解决的，只有在社会主义建设得到更大的发展以后才能解决。我们还需要艰苦奋斗，不应当只管个人利益和目前利益，而忽视全国的和长远的利益。这一点必须向职工群众说明。但是，在另一方面，片面地强调全国的和长远的利益，而忽视职工的个人利益和目前利益，也是不对的。目前职工生活中的有些问题是必须解决而且是可能解决的，其所以没有解决，只是因为企业的领导者、工会组织和有关主管部门没有积极努力。我们必须坚决反对这种不关心群众痛痒的官僚主义态度。<br/>处理职工生活问题的上述原则，适用于一切企业的职工和国家机关的公务人员。</p>
             * addtime : 2019-07-02 08:50:34
             * creater : admin
             * status : 2
             * deleteflg : 1
             * settop : 1
             * type : 9999
             * orign : 中国共产党新闻网
             * filetype :
             * filesize : 0
             * fileaddress :
             * price : 0
             * clicknumber : 26
             * downnumber : 0
             * collectionnumber : null
             * tag :
             * mp3file : null
             * timelength : null
             * supportnumber : 0
             */

            private String id;
            private String title;
            private String subtitle;
            private String thumbnail;
            private String introduce;
            private String content;
            private String addtime;
            private String creater;
            private String status;
            private String deleteflg;
            private String settop;
            private String type;
            private String orign;
            private String filetype;
            private String filesize;
            private String fileaddress;
            private String price;
            private String clicknumber;
            private String downnumber;
            private Object collectionnumber;
            private String tag;
            private Object mp3file;
            private Object timelength;
            private String supportnumber;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getOrign() {
                return orign;
            }

            public void setOrign(String orign) {
                this.orign = orign;
            }

            public String getFiletype() {
                return filetype;
            }

            public void setFiletype(String filetype) {
                this.filetype = filetype;
            }

            public String getFilesize() {
                return filesize;
            }

            public void setFilesize(String filesize) {
                this.filesize = filesize;
            }

            public String getFileaddress() {
                return fileaddress;
            }

            public void setFileaddress(String fileaddress) {
                this.fileaddress = fileaddress;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getClicknumber() {
                return clicknumber;
            }

            public void setClicknumber(String clicknumber) {
                this.clicknumber = clicknumber;
            }

            public String getDownnumber() {
                return downnumber;
            }

            public void setDownnumber(String downnumber) {
                this.downnumber = downnumber;
            }

            public Object getCollectionnumber() {
                return collectionnumber;
            }

            public void setCollectionnumber(Object collectionnumber) {
                this.collectionnumber = collectionnumber;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public Object getMp3file() {
                return mp3file;
            }

            public void setMp3file(Object mp3file) {
                this.mp3file = mp3file;
            }

            public Object getTimelength() {
                return timelength;
            }

            public void setTimelength(Object timelength) {
                this.timelength = timelength;
            }

            public String getSupportnumber() {
                return supportnumber;
            }

            public void setSupportnumber(String supportnumber) {
                this.supportnumber = supportnumber;
            }

            @Override
            public String toString() {
                return "ReallysBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", subtitle='" + subtitle + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", introduce='" + introduce + '\'' +
                        ", content='" + content + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", creater='" + creater + '\'' +
                        ", status='" + status + '\'' +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", settop='" + settop + '\'' +
                        ", type='" + type + '\'' +
                        ", orign='" + orign + '\'' +
                        ", filetype='" + filetype + '\'' +
                        ", filesize='" + filesize + '\'' +
                        ", fileaddress='" + fileaddress + '\'' +
                        ", price='" + price + '\'' +
                        ", clicknumber='" + clicknumber + '\'' +
                        ", downnumber='" + downnumber + '\'' +
                        ", collectionnumber=" + collectionnumber +
                        ", tag='" + tag + '\'' +
                        ", mp3file=" + mp3file +
                        ", timelength=" + timelength +
                        ", supportnumber='" + supportnumber + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", menus=" + menus +
                    ", reallys=" + reallys +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Really{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
