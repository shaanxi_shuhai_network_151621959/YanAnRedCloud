package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/30 9:35
 * E_Mail：yh131412hys@163.com
 * Describe：页面数据分类
 * Change：
 * @Version：V1.0
 */
public class TypeData {

    private String type;
    private String typeId;
    private List<RedTravel.DataBean.MenusBean.ChildrensBean> childrenBeanList;

    public TypeData() {
    }

    public TypeData(String type, String typeId) {
        this.type = type;
        this.typeId = typeId;
    }

    public TypeData(String type, String typeId, List<RedTravel.DataBean.MenusBean.ChildrensBean> childrenBeanList) {
        this.type = type;
        this.typeId = typeId;
        this.childrenBeanList = childrenBeanList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public List<RedTravel.DataBean.MenusBean.ChildrensBean> getChildrenBeanList() {
        return childrenBeanList;
    }

    public void setChildrenBeanList(List<RedTravel.DataBean.MenusBean.ChildrensBean> childrenBeanList) {
        this.childrenBeanList = childrenBeanList;
    }

    @Override
    public String toString() {
        return "TypeData{" +
                "type='" + type + '\'' +
                ", typeId='" + typeId + '\'' +
                ", childrenBeanList=" + childrenBeanList +
                '}';
    }
}
