package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @author：风不会停息 on 2019/8/6 20:59
 * mailbox：yh131412hys@163.com
 * project：YanAnRedCloud
 * describe：延云视听
 * change：
 * @Version：V1.0
 */
public class VideoList {

    /**
     * code : 0000
     * data : {"page":{"count":"309","totalPage":31,"page":"1","limit":"10","firstRow":0},"video":[{"id":"661","title":"通往延安之旅","thumbnail":"/Uploads/video/15560021779241.jpg","introduce":"","content":"<p><video class=\"edui-upload-video  vjs-default-skin     video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" data-setup=\"{}\"><source src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" type=\"video/mp4\"/><\/video><\/p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。<\/p>","addtime":"2019-04-23 14:49:37","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2404","score":"2","fileaddress":"","clicknumber":"221","downnumber":"0","collectionnumber":null,"tag":"纪录片","redian":"","niandai":"0","guige":null,"jundui":"","renwu":"","daoyan":"瓦尔特·博斯哈德","daoyancon":"瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。","chupintime":"1938年","company":"瑞士理工学院","bianju":"","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":"无声","timelength":"","guojia":"","zhanzheng":"","leixing":"","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":{"id":"329","userid":"223","time":"19-08-14 20:03:51","title":"通往延安之旅","url":"http://www.yancloud.red/Portal/Media/info/id/661.html","typename":"纪录片","typeid":"2404","partypeid":"2400","model":"media"}},{"id":"659","title":"智取威虎山","thumbnail":"/Uploads/video/15444082372914.jpg","introduce":"李仲林（饰杨子荣)、纪玉良(饰少剑波)、王正屏(饰李勇奇)、贺水华（饰坐山雕)等","content":"京剧《智取威虎山》讲述的是杨子荣英勇消灭土匪座山雕的故事。","addtime":"2018-12-10 10:17:17","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"86","fileaddress":"https://www.iqiyi.com/v_19rr7kx900.html","clicknumber":"110","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":"陶雄、李桐森、黄正勤、曹寿春、申阳生(执笔)","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"歌舞、 战争","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"660","title":"海港","thumbnail":"/Uploads/video/15444082378490.jpg","introduce":"李丽芳、赵文奎、朱文虎","content":"新中国建立起来后，作为一个发展中的国家，我们和第三世界的兄弟手拉手，心连心，这正是伟大领袖所说的国际主义的义务。紧张忙碌的海港码头，一批又一批稻种和小麦闯入船舱，即将作为援建物资跨过重洋，送到兄弟国家手中。装卸工人们干劲十足，争分夺秒，热火朝天，充满了社会主义当家做主人的自豪感。然而在他们中间，青年工人韩小强却对自己的工作不以为然。他百无聊赖，虚以委蛇，于是工作中难免...","addtime":"2018-12-10 10:17:17","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"89","fileaddress":"https://www.iqiyi.com/v_19rr7nnr3o.html","clicknumber":"96","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"653","title":"红灯记","thumbnail":"/Uploads/video/15444079318325.jpg","introduce":"刘长瑜","content":null,"addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"89","fileaddress":"https://www.56.com/u49/v_MTQ0NjA1MTU4.html","clicknumber":"75","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"成荫","daoyancon":"成荫（1917年1月21日－1984年4月26日），原名成蕴保，出生于山东省曹县市，中国内地导演、编剧，毕业于鲁迅艺术学院戏剧系。","chupintime":null,"company":"八一电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"654","title":"奇袭白虎团","thumbnail":"/Uploads/video/1544407931421.jpg","introduce":"宋玉庆、方荣翔、谢同喜、邢玉民、陈玉申、沈健瑾、栗敏","content":"抗美援朝战争期间，志愿军某部侦察排长严伟才（宋玉庆 饰）和安平村的崔大娘（栗敏 饰）一家结下深厚友谊。但不久后，崔大娘和部分村民被抓去给美李匪帮修工事，敌人得知崔大娘的儿子参加了人民军，便将她惨忍杀害了。严伟才闻听，怒火中烧，发誓为大娘报仇。","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"79","fileaddress":"http://tv.cntv.cn/video/C36165/46b532f672aa4dfaa56d5761a7e5c078","clicknumber":"77","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"苏里","daoyancon":"苏里（1919年\u20142005年5月2日），原名夏传尧，出生于安徽省当涂市，中国内地导演、编剧。","chupintime":"1972","company":"长春电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"战争、戏曲","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"655","title":"杜鹃山","thumbnail":"/Uploads/video/15444079317465.jpg","introduce":"杨春霞","content":"《杜鹃山》讲述的是党从井冈山派柯湘到湘赣边界的杜鹃山领导一支农民自卫军。途中柯湘被捕，恰巧被自卫军营救。自此，柯湘担任了自卫军的党代表。她宣传党的阶级政策，团结群众，不断扩大武装。地主武装头子毒蛇胆勾结自卫军中的叛徒温其久，抓住雷刚的义母，诱雷刚下山，妄图一举消灭自卫军。柯湘识破敌人毒计，主张用敌进我退的办法，会合主力，粉碎敌人的进犯。雷刚不听劝阻，莽撞下山救母，结果中计被捕。柯湘率尖刀班救出雷刚母子，清除了叛徒。改造了这支自发的农民自卫军。","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"90","fileaddress":"https://www.iqiyi.com/v_19rr7rawkw.html","clicknumber":"65","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":"秋收起义","leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"656","title":"红色娘子军","thumbnail":"/Uploads/video/15444079317394.jpg","introduce":"冯志效、 杜近芳、曲素英","content":null,"addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"88","fileaddress":"https://v.qq.com/x/page/n0527c9cmx6.html","clicknumber":"65","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":"1972","company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"剧情、戏曲","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"657","title":"白毛女","thumbnail":"/Uploads/video/15444079315268.jpg","introduce":"陈强、田华、胡朋、李百万、张守维、李壬林","content":"在华北农村，佃户杨白老(张守维 饰)的女儿喜儿（田华 饰）和青年王大春（李白万 饰）正在相爱。地主黄世仁（陈强 饰）看中了喜儿，设下圈套，逼杨白劳把欠他家的六斗谷子年前连本带利还清。杨白老拿一冬的血汗钱七块五毛到黄家还利，被逼本利全清，否则用喜儿抵债。并硬逼他在女儿的卖身契上按了手印，除夕夜杨白老含恨喝下卤水自杀了，初一悲痛中的喜儿被抢进了黄家，不久被黄世仁奸污了，大春配合张二婶（管林仆 饰）搭救喜儿被发现，逃跑投奔了红军。后来二婶再次帮喜儿逃出了黄家，藏进了深山丛林里，喜儿靠捡奶奶庙的贡品和吃野果活命。非人的生活使喜儿的头发慢慢成满头白发。村子里有人看见，传说成收贡品的白毛仙姑。之后大春随部队回到家乡，展开减租减息运动，黄世仁利用白毛仙姑的传说企图动摇民心，为粉碎地主阴谋，大春带领乡亲夜间潜伏在奶奶庙，寻找白毛仙姑......","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"87","fileaddress":"https://www.iqiyi.com/v_19rrn6ryvs.html","clicknumber":"60","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"王滨、水华 / 王滨 / 杨润身 / 贺敬之 / 丁毅水华","daoyancon":"王滨曾先后执导过《日出》《佃户》《蠢货》《海燕》等著名话剧、电影作品，电影版《白毛女》是他与水华联合执导的经典作品，新中国第一部故事长片《桥》也出自王滨之手。2012年是王滨诞辰100周年，中国电影博物馆举办了纪念王滨诞辰100周年的活动。在活动现场，王滨的女儿王丁菲向中国电影博物馆捐赠了父亲生前的部分手稿和原版照片等藏品，于蓝、田华等老艺术家追忆了与王滨合作的过往岁月，王滨的代表作《白毛女》也在现场举行放映。","chupintime":"1951","company":"长春电影制片厂","bianju":"水华、王滨、杨润身、贺敬之、丁毅","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"658","title":"沙家浜","thumbnail":"/Uploads/video/15444079316175.jpg","introduce":" 谭元寿、洪雪飞、马长礼、万一英、贺永瑛","content":"抗日战争期间，江南新四军的将士与日寇展开激烈的较量。光荣负伤的指导员郭建光（谭元寿 饰）和18名新四军战士被迫在沙家浜养伤，耐心等待再次奔赴沙场的那一刻。阿庆嫂（洪雪飞 饰）和沙奶奶（万一英 饰）等进步群众积极接纳、照顾伤员，战士们则各尽所能为乡亲分担农活，军民鱼水情，可歌可泣，难分难舍。未过多久，鬼子开始扫荡，阿庆嫂引领同志们躲进芦荡隐蔽。扑了一个空的日寇气急败坏，指示假意革命暗通日寇的忠义救国军司令胡传魁（周和桐 饰）围剿新四军伤员。在参谋长刁德一（马长礼 饰）和日本翻译官的怂恿下，觊觎\u201c鱼米之乡\u201d的胡传魁开赴沙家浜\u2026\u2026","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"91","fileaddress":"https://v.youku.com/v_show/id_XMTM0NjY3MDgw.html","clicknumber":"84","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"武兆堤","daoyancon":"武兆堤（1920年11月18日\u20141992年9月3日），祖籍山西省襄汾市，出生于美国匹兹堡市，中国内地导演、编剧，毕业于北京电影学院。","chupintime":"1971","company":"长春电影制片厂","bianju":"北京京剧团集体改编","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"596","title":"保卫黄河","thumbnail":"/Uploads/video/15439052955269.jpg","introduce":"","content":"<p>《保卫黄河》是《黄河大合唱》的第七乐章，写成于抗日战争时期。歌曲采用齐唱、轮唱的演唱形式。具有广泛的群众性，是抗日军民广为传播的一首歌曲。<\/p>","addtime":"2018-12-04 14:34:55","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2408","score":"80","fileaddress":"/Uploads/video/15439052955269.mp4","clicknumber":"81","downnumber":"0","collectionnumber":null,"tag":"《黄河大合唱》第七乐章","redian":"红云经典","niandai":"1969","guige":null,"jundui":"红军","renwu":"","daoyan":"合唱","daoyancon":"无","chupintime":"1969年","company":null,"bianju":null,"zuoci":"光未然","zuoqu":"冼星海","mp3file":"/Uploads/video/15439052955269.mp3","geci":"但是\n中华民族的儿女啊\n谁愿意像猪羊一般\n任人宰割\n我们要抱定必死的决心\n保卫黄河\n保卫华北\n保卫全中国\n《保卫黄河》谱\n《保卫黄河》谱\n　风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国\n风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国\n风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北\n高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国","yuyan":null,"timelength":"2分42秒","guojia":"中国","zhanzheng":"其他","leixing":"其他","gequfenlei":"","file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"309","totalPage":31,"page":"1","limit":"10","firstRow":0}
         * video : [{"id":"661","title":"通往延安之旅","thumbnail":"/Uploads/video/15560021779241.jpg","introduce":"","content":"<p><video class=\"edui-upload-video  vjs-default-skin     video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" data-setup=\"{}\"><source src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" type=\"video/mp4\"/><\/video><\/p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。<\/p>","addtime":"2019-04-23 14:49:37","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2404","score":"2","fileaddress":"","clicknumber":"221","downnumber":"0","collectionnumber":null,"tag":"纪录片","redian":"","niandai":"0","guige":null,"jundui":"","renwu":"","daoyan":"瓦尔特·博斯哈德","daoyancon":"瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。","chupintime":"1938年","company":"瑞士理工学院","bianju":"","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":"无声","timelength":"","guojia":"","zhanzheng":"","leixing":"","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":{"id":"329","userid":"223","time":"19-08-14 20:03:51","title":"通往延安之旅","url":"http://www.yancloud.red/Portal/Media/info/id/661.html","typename":"纪录片","typeid":"2404","partypeid":"2400","model":"media"}},{"id":"659","title":"智取威虎山","thumbnail":"/Uploads/video/15444082372914.jpg","introduce":"李仲林（饰杨子荣)、纪玉良(饰少剑波)、王正屏(饰李勇奇)、贺水华（饰坐山雕)等","content":"京剧《智取威虎山》讲述的是杨子荣英勇消灭土匪座山雕的故事。","addtime":"2018-12-10 10:17:17","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"86","fileaddress":"https://www.iqiyi.com/v_19rr7kx900.html","clicknumber":"110","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":"陶雄、李桐森、黄正勤、曹寿春、申阳生(执笔)","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"歌舞、 战争","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"660","title":"海港","thumbnail":"/Uploads/video/15444082378490.jpg","introduce":"李丽芳、赵文奎、朱文虎","content":"新中国建立起来后，作为一个发展中的国家，我们和第三世界的兄弟手拉手，心连心，这正是伟大领袖所说的国际主义的义务。紧张忙碌的海港码头，一批又一批稻种和小麦闯入船舱，即将作为援建物资跨过重洋，送到兄弟国家手中。装卸工人们干劲十足，争分夺秒，热火朝天，充满了社会主义当家做主人的自豪感。然而在他们中间，青年工人韩小强却对自己的工作不以为然。他百无聊赖，虚以委蛇，于是工作中难免...","addtime":"2018-12-10 10:17:17","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"89","fileaddress":"https://www.iqiyi.com/v_19rr7nnr3o.html","clicknumber":"96","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"653","title":"红灯记","thumbnail":"/Uploads/video/15444079318325.jpg","introduce":"刘长瑜","content":null,"addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"89","fileaddress":"https://www.56.com/u49/v_MTQ0NjA1MTU4.html","clicknumber":"75","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"成荫","daoyancon":"成荫（1917年1月21日－1984年4月26日），原名成蕴保，出生于山东省曹县市，中国内地导演、编剧，毕业于鲁迅艺术学院戏剧系。","chupintime":null,"company":"八一电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"654","title":"奇袭白虎团","thumbnail":"/Uploads/video/1544407931421.jpg","introduce":"宋玉庆、方荣翔、谢同喜、邢玉民、陈玉申、沈健瑾、栗敏","content":"抗美援朝战争期间，志愿军某部侦察排长严伟才（宋玉庆 饰）和安平村的崔大娘（栗敏 饰）一家结下深厚友谊。但不久后，崔大娘和部分村民被抓去给美李匪帮修工事，敌人得知崔大娘的儿子参加了人民军，便将她惨忍杀害了。严伟才闻听，怒火中烧，发誓为大娘报仇。","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"79","fileaddress":"http://tv.cntv.cn/video/C36165/46b532f672aa4dfaa56d5761a7e5c078","clicknumber":"77","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"苏里","daoyancon":"苏里（1919年\u20142005年5月2日），原名夏传尧，出生于安徽省当涂市，中国内地导演、编剧。","chupintime":"1972","company":"长春电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"战争、戏曲","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"655","title":"杜鹃山","thumbnail":"/Uploads/video/15444079317465.jpg","introduce":"杨春霞","content":"《杜鹃山》讲述的是党从井冈山派柯湘到湘赣边界的杜鹃山领导一支农民自卫军。途中柯湘被捕，恰巧被自卫军营救。自此，柯湘担任了自卫军的党代表。她宣传党的阶级政策，团结群众，不断扩大武装。地主武装头子毒蛇胆勾结自卫军中的叛徒温其久，抓住雷刚的义母，诱雷刚下山，妄图一举消灭自卫军。柯湘识破敌人毒计，主张用敌进我退的办法，会合主力，粉碎敌人的进犯。雷刚不听劝阻，莽撞下山救母，结果中计被捕。柯湘率尖刀班救出雷刚母子，清除了叛徒。改造了这支自发的农民自卫军。","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"90","fileaddress":"https://www.iqiyi.com/v_19rr7rawkw.html","clicknumber":"65","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":null,"company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":"秋收起义","leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"656","title":"红色娘子军","thumbnail":"/Uploads/video/15444079317394.jpg","introduce":"冯志效、 杜近芳、曲素英","content":null,"addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"88","fileaddress":"https://v.qq.com/x/page/n0527c9cmx6.html","clicknumber":"65","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"谢铁骊","daoyancon":"谢铁骊（1925年12月27日-2015年6月19日），出生于江苏省淮阴市，中国内地导演、编剧，毕业于淮海军政干部学校。","chupintime":"1972","company":"北京电影制片厂","bianju":null,"zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":"剧情、戏曲","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"657","title":"白毛女","thumbnail":"/Uploads/video/15444079315268.jpg","introduce":"陈强、田华、胡朋、李百万、张守维、李壬林","content":"在华北农村，佃户杨白老(张守维 饰)的女儿喜儿（田华 饰）和青年王大春（李白万 饰）正在相爱。地主黄世仁（陈强 饰）看中了喜儿，设下圈套，逼杨白劳把欠他家的六斗谷子年前连本带利还清。杨白老拿一冬的血汗钱七块五毛到黄家还利，被逼本利全清，否则用喜儿抵债。并硬逼他在女儿的卖身契上按了手印，除夕夜杨白老含恨喝下卤水自杀了，初一悲痛中的喜儿被抢进了黄家，不久被黄世仁奸污了，大春配合张二婶（管林仆 饰）搭救喜儿被发现，逃跑投奔了红军。后来二婶再次帮喜儿逃出了黄家，藏进了深山丛林里，喜儿靠捡奶奶庙的贡品和吃野果活命。非人的生活使喜儿的头发慢慢成满头白发。村子里有人看见，传说成收贡品的白毛仙姑。之后大春随部队回到家乡，展开减租减息运动，黄世仁利用白毛仙姑的传说企图动摇民心，为粉碎地主阴谋，大春带领乡亲夜间潜伏在奶奶庙，寻找白毛仙姑......","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"87","fileaddress":"https://www.iqiyi.com/v_19rrn6ryvs.html","clicknumber":"60","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"王滨、水华 / 王滨 / 杨润身 / 贺敬之 / 丁毅水华","daoyancon":"王滨曾先后执导过《日出》《佃户》《蠢货》《海燕》等著名话剧、电影作品，电影版《白毛女》是他与水华联合执导的经典作品，新中国第一部故事长片《桥》也出自王滨之手。2012年是王滨诞辰100周年，中国电影博物馆举办了纪念王滨诞辰100周年的活动。在活动现场，王滨的女儿王丁菲向中国电影博物馆捐赠了父亲生前的部分手稿和原版照片等藏品，于蓝、田华等老艺术家追忆了与王滨合作的过往岁月，王滨的代表作《白毛女》也在现场举行放映。","chupintime":"1951","company":"长春电影制片厂","bianju":"水华、王滨、杨润身、贺敬之、丁毅","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"658","title":"沙家浜","thumbnail":"/Uploads/video/15444079316175.jpg","introduce":" 谭元寿、洪雪飞、马长礼、万一英、贺永瑛","content":"抗日战争期间，江南新四军的将士与日寇展开激烈的较量。光荣负伤的指导员郭建光（谭元寿 饰）和18名新四军战士被迫在沙家浜养伤，耐心等待再次奔赴沙场的那一刻。阿庆嫂（洪雪飞 饰）和沙奶奶（万一英 饰）等进步群众积极接纳、照顾伤员，战士们则各尽所能为乡亲分担农活，军民鱼水情，可歌可泣，难分难舍。未过多久，鬼子开始扫荡，阿庆嫂引领同志们躲进芦荡隐蔽。扑了一个空的日寇气急败坏，指示假意革命暗通日寇的忠义救国军司令胡传魁（周和桐 饰）围剿新四军伤员。在参谋长刁德一（马长礼 饰）和日本翻译官的怂恿下，觊觎\u201c鱼米之乡\u201d的胡传魁开赴沙家浜\u2026\u2026","addtime":"2018-12-10 10:12:11","creater":"testlys","status":"2","deleteflg":"1","settop":"1","type":"2405","score":"91","fileaddress":"https://v.youku.com/v_show/id_XMTM0NjY3MDgw.html","clicknumber":"84","downnumber":"0","collectionnumber":null,"tag":null,"redian":null,"niandai":null,"guige":null,"jundui":null,"renwu":null,"daoyan":"武兆堤","daoyancon":"武兆堤（1920年11月18日\u20141992年9月3日），祖籍山西省襄汾市，出生于美国匹兹堡市，中国内地导演、编剧，毕业于北京电影学院。","chupintime":"1971","company":"长春电影制片厂","bianju":"北京京剧团集体改编","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":null,"timelength":null,"guojia":"中国","zhanzheng":null,"leixing":null,"gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""},{"id":"596","title":"保卫黄河","thumbnail":"/Uploads/video/15439052955269.jpg","introduce":"","content":"<p>《保卫黄河》是《黄河大合唱》的第七乐章，写成于抗日战争时期。歌曲采用齐唱、轮唱的演唱形式。具有广泛的群众性，是抗日军民广为传播的一首歌曲。<\/p>","addtime":"2018-12-04 14:34:55","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2408","score":"80","fileaddress":"/Uploads/video/15439052955269.mp4","clicknumber":"81","downnumber":"0","collectionnumber":null,"tag":"《黄河大合唱》第七乐章","redian":"红云经典","niandai":"1969","guige":null,"jundui":"红军","renwu":"","daoyan":"合唱","daoyancon":"无","chupintime":"1969年","company":null,"bianju":null,"zuoci":"光未然","zuoqu":"冼星海","mp3file":"/Uploads/video/15439052955269.mp3","geci":"但是\n中华民族的儿女啊\n谁愿意像猪羊一般\n任人宰割\n我们要抱定必死的决心\n保卫黄河\n保卫华北\n保卫全中国\n《保卫黄河》谱\n《保卫黄河》谱\n　风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国\n风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国\n风在吼\n马在叫\n黄河在咆哮\n黄河在咆哮\n河西山冈万丈高\n河东河北\n高梁熟了\n万山丛中\n抗日英雄真不少\n青纱帐里\n游击健儿逞英豪\n端起了土枪洋枪\n挥动着大刀长矛\n保卫家乡\n保卫黄河\n保卫华北\n保卫全中国","yuyan":null,"timelength":"2分42秒","guojia":"中国","zhanzheng":"其他","leixing":"其他","gequfenlei":"","file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":"","favorite":""}]
         */

        private PageBean page;
        private List<VideoBean> video;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<VideoBean> getVideo() {
            return video;
        }

        public void setVideo(List<VideoBean> video) {
            this.video = video;
        }

        public static class PageBean {
            /**
             * count : 309
             * totalPage : 31
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public PageBean() {
            }

            public PageBean(String count, int totalPage, String page, String limit, int firstRow) {
                this.count = count;
                this.totalPage = totalPage;
                this.page = page;
                this.limit = limit;
                this.firstRow = firstRow;
            }

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class VideoBean {
            /**
             * id : 661
             * title : 通往延安之旅
             * thumbnail : /Uploads/video/15560021779241.jpg
             * introduce :
             * content : <p><video class="edui-upload-video  vjs-default-skin     video-js" controls="" preload="none" width="420" height="280" src="/Uploads/videoTemp/2019423/通往延安之旅.mp4" data-setup="{}"><source src="/Uploads/videoTemp/2019423/通往延安之旅.mp4" type="video/mp4"/></video></p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。</p>
             * addtime : 2019-04-23 14:49:37
             * creater : admin
             * status : 2
             * deleteflg : 1
             * settop : 1
             * type : 2404
             * score : 2
             * fileaddress :
             * clicknumber : 221
             * downnumber : 0
             * collectionnumber : null
             * tag : 纪录片
             * redian :
             * niandai : 0
             * guige : null
             * jundui :
             * renwu :
             * daoyan : 瓦尔特·博斯哈德
             * daoyancon : 瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。
             * chupintime : 1938年
             * company : 瑞士理工学院
             * bianju :
             * zuoci : null
             * zuoqu : null
             * mp3file : null
             * geci : null
             * yuyan : 无声
             * timelength :
             * guojia :
             * zhanzheng :
             * leixing :
             * gequfenlei : null
             * file1 :
             * file2 :
             * file3 :
             * file4 :
             * vipflg : 1
             * userlevel : 0
             * orderno : 1
             * playurl :
             * poster :
             * favorite : {"id":"329","userid":"223","time":"19-08-14 20:03:51","title":"通往延安之旅","url":"http://www.yancloud.red/Portal/Media/info/id/661.html","typename":"纪录片","typeid":"2404","partypeid":"2400","model":"media"}
             */

            private String id;
            private String title;
            private String thumbnail;
            private String introduce;
            private String content;
            private String addtime;
            private String creater;
            private String status;
            private String deleteflg;
            private String settop;
            private String type;
            private String score;
            private String fileaddress;
            private String clicknumber;
            private String downnumber;
            private Object collectionnumber;
            private String tag;
            private String redian;
            private String niandai;
            private Object guige;
            private String jundui;
            private String renwu;
            private String daoyan;
            private String daoyancon;
            private String chupintime;
            private String company;
            private String bianju;
            private Object zuoci;
            private Object zuoqu;
            private Object mp3file;
            private Object geci;
            private String yuyan;
            private String timelength;
            private String guojia;
            private String zhanzheng;
            private String leixing;
            private Object gequfenlei;
            private String file1;
            private String file2;
            private String file3;
            private String file4;
            private String vipflg;
            private String userlevel;
            private String orderno;
            private String playurl;
            private String poster;
            private FavoriteBean favorite;

            public VideoBean() {
            }

            public VideoBean(String id, String title, String thumbnail, String introduce, String content,
                             String addtime, String fileaddress, String tag, String daoyancon, String chupintime,
                             String company, String yuyan, FavoriteBean favorite) {
                this.id = id;
                this.title = title;
                this.thumbnail = thumbnail;
                this.introduce = introduce;
                this.content = content;
                this.addtime = addtime;
                this.fileaddress = fileaddress;
                this.tag = tag;
                this.daoyancon = daoyancon;
                this.chupintime = chupintime;
                this.company = company;
                this.yuyan = yuyan;
                this.favorite = favorite;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getScore() {
                return score;
            }

            public void setScore(String score) {
                this.score = score;
            }

            public String getFileaddress() {
                return fileaddress;
            }

            public void setFileaddress(String fileaddress) {
                this.fileaddress = fileaddress;
            }

            public String getClicknumber() {
                return clicknumber;
            }

            public void setClicknumber(String clicknumber) {
                this.clicknumber = clicknumber;
            }

            public String getDownnumber() {
                return downnumber;
            }

            public void setDownnumber(String downnumber) {
                this.downnumber = downnumber;
            }

            public Object getCollectionnumber() {
                return collectionnumber;
            }

            public void setCollectionnumber(Object collectionnumber) {
                this.collectionnumber = collectionnumber;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public String getRedian() {
                return redian;
            }

            public void setRedian(String redian) {
                this.redian = redian;
            }

            public String getNiandai() {
                return niandai;
            }

            public void setNiandai(String niandai) {
                this.niandai = niandai;
            }

            public Object getGuige() {
                return guige;
            }

            public void setGuige(Object guige) {
                this.guige = guige;
            }

            public String getJundui() {
                return jundui;
            }

            public void setJundui(String jundui) {
                this.jundui = jundui;
            }

            public String getRenwu() {
                return renwu;
            }

            public void setRenwu(String renwu) {
                this.renwu = renwu;
            }

            public String getDaoyan() {
                return daoyan;
            }

            public void setDaoyan(String daoyan) {
                this.daoyan = daoyan;
            }

            public String getDaoyancon() {
                return daoyancon;
            }

            public void setDaoyancon(String daoyancon) {
                this.daoyancon = daoyancon;
            }

            public String getChupintime() {
                return chupintime;
            }

            public void setChupintime(String chupintime) {
                this.chupintime = chupintime;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public String getBianju() {
                return bianju;
            }

            public void setBianju(String bianju) {
                this.bianju = bianju;
            }

            public Object getZuoci() {
                return zuoci;
            }

            public void setZuoci(Object zuoci) {
                this.zuoci = zuoci;
            }

            public Object getZuoqu() {
                return zuoqu;
            }

            public void setZuoqu(Object zuoqu) {
                this.zuoqu = zuoqu;
            }

            public Object getMp3file() {
                return mp3file;
            }

            public void setMp3file(Object mp3file) {
                this.mp3file = mp3file;
            }

            public Object getGeci() {
                return geci;
            }

            public void setGeci(Object geci) {
                this.geci = geci;
            }

            public String getYuyan() {
                return yuyan;
            }

            public void setYuyan(String yuyan) {
                this.yuyan = yuyan;
            }

            public String getTimelength() {
                return timelength;
            }

            public void setTimelength(String timelength) {
                this.timelength = timelength;
            }

            public String getGuojia() {
                return guojia;
            }

            public void setGuojia(String guojia) {
                this.guojia = guojia;
            }

            public String getZhanzheng() {
                return zhanzheng;
            }

            public void setZhanzheng(String zhanzheng) {
                this.zhanzheng = zhanzheng;
            }

            public String getLeixing() {
                return leixing;
            }

            public void setLeixing(String leixing) {
                this.leixing = leixing;
            }

            public Object getGequfenlei() {
                return gequfenlei;
            }

            public void setGequfenlei(Object gequfenlei) {
                this.gequfenlei = gequfenlei;
            }

            public String getFile1() {
                return file1;
            }

            public void setFile1(String file1) {
                this.file1 = file1;
            }

            public String getFile2() {
                return file2;
            }

            public void setFile2(String file2) {
                this.file2 = file2;
            }

            public String getFile3() {
                return file3;
            }

            public void setFile3(String file3) {
                this.file3 = file3;
            }

            public String getFile4() {
                return file4;
            }

            public void setFile4(String file4) {
                this.file4 = file4;
            }

            public String getVipflg() {
                return vipflg;
            }

            public void setVipflg(String vipflg) {
                this.vipflg = vipflg;
            }

            public String getUserlevel() {
                return userlevel;
            }

            public void setUserlevel(String userlevel) {
                this.userlevel = userlevel;
            }

            public String getOrderno() {
                return orderno;
            }

            public void setOrderno(String orderno) {
                this.orderno = orderno;
            }

            public String getPlayurl() {
                return playurl;
            }

            public void setPlayurl(String playurl) {
                this.playurl = playurl;
            }

            public String getPoster() {
                return poster;
            }

            public void setPoster(String poster) {
                this.poster = poster;
            }

            public FavoriteBean getFavorite() {
                return favorite;
            }

            public void setFavorite(FavoriteBean favorite) {
                this.favorite = favorite;
            }

            public static class FavoriteBean {
                /**
                 * id : 329
                 * userid : 223
                 * time : 19-08-14 20:03:51
                 * title : 通往延安之旅
                 * url : http://www.yancloud.red/Portal/Media/info/id/661.html
                 * typename : 纪录片
                 * typeid : 2404
                 * partypeid : 2400
                 * model : media
                 */

                private String id;
                private String userid;
                private String time;
                private String title;
                private String url;
                private String typename;
                private String typeid;
                private String partypeid;
                private String model;

                public FavoriteBean() {
                }

                public FavoriteBean(String id, String userid, String title, String model) {
                    this.id = id;
                    this.userid = userid;
                    this.title = title;
                    this.model = model;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getTypename() {
                    return typename;
                }

                public void setTypename(String typename) {
                    this.typename = typename;
                }

                public String getTypeid() {
                    return typeid;
                }

                public void setTypeid(String typeid) {
                    this.typeid = typeid;
                }

                public String getPartypeid() {
                    return partypeid;
                }

                public void setPartypeid(String partypeid) {
                    this.partypeid = partypeid;
                }

                public String getModel() {
                    return model;
                }

                public void setModel(String model) {
                    this.model = model;
                }
            }

            @Override
            public String toString() {
                return "VideoBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", introduce='" + introduce + '\'' +
                        ", content='" + content + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", creater='" + creater + '\'' +
                        ", status='" + status + '\'' +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", settop='" + settop + '\'' +
                        ", type='" + type + '\'' +
                        ", score='" + score + '\'' +
                        ", fileaddress='" + fileaddress + '\'' +
                        ", clicknumber='" + clicknumber + '\'' +
                        ", downnumber='" + downnumber + '\'' +
                        ", collectionnumber=" + collectionnumber +
                        ", tag='" + tag + '\'' +
                        ", redian='" + redian + '\'' +
                        ", niandai='" + niandai + '\'' +
                        ", guige=" + guige +
                        ", jundui='" + jundui + '\'' +
                        ", renwu='" + renwu + '\'' +
                        ", daoyan='" + daoyan + '\'' +
                        ", daoyancon='" + daoyancon + '\'' +
                        ", chupintime='" + chupintime + '\'' +
                        ", company='" + company + '\'' +
                        ", bianju='" + bianju + '\'' +
                        ", zuoci=" + zuoci +
                        ", zuoqu=" + zuoqu +
                        ", mp3file=" + mp3file +
                        ", geci=" + geci +
                        ", yuyan='" + yuyan + '\'' +
                        ", timelength='" + timelength + '\'' +
                        ", guojia='" + guojia + '\'' +
                        ", zhanzheng='" + zhanzheng + '\'' +
                        ", leixing='" + leixing + '\'' +
                        ", gequfenlei=" + gequfenlei +
                        ", file1='" + file1 + '\'' +
                        ", file2='" + file2 + '\'' +
                        ", file3='" + file3 + '\'' +
                        ", file4='" + file4 + '\'' +
                        ", vipflg='" + vipflg + '\'' +
                        ", userlevel='" + userlevel + '\'' +
                        ", orderno='" + orderno + '\'' +
                        ", playurl='" + playurl + '\'' +
                        ", poster='" + poster + '\'' +
                        ", favorite=" + favorite +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", video=" + video +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "VideoList{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
