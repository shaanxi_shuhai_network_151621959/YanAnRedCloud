package red.yancloud.www.internet.api;


import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import red.yancloud.www.internet.bean.*;
import retrofit2.http.*;

import java.util.List;
import java.util.Map;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/31 13:18
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
public interface ApiService {

    /**
     * get 请求
     * @param url
     * @param maps
     * @return
     */
    @GET
    Observable<ResponseBody> get(@Url String url, @QueryMap Map<String, String> maps);

    /**
     * 异常
     * 形如?t=1&p=2&size=3的url链接不能用@PATH注解(http://www.yancloud.red/Yancloudapp/API/?ac=login&username=yh131412&pwd=yh123456)
     * 使用@Query注解
     * @param entrance
     * @param username
     * @param pwd
     * @return
     */
    @POST("API")
    Observable<Login> postMultipart(@Query("ac") String entrance, @Query("username") String username, @Query("pwd") String pwd);

    /*当POST请求时，@FormUrlEncoded和@Field简单的表单键值对。两个需要结合使用，否则会报错
    @Query请求参数。无论是GET或POST的参数都可以用它来实现 */
    @FormUrlEncoded
    @POST("API")
    Observable<Login> postMultipart(/*@Field("ac") String entrance, @Field("username") String username, @Field("pwd") String pwd*/@FieldMap Map<String, String> params);

    @GET("API")
    Observable<Login> getMultipart(/*@Query("ac") String entrance,@Query("username") String username,@Query("pwd") String pwd*/@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> register(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<MyBookShelf> bookcase(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> booklist(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<MyCollect> myFavorite(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> chapter(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<BookInfo> bookInfo(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<BookCatalog> catalog(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RedTravel> getTravel(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RevolutionMemorialType> travelType(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RevolutionMemorial> memorialList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ReadBookType> bookConfig(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> myCollectDelete(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ReadBookType> myBookDelte(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<BookRecord> bookRecord(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<Default> addBookcase(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> user(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<Default> editPwd(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<Login> editUser(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getZhiKuAllSortData(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RepositorySort> getZhiKuSecondLevelSortData(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RepositoryFirstLevelData> getZhiKuFirstLevelData(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RepositoryFirstLevelData> getZhiKuSecondLevelData(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<BroadcastMessage> getEventNews(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getSearchBookList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<Really> getReallyList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getZhiKuSearch(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RedBag> getRedBagList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<HotSearchEntryMore> getHotEntry(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<RepositoryFirstLevelData> getMoreEntry(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ExpertTeamMore> getExpertTeamList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ExpertTeamMore> getAuthorList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<SpecialTopic> getSubject(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<SpecialTopicContent> getSubjectTheme(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<SpecialTopicMore> getSubjectMore(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getMedia(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<VideoInfo> getVideo(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<DiffuseYanAn> getPaint(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<FavoriteState> getFavoriteState(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> addFavorite(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getThirdLogin(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("API")
    Observable<ResponseBody> getAbout(@FieldMap Map<String, String> params);
}
