package red.yancloud.www.internet.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/31 16:13
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
public class Test extends SectionEntity<SpecialTopicContent.DataBeanXXX.TanbensuyuanBean.DataBean> {

    private String typeId;

    public Test(boolean isHeader, String header, String typeId) {
        super(isHeader, header);
        this.typeId = typeId;
    }

    public Test(SpecialTopicContent.DataBeanXXX.TanbensuyuanBean.DataBean dataBean) {
        super(dataBean);
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
