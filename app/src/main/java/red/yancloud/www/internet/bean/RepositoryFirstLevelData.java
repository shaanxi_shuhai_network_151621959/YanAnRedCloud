package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/16 21:37
 * E_Mail：yh131412hys@163.com
 * Describe：知库一级分类列表
 * Change：
 * @Version：V1.0
 */
public class RepositoryFirstLevelData {

    /**
     * code : 0000
     * data : [{"id":1220,"title_id":1365,"title":"詹姆斯·贝特兰","description":"<p style=\"text-align:justify; text-indent:2em\"><span style=\"color:null\"><span style=\"font-size:16px\"><span style=\"font-family:Microsoft YaHei\"><span style=\"line-height:30px\">詹姆斯&middot;贝特兰<\/span>（1910~ 1993），出生于新西兰奥克兰。1932年考入牛津大学，后又就读于意大利佩鲁大学。<\/span>1936年来到中国，在北平(今北京)担任英国工党机关报《每日先驱论坛报》《曼彻斯特卫报》驻中国特派员，并在燕京大学进修。<span style=\"font-family:Microsoft YaHei\">1937年10月上旬，贝特兰带着抗战后出现的一系列新问题到达延安，与毛泽东进行了多次交谈。随后，赴晋南八路军总部和第120师采访，对中国共产党和八路军的领导人有了直接的、较深的了解。他公正客观的报道和有关著作，为中国人民的英勇斗争得到了国际上的同情和支持，为中华民族的抗战事业作出了应有的贡献。<\/span><\/span><\/span><\/p>\n","img":"http://zhi.yancloud.red/uploads/entry/20190517195633_5cdea171ac4b8.jpg","author_id":71,"category_id":22,"is_published":1,"is_recommend":0,"version":"2","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"人物简介\" rel=\"0\">人物简介<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物故事\" rel=\"1\">人物故事<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"代表作品\" rel=\"2\">代表作品<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物贡献\" rel=\"3\">人物贡献<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物评价\" rel=\"4\">人物评价<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物图册\" rel=\"5\">人物图册<\/a><\/h2>","created_at":1558018804,"updated_at":1558018804,"submitted_at":1558094239,"collect":0,"like":0,"search":null},{"id":1221,"title_id":1364,"title":"哈里森\u2022tle\" title=\"人物简介\" rel=\"0\">人物简介<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物故事     \" rel=\"1\">人物故事     <\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"代表作品\" rel=\"2\">代表作品<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物贡献\" rel=\"3\">人物贡献<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物评价\" rel=\"4\">人物评价<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物图册\" rel=\"5\">人物图册<\/a><\/h2>","created_at":1558018673,"updated_at":1558018673,"submitted_at":1558096212,"collect":0,"like":0,"search":1},{"id":1212,"title_id":1337,"title":"罗别愁","description":"<p style=\"text-align:justify; text-indent:2em\"><span style=\"font-family:Microsoft YaHei\"><span style=\"font-size:16px\"><span style=\"color:#000000\">罗别愁（1899~1972），捷克斯洛伐克人。联合国善后救济总署医生。于1946年来到中国，自愿申请到解放区白求恩国际和平医院工作。在医疗器械奇缺的条件下，她热心为病人治病。除了看门诊、上课外，她还在子长县发起了&ldquo;健康运动&rdquo;的宣传。在中国期间，罗别愁发扬了国际人道主义精神，与中国人民结下了深厚的友谊，为中国革命事业的发展贡献出了自己的力量。<\/span><\/span><\/span><\/p>\n","img":"http://zhi.yancloud.red/uploads/entry/20190517220955_5cdec0b3989a7.jpg","author_id":71,"category_id":22,"is_published":1,"is_recommend":0,"version":"2","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"人物简介\" rel=\"0\">人物简介<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物故事\" rel=\"1\">人物故事<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物贡献\" rel=\"2\">人物贡献<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物评价\" rel=\"3\">人物评价<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物图册\" rel=\"4\">人物图册<\/a><\/h2>","created_at":1557759216,"updated_at":1557759216,"submitted_at":1558102246,"collect":0,"like":0,"search":null}]
     * msg : success
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1220
         * title_id : 1365
         * title : 詹姆斯·贝特兰
         * description : <p style="text-align:justify; text-indent:2em"><span style="color:null"><span style="font-size:16px"><span style="font-family:Microsoft YaHei"><span style="line-height:30px">詹姆斯&middot;贝特兰</span>（1910~ 1993），出生于新西兰奥克兰。1932年考入牛津大学，后又就读于意大利佩鲁大学。</span>1936年来到中国，在北平(今北京)担任英国工党机关报《每日先驱论坛报》《曼彻斯特卫报》驻中国特派员，并在燕京大学进修。<span style="font-family:Microsoft YaHei">1937年10月上旬，贝特兰带着抗战后出现的一系列新问题到达延安，与毛泽东进行了多次交谈。随后，赴晋南八路军总部和第120师采访，对中国共产党和八路军的领导人有了直接的、较深的了解。他公正客观的报道和有关著作，为中国人民的英勇斗争得到了国际上的同情和支持，为中华民族的抗战事业作出了应有的贡献。</span></span></span></p>
         * img : http://zhi.yancloud.red/uploads/entry/20190517195633_5cdea171ac4b8.jpg
         * author_id : 71
         * category_id : 22
         * is_published : 1
         * is_recommend : 0
         * version : 2
         * catalog : <h2><a  href="#" class="catalog-title" title="人物简介" rel="0">人物简介</a></h2><h2><a  href="#" class="catalog-title" title="人物故事" rel="1">人物故事</a></h2><h2><a  href="#" class="catalog-title" title="代表作品" rel="2">代表作品</a></h2><h2><a  href="#" class="catalog-title" title="人物贡献" rel="3">人物贡献</a></h2><h2><a  href="#" class="catalog-title" title="人物评价" rel="4">人物评价</a></h2><h2><a  href="#" class="catalog-title" title="人物图册" rel="5">人物图册</a></h2>
         * created_at : 1558018804
         * updated_at : 1558018804
         * submitted_at : 1558094239
         * collect : 0
         * like : 0
         * search : 94
         */

        private int id;
        private int title_id;
        private String title;
        private String description;
        private String img;
        private int author_id;
        private int category_id;
        private int is_published;
        private int is_recommend;
        private String version;
        private String catalog;
        private int created_at;
        private int updated_at;
        private int submitted_at;
        private int collect;
        private int like;
        private int search;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getTitle_id() {
            return title_id;
        }

        public void setTitle_id(int title_id) {
            this.title_id = title_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getAuthor_id() {
            return author_id;
        }

        public void setAuthor_id(int author_id) {
            this.author_id = author_id;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getIs_published() {
            return is_published;
        }

        public void setIs_published(int is_published) {
            this.is_published = is_published;
        }

        public int getIs_recommend() {
            return is_recommend;
        }

        public void setIs_recommend(int is_recommend) {
            this.is_recommend = is_recommend;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getCatalog() {
            return catalog;
        }

        public void setCatalog(String catalog) {
            this.catalog = catalog;
        }

        public int getCreated_at() {
            return created_at;
        }

        public void setCreated_at(int created_at) {
            this.created_at = created_at;
        }

        public int getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(int updated_at) {
            this.updated_at = updated_at;
        }

        public int getSubmitted_at() {
            return submitted_at;
        }

        public void setSubmitted_at(int submitted_at) {
            this.submitted_at = submitted_at;
        }

        public int getCollect() {
            return collect;
        }

        public void setCollect(int collect) {
            this.collect = collect;
        }

        public int getLike() {
            return like;
        }

        public void setLike(int like) {
            this.like = like;
        }

        public int getSearch() {
            return search;
        }

        public void setSearch(int search) {
            this.search = search;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", title_id=" + title_id +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", img='" + img + '\'' +
                    ", author_id=" + author_id +
                    ", category_id=" + category_id +
                    ", is_published=" + is_published +
                    ", is_recommend=" + is_recommend +
                    ", version='" + version + '\'' +
                    ", catalog='" + catalog + '\'' +
                    ", created_at=" + created_at +
                    ", updated_at=" + updated_at +
                    ", submitted_at=" + submitted_at +
                    ", collect=" + collect +
                    ", like=" + like +
                    ", search=" + search +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RepositoryFirstLevelData{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
