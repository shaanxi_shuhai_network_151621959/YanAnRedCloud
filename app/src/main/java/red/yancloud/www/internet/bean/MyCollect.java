package red.yancloud.www.internet.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/11 11:07
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
public class MyCollect {
    /**
     * code : 0000
     * data : {"page":{"count":"4","totalPage":1,"page":"1","limit":"10","firstRow":0},"records":[{"id":"161","time":"19-06-13 19:08:00","url":"http://book.yancloud.red/Index/info/id/819.html","model":"book","contentId":819,"title":"血火深情-周恩来与共和国三军将领","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556350215_598130528.jpg","introduce":"  "},{"id":"158","time":"19-06-12 11:36:27","url":"http://book.yancloud.red/Index/info/id/767.html","model":"book","contentId":767,"title":"难忘峥嵘岁月","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg","introduce":"在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在\n铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d"},{"id":"157","time":"19-06-11 10:55:55","url":"http://zhi.yancloud.red/index.php?r=entry%2Fentry%2Fview&id=1229","model":"zhiku","contentId":0},{"id":"156","time":"19-06-11 10:55:08","url":"http://book.yancloud.red/Index/info/id/927.html","model":"book","contentId":927,"title":"哈里森.福尔曼的中国摄影集7","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png","introduce":"  "}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"4","totalPage":1,"page":"1","limit":"10","firstRow":0}
         * records : [{"id":"161","time":"19-06-13 19:08:00","url":"http://book.yancloud.red/Index/info/id/819.html","model":"book","contentId":819,"title":"血火深情-周恩来与共和国三军将领","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556350215_598130528.jpg","introduce":"  "},{"id":"158","time":"19-06-12 11:36:27","url":"http://book.yancloud.red/Index/info/id/767.html","model":"book","contentId":767,"title":"难忘峥嵘岁月","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg","introduce":"在共和国六十周年大庆之际,发自肺腑创作了一篇题为《难忘的岁月》歌颂中铁二院的长篇诗章。这两篇诗、文先后呈送给中铁二院工程集团公司朱颖总经理和院党办马有举主任,得到了他们的赞誉并推荐给院党委主办的《铁道勘设报》发表,给了我莫大的褒奖和激励!国庆六十周年之后,我以自己为铁二院建院五十周年之际发表在\n铁二院刊印的《难忘的岁月》一书中的《忆往昔峥嵘岁月》一文为基础,重新整理、撰写我跟随铁二院征战五十年亲身经历的、在脑海里抹不去的、对难忘峥嵘岁月的记忆,经过一年断断续续的努力,书稿终于完成;我又把为纪念\u201c中国华西设计\u201d创建二十年撰写的文稿作为附录,并将1991~1994年在有关杂志上发表的文章精选了三篇重新校正纳入集录为一本小册子,献给培育我一生的中铁二院。遥望天际,\u201c莫道桑榆晚,满目夕照明;喜看别生面,寄望后来人。\u201d"},{"id":"157","time":"19-06-11 10:55:55","url":"http://zhi.yancloud.red/index.php?r=entry%2Fentry%2Fview&id=1229","model":"zhiku","contentId":0},{"id":"156","time":"19-06-11 10:55:08","url":"http://book.yancloud.red/Index/info/id/927.html","model":"book","contentId":927,"title":"哈里森.福尔曼的中国摄影集7","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png","introduce":"  "}]
         */

        private PageBean page;
        private List<RecordsBean> records;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class PageBean {
            /**
             * count : 4
             * totalPage : 1
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class RecordsBean {
            /**
             * id : 161
             * time : 19-06-13 19:08:00
             * url : http://book.yancloud.red/Index/info/id/819.html
             * model : book
             * contentId : 819
             * title : 血火深情-周恩来与共和国三军将领
             * thumbnail : /Uploads/newsThumbnail/2019-04-27/1556350215_598130528.jpg
             * introduce :
             * author: “八一五”——中国人民扬眉吐气的日子
             * content: "新中国建立起来后，作为一个发展中的国家，我们和第三世界的兄弟手拉手，心连心，这正是伟大领袖所说的国际主义的义务。紧张忙的海港码头，一批又一批稻种和将作为援建物资跨过重洋，送到兄弟国家手中。装卸工人们干劲十足，争分夺秒，热火朝天，充满了社会主义当家做主人的自豪感。然而在他们中间，青年工人韩小强却对自己的工作不以为然。他百无聊赖，虚以委蛇，于是工作中难免...",
             * fileaddress: "https://www.iqiyi.com/v_19rr7nnr3o.html"
             */

            private String id;
            private String time;
            private String url;
            private String model;
            private int contentId;
            private String title;
            private String thumbnail;
            private String introduce;
            private String author;
            private String content;
            private String fileaddress;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public int getContentId() {
                return contentId;
            }

            public void setContentId(int contentId) {
                this.contentId = contentId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getFileaddress() {
                return fileaddress;
            }

            public void setFileaddress(String fileaddress) {
                this.fileaddress = fileaddress;
            }

            @Override
            public String toString() {
                return "RecordsBean{" +
                        "id='" + id + '\'' +
                        ", time='" + time + '\'' +
                        ", url='" + url + '\'' +
                        ", model='" + model + '\'' +
                        ", contentId=" + contentId +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", introduce='" + introduce + '\'' +
                        ", author='" + author + '\'' +
                        ", content='" + content + '\'' +
                        ", fileaddress='" + fileaddress + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", records=" + records +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MyCollect{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
