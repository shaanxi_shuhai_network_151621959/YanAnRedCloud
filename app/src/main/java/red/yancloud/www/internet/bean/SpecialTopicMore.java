package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/31 15:58
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题内容更多
 * Change：
 * @Version：V1.0
 */
public class SpecialTopicMore {

    /**
     * code : 0000
     * data : {"page":{"count":"9","totalPage":1,"page":"1","limit":"10","firstRow":0},"list":[{"title":"华而不实的胜利：老蒋占延安损兵折将两万","id":"2697","addtime":"2018-09-21 10:09:22","introduce":"","type":"special_1_21"},{"title":"1947胡宗南攻占延安 毛泽东哪里去了？","id":"2696","addtime":"2018-09-21 09:37:45","introduce":"","type":"special_1_21"},{"title":"1947年3月6日：中共中央军委给刘伯承、邓小平关于保卫延安的指示（节录）","id":"2695","addtime":"2018-09-21 09:34:07","introduce":"","type":"special_1_21"},{"title":"红色记忆：延安保卫战和党中央撤离延安","id":"2694","addtime":"2018-09-21 09:29:59","introduce":"","type":"special_1_21"},{"title":"为什么从保卫延安到主动撤出？","id":"2667","addtime":"2018-09-19 16:59:02","introduce":"","type":"special_1_21"},{"title":"罗元发忆延安保卫战：教导旅阻击胡宗南七天七夜","id":"2665","addtime":"2018-09-19 15:12:58","introduce":"","type":"special_1_21"},{"title":"保卫延安的第一天一夜","id":"2662","addtime":"2016-03-23 14:48:37","introduce":"","type":"special_1_21"},{"title":"最残酷的战斗是七天七夜保卫延安","id":"2663","addtime":"2012-04-17 14:52:30","introduce":"","type":"special_1_21"},{"title":"亲历延安保卫战，81岁老兵想见战友","id":"2693","addtime":"2009-06-17 09:16:00","introduce":"","type":"special_1_21"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"9","totalPage":1,"page":"1","limit":"10","firstRow":0}
         * list : [{"title":"华而不实的胜利：老蒋占延安损兵折将两万","id":"2697","addtime":"2018-09-21 10:09:22","introduce":"","type":"special_1_21"},{"title":"1947胡宗南攻占延安 毛泽东哪里去了？","id":"2696","addtime":"2018-09-21 09:37:45","introduce":"","type":"special_1_21"},{"title":"1947年3月6日：中共中央军委给刘伯承、邓小平关于保卫延安的指示（节录）","id":"2695","addtime":"2018-09-21 09:34:07","introduce":"","type":"special_1_21"},{"title":"红色记忆：延安保卫战和党中央撤离延安","id":"2694","addtime":"2018-09-21 09:29:59","introduce":"","type":"special_1_21"},{"title":"为什么从保卫延安到主动撤出？","id":"2667","addtime":"2018-09-19 16:59:02","introduce":"","type":"special_1_21"},{"title":"罗元发忆延安保卫战：教导旅阻击胡宗南七天七夜","id":"2665","addtime":"2018-09-19 15:12:58","introduce":"","type":"special_1_21"},{"title":"保卫延安的第一天一夜","id":"2662","addtime":"2016-03-23 14:48:37","introduce":"","type":"special_1_21"},{"title":"最残酷的战斗是七天七夜保卫延安","id":"2663","addtime":"2012-04-17 14:52:30","introduce":"","type":"special_1_21"},{"title":"亲历延安保卫战，81岁老兵想见战友","id":"2693","addtime":"2009-06-17 09:16:00","introduce":"","type":"special_1_21"}]
         */

        private PageBean page;
        private List<ListBean> list;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class PageBean {
            /**
             * count : 9
             * totalPage : 1
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }
        }

        public static class ListBean {
            /**
             * title : 华而不实的胜利：老蒋占延安损兵折将两万
             * id : 2697
             * addtime : 2018-09-21 10:09:22
             * introduce :
             * type : special_1_21
             */

            private String title;
            private String id;
            private String addtime;
            private String introduce;
            private String type;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
}
