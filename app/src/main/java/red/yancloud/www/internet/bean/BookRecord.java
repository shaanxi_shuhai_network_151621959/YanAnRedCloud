package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/8 20:24
 * E_Mail：yh131412hys@163.com
 * Describe：用户书籍阅读记录
 * Change：
 * @Version：V1.0
 */
public class BookRecord {

    /**
     * code : 0000
     * data : {"page":{"count":"13","totalPage":2,"page":"1","limit":"10","firstRow":0},"records":[{"id":"107","userid":"137","bookid":"1162","bookchapterid":"23404","time":"1562587367","title":"将台堡会师","author":"马正文 司玉国","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491345_1877231747.jpg"},{"id":"78","userid":"137","bookid":"767","bookchapterid":"16678","time":"1562582072","title":"难忘峥嵘岁月","author":"何光权","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg"},{"id":"95","userid":"137","bookid":"927","bookchapterid":"-1","time":"1562074953","title":"哈里森.福尔曼的中国摄影集7","author":"Harrison Forman","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png"},{"id":"74","userid":"137","bookid":"745","bookchapterid":"-1","time":"1560424188","title":"追寻红色足迹固原革命遗址览胜","author":"田俊秀","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556365734_1257838559.jpg"},{"id":"73","userid":"137","bookid":"418","bookchapterid":"14712","time":"1560424182","title":"日本老兵忏悔录","author":"星彻","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553157649_1831994366.jpg"},{"id":"72","userid":"137","bookid":"415","bookchapterid":"14680","time":"1560424177","title":"南京血祭","author":"阿垅","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553141024_283085944.jpg"},{"id":"71","userid":"137","bookid":"390","bookchapterid":"13531","time":"1560424133","title":"延安精神与社会主义核心价值观论集","author":"陕西省延安精神研究会 西北大学马克思主义学院 榆林市延安精神研究会","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642172_238657958.jpg"},{"id":"70","userid":"137","bookid":"404","bookchapterid":"14205","time":"1560424115","title":"延安时期青年运动史","author":"贺海轮","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642632_588185016.jpg"},{"id":"69","userid":"137","bookid":"402","bookchapterid":"14133","time":"1560424108","title":"中国革命和建设史重要问题研究","author":"袁武振","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642562_1925009919.jpg"},{"id":"68","userid":"137","bookid":"389","bookchapterid":"13516","time":"1560424103","title":"延安红色革命文化资源的有效整合及当代演绎","author":"张新生","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642108_380658785.jpg"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"13","totalPage":2,"page":"1","limit":"10","firstRow":0}
         * records : [{"id":"107","userid":"137","bookid":"1162","bookchapterid":"23404","time":"1562587367","title":"将台堡会师","author":"马正文 司玉国","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491345_1877231747.jpg"},{"id":"78","userid":"137","bookid":"767","bookchapterid":"16678","time":"1562582072","title":"难忘峥嵘岁月","author":"何光权","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg"},{"id":"95","userid":"137","bookid":"927","bookchapterid":"-1","time":"1562074953","title":"哈里森.福尔曼的中国摄影集7","author":"Harrison Forman","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png"},{"id":"74","userid":"137","bookid":"745","bookchapterid":"-1","time":"1560424188","title":"追寻红色足迹固原革命遗址览胜","author":"田俊秀","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556365734_1257838559.jpg"},{"id":"73","userid":"137","bookid":"418","bookchapterid":"14712","time":"1560424182","title":"日本老兵忏悔录","author":"星彻","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553157649_1831994366.jpg"},{"id":"72","userid":"137","bookid":"415","bookchapterid":"14680","time":"1560424177","title":"南京血祭","author":"阿垅","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553141024_283085944.jpg"},{"id":"71","userid":"137","bookid":"390","bookchapterid":"13531","time":"1560424133","title":"延安精神与社会主义核心价值观论集","author":"陕西省延安精神研究会 西北大学马克思主义学院 榆林市延安精神研究会","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642172_238657958.jpg"},{"id":"70","userid":"137","bookid":"404","bookchapterid":"14205","time":"1560424115","title":"延安时期青年运动史","author":"贺海轮","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642632_588185016.jpg"},{"id":"69","userid":"137","bookid":"402","bookchapterid":"14133","time":"1560424108","title":"中国革命和建设史重要问题研究","author":"袁武振","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642562_1925009919.jpg"},{"id":"68","userid":"137","bookid":"389","bookchapterid":"13516","time":"1560424103","title":"延安红色革命文化资源的有效整合及当代演绎","author":"张新生","thumbnail":"/Uploads/newsThumbnail/2019-02-20/1550642108_380658785.jpg"}]
         */

        private PageBean page;
        private List<RecordsBean> records;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class PageBean {
            /**
             * count : 13
             * totalPage : 2
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }
        }

        public static class RecordsBean {
            /**
             * id : 107
             * userid : 137
             * bookid : 1162
             * bookchapterid : 23404
             * time : 1562587367
             * title : 将台堡会师
             * author : 马正文 司玉国
             * thumbnail : /Uploads/newsThumbnail/2019-06-14/1560491345_1877231747.jpg
             */

            private String id;
            private String userid;
            private String bookid;
            private String bookchapterid;
            private String time;
            private String title;
            private String author;
            private String thumbnail;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getBookid() {
                return bookid;
            }

            public void setBookid(String bookid) {
                this.bookid = bookid;
            }

            public String getBookchapterid() {
                return bookchapterid;
            }

            public void setBookchapterid(String bookchapterid) {
                this.bookchapterid = bookchapterid;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }
        }
    }
}
