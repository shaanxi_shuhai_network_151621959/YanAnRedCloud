package red.yancloud.www.internet.bean;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 19:26
 * E_Mail：yh131412hys@163.com
 * Describe：知库词条
 * Change：
 * @Version：V1.0
 */
public class RepositoryEntry {

    private String title;
    private String content;
    private String imgUrl;
    private String time;

    public RepositoryEntry(String title, String content, String imgUrl, String time) {
        this.title = title;
        this.content = content;
        this.imgUrl = imgUrl;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
