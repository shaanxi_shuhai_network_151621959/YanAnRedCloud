package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/1 19:10
 * E_Mail：yh131412hys@163.com
 * Describe：红色文旅
 * Change：
 * @Version：V1.0
 */
public class RedTravel {

    /**
     * code : 0000
     * data : {"menus":[{"id":"3001","title":"伟人故居"},{"id":"3002","title":"战争遗址"},{"id":"3003","title":"烈士陵园"},{"id":"3004","title":"综合博物馆"},{"id":"3005","title":"红色故事"},{"id":"5200","title":"延云书画","childrens":[{"id":"5201","title":"国画赏析"},{"id":"5202","title":"书法鉴赏"},{"id":"5203","title":"摄影聚焦"},{"id":"5204","title":"雕塑艺术"},{"id":"5205","title":"篆刻天地"},{"id":"5206","title":"诗意人生"}]},{"id":"3100","title":"红馆珍藏","childrens":[{"id":"3102","title":"文献报刊"},{"id":"3104","title":"证章证书"},{"id":"3106","title":"军械器具"},{"id":"3107","title":"票据钱币"},{"id":"3110","title":"收藏快讯"}]}],"page":{"count":"15","totalPage":2,"page":"1","limit":"10","firstRow":0},"list":[{"id":"11944","title":"佳县贺龙旧居","thumbnail":"/Uploads/newsThumbnail/2019-07-05/1562312701_899938881.jpg","introduce":"1947年10月29日，贺龙护送江青带李讷从临县三交镇双塔村出发，渡过黄河来到佳县吕家坪村。与此同时，毛泽东也由南河底村来吕家坪迎接江青和李讷。中午时分，两路人马在吕家坪村会合。贺龙在吕家坪住了一晚，1947年10月30日离开。","addtime":"1562312277.000000","parentid":"3000"},{"id":"3602","title":"郭洪涛故居","thumbnail":"/Uploads/newsThumbnail/2019-04-12/1555053170_613302357.jpg","introduce":"1933年冬回陕北，历任陕北特委委员、中共陕北特委组织部长、陕北游击队总指挥坐落在米脂县烈士陵园的郭洪涛铜像部政委、西北工委秘书长、陕甘晋省委副书记、陕北省委书记等职，为创建陕北红军和陕北革命根据地做出了重要贡献，是陕北红军和陕北革命根据地的创建者之一。","addtime":"1555052863.000000","parentid":"3000"},{"id":"3597","title":"汪锋故居","thumbnail":"/Uploads/newsThumbnail/2019-04-12/1555040515_1748315431.jpg","introduce":"汪锋故居纪念馆，主要围绕汪锋同志投身革命创建西北革命根据地、党中央委派汪锋同志到十七路军和杨虎城将军谈判，促成西安事变和平解决；抗日民族统一战线的形成、建国后汪锋同志领导地方工作和对台统战工作等块进行。","addtime":"1555040406.000000","parentid":"3000"},{"id":"2359","title":"毛主席故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478154_1356997810.png","introduce":"毛泽东同志故居，位于湖南省韶山市韶山乡韶山村土地冲上屋场，坐南朝北，属于土木结构的\u201c凹\u201d字型建筑，东边是毛泽东家，西边是邻居，中间堂屋两家共用。","addtime":"1536478129.000000","parentid":"3000"},{"id":"2358","title":"邓小平故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478102_1750086214.png","introduce":"邓小平诞生在四川省广安市广安区协兴镇牌坊村的一座普通农家三合院里。 邓家老院子是一座坐东朝西的传统农家三合院。占地800余平方米，大小房屋17间，穿木斗平房，青瓦粉壁，古朴典雅。","addtime":"1536478086.000000","parentid":"3000"},{"id":"2357","title":"周恩来故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478043_142199400.png","introduce":"周恩来故里旅游景区包括周恩来纪念馆、周恩来故居、驸马巷、河下古镇，总占地3.15平方公里。","addtime":"1536478020.000000","parentid":"3000"},{"id":"2356","title":"刘少奇故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477998_207241875.jpg","introduce":"刘少奇故居位于中国湖南省宁乡县花明楼炭子冲，是刘少奇出生以及度过童年及少年时代的四合院式房子。","addtime":"1536477969.000000","parentid":"3000"},{"id":"2355","title":"朱德故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477938_11546095.png","introduce":"朱德故居始建于清代嘉庆末年，是朱家先辈从广东入川的第二住地，至今已有190多年的历史，是一座坐北朝南的土木结构房屋，典型的川北农家小院。","addtime":"1536477901.000000","parentid":"3000"},{"id":"2354","title":"刘伯承故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477878_1518627154.jpg","introduce":"刘伯承故居位于重庆市开州区赵家街道，坐落在风光旖旎的小华山一台地沈家湾，门前的浦里河沿山脚缓缓流过，直通长江。","addtime":"1536477842.000000","parentid":"3000"},{"id":"2353","title":"彭德怀故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477815_85735854.png","introduce":"彭德怀故居位于湖南省湘潭县乌石镇乌石村彭家围子，始建于1925年，故居现存建筑是彭德怀为湘军任团长时出资修建，是彭德怀早年居住地，也是其投身革命后在家乡唯一长时间居住活动的场所。","addtime":"1536477790.000000","parentid":"3000"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * menus : [{"id":"3001","title":"伟人故居"},{"id":"3002","title":"战争遗址"},{"id":"3003","title":"烈士陵园"},{"id":"3004","title":"综合博物馆"},{"id":"3005","title":"红色故事"},{"id":"5200","title":"延云书画","childrens":[{"id":"5201","title":"国画赏析"},{"id":"5202","title":"书法鉴赏"},{"id":"5203","title":"摄影聚焦"},{"id":"5204","title":"雕塑艺术"},{"id":"5205","title":"篆刻天地"},{"id":"5206","title":"诗意人生"}]},{"id":"3100","title":"红馆珍藏","childrens":[{"id":"3102","title":"文献报刊"},{"id":"3104","title":"证章证书"},{"id":"3106","title":"军械器具"},{"id":"3107","title":"票据钱币"},{"id":"3110","title":"收藏快讯"}]}]
         * page : {"count":"15","totalPage":2,"page":"1","limit":"10","firstRow":0}
         * list : [{"id":"11944","title":"佳县贺龙旧居","thumbnail":"/Uploads/newsThumbnail/2019-07-05/1562312701_899938881.jpg","introduce":"1947年10月29日，贺龙护送江青带李讷从临县三交镇双塔村出发，渡过黄河来到佳县吕家坪村。与此同时，毛泽东也由南河底村来吕家坪迎接江青和李讷。中午时分，两路人马在吕家坪村会合。贺龙在吕家坪住了一晚，1947年10月30日离开。","addtime":"1562312277.000000","parentid":"3000"},{"id":"3602","title":"郭洪涛故居","thumbnail":"/Uploads/newsThumbnail/2019-04-12/1555053170_613302357.jpg","introduce":"1933年冬回陕北，历任陕北特委委员、中共陕北特委组织部长、陕北游击队总指挥坐落在米脂县烈士陵园的郭洪涛铜像部政委、西北工委秘书长、陕甘晋省委副书记、陕北省委书记等职，为创建陕北红军和陕北革命根据地做出了重要贡献，是陕北红军和陕北革命根据地的创建者之一。","addtime":"1555052863.000000","parentid":"3000"},{"id":"3597","title":"汪锋故居","thumbnail":"/Uploads/newsThumbnail/2019-04-12/1555040515_1748315431.jpg","introduce":"汪锋故居纪念馆，主要围绕汪锋同志投身革命创建西北革命根据地、党中央委派汪锋同志到十七路军和杨虎城将军谈判，促成西安事变和平解决；抗日民族统一战线的形成、建国后汪锋同志领导地方工作和对台统战工作等块进行。","addtime":"1555040406.000000","parentid":"3000"},{"id":"2359","title":"毛主席故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478154_1356997810.png","introduce":"毛泽东同志故居，位于湖南省韶山市韶山乡韶山村土地冲上屋场，坐南朝北，属于土木结构的\u201c凹\u201d字型建筑，东边是毛泽东家，西边是邻居，中间堂屋两家共用。","addtime":"1536478129.000000","parentid":"3000"},{"id":"2358","title":"邓小平故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478102_1750086214.png","introduce":"邓小平诞生在四川省广安市广安区协兴镇牌坊村的一座普通农家三合院里。 邓家老院子是一座坐东朝西的传统农家三合院。占地800余平方米，大小房屋17间，穿木斗平房，青瓦粉壁，古朴典雅。","addtime":"1536478086.000000","parentid":"3000"},{"id":"2357","title":"周恩来故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536478043_142199400.png","introduce":"周恩来故里旅游景区包括周恩来纪念馆、周恩来故居、驸马巷、河下古镇，总占地3.15平方公里。","addtime":"1536478020.000000","parentid":"3000"},{"id":"2356","title":"刘少奇故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477998_207241875.jpg","introduce":"刘少奇故居位于中国湖南省宁乡县花明楼炭子冲，是刘少奇出生以及度过童年及少年时代的四合院式房子。","addtime":"1536477969.000000","parentid":"3000"},{"id":"2355","title":"朱德故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477938_11546095.png","introduce":"朱德故居始建于清代嘉庆末年，是朱家先辈从广东入川的第二住地，至今已有190多年的历史，是一座坐北朝南的土木结构房屋，典型的川北农家小院。","addtime":"1536477901.000000","parentid":"3000"},{"id":"2354","title":"刘伯承故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477878_1518627154.jpg","introduce":"刘伯承故居位于重庆市开州区赵家街道，坐落在风光旖旎的小华山一台地沈家湾，门前的浦里河沿山脚缓缓流过，直通长江。","addtime":"1536477842.000000","parentid":"3000"},{"id":"2353","title":"彭德怀故居","thumbnail":"/Uploads/newsThumbnail/2018-09-09/1536477815_85735854.png","introduce":"彭德怀故居位于湖南省湘潭县乌石镇乌石村彭家围子，始建于1925年，故居现存建筑是彭德怀为湘军任团长时出资修建，是彭德怀早年居住地，也是其投身革命后在家乡唯一长时间居住活动的场所。","addtime":"1536477790.000000","parentid":"3000"}]
         */

        private PageBean page;
        private List<MenusBean> menus;
        private List<ListBean> list;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<MenusBean> getMenus() {
            return menus;
        }

        public void setMenus(List<MenusBean> menus) {
            this.menus = menus;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class PageBean {
            /**
             * count : 15
             * totalPage : 2
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class MenusBean {
            /**
             * id : 3001
             * title : 伟人故居
             * childrens : [{"id":"5201","title":"国画赏析"},{"id":"5202","title":"书法鉴赏"},{"id":"5203","title":"摄影聚焦"},{"id":"5204","title":"雕塑艺术"},{"id":"5205","title":"篆刻天地"},{"id":"5206","title":"诗意人生"}]
             */

            private String id;
            private String title;
            private List<ChildrensBean> childrens;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<ChildrensBean> getChildrens() {
                return childrens;
            }

            public void setChildrens(List<ChildrensBean> childrens) {
                this.childrens = childrens;
            }

            public static class ChildrensBean {
                /**
                 * id : 5201
                 * title : 国画赏析
                 */

                private String id;
                private String title;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                @Override
                public String toString() {
                    return "ChildrensBean{" +
                            "id='" + id + '\'' +
                            ", title='" + title + '\'' +
                            '}';
                }
            }

            @Override
            public String toString() {
                return "MenusBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", childrens=" + childrens +
                        '}';
            }
        }

        public static class ListBean {
            /**
             * id : 11944
             * title : 佳县贺龙旧居
             * thumbnail : /Uploads/newsThumbnail/2019-07-05/1562312701_899938881.jpg
             * introduce : 1947年10月29日，贺龙护送江青带李讷从临县三交镇双塔村出发，渡过黄河来到佳县吕家坪村。与此同时，毛泽东也由南河底村来吕家坪迎接江青和李讷。中午时分，两路人马在吕家坪村会合。贺龙在吕家坪住了一晚，1947年10月30日离开。
             * addtime : 1562312277.000000
             * parentid : 3000
             */

            private String id;
            private String title;
            private String thumbnail;
            private String introduce;
            private String addtime;
            private String parentid;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            @Override
            public String toString() {
                return "ListBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", introduce='" + introduce + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", parentid='" + parentid + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", menus=" + menus +
                    ", list=" + list +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RedTravel{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
