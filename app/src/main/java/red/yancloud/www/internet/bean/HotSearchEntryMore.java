package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @author：风不会停息 on 2019/7/28 13:32
 * mailbox：yh131412hys@163.com
 * project：YanAnRedCloud
 * describe：热搜词条更多
 * change：
 * @Version：V1.0
 */
public class HotSearchEntryMore {

    /**
     * code : 0000
     * data : [{"id":"167","title_id":"168","title":"《延安与八路军》","description":"<p style=\"text-align:left\"><span style=\"line-height:150%\"><span style=\"line-height:150%\">《延安与八路军》，由袁牧之编导，吴印咸等摄影，于1938年10月到1940年拍摄的我国第一部反映抗战爆发后延安的政治经济文化面貌、八路军战斗生活和全国各地奔赴延安的青年学习工作等内容的纪录片。<\/span><\/span><\/p>\r\n","img":"http://zhi.yancloud.red/uploads/entry/20190513113825_5cd8e6b1cd82d.jpg","author_id":"8","category_id":"55","is_published":"1","is_recommend":"0","version":"2","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"作者介绍\" rel=\"0\">作者介绍<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"创作背景\" rel=\"1\">创作背景<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"剧情介绍\" rel=\"2\">剧情介绍<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"角色介绍\" rel=\"3\">角色介绍<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"社会影响\" rel=\"4\">社会影响<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"相关图册\" rel=\"5\">相关图册<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"相关影像\" rel=\"6\">相关影像<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"拍摄故事\" rel=\"7\">拍摄故事<\/a><\/h2>","created_at":"1543892057","updated_at":"1544082179","submitted_at":"1543892031","collect":"0","like":"0","search":"2","category":{"id":"55","name":"小说园地","parent_id":"10","description":"","sort":"2","department_id":"0","news_category_id":null,"created_at":"1540882001","updated_at":"1550121660","deleted_at":null},"view_count":"25"},{"id":"64","title_id":"65","title":"延安新华广播电台","description":"<p>延安新华广播电台（1940～1949年)，是中国共产党在延安创办的第一座广播电台。1940年党中央为了向全国、全世界人民宣传我党方针、政策，随时揭露国民党顽固派不愿抗日的丑恶行为，决定用周恩来从苏联带回的一架发射机创办的。<\/p>\r\n","img":"http://zhi.yancloud.red/uploads/entry/20181204151038_5c06286ea11fd.jpg","author_id":"4","category_id":"49","is_published":"1","is_recommend":"0","version":"3","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"创办背景\" rel=\"0\">创办背景<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"历史沿革\" rel=\"1\">历史沿革<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"创办过程\" rel=\"2\">创办过程<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"社会影响\" rel=\"3\">社会影响<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"重要贡献\" rel=\"4\">重要贡献<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"相关图册\" rel=\"5\">相关图册<\/a><\/h2>","created_at":"1548145309","updated_at":"1543907438","submitted_at":"1554703154","collect":"0","like":"0","search":"2","category":{"id":"49","name":"报刊电台","parent_id":"9","description":"","sort":"0","department_id":"0","news_category_id":null,"created_at":"1540881907","updated_at":"1540881907","deleted_at":null},"view_count":"20"},{"id":"1179","title_id":"1321","title":"青阳岔中共中央驻地旧址","description":"<p style=\"text-align:justify; text-indent:2em\"><span style=\"font-family:Microsoft YaHei\"><span style=\"font-size:16px\">青阳岔中共中央驻地旧址位于靖边县青阳岔镇。青阳岔镇是靖边县革命的发源地之一，也是中共靖边县委的诞生地。<\/span><\/span><span style=\"font-size:16px\"><span style=\"font-family:Microsoft YaHei\">早在1932年就成立了党的地下组织，<span style=\"color:#000000\">1947年4月5日至12日、8月1日至2日，毛泽东在该地居住了9天，它曾是中共中央办公所在地。<\/span><\/span><\/span><span style=\"font-family:Microsoft YaHei\"><span style=\"font-size:16px\">2012年5月，靖边县人民政府决定保护维修，2013年，开始动工建设，完成维修保护工作。2008年，被列为省级重点文物保护单位。<\/span><\/span><\/p>\n","img":"http://zhi.yancloud.red/uploads/entry/20190523172436_5ce666d40c6db.jpg","author_id":"6","category_id":"63","is_published":"1","is_recommend":"1","version":"2","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"名称由来\" rel=\"0\">名称由来<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"设立背景\" rel=\"1\">设立背景<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"旧址概况\" rel=\"2\">旧址概况<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"遗址图册\" rel=\"3\">遗址图册<\/a><\/h2>","created_at":"1557303209","updated_at":"1557303209","submitted_at":"1558668588","collect":"0","like":"0","search":"1","category":{"id":"63","name":"红色遗址","parent_id":"62","description":"","sort":"0","department_id":"0","news_category_id":null,"created_at":"1540882225","updated_at":"1540882225","deleted_at":null},"view_count":"18"},{"id":"1658","title_id":"1980","title":"廖容标","description":"<p style=\"text-align:justify; text-indent:2em\"><span style=\"font-family:Microsoft YaHei\"><span style=\"font-size:16px\">廖容标（1912年10月3日~1979年5月2日），曾用名廖之秀，江西省赣县人。1929年8月参加中国工农红军。1931年加入中国共产党。在革命生涯中，历任班长、连长、营长、团长、支队司令员、副旅长、旅长、师长、军区副司令员、军区司令员等职。参加了中央苏区反&quot;围剿&quot;、南雄水口、乐安宜黄、金溪资溪、长征、直罗镇等战役，参与发动领导了黑铁山起义、创建了泰山区抗日根据地等。他是中国共产党优秀党员、久经考验的忠诚共产主义战士、无产阶级革命家、中国人民解放军优秀的军事指挥员、中国人民解放军高级将领。1955年被授予中将军衔。荣获二级八一勋章、一级独立自由勋章、一级解放勋章。<\/span><\/span><span style=\"font-size:16px\">是中国人民政治协商会议第五届全国委员会委员。<\/span><\/p>\n","img":"http://zhi.yancloud.red/uploads/entry/20190711172243_5d26ffe34f11f.png","author_id":"111","category_id":"15","is_published":"1","is_recommend":"0","version":"2","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"大事年表\" rel=\"0\">大事年表<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"主要贡献\" rel=\"1\">主要贡献<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物故事\" rel=\"2\">人物故事<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物评价\" rel=\"3\">人物评价<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"主要著作\" rel=\"4\">主要著作<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"家庭成员\" rel=\"5\">家庭成员<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"人物图册\" rel=\"6\">人物图册<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\" \" rel=\"7\"> <\/a><\/h2>","created_at":"1562321990","updated_at":"1562321990","submitted_at":"1562837310","collect":"0","like":"0","search":"15","category":{"id":"15","name":"军事将领","parent_id":"3","description":"","sort":"1","department_id":"0","news_category_id":null,"created_at":"1540881157","updated_at":"1550116773","deleted_at":null},"view_count":"17"},{"id":"561","title_id":"589","title":"《中共中央关于延安干部学校的决定》","description":"<p style=\"text-align:justify; text-indent:2em\"><span style=\"font-size:16px\"><span style=\"font-family:Microsoft YaHei\"><span style=\"color:#333333\">《中共中央关于延安干部学校的决定》，是1941年12月中央政治局为纠正当时干部教育和办学工作中出现的问题而发布的一份指导性文件。《决定》内容共14条，对延安干部学校教育中出现的主观主义、教条主义倾向作了严肃批评，同时指明了正确的方向与实施措施。<\/span><\/span><\/span><\/p>\n","img":"http://zhi.yancloud.red/uploads/entry/20190124110808_5c492c1848b79.jpg","author_id":"14","category_id":"30","is_published":"1","is_recommend":"0","version":"3","catalog":"<h2><a  href=\"#\" class=\"catalog-title\" title=\"历史背景\" rel=\"0\">历史背景<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"决定目的\" rel=\"1\">决定目的<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"决定内容\" rel=\"2\">决定内容<\/a><\/h2><h2><a  href=\"#\" class=\"catalog-title\" title=\"词条图册\" rel=\"3\">词条图册<\/a><\/h2>","created_at":"1547191250","updated_at":"1549852911","submitted_at":"1554343393","collect":"0","like":"0","search":"3","category":{"id":"30","name":"重要会议","parent_id":"5","description":"","sort":"1","department_id":"0","news_category_id":null,"created_at":"1540881519","updated_at":"1550116911","deleted_at":null},"view_count":"14"}]
     * msg : success
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 167
         * title_id : 168
         * title : 《延安与八路军》
         * description : <p style="text-align:left"><span style="line-height:150%"><span style="line-height:150%">《延安与八路军》，由袁牧之编导，吴印咸等摄影，于1938年10月到1940年拍摄的我国第一部反映抗战爆发后延安的政治经济文化面貌、八路军战斗生活和全国各地奔赴延安的青年学习工作等内容的纪录片。</span></span></p>
         * img : http://zhi.yancloud.red/uploads/entry/20190513113825_5cd8e6b1cd82d.jpg
         * author_id : 8
         * category_id : 55
         * is_published : 1
         * is_recommend : 0
         * version : 2
         * catalog : <h2><a  href="#" class="catalog-title" title="作者介绍" rel="0">作者介绍</a></h2><h2><a  href="#" class="catalog-title" title="创作背景" rel="1">创作背景</a></h2><h2><a  href="#" class="catalog-title" title="剧情介绍" rel="2">剧情介绍</a></h2><h2><a  href="#" class="catalog-title" title="角色介绍" rel="3">角色介绍</a></h2><h2><a  href="#" class="catalog-title" title="社会影响" rel="4">社会影响</a></h2><h2><a  href="#" class="catalog-title" title="相关图册" rel="5">相关图册</a></h2><h2><a  href="#" class="catalog-title" title="相关影像" rel="6">相关影像</a></h2><h2><a  href="#" class="catalog-title" title="拍摄故事" rel="7">拍摄故事</a></h2>
         * created_at : 1543892057
         * updated_at : 1544082179
         * submitted_at : 1543892031
         * collect : 0
         * like : 0
         * search : 2
         * category : {"id":"55","name":"小说园地","parent_id":"10","description":"","sort":"2","department_id":"0","news_category_id":null,"created_at":"1540882001","updated_at":"1550121660","deleted_at":null}
         * view_count : 25
         */

        private String id;
        private String title_id;
        private String title;
        private String description;
        private String img;
        private String author_id;
        private String category_id;
        private String is_published;
        private String is_recommend;
        private String version;
        private String catalog;
        private String created_at;
        private String updated_at;
        private String submitted_at;
        private String collect;
        private String like;
        private String search;
        private CategoryBean category;
        private String view_count;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle_id() {
            return title_id;
        }

        public void setTitle_id(String title_id) {
            this.title_id = title_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getAuthor_id() {
            return author_id;
        }

        public void setAuthor_id(String author_id) {
            this.author_id = author_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getIs_published() {
            return is_published;
        }

        public void setIs_published(String is_published) {
            this.is_published = is_published;
        }

        public String getIs_recommend() {
            return is_recommend;
        }

        public void setIs_recommend(String is_recommend) {
            this.is_recommend = is_recommend;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getCatalog() {
            return catalog;
        }

        public void setCatalog(String catalog) {
            this.catalog = catalog;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getSubmitted_at() {
            return submitted_at;
        }

        public void setSubmitted_at(String submitted_at) {
            this.submitted_at = submitted_at;
        }

        public String getCollect() {
            return collect;
        }

        public void setCollect(String collect) {
            this.collect = collect;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getSearch() {
            return search;
        }

        public void setSearch(String search) {
            this.search = search;
        }

        public CategoryBean getCategory() {
            return category;
        }

        public void setCategory(CategoryBean category) {
            this.category = category;
        }

        public String getView_count() {
            return view_count;
        }

        public void setView_count(String view_count) {
            this.view_count = view_count;
        }

        public static class CategoryBean {
            /**
             * id : 55
             * name : 小说园地
             * parent_id : 10
             * description :
             * sort : 2
             * department_id : 0
             * news_category_id : null
             * created_at : 1540882001
             * updated_at : 1550121660
             * deleted_at : null
             */

            private String id;
            private String name;
            private String parent_id;
            private String description;
            private String sort;
            private String department_id;
            private Object news_category_id;
            private String created_at;
            private String updated_at;
            private Object deleted_at;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getParent_id() {
                return parent_id;
            }

            public void setParent_id(String parent_id) {
                this.parent_id = parent_id;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getDepartment_id() {
                return department_id;
            }

            public void setDepartment_id(String department_id) {
                this.department_id = department_id;
            }

            public Object getNews_category_id() {
                return news_category_id;
            }

            public void setNews_category_id(Object news_category_id) {
                this.news_category_id = news_category_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }
        }
    }
}
