package red.yancloud.www.internet.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/20 15:10
 * E_Mail：yh131412hys@163.com
 * Describe：知库分类
 * Change：
 * @Version：V1.0
 */
public class RepositorySort implements Parcelable{
    /**
     * code : 0000
     * data : [{"id":1,"parent_id":null,"sort":1,"name":"理论","children":[{"id":11,"parent_id":1,"sort":0,"name":"理论名词"},{"id":12,"parent_id":1,"sort":1,"name":"理论思想"},{"id":13,"parent_id":1,"sort":2,"name":"理论著作"}]},{"id":3,"parent_id":null,"sort":2,"name":"人物","children":{"0":{"id":14,"parent_id":3,"sort":0,"name":"革命领袖"},"1":{"id":15,"parent_id":3,"sort":1,"name":"军事将领"},"3":{"id":16,"parent_id":3,"sort":3,"name":"党政领导"},"4":{"id":17,"parent_id":3,"sort":4,"name":"英模人物"},"6":{"id":19,"parent_id":3,"sort":6,"name":"社会名流"},"7":{"id":20,"parent_id":3,"sort":7,"name":"民主人士"},"8":{"id":21,"parent_id":3,"sort":8,"name":"爱国华侨"},"9":{"id":22,"parent_id":3,"sort":9,"name":"国际友人"}}},{"id":4,"parent_id":null,"sort":3,"name":"事件","children":{"2":{"id":25,"parent_id":4,"sort":2,"name":"文化事件"}}},{"id":5,"parent_id":null,"sort":4,"name":"政治","children":{"0":{"id":28,"parent_id":5,"sort":0,"name":"政权组织"},"1":{"id":30,"parent_id":5,"sort":1,"name":"重要会议"},"3":{"id":31,"parent_id":5,"sort":3,"name":"政治运动"},"4":{"id":23,"parent_id":5,"sort":4,"name":"政治事件"}}},{"id":6,"parent_id":null,"sort":5,"name":"军事","children":[{"id":34,"parent_id":6,"sort":0,"name":"军事思想"},{"id":32,"parent_id":6,"sort":1,"name":"军事组织"},{"id":24,"parent_id":6,"sort":2,"name":"军事事件"},{"id":33,"parent_id":6,"sort":3,"name":"军队序列"},{"id":35,"parent_id":6,"sort":4,"name":"战略战术"},{"id":36,"parent_id":6,"sort":5,"name":"战役战斗"},{"id":37,"parent_id":6,"sort":6,"name":"军工建设"}]},{"id":7,"parent_id":null,"sort":6,"name":"经济","children":[{"id":39,"parent_id":7,"sort":0,"name":"经济政策"},{"id":40,"parent_id":7,"sort":1,"name":"经济活动"},{"id":41,"parent_id":7,"sort":2,"name":"经济人物"},{"id":42,"parent_id":7,"sort":3,"name":"银行货币"}]},{"id":8,"parent_id":null,"sort":7,"name":"社会","children":{"0":{"id":44,"parent_id":8,"sort":0,"name":"社会组织"},"1":{"id":45,"parent_id":8,"sort":1,"name":"社会事件"},"3":{"id":47,"parent_id":8,"sort":3,"name":"风俗民情"}}},{"id":9,"parent_id":null,"sort":8,"name":"新闻","children":{"0":{"id":49,"parent_id":9,"sort":0,"name":"报刊电台"},"1":{"id":50,"parent_id":9,"sort":1,"name":"新闻作品"},"3":{"id":52,"parent_id":9,"sort":3,"name":"新闻记者"}}},{"id":10,"parent_id":null,"sort":9,"name":"文化","children":{"0":{"id":77,"parent_id":10,"sort":0,"name":"散文诗歌"},"2":{"id":55,"parent_id":10,"sort":2,"name":"小说园地"},"3":{"id":56,"parent_id":10,"sort":3,"name":"音乐天地"},"4":{"id":57,"parent_id":10,"sort":4,"name":"文艺人士"},"5":{"id":76,"parent_id":10,"sort":5,"name":"报告文学"},"7":{"id":80,"parent_id":10,"sort":7,"name":"影视戏剧"},"9":{"id":82,"parent_id":10,"sort":9,"name":"文化事件"}}},{"id":58,"parent_id":null,"sort":10,"name":"教育","children":[{"id":59,"parent_id":58,"sort":0,"name":"教育机构"},{"id":60,"parent_id":58,"sort":1,"name":"教育人物"},{"id":61,"parent_id":58,"sort":2,"name":"校友风采"},{"id":81,"parent_id":58,"sort":3,"name":"教苑故事"}]},{"id":62,"parent_id":null,"sort":11,"name":"遗址","children":[{"id":63,"parent_id":62,"sort":0,"name":"红色遗址"},{"id":64,"parent_id":62,"sort":1,"name":"纪念场馆"},{"id":84,"parent_id":62,"sort":2,"name":"烈士陵园"}]},{"id":66,"parent_id":null,"sort":12,"name":"收藏","children":[{"id":67,"parent_id":66,"sort":0,"name":"收藏信息"},{"id":68,"parent_id":66,"sort":1,"name":"文史资料"},{"id":69,"parent_id":66,"sort":2,"name":"证章证书"},{"id":70,"parent_id":66,"sort":3,"name":"钱币票据"},{"id":71,"parent_id":66,"sort":4,"name":"器具实物"}]}]
     * msg : success
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public RepositorySort() {
    }

    protected RepositorySort(Parcel in) {
        code = in.readString();
        msg = in.readString();
    }

    public static final Creator<RepositorySort> CREATOR = new Creator<RepositorySort>() {
        @Override
        public RepositorySort createFromParcel(Parcel in) {
            return new RepositorySort(in);
        }

        @Override
        public RepositorySort[] newArray(int size) {
            return new RepositorySort[size];
        }
    };

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(msg);
    }

    public static class DataBean implements Parcelable{
        /**
         * id : 1
         * parent_id : null
         * sort : 1
         * name : 理论
         * children : [{"id":11,"parent_id":1,"sort":0,"name":"理论名词"},{"id":12,"parent_id":1,"sort":1,"name":"理论思想"},{"id":13,"parent_id":1,"sort":2,"name":"理论著作"}]
         */

        private int id;
        private Object parent_id;
        private int sort;
        private String name;
        private List<ChildrenBean> children;

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            id = in.readInt();
            sort = in.readInt();
            name = in.readString();
            children = in.createTypedArrayList(ChildrenBean.CREATOR);
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getParent_id() {
            return parent_id;
        }

        public void setParent_id(Object parent_id) {
            this.parent_id = parent_id;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<ChildrenBean> getChildren() {
            return children;
        }

        public void setChildren(List<ChildrenBean> children) {
            this.children = children;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(sort);
            dest.writeString(name);
            dest.writeTypedList(children);
        }

        public static class ChildrenBean implements Parcelable {
            /**
             * id : 11
             * parent_id : 1
             * sort : 0
             * name : 理论名词
             */

            private int id;
            private int parent_id;
            private int sort;
            private String name;

            public ChildrenBean() {
            }

            public ChildrenBean(int id, int parent_id, int sort, String name) {
                this.id = id;
                this.parent_id = parent_id;
                this.sort = sort;
                this.name = name;
            }

            protected ChildrenBean(Parcel in) {
                id = in.readInt();
                parent_id = in.readInt();
                sort = in.readInt();
                name = in.readString();
            }

            public static final Creator<ChildrenBean> CREATOR = new Creator<ChildrenBean>() {
                @Override
                public ChildrenBean createFromParcel(Parcel in) {
                    return new ChildrenBean(in);
                }

                @Override
                public ChildrenBean[] newArray(int size) {
                    return new ChildrenBean[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParent_id() {
                return parent_id;
            }

            public void setParent_id(int parent_id) {
                this.parent_id = parent_id;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ChildrenBean{" +
                        "id=" + id +
                        ", parent_id=" + parent_id +
                        ", sort=" + sort +
                        ", name='" + name + '\'' +
                        '}';
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(id);
                dest.writeInt(parent_id);
                dest.writeInt(sort);
                dest.writeString(name);
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", parent_id=" + parent_id +
                    ", sort=" + sort +
                    ", name='" + name + '\'' +
                    ", children=" + children +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RepositorySort{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    /*  private String key;
    private List<String> valueList;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(List<String> valueList) {
        this.valueList = valueList;
    }

    @Override
    public String toString() {
        return "RepositorySort{" +
                "key='" + key + '\'' +
                ", valueList=" + valueList +
                '}';
    }*/
}
