package red.yancloud.www.internet.bean;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/12 16:23
 * E_Mail：yh131412hys@163.com
 * Describe：收藏状态
 * Change：
 * @Version：V1.0
 */
public class FavoriteState {

    /**
     * code : 0000
     * data : {"id":"244","userid":"223","time":"19-08-12 21:16:10","title":"人民不会忘记（下）","url":"http://www.yancloud.red/Portal/Book/info/id/1164.html","typename":null,"typeid":null,"partypeid":null,"model":"book"}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * id : 244
         * userid : 223
         * time : 19-08-12 21:16:10
         * title : 人民不会忘记（下）
         * url : http://www.yancloud.red/Portal/Book/info/id/1164.html
         * typename : null
         * typeid : null
         * partypeid : null
         * model : book
         */

        private String id;
        private String userid;
        private String time;
        private String title;
        private String url;
        private Object typename;
        private Object typeid;
        private Object partypeid;
        private String model;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Object getTypename() {
            return typename;
        }

        public void setTypename(Object typename) {
            this.typename = typename;
        }

        public Object getTypeid() {
            return typeid;
        }

        public void setTypeid(Object typeid) {
            this.typeid = typeid;
        }

        public Object getPartypeid() {
            return partypeid;
        }

        public void setPartypeid(Object partypeid) {
            this.partypeid = partypeid;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }
    }
}
