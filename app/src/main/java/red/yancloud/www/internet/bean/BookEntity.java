package red.yancloud.www.internet.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/13 20:02
 * E_Mail：yh131412hys@163.com
 * Describe：书籍表对应的实体类
 * Change：
 * @Version：V1.0
 */
@Entity
public class BookEntity {

    /**
     * 书籍表中的id
     */
    @Id
    private Long id;
    /**
     * 书Id(唯一约束)
     */
    @Unique
    private Long bookId;
    /**
     * 书籍名称
     */
    private String bookName;
    /**
     * 书籍封面
     */
    private String bookCover;
    /**
     * 书籍作者
     */
    private String author;
    /**
     * 书籍类型 0:ePup 1:pdf
     */
    private String bookType;
    /**
     * pdf书籍链接
     */
    private String firstBookPdfUrl;
    /**
     * 书籍第一章章节id -1 :pdf 否则是epup的第一章 927
     */
    private String firstBookChapterId;
    /**
     * 书籍最后章节
     */
    private String lastBookChapterId;
    /**
     * 记录最后阅读的章节id
     */
    private String lastReadBookChapterId;
    /**
     * 记录ePup最后阅读位置
     */
    private int scrollY;

    @Generated(hash = 147672492)
    public BookEntity(Long id, Long bookId, String bookName, String bookCover,
            String author, String bookType, String firstBookPdfUrl,
            String firstBookChapterId, String lastBookChapterId,
            String lastReadBookChapterId, int scrollY) {
        this.id = id;
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookCover = bookCover;
        this.author = author;
        this.bookType = bookType;
        this.firstBookPdfUrl = firstBookPdfUrl;
        this.firstBookChapterId = firstBookChapterId;
        this.lastBookChapterId = lastBookChapterId;
        this.lastReadBookChapterId = lastReadBookChapterId;
        this.scrollY = scrollY;
    }

    @Generated(hash = 1373651409)
    public BookEntity() {
    }

    public BookEntity(Long bookId, String bookName, String bookCover, String author, String bookType, String firstBookPdfUrl, String firstBookChapterId, String lastBookChapterId, String lastReadBookChapterId, int scrollY) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookCover = bookCover;
        this.author = author;
        this.bookType = bookType;
        this.firstBookPdfUrl = firstBookPdfUrl;
        this.firstBookChapterId = firstBookChapterId;
        this.lastBookChapterId = lastBookChapterId;
        this.lastReadBookChapterId = lastReadBookChapterId;
        this.scrollY = scrollY;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBookId() {
        return this.bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return this.bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookCover() {
        return this.bookCover;
    }

    public void setBookCover(String bookCover) {
        this.bookCover = bookCover;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookType() {
        return this.bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getFirstBookPdfUrl() {
        return this.firstBookPdfUrl;
    }

    public void setFirstBookPdfUrl(String firstBookPdfUrl) {
        this.firstBookPdfUrl = firstBookPdfUrl;
    }

    public String getFirstBookChapterId() {
        return this.firstBookChapterId;
    }

    public void setFirstBookChapterId(String firstBookChapterId) {
        this.firstBookChapterId = firstBookChapterId;
    }

    public String getLastBookChapterId() {
        return this.lastBookChapterId;
    }

    public void setLastBookChapterId(String lastBookChapterId) {
        this.lastBookChapterId = lastBookChapterId;
    }

    public String getLastReadBookChapterId() {
        return this.lastReadBookChapterId;
    }

    public void setLastReadBookChapterId(String lastReadBookChapterId) {
        this.lastReadBookChapterId = lastReadBookChapterId;
    }

    public int getScrollY() {
        return this.scrollY;
    }

    public void setScrollY(int scrollY) {
        this.scrollY = scrollY;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", bookCover='" + bookCover + '\'' +
                ", author='" + author + '\'' +
                ", bookType='" + bookType + '\'' +
                ", firstBookPdfUrl='" + firstBookPdfUrl + '\'' +
                ", firstBookChapterId='" + firstBookChapterId + '\'' +
                ", lastBookChapterId='" + lastBookChapterId + '\'' +
                ", lastReadBookChapterId='" + lastReadBookChapterId + '\'' +
                ", scrollY=" + scrollY +
                '}';
    }
}
