package red.yancloud.www.internet.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/23 14:08
 * E_Mail：yh131412hys@163.com
 * Describe：书籍目录
 * Change：
 * @Version：V1.0
 */
public class BookCatalog {

    /**
     * code : 0000
     * data : {"page":{"count":"27","totalPage":3,"page":1,"limit":10,"firstRow":0},"chapters":[{"id":"23384","bookid":"1162","level":"1","parentid":"0","title":"一","path":"/book/15604912922749/OEBPS/Text/chapter01.xhtml","isfree":"0","deleteflg":"1","dot":"72"},{"id":"23385","bookid":"1162","level":"1","parentid":"0","title":"随斯诺访问宁夏和目击红军大会师","path":"/book/15604912922749/OEBPS/Text/chapter02.xhtml","isfree":"0","deleteflg":"1","dot":"34"},{"id":"23386","bookid":"1162","level":"1","parentid":"0","title":"目击红军三大主力会师","path":"/book/15604912922749/OEBPS/Text/chapter03.xhtml","isfree":"0","deleteflg":"1","dot":"72"},{"id":"23387","bookid":"1162","level":"1","parentid":"0","title":"红军长征、西征与三大主力会师","path":"/book/15604912922749/OEBPS/Text/chapter04.xhtml","isfree":"0","deleteflg":"1","dot":"30"},{"id":"23388","bookid":"1162","level":"1","parentid":"0","title":"三军大会师","path":"/book/15604912922749/OEBPS/Text/chapter05.xhtml","isfree":"0","deleteflg":"1","dot":"53"},{"id":"23389","bookid":"1162","level":"1","parentid":"0","title":"会师前的战斗","path":"/book/15604912922749/OEBPS/Text/chapter06.xhtml","isfree":"0","deleteflg":"1","dot":"4"},{"id":"23390","bookid":"1162","level":"1","parentid":"0","title":"忆红七十八师西征","path":"/book/15604912922749/OEBPS/Text/chapter07.xhtml","isfree":"0","deleteflg":"1","dot":"1"},{"id":"23391","bookid":"1162","level":"1","parentid":"0","title":"会师前后","path":"/book/15604912922749/OEBPS/Text/chapter08.xhtml","isfree":"0","deleteflg":"1","dot":"2"},{"id":"23392","bookid":"1162","level":"1","parentid":"0","title":"难忘的岁月","path":"/book/15604912922749/OEBPS/Text/chapter09.xhtml","isfree":"0","deleteflg":"1","dot":"1"},{"id":"23393","bookid":"1162","level":"1","parentid":"0","title":"会师后的喜讯","path":"/book/15604912922749/OEBPS/Text/chapter10.xhtml","isfree":"0","deleteflg":"1","dot":"2"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"27","totalPage":3,"page":1,"limit":10,"firstRow":0}
         * chapters : [{"id":"23384","bookid":"1162","level":"1","parentid":"0","title":"一","path":"/book/15604912922749/OEBPS/Text/chapter01.xhtml","isfree":"0","deleteflg":"1","dot":"72"},{"id":"23385","bookid":"1162","level":"1","parentid":"0","title":"随斯诺访问宁夏和目击红军大会师","path":"/book/15604912922749/OEBPS/Text/chapter02.xhtml","isfree":"0","deleteflg":"1","dot":"34"},{"id":"23386","bookid":"1162","level":"1","parentid":"0","title":"目击红军三大主力会师","path":"/book/15604912922749/OEBPS/Text/chapter03.xhtml","isfree":"0","deleteflg":"1","dot":"72"},{"id":"23387","bookid":"1162","level":"1","parentid":"0","title":"红军长征、西征与三大主力会师","path":"/book/15604912922749/OEBPS/Text/chapter04.xhtml","isfree":"0","deleteflg":"1","dot":"30"},{"id":"23388","bookid":"1162","level":"1","parentid":"0","title":"三军大会师","path":"/book/15604912922749/OEBPS/Text/chapter05.xhtml","isfree":"0","deleteflg":"1","dot":"53"},{"id":"23389","bookid":"1162","level":"1","parentid":"0","title":"会师前的战斗","path":"/book/15604912922749/OEBPS/Text/chapter06.xhtml","isfree":"0","deleteflg":"1","dot":"4"},{"id":"23390","bookid":"1162","level":"1","parentid":"0","title":"忆红七十八师西征","path":"/book/15604912922749/OEBPS/Text/chapter07.xhtml","isfree":"0","deleteflg":"1","dot":"1"},{"id":"23391","bookid":"1162","level":"1","parentid":"0","title":"会师前后","path":"/book/15604912922749/OEBPS/Text/chapter08.xhtml","isfree":"0","deleteflg":"1","dot":"2"},{"id":"23392","bookid":"1162","level":"1","parentid":"0","title":"难忘的岁月","path":"/book/15604912922749/OEBPS/Text/chapter09.xhtml","isfree":"0","deleteflg":"1","dot":"1"},{"id":"23393","bookid":"1162","level":"1","parentid":"0","title":"会师后的喜讯","path":"/book/15604912922749/OEBPS/Text/chapter10.xhtml","isfree":"0","deleteflg":"1","dot":"2"}]
         */

        private PageBean page;
        private List<ChaptersBean> chapters;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<ChaptersBean> getChapters() {
            return chapters;
        }

        public void setChapters(List<ChaptersBean> chapters) {
            this.chapters = chapters;
        }

        public static class PageBean {
            /**
             * count : 27
             * totalPage : 3
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private int page;
            private int limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public int getPage() {
                return page;
            }

            public void setPage(int page) {
                this.page = page;
            }

            public int getLimit() {
                return limit;
            }

            public void setLimit(int limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page=" + page +
                        ", limit=" + limit +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class ChaptersBean implements Parcelable {
            /**
             * id : 23384
             * bookid : 1162
             * level : 1
             * parentid : 0
             * title : 一
             * path : /book/15604912922749/OEBPS/Text/chapter01.xhtml
             * isfree : 0
             * deleteflg : 1
             * dot : 72
             */

            private String id;
            private String bookid;
            private String level;
            private String parentid;
            private String title;
            private String path;
            private String isfree;
            private String deleteflg;
            private String dot;

            protected ChaptersBean(Parcel in) {
                id = in.readString();
                bookid = in.readString();
                level = in.readString();
                parentid = in.readString();
                title = in.readString();
                path = in.readString();
                isfree = in.readString();
                deleteflg = in.readString();
                dot = in.readString();
            }

            public static final Creator<ChaptersBean> CREATOR = new Creator<ChaptersBean>() {
                @Override
                public ChaptersBean createFromParcel(Parcel in) {
                    return new ChaptersBean(in);
                }

                @Override
                public ChaptersBean[] newArray(int size) {
                    return new ChaptersBean[size];
                }
            };

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getBookid() {
                return bookid;
            }

            public void setBookid(String bookid) {
                this.bookid = bookid;
            }

            public String getLevel() {
                return level;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getIsfree() {
                return isfree;
            }

            public void setIsfree(String isfree) {
                this.isfree = isfree;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getDot() {
                return dot;
            }

            public void setDot(String dot) {
                this.dot = dot;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(bookid);
                dest.writeString(level);
                dest.writeString(parentid);
                dest.writeString(title);
                dest.writeString(path);
                dest.writeString(isfree);
                dest.writeString(deleteflg);
                dest.writeString(dot);
            }

            @Override
            public String toString() {
                return "ChaptersBean{" +
                        "id='" + id + '\'' +
                        ", bookid='" + bookid + '\'' +
                        ", level='" + level + '\'' +
                        ", parentid='" + parentid + '\'' +
                        ", title='" + title + '\'' +
                        ", path='" + path + '\'' +
                        ", isfree='" + isfree + '\'' +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", dot='" + dot + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", chapters=" + chapters +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "BookCatalog{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
