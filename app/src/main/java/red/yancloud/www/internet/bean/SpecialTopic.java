package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/29 20:21
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题
 * Change：
 * @Version：V1.0
 */
public class SpecialTopic {

    /**
     * code : 0000
     * data : {"menus":[{"id":"11027","title":"重大事件"},{"id":"11028","title":"重点人物"},{"id":"11029","title":"重要机构"},{"id":"12002","title":"重点探索"}],"page":{"count":"4","totalPage":1,"page":"1","limit":"10","firstRow":0},"subjects":[{"id":"21","name":"1947年，毛泽东为何\"放弃\"延安？","subtitle":"撤离延安的背后：把包袱让给蒋介石背上！","key":"00-0004-04","creater":"白皓","addtime":"2018-09-19 10:45:44","thumbnail":"/Uploads/newsThumbnail/2018-09-19/1537324758_277660646.jpg","img":"/Uploads/newsThumbnail/2018-09-19/1537325141_886554238.jpg","introduce":"我们暂时放弃延安，就是把包袱让给敌人背上，使自己打起仗来更主动，更灵活，这样就能大量消灭敌人，到了一定的时机，再举行反攻，延安就会重新回到我们的手里。\u2014\u2014毛泽东","type":"11027","type1":"special_1_21","type2":"special_2_21","type3":"special_3_21","status":"2"},{"id":"17","name":"红军改编八路军的艰难历程","subtitle":"中国共产党在民族独立解放面前的伟大历史担当","key":"00-0005-05","creater":"白皓","addtime":"2018-09-13 17:15:42","thumbnail":"/Uploads/newsThumbnail/2018-09-13/1536830075_2088549174.jpg","img":"/Uploads/newsThumbnail/2018-09-13/1536830003_339150924.jpg","introduce":"为了促进全国抗战，中共中央从1937年2月开始，同国民党当局就红军改编问题进行谈判。但直到卢沟桥事变后，日本侵略军进攻上海，威胁南京时，国民党当局才与中共中央达成红军改编的协议。1937年8月22日，国民党军事委员会宣布红军主力部队改编为国民革命军第八路军，并同意设立总指挥部，下辖3个师，每师15万人，师辖2个旅，每旅辖2个团。","type":"11027","type1":"special_1_17","type2":"special_2_17","type3":"special_3_17","status":"2"},{"id":"14","name":"延安时期的秧歌运动","subtitle":"从民间旧艺术走向与民族艺术形式","key":"00-0003-03","creater":"白皓","addtime":"2018-09-13 14:52:00","thumbnail":"/Uploads/newsThumbnail/2018-09-13/1536821892_759289461.jpg","img":"/Uploads/newsThumbnail/2018-09-13/1536821899_847207864.jpg","introduce":"在上世纪30年代，延安的新秧歌在陕北黄土地上呈现出繁荣之势，并因此而迅速发展。延安新秧歌运动在抗战时期的繁盛是与当时的社会、历史、政治、区域性等因素密切联系在一起的。新秧歌运动使秧歌这一陕北特色的文化随社会变迁得以创新与发展。陕北秧歌有着体育文化的要素，分析这一运动的形成及其内含的文艺形态转型, 对研究民俗体育文化传承与变迁都有着理论指导和实践的意义。","type":"11027","type1":"special_1_14","type2":"special_2_14","type3":"special_3_14","status":"2"},{"id":"12","name":"解密西安事变","subtitle":"扭转时局，国内战争走向抗日民族战争的历史转折","key":"00-0002-02","creater":"白皓","addtime":"2018-09-13 14:22:08","thumbnail":"/Uploads/newsThumbnail/2018-09-18/1537259571_1469312189.png","img":"/Uploads/newsThumbnail/2018-09-13/1536819666_2006127287.jpg","introduce":"中共中央处理\u201c西安事变\u201d的前前后后\n1936年12月12日张学良和杨虎城为了达到劝谏蒋介石改变&quot;攘外必先安内&quot;的既定国策，停止内战，一致抗日的目的，在西安发动&quot;兵谏&quot;。1936年12月25日，在中共中央和周恩来主导下，以蒋介石接受&quot;停止内战，联共抗日&quot;的主张而和平解决。\n西安事变的和平解决为抗日民族统一战线的建立准备了必要的前提，成为由国内战争走向抗日民族战争的转折点。","type":"11027","type1":"special_1_12","type2":"special_2_12","type3":"special_3_12","status":"2"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * menus : [{"id":"11027","title":"重大事件"},{"id":"11028","title":"重点人物"},{"id":"11029","title":"重要机构"},{"id":"12002","title":"重点探索"}]
         * page : {"count":"4","totalPage":1,"page":"1","limit":"10","firstRow":0}
         * subjects : [{"id":"21","name":"1947年，毛泽东为何\"放弃\"延安？","subtitle":"撤离延安的背后：把包袱让给蒋介石背上！","key":"00-0004-04","creater":"白皓","addtime":"2018-09-19 10:45:44","thumbnail":"/Uploads/newsThumbnail/2018-09-19/1537324758_277660646.jpg","img":"/Uploads/newsThumbnail/2018-09-19/1537325141_886554238.jpg","introduce":"我们暂时放弃延安，就是把包袱让给敌人背上，使自己打起仗来更主动，更灵活，这样就能大量消灭敌人，到了一定的时机，再举行反攻，延安就会重新回到我们的手里。\u2014\u2014毛泽东","type":"11027","type1":"special_1_21","type2":"special_2_21","type3":"special_3_21","status":"2"},{"id":"17","name":"红军改编八路军的艰难历程","subtitle":"中国共产党在民族独立解放面前的伟大历史担当","key":"00-0005-05","creater":"白皓","addtime":"2018-09-13 17:15:42","thumbnail":"/Uploads/newsThumbnail/2018-09-13/1536830075_2088549174.jpg","img":"/Uploads/newsThumbnail/2018-09-13/1536830003_339150924.jpg","introduce":"为了促进全国抗战，中共中央从1937年2月开始，同国民党当局就红军改编问题进行谈判。但直到卢沟桥事变后，日本侵略军进攻上海，威胁南京时，国民党当局才与中共中央达成红军改编的协议。1937年8月22日，国民党军事委员会宣布红军主力部队改编为国民革命军第八路军，并同意设立总指挥部，下辖3个师，每师15万人，师辖2个旅，每旅辖2个团。","type":"11027","type1":"special_1_17","type2":"special_2_17","type3":"special_3_17","status":"2"},{"id":"14","name":"延安时期的秧歌运动","subtitle":"从民间旧艺术走向与民族艺术形式","key":"00-0003-03","creater":"白皓","addtime":"2018-09-13 14:52:00","thumbnail":"/Uploads/newsThumbnail/2018-09-13/1536821892_759289461.jpg","img":"/Uploads/newsThumbnail/2018-09-13/1536821899_847207864.jpg","introduce":"在上世纪30年代，延安的新秧歌在陕北黄土地上呈现出繁荣之势，并因此而迅速发展。延安新秧歌运动在抗战时期的繁盛是与当时的社会、历史、政治、区域性等因素密切联系在一起的。新秧歌运动使秧歌这一陕北特色的文化随社会变迁得以创新与发展。陕北秧歌有着体育文化的要素，分析这一运动的形成及其内含的文艺形态转型, 对研究民俗体育文化传承与变迁都有着理论指导和实践的意义。","type":"11027","type1":"special_1_14","type2":"special_2_14","type3":"special_3_14","status":"2"},{"id":"12","name":"解密西安事变","subtitle":"扭转时局，国内战争走向抗日民族战争的历史转折","key":"00-0002-02","creater":"白皓","addtime":"2018-09-13 14:22:08","thumbnail":"/Uploads/newsThumbnail/2018-09-18/1537259571_1469312189.png","img":"/Uploads/newsThumbnail/2018-09-13/1536819666_2006127287.jpg","introduce":"中共中央处理\u201c西安事变\u201d的前前后后\n1936年12月12日张学良和杨虎城为了达到劝谏蒋介石改变&quot;攘外必先安内&quot;的既定国策，停止内战，一致抗日的目的，在西安发动&quot;兵谏&quot;。1936年12月25日，在中共中央和周恩来主导下，以蒋介石接受&quot;停止内战，联共抗日&quot;的主张而和平解决。\n西安事变的和平解决为抗日民族统一战线的建立准备了必要的前提，成为由国内战争走向抗日民族战争的转折点。","type":"11027","type1":"special_1_12","type2":"special_2_12","type3":"special_3_12","status":"2"}]
         */

        private PageBean page;
        private List<MenusBean> menus;
        private List<SubjectsBean> subjects;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<MenusBean> getMenus() {
            return menus;
        }

        public void setMenus(List<MenusBean> menus) {
            this.menus = menus;
        }

        public List<SubjectsBean> getSubjects() {
            return subjects;
        }

        public void setSubjects(List<SubjectsBean> subjects) {
            this.subjects = subjects;
        }

        public static class PageBean {
            /**
             * count : 4
             * totalPage : 1
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }
        }

        public static class MenusBean {
            /**
             * id : 11027
             * title : 重大事件
             */

            private String id;
            private String title;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class SubjectsBean {
            /**
             * id : 21
             * name : 1947年，毛泽东为何"放弃"延安？
             * subtitle : 撤离延安的背后：把包袱让给蒋介石背上！
             * key : 00-0004-04
             * creater : 白皓
             * addtime : 2018-09-19 10:45:44
             * thumbnail : /Uploads/newsThumbnail/2018-09-19/1537324758_277660646.jpg
             * img : /Uploads/newsThumbnail/2018-09-19/1537325141_886554238.jpg
             * introduce : 我们暂时放弃延安，就是把包袱让给敌人背上，使自己打起仗来更主动，更灵活，这样就能大量消灭敌人，到了一定的时机，再举行反攻，延安就会重新回到我们的手里。——毛泽东
             * type : 11027
             * type1 : special_1_21
             * type2 : special_2_21
             * type3 : special_3_21
             * status : 2
             */

            private String id;
            private String name;
            private String subtitle;
            private String key;
            private String creater;
            private String addtime;
            private String thumbnail;
            private String img;
            private String introduce;
            private String type;
            private String type1;
            private String type2;
            private String type3;
            private String status;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getType1() {
                return type1;
            }

            public void setType1(String type1) {
                this.type1 = type1;
            }

            public String getType2() {
                return type2;
            }

            public void setType2(String type2) {
                this.type2 = type2;
            }

            public String getType3() {
                return type3;
            }

            public void setType3(String type3) {
                this.type3 = type3;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
