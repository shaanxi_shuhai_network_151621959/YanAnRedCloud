package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/27 20:18
 * E_Mail：yh131412hys@163.com
 * Describe：地区列表
 * Change：
 * @Version：V1.0
 */
public class RevolutionMemorialType {

    /**
     * code : 0000
     * data : {"era":[{"id":"3084","name":"newsList","title":"纪念活动","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3051","name":"newsList","title":"土地革命时期","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3052","name":"newsList","title":"抗日战争","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3053","name":"newsList","title":"长征路上","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3054","name":"newsList","title":"解放战争","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3055","name":"newsList","title":"社会主义建设","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3056","name":"newsList","title":"朝鲜战争","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3057","name":"newsList","title":"越南战争","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3058","name":"newsList","title":"重要会议","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null},{"id":"3059","name":"newsList","title":"场馆资讯","status":"1","remark":null,"sort":"0","pid":"3050","level":"3","parentid":"3050","icon":"","isshow":"1","frontfolder":null}],"area":["全部地区","东北地区","西北地区","西南地区","华南地区","华中地区","华东地区","华北地区","海外地区"]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        private List<EraBean> era;
        private List<String> area;

        public List<EraBean> getEra() {
            return era;
        }

        public void setEra(List<EraBean> era) {
            this.era = era;
        }

        public List<String> getArea() {
            return area;
        }

        public void setArea(List<String> area) {
            this.area = area;
        }

        public static class EraBean {
            /**
             * id : 3084
             * name : newsList
             * title : 纪念活动
             * status : 1
             * remark : null
             * sort : 0
             * pid : 3050
             * level : 3
             * parentid : 3050
             * icon :
             * isshow : 1
             * frontfolder : null
             */

            private String id;
            private String name;
            private String title;
            private String status;
            private Object remark;
            private String sort;
            private String pid;
            private String level;
            private String parentid;
            private String icon;
            private String isshow;
            private Object frontfolder;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getPid() {
                return pid;
            }

            public void setPid(String pid) {
                this.pid = pid;
            }

            public String getLevel() {
                return level;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public String getIsshow() {
                return isshow;
            }

            public void setIsshow(String isshow) {
                this.isshow = isshow;
            }

            public Object getFrontfolder() {
                return frontfolder;
            }

            public void setFrontfolder(Object frontfolder) {
                this.frontfolder = frontfolder;
            }
        }
    }
}
