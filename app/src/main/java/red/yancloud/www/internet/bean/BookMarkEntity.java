package red.yancloud.www.internet.bean;

import android.os.Parcel;
import android.os.Parcelable;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/23 14:49
 * E_Mail：yh131412hys@163.com
 * Describe：书签
 * Change：
 * @Version：V1.0
 */
@Entity
public class BookMarkEntity implements Parcelable {

    @Id
    private Long id;
    /**
     * 书的id
     */
    private Long bookId;
    /**
     * 书章节号
     */
    private int chapterId;
    /**
     * 记录ePup最后阅读位置
     */
    private int scrollY;
    /**
     * 书签内容
     */
    private String content;
    /**
     * 时间
     */
    private long time;
    /**
     * 章节名
     */
    private String chapterName;

    @Transient
    public boolean isCheck;

    @Generated(hash = 133243221)
    public BookMarkEntity(Long id, Long bookId, int chapterId, int scrollY,
            String content, long time, String chapterName) {
        this.id = id;
        this.bookId = bookId;
        this.chapterId = chapterId;
        this.scrollY = scrollY;
        this.content = content;
        this.time = time;
        this.chapterName = chapterName;
    }

    @Generated(hash = 1463584955)
    public BookMarkEntity() {
    }

    public BookMarkEntity(Long bookId, int chapterId, int scrollY, String content, long time, String chapterName) {
        this.bookId = bookId;
        this.chapterId = chapterId;
        this.scrollY = scrollY;
        this.content = content;
        this.time = time;
        this.chapterName = chapterName;
    }

    protected BookMarkEntity(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        if (in.readByte() == 0) {
            bookId = null;
        } else {
            bookId = in.readLong();
        }
        chapterId = in.readInt();
        scrollY = in.readInt();
        content = in.readString();
        time = in.readLong();
        chapterName = in.readString();
        isCheck = in.readByte() != 0;
    }

    public static final Creator<BookMarkEntity> CREATOR = new Creator<BookMarkEntity>() {
        @Override
        public BookMarkEntity createFromParcel(Parcel in) {
            return new BookMarkEntity(in);
        }

        @Override
        public BookMarkEntity[] newArray(int size) {
            return new BookMarkEntity[size];
        }
    };

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBookId() {
        return this.bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public int getChapterId() {
        return this.chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getScrollY() {
        return this.scrollY;
    }

    public void setScrollY(int scrollY) {
        this.scrollY = scrollY;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getChapterName() {
        return this.chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        if (bookId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(bookId);
        }
        dest.writeInt(chapterId);
        dest.writeInt(scrollY);
        dest.writeString(content);
        dest.writeLong(time);
        dest.writeString(chapterName);
        dest.writeByte((byte) (isCheck ? 1 : 0));
    }
}
