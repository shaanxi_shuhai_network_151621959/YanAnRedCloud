package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/17 20:17
 * E_Mail：yh131412hys@163.com
 * Describe：
 * Change：
 * @Version：V1.0
 */
public class ReadBook {

    /**
     * code : 0000
     * data : {"page":{"count":"575","totalPage":58,"page":"1","limit":"10","firstRow":0},"books":[{"id":"1164","creater":"admin","bookpath":"15605023918019","tid":"2210","title":"人民不会忘记（下）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502923_1279978986.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"","publicationdate":"","publisher":"","price":"","content":" ","addtime":"2019-06-14 16:53:12","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1163","creater":"admin","bookpath":"15605023913863","tid":"2210","title":"人民不会忘记（上）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502941_1323525361.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"","publicationdate":"","publisher":"","price":"","content":"  ","addtime":"2019-06-14 16:53:11","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1162","creater":"admin","bookpath":"15604912922749","tid":"2210","title":"将台堡会师","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491345_1877231747.jpg","seriesname":"","vicetitle":"","author":"马正文 司玉国","authordes":"","isbn":"227-04552-6","publicationdate":"2010-09-07","publisher":"宁夏人民出版社","price":"38","content":"六十年前，中国工农红军第一、第二、第四方面军，经过艰苦卓绝的万里长征，在会宁和将台堡胜利会师，宣告国民党反动派消灭共产党和红军的图谋彻底失败，宣告中国共产党和红军肩负民族希望胜利实现了北上抗日的战略转移。长征，是历史上无与伦比的革命壮举，中国共产党及其领导的工农红军创造的人间奇迹，是中华民族一部惊天动地的英雄史诗。长征将永远铭刻在中国革命的丰碑上。","addtime":"2019-06-14 13:48:12","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1161","creater":"admin","bookpath":"1560488970584","tid":"9964","title":"党的光辉思想与优良传统：上卷","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490408_765148588.jpg","seriesname":"","vicetitle":"","author":"中共宁夏回族自治区委员会党史研究室宁夏中共党史学会编著","authordes":"","isbn":"227-05782-6","publicationdate":"2014-07-19","publisher":"宁夏人民出版社","price":"78","content":"以人为本、执政为民以人为本、执政为民是我们党的性质和全心全意为人民服务根本宗旨的集中体现、时代要求，是指引、评价、检验我们党一切执政活动的最高标准。来自人民、植根人民、服务人民，是我们党永远立于不败之地的根本。必须始终把人民利益放在第一位，切实把以人为本、执政为民的基本要求转化为自觉行动，贯彻落实到各项工作中去，使我们党的事业和中国特色社会主义伟大事业获得最广泛最可靠最牢固的群众基础和力量源泉，为实现中华民族伟大复兴的中国梦提供坚强的政治保证。","addtime":"2019-06-14 13:09:30","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"政论","bookniandai":"","bookredian":""},{"id":"1158","creater":"admin","bookpath":"156048871554","tid":"2210","title":"天高云淡 红军长征西征在宁夏","thumbnail":"/Uploads/newsThumbnail//1560490874_1097616415.png","seriesname":"","vicetitle":"","author":"李云桥","authordes":"","isbn":"227-03026-1","publicationdate":"2005-09-05","publisher":"宁夏人民出版社","price":"40","content":"中国工农红军二万五千里长征，是震惊世界的壮举。中国工农红军以大元畏的英雄气概走过万水千山，走过了雪山草地，战胜了国民党几十万大军的围追堵截，终于胜利到达陕甘宁地区。长征所表现出来的革命英雄主义精神，是中国人民宝贵的精神财富。红军在宁夏六盘山区的足迹，同样在中国革命史上、中国工农红军史上留下了光辉灿烂的壮丽篇章。","addtime":"2019-06-14 13:05:15","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"","bookniandai":"","bookredian":""},{"id":"1159","creater":"admin","bookpath":"15604887155106","tid":"2210","title":"安危文集（英文部分）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491009_734246298.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"978-7-5112-6039-0","publicationdate":"2014-03-20","publisher":"光明日报出版社","price":"68","content":"The Hitherto Untold Role of Helen Snowin the Chinese Industrial Cooperative Movement\n","addtime":"2019-06-14 13:05:15","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1155","creater":"admin","bookpath":"15604887148500","tid":"2210","title":"新西部随笔","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490440_719798038.jpg","seriesname":"","vicetitle":"","author":"刘汉立","authordes":"","isbn":"227-05221-0","publicationdate":"2012-06-04","publisher":"宁夏人民出版社","price":"30","content":"辽阔无垠的西部，美丽富饶的西部，神秘厚重的西部，中华民族的发祥地，祖国母亲河的源头。科学发展，以人为本，和谐共进。啊！新西部，正掀起了开发热潮。啊！新西部，我们期待你重振辉煌。山青翠，水长流，满眼春色新，强国富民安天下，安天下，民族大团结亲如一家人，亲如一家人。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1156","creater":"admin","bookpath":"15604887143651","tid":"2210","title":"圣地铸魂","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491506_823946311.png","seriesname":"","vicetitle":"","author":"田伏虎主编","authordes":"","isbn":"978-7-5450-4791-2","publicationdate":"2016-06-19","publisher":"陕西人民教育出版社","price":"","content":"新时期,延安大学立足革命圣地红色资源，秉承\u201c立身为公学以致用\u201d的校训，坚持\u201c用延安精神办学育人，理论实践并重，学用一致，造就品德优、业务精人才\u201d的办学理念，铸就了\u201c艰苦奋斗、自强不息、扎根老区、乐于奉献\u201d的延大精神，形成了\u201c严谨、质朴、求实、创新\u201d的校风和\u201c勤学、善思、明德、笃行\u201d的学风，为祖国各地特别是为服务西部、陕西、陕北老区经济和社会发展培养了大批\u201c基础厚实、为人诚实、作风务实、工作扎实\u201d的优秀人才，探索出一套符合学校实际、富有地域特色的立德树人\u201c圣地铸魂\u201d育人模式。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1157","creater":"admin","bookpath":"1560488714379","tid":"2210","title":"勿忘历史：抗日战争回忆录","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560489381_2114357873.jpg","seriesname":"","vicetitle":"","author":"王树申","authordes":"","isbn":"227-02955","publicationdate":"2011-12-04","publisher":"宁夏人民出版社","price":"16","content":"在抗日战争和世界反法西斯战争胜利60周年之际，银川市离休干部党委组织编写了《勿忘历史》一书，是我们银川市参加过抗日战争的老同志撰写的回忆录。在中华民族正走向和平崛起、国际形势复杂多变的今天，编写这样一本书，具有重大的现实意义。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"","bookniandai":"","bookredian":""},{"id":"1153","creater":"admin","bookpath":"15604887075640","tid":"2210","title":"红军长征在宁夏","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490533_1956372859.jpg","seriesname":"","vicetitle":"","author":"宁夏回族自治区档案馆","authordes":"","isbn":"227-06595-1","publicationdate":"2016-12-19","publisher":"宁夏人民出版社","price":"68","content":"2016年10月22日，是中国工农红军长征胜利80周年纪念日。打开尘封的历史，那一件件承载着信念和追求的档案，真实地还原了上世纪三十年代中国共产党领导的那场惊天地、泣鬼魂的英雄壮举，仿佛把我们带回那如火如荼的战斗岁月。","addtime":"2019-06-14 13:05:08","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"7","bookcon":"","bookniandai":"","bookredian":""}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"575","totalPage":58,"page":"1","limit":"10","firstRow":0}
         * books : [{"id":"1164","creater":"admin","bookpath":"15605023918019","tid":"2210","title":"人民不会忘记（下）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502923_1279978986.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"","publicationdate":"","publisher":"","price":"","content":" ","addtime":"2019-06-14 16:53:12","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1163","creater":"admin","bookpath":"15605023913863","tid":"2210","title":"人民不会忘记（上）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502941_1323525361.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"","publicationdate":"","publisher":"","price":"","content":"  ","addtime":"2019-06-14 16:53:11","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1162","creater":"admin","bookpath":"15604912922749","tid":"2210","title":"将台堡会师","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491345_1877231747.jpg","seriesname":"","vicetitle":"","author":"马正文 司玉国","authordes":"","isbn":"227-04552-6","publicationdate":"2010-09-07","publisher":"宁夏人民出版社","price":"38","content":"六十年前，中国工农红军第一、第二、第四方面军，经过艰苦卓绝的万里长征，在会宁和将台堡胜利会师，宣告国民党反动派消灭共产党和红军的图谋彻底失败，宣告中国共产党和红军肩负民族希望胜利实现了北上抗日的战略转移。长征，是历史上无与伦比的革命壮举，中国共产党及其领导的工农红军创造的人间奇迹，是中华民族一部惊天动地的英雄史诗。长征将永远铭刻在中国革命的丰碑上。","addtime":"2019-06-14 13:48:12","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1161","creater":"admin","bookpath":"1560488970584","tid":"9964","title":"党的光辉思想与优良传统：上卷","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490408_765148588.jpg","seriesname":"","vicetitle":"","author":"中共宁夏回族自治区委员会党史研究室宁夏中共党史学会编著","authordes":"","isbn":"227-05782-6","publicationdate":"2014-07-19","publisher":"宁夏人民出版社","price":"78","content":"以人为本、执政为民以人为本、执政为民是我们党的性质和全心全意为人民服务根本宗旨的集中体现、时代要求，是指引、评价、检验我们党一切执政活动的最高标准。来自人民、植根人民、服务人民，是我们党永远立于不败之地的根本。必须始终把人民利益放在第一位，切实把以人为本、执政为民的基本要求转化为自觉行动，贯彻落实到各项工作中去，使我们党的事业和中国特色社会主义伟大事业获得最广泛最可靠最牢固的群众基础和力量源泉，为实现中华民族伟大复兴的中国梦提供坚强的政治保证。","addtime":"2019-06-14 13:09:30","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"政论","bookniandai":"","bookredian":""},{"id":"1158","creater":"admin","bookpath":"156048871554","tid":"2210","title":"天高云淡 红军长征西征在宁夏","thumbnail":"/Uploads/newsThumbnail//1560490874_1097616415.png","seriesname":"","vicetitle":"","author":"李云桥","authordes":"","isbn":"227-03026-1","publicationdate":"2005-09-05","publisher":"宁夏人民出版社","price":"40","content":"中国工农红军二万五千里长征，是震惊世界的壮举。中国工农红军以大元畏的英雄气概走过万水千山，走过了雪山草地，战胜了国民党几十万大军的围追堵截，终于胜利到达陕甘宁地区。长征所表现出来的革命英雄主义精神，是中国人民宝贵的精神财富。红军在宁夏六盘山区的足迹，同样在中国革命史上、中国工农红军史上留下了光辉灿烂的壮丽篇章。","addtime":"2019-06-14 13:05:15","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"","bookniandai":"","bookredian":""},{"id":"1159","creater":"admin","bookpath":"15604887155106","tid":"2210","title":"安危文集（英文部分）","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491009_734246298.png","seriesname":"","vicetitle":"","author":"安危","authordes":"","isbn":"978-7-5112-6039-0","publicationdate":"2014-03-20","publisher":"光明日报出版社","price":"68","content":"The Hitherto Untold Role of Helen Snowin the Chinese Industrial Cooperative Movement\n","addtime":"2019-06-14 13:05:15","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1155","creater":"admin","bookpath":"15604887148500","tid":"2210","title":"新西部随笔","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490440_719798038.jpg","seriesname":"","vicetitle":"","author":"刘汉立","authordes":"","isbn":"227-05221-0","publicationdate":"2012-06-04","publisher":"宁夏人民出版社","price":"30","content":"辽阔无垠的西部，美丽富饶的西部，神秘厚重的西部，中华民族的发祥地，祖国母亲河的源头。科学发展，以人为本，和谐共进。啊！新西部，正掀起了开发热潮。啊！新西部，我们期待你重振辉煌。山青翠，水长流，满眼春色新，强国富民安天下，安天下，民族大团结亲如一家人，亲如一家人。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1156","creater":"admin","bookpath":"15604887143651","tid":"2210","title":"圣地铸魂","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560491506_823946311.png","seriesname":"","vicetitle":"","author":"田伏虎主编","authordes":"","isbn":"978-7-5450-4791-2","publicationdate":"2016-06-19","publisher":"陕西人民教育出版社","price":"","content":"新时期,延安大学立足革命圣地红色资源，秉承\u201c立身为公学以致用\u201d的校训，坚持\u201c用延安精神办学育人，理论实践并重，学用一致，造就品德优、业务精人才\u201d的办学理念，铸就了\u201c艰苦奋斗、自强不息、扎根老区、乐于奉献\u201d的延大精神，形成了\u201c严谨、质朴、求实、创新\u201d的校风和\u201c勤学、善思、明德、笃行\u201d的学风，为祖国各地特别是为服务西部、陕西、陕北老区经济和社会发展培养了大批\u201c基础厚实、为人诚实、作风务实、工作扎实\u201d的优秀人才，探索出一套符合学校实际、富有地域特色的立德树人\u201c圣地铸魂\u201d育人模式。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"0","bookcon":"","bookniandai":"","bookredian":""},{"id":"1157","creater":"admin","bookpath":"1560488714379","tid":"2210","title":"勿忘历史：抗日战争回忆录","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560489381_2114357873.jpg","seriesname":"","vicetitle":"","author":"王树申","authordes":"","isbn":"227-02955","publicationdate":"2011-12-04","publisher":"宁夏人民出版社","price":"16","content":"在抗日战争和世界反法西斯战争胜利60周年之际，银川市离休干部党委组织编写了《勿忘历史》一书，是我们银川市参加过抗日战争的老同志撰写的回忆录。在中华民族正走向和平崛起、国际形势复杂多变的今天，编写这样一本书，具有重大的现实意义。","addtime":"2019-06-14 13:05:14","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"1","bookcon":"","bookniandai":"","bookredian":""},{"id":"1153","creater":"admin","bookpath":"15604887075640","tid":"2210","title":"红军长征在宁夏","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560490533_1956372859.jpg","seriesname":"","vicetitle":"","author":"宁夏回族自治区档案馆","authordes":"","isbn":"227-06595-1","publicationdate":"2016-12-19","publisher":"宁夏人民出版社","price":"68","content":"2016年10月22日，是中国工农红军长征胜利80周年纪念日。打开尘封的历史，那一件件承载着信念和追求的档案，真实地还原了上世纪三十年代中国共产党领导的那场惊天地、泣鬼魂的英雄壮举，仿佛把我们带回那如火如荼的战斗岁月。","addtime":"2019-06-14 13:05:08","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"7","bookcon":"","bookniandai":"","bookredian":""}]
         */

        private PageBean page;
        private List<BooksBean> books;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<BooksBean> getBooks() {
            return books;
        }

        public void setBooks(List<BooksBean> books) {
            this.books = books;
        }

        public static class PageBean {
            /**
             * count : 575
             * totalPage : 58
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }
        }

        public static class BooksBean {
            /**
             * id : 1164
             * creater : admin
             * bookpath : 15605023918019
             * tid : 2210
             * title : 人民不会忘记（下）
             * thumbnail : /Uploads/newsThumbnail/2019-06-14/1560502923_1279978986.png
             * seriesname :
             * vicetitle :
             * author : 安危
             * authordes :
             * isbn :
             * publicationdate :
             * publisher :
             * price :
             * content :
             * addtime : 2019-06-14 16:53:12
             * status : 2
             * type : null
             * deleteflg : 1
             * settop : 1
             * dot : 0
             * bookcon :
             * bookniandai :
             * bookredian :
             */

            private String id;
            private String creater;
            private String bookpath;
            private String tid;
            private String title;
            private String thumbnail;
            private String seriesname;
            private String vicetitle;
            private String author;
            private String authordes;
            private String isbn;
            private String publicationdate;
            private String publisher;
            private String price;
            private String content;
            private String addtime;
            private String status;
            private Object type;
            private String deleteflg;
            private String settop;
            private String dot;
            private String bookcon;
            private String bookniandai;
            private String bookredian;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getBookpath() {
                return bookpath;
            }

            public void setBookpath(String bookpath) {
                this.bookpath = bookpath;
            }

            public String getTid() {
                return tid;
            }

            public void setTid(String tid) {
                this.tid = tid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getSeriesname() {
                return seriesname;
            }

            public void setSeriesname(String seriesname) {
                this.seriesname = seriesname;
            }

            public String getVicetitle() {
                return vicetitle;
            }

            public void setVicetitle(String vicetitle) {
                this.vicetitle = vicetitle;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getAuthordes() {
                return authordes;
            }

            public void setAuthordes(String authordes) {
                this.authordes = authordes;
            }

            public String getIsbn() {
                return isbn;
            }

            public void setIsbn(String isbn) {
                this.isbn = isbn;
            }

            public String getPublicationdate() {
                return publicationdate;
            }

            public void setPublicationdate(String publicationdate) {
                this.publicationdate = publicationdate;
            }

            public String getPublisher() {
                return publisher;
            }

            public void setPublisher(String publisher) {
                this.publisher = publisher;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getType() {
                return type;
            }

            public void setType(Object type) {
                this.type = type;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getDot() {
                return dot;
            }

            public void setDot(String dot) {
                this.dot = dot;
            }

            public String getBookcon() {
                return bookcon;
            }

            public void setBookcon(String bookcon) {
                this.bookcon = bookcon;
            }

            public String getBookniandai() {
                return bookniandai;
            }

            public void setBookniandai(String bookniandai) {
                this.bookniandai = bookniandai;
            }

            public String getBookredian() {
                return bookredian;
            }

            public void setBookredian(String bookredian) {
                this.bookredian = bookredian;
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", books=" + books +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ReadBook{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
