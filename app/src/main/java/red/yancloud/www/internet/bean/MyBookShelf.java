package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/11 18:33
 * E_Mail：yh131412hys@163.com
 * Describe：我的书架
 * Change：
 * @Version：V1.0
 */
public class MyBookShelf {
    /**
     * code : 0000
     * data : {"page":{"count":"17","totalPage":2,"page":"1","limit":"10","firstRow":0},"records":[{"url":"http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html","id":"82","userid":"137","bookid":"1163","bookchapterid":"-1","jindu":null,"time":"1562722987","title":"人民不会忘记（上）","author":"安危","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502941_1323525361.png"},{"url":"http://www.yancloud.red/Uploads/book/15605023918019/OEBPS/Text/pdf_frame.html","id":"81","userid":"137","bookid":"1164","bookchapterid":"-1","jindu":null,"time":"1562722954","title":"人民不会忘记（下）","author":"安危","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502923_1279978986.png"},{"url":"http://www.yancloud.red/Uploads/book/15555541697886/OEBPS/Text/pdf_frame.html","id":"80","userid":"137","bookid":"700","bookchapterid":"-1","jindu":null,"time":"1562722385","title":"红色名媛 章含之","author":"罗银胜","thumbnail":"/Uploads/newsThumbnail/2019-04-28/1556429987_1110536899.jpg"},{"url":"","id":"57","userid":"137","bookid":"767","bookchapterid":"16678","jindu":"0.00","time":"1562582072","title":"难忘峥嵘岁月","author":"何光权","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg"},{"url":"http://www.yancloud.red/Uploads/book/15565896441254/OEBPS/Text/pdf_frame.html","id":"70","userid":"137","bookid":"927","bookchapterid":"-1","jindu":null,"time":"1562074953","title":"哈里森.福尔曼的中国摄影集7","author":"Harrison Forman","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png"},{"url":"http://www.yancloud.red/Uploads/book/1555664916698/OEBPS/Text/pdf_frame.html","id":"55","userid":"137","bookid":"745","bookchapterid":"-1","jindu":null,"time":"1560424188","title":"追寻红色足迹固原革命遗址览胜","author":"田俊秀","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556365734_1257838559.jpg"},{"url":"","id":"54","userid":"137","bookid":"418","bookchapterid":"14712","jindu":null,"time":"1560424182","title":"日本老兵忏悔录","author":"星彻","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553157649_1831994366.jpg"},{"url":"","id":"53","userid":"137","bookid":"415","bookchapterid":"14680","jindu":null,"time":"1560424177","title":"南京血祭","author":"阿垅","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553141024_283085944.jpg"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"17","totalPage":2,"page":"1","limit":"10","firstRow":0}
         * records : [{"url":"http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html","id":"82","userid":"137","bookid":"1163","bookchapterid":"-1","jindu":null,"time":"1562722987","title":"人民不会忘记（上）","author":"安危","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502941_1323525361.png"},{"url":"http://www.yancloud.red/Uploads/book/15605023918019/OEBPS/Text/pdf_frame.html","id":"81","userid":"137","bookid":"1164","bookchapterid":"-1","jindu":null,"time":"1562722954","title":"人民不会忘记（下）","author":"安危","thumbnail":"/Uploads/newsThumbnail/2019-06-14/1560502923_1279978986.png"},{"url":"http://www.yancloud.red/Uploads/book/15555541697886/OEBPS/Text/pdf_frame.html","id":"80","userid":"137","bookid":"700","bookchapterid":"-1","jindu":null,"time":"1562722385","title":"红色名媛 章含之","author":"罗银胜","thumbnail":"/Uploads/newsThumbnail/2019-04-28/1556429987_1110536899.jpg"},{"url":"","id":"57","userid":"137","bookid":"767","bookchapterid":"16678","jindu":"0.00","time":"1562582072","title":"难忘峥嵘岁月","author":"何光权","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556363267_966067714.jpg"},{"url":"http://www.yancloud.red/Uploads/book/15565896441254/OEBPS/Text/pdf_frame.html","id":"70","userid":"137","bookid":"927","bookchapterid":"-1","jindu":null,"time":"1562074953","title":"哈里森.福尔曼的中国摄影集7","author":"Harrison Forman","thumbnail":"/Uploads/newsThumbnail/2019-04-30/1556610010_1157892462.png"},{"url":"http://www.yancloud.red/Uploads/book/1555664916698/OEBPS/Text/pdf_frame.html","id":"55","userid":"137","bookid":"745","bookchapterid":"-1","jindu":null,"time":"1560424188","title":"追寻红色足迹固原革命遗址览胜","author":"田俊秀","thumbnail":"/Uploads/newsThumbnail/2019-04-27/1556365734_1257838559.jpg"},{"url":"","id":"54","userid":"137","bookid":"418","bookchapterid":"14712","jindu":null,"time":"1560424182","title":"日本老兵忏悔录","author":"星彻","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553157649_1831994366.jpg"},{"url":"","id":"53","userid":"137","bookid":"415","bookchapterid":"14680","jindu":null,"time":"1560424177","title":"南京血祭","author":"阿垅","thumbnail":"/Uploads/newsThumbnail/2019-03-21/1553141024_283085944.jpg"}]
         */

        private PageBean page;
        private List<RecordsBean> records;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class PageBean {
            /**
             * count : 17
             * totalPage : 2
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class RecordsBean {
            /**
             * url : http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html
             * id : 82
             * userid : 137
             * bookid : 1163
             * bookchapterid : -1
             * jindu : null
             * time : 1562722987
             * title : 人民不会忘记（上）
             * author : 安危
             * thumbnail : /Uploads/newsThumbnail/2019-06-14/1560502941_1323525361.png
             */

            private String url;
            private String id;
            private String userid;
            private String bookid;
            private String bookchapterid;
            private String jindu;
            private String time;
            private String title;
            private String author;
            private String thumbnail;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getBookid() {
                return bookid;
            }

            public void setBookid(String bookid) {
                this.bookid = bookid;
            }

            public String getBookchapterid() {
                return bookchapterid;
            }

            public void setBookchapterid(String bookchapterid) {
                this.bookchapterid = bookchapterid;
            }

            public String getJindu() {
                return jindu;
            }

            public void setJindu(String jindu) {
                this.jindu = jindu;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            @Override
            public String toString() {
                return "RecordsBean{" +
                        "url='" + url + '\'' +
                        ", id='" + id + '\'' +
                        ", userid='" + userid + '\'' +
                        ", bookid='" + bookid + '\'' +
                        ", bookchapterid='" + bookchapterid + '\'' +
                        ", jindu=" + jindu +
                        ", time='" + time + '\'' +
                        ", title='" + title + '\'' +
                        ", author='" + author + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", records=" + records +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MyBookShelf{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
