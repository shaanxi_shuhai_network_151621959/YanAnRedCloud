package red.yancloud.www.internet.bean;

import org.greenrobot.greendao.annotation.Transient;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/25 21:02
 * E_Mail：yh131412hys@163.com
 * Describe：书籍详情
 * Change：
 * @Version：V1.0
 */
public class BookInfo {

    /**
     * code : 0000
     * data : {"book":{"id":"1161","creater":"admin","bookpath":"1560488970584","tid":"9964","title":"党的光辉思想与优良传统：上卷","thumbnail":"http://www.yancloud.red/Uploads/newsThumbnail/2019-06-14/1560490408_765148588.jpg","seriesname":"","vicetitle":"","author":"中共宁夏回族自治区委员会党史研究室宁夏中共党史学会编著","authordes":"","isbn":"227-05782-6","publicationdate":"2014-07-19","publisher":"宁夏人民出版社","price":"78","content":"以人为本、执政为民以人为本、执政为民是我们党的性质和全心全意为人民服务根本宗旨的集中体现、时代要求，是指引、评价、检验我们党一切执政活动的最高标准。来自人民、植根人民、服务人民，是我们党永远立于不败之地的根本。必须始终把人民利益放在第一位，切实把以人为本、执政为民的基本要求转化为自觉行动，贯彻落实到各项工作中去，使我们党的事业和中国特色社会主义伟大事业获得最广泛最可靠最牢固的群众基础和力量源泉，为实现中华民族伟大复兴的中国梦提供坚强的政治保证。","addtime":"2019-06-14 13:09:30","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"2","bookcon":"政论","bookniandai":"","bookredian":"","userlevel":"0","bookchapterlevel":"1","lastchapter":{"id":"23383","bookid":"1161","level":"1","parentid":"0","title":"【科学内涵】","path":"/book/1560488970584/OEBPS/Text/chapter01.xhtml","isfree":"0","deleteflg":"1","dot":"2"},"firstbookchapterid":"23383","firstbookpdfurl":"http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html"},"typeinfo":{"id":"9964","name":"bookList","title":"政论","status":"1","remark":null,"sort":"0","pid":"2200","level":"3","parentid":"2200","icon":null,"isshow":"1","frontfolder":null},"bookcaseState":0}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * book : {"id":"1161","creater":"admin","bookpath":"1560488970584","tid":"9964","title":"党的光辉思想与优良传统：上卷","thumbnail":"http://www.yancloud.red/Uploads/newsThumbnail/2019-06-14/1560490408_765148588.jpg","seriesname":"","vicetitle":"","author":"中共宁夏回族自治区委员会党史研究室宁夏中共党史学会编著","authordes":"","isbn":"227-05782-6","publicationdate":"2014-07-19","publisher":"宁夏人民出版社","price":"78","content":"以人为本、执政为民以人为本、执政为民是我们党的性质和全心全意为人民服务根本宗旨的集中体现、时代要求，是指引、评价、检验我们党一切执政活动的最高标准。来自人民、植根人民、服务人民，是我们党永远立于不败之地的根本。必须始终把人民利益放在第一位，切实把以人为本、执政为民的基本要求转化为自觉行动，贯彻落实到各项工作中去，使我们党的事业和中国特色社会主义伟大事业获得最广泛最可靠最牢固的群众基础和力量源泉，为实现中华民族伟大复兴的中国梦提供坚强的政治保证。","addtime":"2019-06-14 13:09:30","status":"2","type":null,"deleteflg":"1","settop":"1","dot":"2","bookcon":"政论","bookniandai":"","bookredian":"","userlevel":"0","bookchapterlevel":"1","lastchapter":{"id":"23383","bookid":"1161","level":"1","parentid":"0","title":"【科学内涵】","path":"/book/1560488970584/OEBPS/Text/chapter01.xhtml","isfree":"0","deleteflg":"1","dot":"2"},"firstbookchapterid":"23383","firstbookpdfurl":"http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html"}
         * typeinfo : {"id":"9964","name":"bookList","title":"政论","status":"1","remark":null,"sort":"0","pid":"2200","level":"3","parentid":"2200","icon":null,"isshow":"1","frontfolder":null}
         * bookcaseState : 0
         */

        private BookBean book;
        private TypeinfoBean typeinfo;
        private int bookcaseState;

        public BookBean getBook() {
            return book;
        }

        public void setBook(BookBean book) {
            this.book = book;
        }

        public TypeinfoBean getTypeinfo() {
            return typeinfo;
        }

        public void setTypeinfo(TypeinfoBean typeinfo) {
            this.typeinfo = typeinfo;
        }

        public int getBookcaseState() {
            return bookcaseState;
        }

        public void setBookcaseState(int bookcaseState) {
            this.bookcaseState = bookcaseState;
        }

        public static class BookBean {
            /**
             * id : 1161
             * creater : admin
             * bookpath : 1560488970584
             * tid : 9964
             * title : 党的光辉思想与优良传统：上卷
             * thumbnail : http://www.yancloud.red/Uploads/newsThumbnail/2019-06-14/1560490408_765148588.jpg
             * seriesname :
             * vicetitle :
             * author : 中共宁夏回族自治区委员会党史研究室宁夏中共党史学会编著
             * authordes :
             * isbn : 227-05782-6
             * publicationdate : 2014-07-19
             * publisher : 宁夏人民出版社
             * price : 78
             * content : 以人为本、执政为民以人为本、执政为民是我们党的性质和全心全意为人民服务根本宗旨的集中体现、时代要求，是指引、评价、检验我们党一切执政活动的最高标准。来自人民、植根人民、服务人民，是我们党永远立于不败之地的根本。必须始终把人民利益放在第一位，切实把以人为本、执政为民的基本要求转化为自觉行动，贯彻落实到各项工作中去，使我们党的事业和中国特色社会主义伟大事业获得最广泛最可靠最牢固的群众基础和力量源泉，为实现中华民族伟大复兴的中国梦提供坚强的政治保证。
             * addtime : 2019-06-14 13:09:30
             * status : 2
             * type : null
             * deleteflg : 1
             * settop : 1
             * dot : 2
             * bookcon : 政论
             * bookniandai :
             * bookredian :
             * userlevel : 0
             * bookchapterlevel : 1
             * lastchapter : {"id":"23383","bookid":"1161","level":"1","parentid":"0","title":"【科学内涵】","path":"/book/1560488970584/OEBPS/Text/chapter01.xhtml","isfree":"0","deleteflg":"1","dot":"2"}
             * firstbookchapterid : 23383
             * firstbookpdfurl : http://www.yancloud.red/Uploads/book/15605023913863/OEBPS/Text/pdf_frame.html
             */

            private String id;
            private String creater;
            private String bookpath;
            private String tid;
            private String title;
            private String thumbnail;
            private String seriesname;
            private String vicetitle;
            private String author;
            private String authordes;
            private String isbn;
            private String publicationdate;
            private String publisher;
            private String price;
            private String content;
            private String addtime;
            private String status;
            private Object type;
            private String deleteflg;
            private String settop;
            private String dot;
            private String bookcon;
            private String bookniandai;
            private String bookredian;
            private String userlevel;
            private String bookchapterlevel;
            private LastchapterBean lastchapter;
            private String firstbookchapterid;
            private String firstbookpdfurl;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getBookpath() {
                return bookpath;
            }

            public void setBookpath(String bookpath) {
                this.bookpath = bookpath;
            }

            public String getTid() {
                return tid;
            }

            public void setTid(String tid) {
                this.tid = tid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getSeriesname() {
                return seriesname;
            }

            public void setSeriesname(String seriesname) {
                this.seriesname = seriesname;
            }

            public String getVicetitle() {
                return vicetitle;
            }

            public void setVicetitle(String vicetitle) {
                this.vicetitle = vicetitle;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public String getAuthordes() {
                return authordes;
            }

            public void setAuthordes(String authordes) {
                this.authordes = authordes;
            }

            public String getIsbn() {
                return isbn;
            }

            public void setIsbn(String isbn) {
                this.isbn = isbn;
            }

            public String getPublicationdate() {
                return publicationdate;
            }

            public void setPublicationdate(String publicationdate) {
                this.publicationdate = publicationdate;
            }

            public String getPublisher() {
                return publisher;
            }

            public void setPublisher(String publisher) {
                this.publisher = publisher;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getType() {
                return type;
            }

            public void setType(Object type) {
                this.type = type;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getDot() {
                return dot;
            }

            public void setDot(String dot) {
                this.dot = dot;
            }

            public String getBookcon() {
                return bookcon;
            }

            public void setBookcon(String bookcon) {
                this.bookcon = bookcon;
            }

            public String getBookniandai() {
                return bookniandai;
            }

            public void setBookniandai(String bookniandai) {
                this.bookniandai = bookniandai;
            }

            public String getBookredian() {
                return bookredian;
            }

            public void setBookredian(String bookredian) {
                this.bookredian = bookredian;
            }

            public String getUserlevel() {
                return userlevel;
            }

            public void setUserlevel(String userlevel) {
                this.userlevel = userlevel;
            }

            public String getBookchapterlevel() {
                return bookchapterlevel;
            }

            public void setBookchapterlevel(String bookchapterlevel) {
                this.bookchapterlevel = bookchapterlevel;
            }

            public LastchapterBean getLastchapter() {
                return lastchapter;
            }

            public void setLastchapter(LastchapterBean lastchapter) {
                this.lastchapter = lastchapter;
            }

            public String getFirstbookchapterid() {
                return firstbookchapterid;
            }

            public void setFirstbookchapterid(String firstbookchapterid) {
                this.firstbookchapterid = firstbookchapterid;
            }

            public String getFirstbookpdfurl() {
                return firstbookpdfurl;
            }

            public void setFirstbookpdfurl(String firstbookpdfurl) {
                this.firstbookpdfurl = firstbookpdfurl;
            }

            public static class LastchapterBean {
                /**
                 * id : 23383
                 * bookid : 1161
                 * level : 1
                 * parentid : 0
                 * title : 【科学内涵】
                 * path : /book/1560488970584/OEBPS/Text/chapter01.xhtml
                 * isfree : 0
                 * deleteflg : 1
                 * dot : 2
                 */

                private String id;
                private String bookid;
                private String level;
                private String parentid;
                private String title;
                private String path;
                private String isfree;
                private String deleteflg;
                private String dot;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getBookid() {
                    return bookid;
                }

                public void setBookid(String bookid) {
                    this.bookid = bookid;
                }

                public String getLevel() {
                    return level;
                }

                public void setLevel(String level) {
                    this.level = level;
                }

                public String getParentid() {
                    return parentid;
                }

                public void setParentid(String parentid) {
                    this.parentid = parentid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getPath() {
                    return path;
                }

                public void setPath(String path) {
                    this.path = path;
                }

                public String getIsfree() {
                    return isfree;
                }

                public void setIsfree(String isfree) {
                    this.isfree = isfree;
                }

                public String getDeleteflg() {
                    return deleteflg;
                }

                public void setDeleteflg(String deleteflg) {
                    this.deleteflg = deleteflg;
                }

                public String getDot() {
                    return dot;
                }

                public void setDot(String dot) {
                    this.dot = dot;
                }

                @Override
                public String toString() {
                    return "LastchapterBean{" +
                            "id='" + id + '\'' +
                            ", bookid='" + bookid + '\'' +
                            ", level='" + level + '\'' +
                            ", parentid='" + parentid + '\'' +
                            ", title='" + title + '\'' +
                            ", path='" + path + '\'' +
                            ", isfree='" + isfree + '\'' +
                            ", deleteflg='" + deleteflg + '\'' +
                            ", dot='" + dot + '\'' +
                            '}';
                }
            }

            @Override
            public String toString() {
                return "BookBean{" +
                        "id='" + id + '\'' +
                        ", creater='" + creater + '\'' +
                        ", bookpath='" + bookpath + '\'' +
                        ", tid='" + tid + '\'' +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", seriesname='" + seriesname + '\'' +
                        ", vicetitle='" + vicetitle + '\'' +
                        ", author='" + author + '\'' +
                        ", authordes='" + authordes + '\'' +
                        ", isbn='" + isbn + '\'' +
                        ", publicationdate='" + publicationdate + '\'' +
                        ", publisher='" + publisher + '\'' +
                        ", price='" + price + '\'' +
                        ", content='" + content + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", status='" + status + '\'' +
                        ", type=" + type +
                        ", deleteflg='" + deleteflg + '\'' +
                        ", settop='" + settop + '\'' +
                        ", dot='" + dot + '\'' +
                        ", bookcon='" + bookcon + '\'' +
                        ", bookniandai='" + bookniandai + '\'' +
                        ", bookredian='" + bookredian + '\'' +
                        ", userlevel='" + userlevel + '\'' +
                        ", bookchapterlevel='" + bookchapterlevel + '\'' +
                        ", lastchapter=" + lastchapter +
                        ", firstbookchapterid='" + firstbookchapterid + '\'' +
                        ", firstbookpdfurl='" + firstbookpdfurl + '\'' +
                        '}';
            }
        }

        public static class TypeinfoBean {
            /**
             * id : 9964
             * name : bookList
             * title : 政论
             * status : 1
             * remark : null
             * sort : 0
             * pid : 2200
             * level : 3
             * parentid : 2200
             * icon : null
             * isshow : 1
             * frontfolder : null
             */

            private String id;
            private String name;
            private String title;
            private String status;
            private Object remark;
            private String sort;
            private String pid;
            private String level;
            private String parentid;
            private Object icon;
            private String isshow;
            private Object frontfolder;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getPid() {
                return pid;
            }

            public void setPid(String pid) {
                this.pid = pid;
            }

            public String getLevel() {
                return level;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public Object getIcon() {
                return icon;
            }

            public void setIcon(Object icon) {
                this.icon = icon;
            }

            public String getIsshow() {
                return isshow;
            }

            public void setIsshow(String isshow) {
                this.isshow = isshow;
            }

            public Object getFrontfolder() {
                return frontfolder;
            }

            public void setFrontfolder(Object frontfolder) {
                this.frontfolder = frontfolder;
            }

            @Override
            public String toString() {
                return "TypeinfoBean{" +
                        "id='" + id + '\'' +
                        ", name='" + name + '\'' +
                        ", title='" + title + '\'' +
                        ", status='" + status + '\'' +
                        ", remark=" + remark +
                        ", sort='" + sort + '\'' +
                        ", pid='" + pid + '\'' +
                        ", level='" + level + '\'' +
                        ", parentid='" + parentid + '\'' +
                        ", icon=" + icon +
                        ", isshow='" + isshow + '\'' +
                        ", frontfolder=" + frontfolder +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "book=" + book +
                    ", typeinfo=" + typeinfo +
                    ", bookcaseState=" + bookcaseState +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "BookInfo{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
