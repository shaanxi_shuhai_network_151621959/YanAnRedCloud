package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/7 19:12
 * E_Mail：yh131412hys@163.com
 * Describe：漫绘延安
 * Change：
 * @Version：V1.0
 */
public class DiffuseYanAn {

    /**
     * code : 0000
     * data : {"menus":[{"id":"9994","title":"历史淘"},{"id":"9995","title":"人物志"},{"id":"9996","title":"趣生活"}],"page":{"count":"19","totalPage":2,"page":"1","limit":"10","firstRow":0},"list":[{"id":"12053","title":"打麻将治肩周炎，开什么玩笑？！","thumbnail":"/Uploads/newsThumbnail/2019-07-18/1563439277_205813574.jpg","introduce":"听毛泽东讲打麻将的大学问","creater":"zhangmz","supportnumber":"3","addtime":"1563439230.000000","headimgurl":null},{"id":"12278","title":"解放军这些事，你真的明白吗？","thumbnail":"/Uploads/newsThumbnail/2019-08-01/1564647640_80777268.jpg","introduce":"幸福生活来之不易，致敬那些最可爱的人","creater":"huyue","supportnumber":"0","addtime":"1564647582.000000","headimgurl":null},{"id":"12277","title":"一分钟了解什么是安塞腰鼓？","thumbnail":"/Uploads/newsThumbnail/2019-08-01/1564622733_1708708452.jpg","introduce":"从陕北山沟走向世界领奖台的安塞腰鼓的成名史","creater":"huyue","supportnumber":"0","addtime":"1564622698.000000","headimgurl":null},{"id":"12249","title":"劳动大学的由来","thumbnail":"/Uploads/newsThumbnail/2019-07-31/1564551309_1747575199.jpg","introduce":"艰苦奋斗仍需继承与发扬，劳动最光荣","creater":"huyue","supportnumber":"0","addtime":"1564551261.000000","headimgurl":null},{"id":"12144","title":"马海德与苏菲的跨国爱情","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564126527_569658185.jpg","introduce":"跨越半个世纪与2个国家的爱情故事","creater":"huyue","supportnumber":"0","addtime":"1564126323.000000","headimgurl":null},{"id":"12143","title":"关于爬山，毛泽东对抗大学员说了这样一段话","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564125315_997465193.jpg","introduce":"毛泽东讲的爬山的大道理","creater":"huyue","supportnumber":"0","addtime":"1564125264.000000","headimgurl":null},{"id":"12141","title":"关向应临终前的心愿","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564110045_1978175866.jpg","introduce":"革命战争时期义海情天的兄弟情","creater":"huyue","supportnumber":"0","addtime":"1564109840.000000","headimgurl":null},{"id":"12139","title":"中国女子大学","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564108572_1353760068.jpg","introduce":"开启革命妇女新纪元，承载理想与希望","creater":"huyue","supportnumber":"0","addtime":"1564108516.000000","headimgurl":null},{"id":"12138","title":"特殊的\u201c开学礼物\u201d","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564108181_1174826976.jpg","introduce":"一起来了解毛泽东送给同学们特殊的\u201c开学礼物\u201d","creater":"huyue","supportnumber":"0","addtime":"1564108149.000000","headimgurl":null},{"id":"12137","title":"三件宝贝","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564107842_1794373545.jpg","introduce":"一起来了解毛泽东送给同学们的三件宝贝","creater":"huyue","supportnumber":"0","addtime":"1564107801.000000","headimgurl":null}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * menus : [{"id":"9994","title":"历史淘"},{"id":"9995","title":"人物志"},{"id":"9996","title":"趣生活"}]
         * page : {"count":"19","totalPage":2,"page":"1","limit":"10","firstRow":0}
         * list : [{"id":"12053","title":"打麻将治肩周炎，开什么玩笑？！","thumbnail":"/Uploads/newsThumbnail/2019-07-18/1563439277_205813574.jpg","introduce":"听毛泽东讲打麻将的大学问","creater":"zhangmz","supportnumber":"3","addtime":"1563439230.000000","headimgurl":null},{"id":"12278","title":"解放军这些事，你真的明白吗？","thumbnail":"/Uploads/newsThumbnail/2019-08-01/1564647640_80777268.jpg","introduce":"幸福生活来之不易，致敬那些最可爱的人","creater":"huyue","supportnumber":"0","addtime":"1564647582.000000","headimgurl":null},{"id":"12277","title":"一分钟了解什么是安塞腰鼓？","thumbnail":"/Uploads/newsThumbnail/2019-08-01/1564622733_1708708452.jpg","introduce":"从陕北山沟走向世界领奖台的安塞腰鼓的成名史","creater":"huyue","supportnumber":"0","addtime":"1564622698.000000","headimgurl":null},{"id":"12249","title":"劳动大学的由来","thumbnail":"/Uploads/newsThumbnail/2019-07-31/1564551309_1747575199.jpg","introduce":"艰苦奋斗仍需继承与发扬，劳动最光荣","creater":"huyue","supportnumber":"0","addtime":"1564551261.000000","headimgurl":null},{"id":"12144","title":"马海德与苏菲的跨国爱情","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564126527_569658185.jpg","introduce":"跨越半个世纪与2个国家的爱情故事","creater":"huyue","supportnumber":"0","addtime":"1564126323.000000","headimgurl":null},{"id":"12143","title":"关于爬山，毛泽东对抗大学员说了这样一段话","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564125315_997465193.jpg","introduce":"毛泽东讲的爬山的大道理","creater":"huyue","supportnumber":"0","addtime":"1564125264.000000","headimgurl":null},{"id":"12141","title":"关向应临终前的心愿","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564110045_1978175866.jpg","introduce":"革命战争时期义海情天的兄弟情","creater":"huyue","supportnumber":"0","addtime":"1564109840.000000","headimgurl":null},{"id":"12139","title":"中国女子大学","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564108572_1353760068.jpg","introduce":"开启革命妇女新纪元，承载理想与希望","creater":"huyue","supportnumber":"0","addtime":"1564108516.000000","headimgurl":null},{"id":"12138","title":"特殊的\u201c开学礼物\u201d","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564108181_1174826976.jpg","introduce":"一起来了解毛泽东送给同学们特殊的\u201c开学礼物\u201d","creater":"huyue","supportnumber":"0","addtime":"1564108149.000000","headimgurl":null},{"id":"12137","title":"三件宝贝","thumbnail":"/Uploads/newsThumbnail/2019-07-26/1564107842_1794373545.jpg","introduce":"一起来了解毛泽东送给同学们的三件宝贝","creater":"huyue","supportnumber":"0","addtime":"1564107801.000000","headimgurl":null}]
         */

        private PageBean page;
        private List<MenusBean> menus;
        private List<ListBean> list;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<MenusBean> getMenus() {
            return menus;
        }

        public void setMenus(List<MenusBean> menus) {
            this.menus = menus;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class PageBean {
            /**
             * count : 19
             * totalPage : 2
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }

            @Override
            public String toString() {
                return "PageBean{" +
                        "count='" + count + '\'' +
                        ", totalPage=" + totalPage +
                        ", page='" + page + '\'' +
                        ", limit='" + limit + '\'' +
                        ", firstRow=" + firstRow +
                        '}';
            }
        }

        public static class MenusBean {
            /**
             * id : 9994
             * title : 历史淘
             */

            private String id;
            private String title;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            @Override
            public String toString() {
                return "MenusBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        '}';
            }
        }

        public static class ListBean {
            /**
             * id : 12053
             * title : 打麻将治肩周炎，开什么玩笑？！
             * thumbnail : /Uploads/newsThumbnail/2019-07-18/1563439277_205813574.jpg
             * introduce : 听毛泽东讲打麻将的大学问
             * creater : zhangmz
             * supportnumber : 3
             * addtime : 1563439230.000000
             * headimgurl : null
             */

            private String id;
            private String title;
            private String thumbnail;
            private String introduce;
            private String creater;
            private String supportnumber;
            private String addtime;
            private Object headimgurl;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getSupportnumber() {
                return supportnumber;
            }

            public void setSupportnumber(String supportnumber) {
                this.supportnumber = supportnumber;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public Object getHeadimgurl() {
                return headimgurl;
            }

            public void setHeadimgurl(Object headimgurl) {
                this.headimgurl = headimgurl;
            }

            @Override
            public String toString() {
                return "ListBean{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", thumbnail='" + thumbnail + '\'' +
                        ", introduce='" + introduce + '\'' +
                        ", creater='" + creater + '\'' +
                        ", supportnumber='" + supportnumber + '\'' +
                        ", addtime='" + addtime + '\'' +
                        ", headimgurl=" + headimgurl +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "page=" + page +
                    ", menus=" + menus +
                    ", list=" + list +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "DiffuseYanAn{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
