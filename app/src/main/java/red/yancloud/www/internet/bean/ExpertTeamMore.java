package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/29 9:22
 * E_Mail：yh131412hys@163.com
 * Describe：专家团队更多
 * Change：
 * @Version：V1.0
 */
public class ExpertTeamMore {

    /**
     * code : 0000
     * data : [{"id":21,"username":"zhangying_1","nickname":"姚文琦","introduction":"<p style=\"text-indent:2em\"><span style=\"font-size:16px\">毕业于西北大学历史系，长期从事中共党史研究，主持编辑、撰写各类党史书籍、论文百余部（篇）。主编、参与撰写的《中国共产党陕西历史》第一卷获中央党史研究室特别奖，《中共中央在延安十三年史》《陕西省志&middot;共产党志》（上、下卷）、《陕甘宁边区史纲》获陕西省政府二等奖，《丰碑》获省政府三等奖；主编的《民主中国的模型》获中央党史研究室一等奖、陕西省政府二等奖，编写的《中共陕西简明历史》获中央党史研究室二等奖。 参与或主持了延安精神、延安革命纪念馆、照金纪念馆等大纲的撰写或审定，以及中国延安干部学院、西安政治学院的教材编写和审定工作。 姚文琦被中共中央党史研究室命名为全国党史部门党史研究领军人物。<\/span><\/p>\r\n","auth_key":"dLWWYcsT_8PJYRdDa64_J4J_OVhZiu8O","password_hash":"e10adc3949ba59abbe56e057f20f883e","password_reset_token":null,"avatar":"http://zhi.yancloud.red/admin/uploads/avatar/20181119091803_5bf20f4b27b91.jpg","email":"yaowenqi@qq.com","department_id":null,"status":10,"created_at":1542590283,"updated_at":1557478162,"deleted_at":null,"title":"","desc":"  "},{"id":17,"username":"liminjie","nickname":"李敏杰","introduction":"<p style=\"text-indent:2em\"><span style=\"font-size:16px\">1958年8月入伍，曾任师政治部宣传科新闻干事、《人民军队》报编辑、新华通讯社驻兰州军区记者、分社社长；担任过青海西宁军分区副政委、中共甘肃党史学会副会长、兰州军区编研室主任。 在新华社、《人民日报》和《解放军报》等报刊发表2000多篇新闻、通讯稿件，撰写的&ldquo;内参&rdquo;和调查报告，多次得到过毛泽东和周恩来、邓小平、叶剑英、徐向前等老一辈无产阶级革命家的批示和转发，并先后两次受到毛泽东、周恩来、邓小平等革命领袖的接见，为部队和地方培训出近百名新闻人才。 著有《留在大西北的脚印》《军马场奇遇》《西探聚宝盆》《金秋壮歌》等散文集，以及《大西北人杰实录》《西北大决战》等传记文学和纪实史书；组织编写《陕甘土地革命战争时期武装起义》《西北地区剿匪斗争》等7套军史资料丛书，参与编写《西北军区和兰州军区党的组织史》《解放军烈士传》《中共党史人物传》等史书。还参与编写《延安和陕甘宁边区的&ldquo;双拥&rdquo;运动》《解放青海》《浩气颂》和《延安精神漫谈》等解放大西北与延安精神的两套20多本系列丛书，以及《中国人民解放军高级将领传》。<\/span><\/p>\r\n","auth_key":"VcQSQnbyqloxr0AquHiW-P5MOJksxBvD","password_hash":"e10adc3949ba59abbe56e057f20f883e","password_reset_token":null,"avatar":"http://zhi.yancloud.red/admin/uploads/avatar/20190428215852_5cc5b19c524a8.png","email":"2089959592@qq.com","department_id":null,"status":10,"created_at":1542164605,"updated_at":1557478123,"deleted_at":null,"title":"","desc":""},{"id":7,"username":"advisor-3","nickname":"梁向阳","introduction":"<p style=\"text-indent:2em\"><span style=\"font-size:16px\">笔名厚夫。出生于1965年8月，教授、硕士研究生导师。陕西省作家协会副主席、延安市文联副主席、延安市作家协会主席、延安大学文学院院长，兼任路遥文学馆馆长。学术领域为当代散文、延安文艺、路遥研究。 1983年7月参加工作。1990年9月调延安大学中文系任教，历任助教、讲师、副教授、教授。代表性著作及主编资料《走过陕北》《延安文艺研究论丛》《红色延安》，代表性论文与评论有《革命历史题材影视剧中的&ldquo;延安&rdquo;元素》《&ldquo;延安时期&rdquo;文化自觉的历史经验与现实意义》《延安文艺座谈会的前前后后》，编纂《延安文学档案&middot;散文卷》《延安文艺与20世纪中国民间文化》《红色延安》获陕西省社科界优秀科普读物奖，论文《由民间到广场&ldquo;延安时期&rdquo;陕北民歌的彰显之路》(第一作者)获陕西省文联首届&ldquo;陕西民间文艺山花奖&rdquo;二等奖，荣获&ldquo;延安市第二批有突出贡献专家、陕西省教学名师奖、陕西省宣传思想文化系统四个一批人才。<\/span><\/p>\r\n","auth_key":"SOS-J_N8m6_GAGBMOu12KfHScl9RcitS","password_hash":"e10adc3949ba59abbe56e057f20f883e","password_reset_token":null,"avatar":"http://zhi.yancloud.red/admin/uploads/avatar/20181119090908_5bf20d344225a.jpg","email":"advisor-2@qq.com","department_id":null,"status":10,"created_at":1540871250,"updated_at":1557478191,"deleted_at":null,"title":"","desc":""},{"id":6,"username":"advisor-2","nickname":"安危","introduction":"<p style=\"text-indent:2em\"><span style=\"font-size:16px\">资深翻译家、斯诺研究专家，中国作家协会会员。现任陕西省翻译协会主席、中国翻译协会副会长。 长期从事英语口译、笔译及业务审稿工作，曾为来访的美国总统卡特、副总统蒙代尔和国务卿基辛格等20多位外国国家元首和政府首脑作过口译。业余钻研美国文学和中美关系史，开创了对海伦▪斯诺生平和著述的研究工作，与海伦▪斯诺200多封书信往来，收集了海伦▪斯诺上千幅图片资料，并将数百件海伦▪斯诺遗物捐献给斯诺展览馆，整理、翻译、出版斯诺多部文稿。 他还翻译、出版译著18部著作，在国内外报刊发表文论、译文100余篇，总计400余万字。 从政府部门退休后，全力开展中美两国民间文化教育交流，致力于公益事业和社会实践，同时笔耕不止，从事翻译和写作。<\/span><\/p>\r\n","auth_key":"SOS-J_N8m6_GAGBMOu12KfHScl9RcitS","password_hash":"e10adc3949ba59abbe56e057f20f883e","password_reset_token":null,"avatar":"http://zhi.yancloud.red/admin/uploads/avatar/20190507144441_5cd1295965d73.jpg","email":"advisor-1@qq.com","department_id":null,"status":10,"created_at":1540871250,"updated_at":1557478261,"deleted_at":null,"title":"特邀嘉宾","desc":""},{"id":5,"username":"advisor-1","nickname":"王东","introduction":"<p style=\"text-indent:2em\"><span style=\"font-size:16px\">出生于1956年3月，祖籍是河北省深泽县，1974年4月参加工作，1975年9月加入中国共产党，研究生文化程度。 1982年在省委农工部工作，1990年任省农村改革试验办主任，2009年先后任省委政策研究室副厅级研究员、全面深化改革领导小组办公室副主任。2016年退休后，在省决策咨询委员会任农村组副组长、省延安精神研究会任副会长。1986年以来，每年撰写调研报告等文章10多篇，多篇调研报告发表在国家和省上报刊上，部分文章荣获国家、省级优秀论文奖，有些被省、地领导决策部门采纳；主编和参与编辑书籍9本；多次陪同中央、省上领导到基层调查研究，主笔起草调查研究报告；参与起草省委重要文件和省上领导同志讲话。近两年在省决咨委工作期间，每年撰写调研报告至少有4篇，分别得到省委书记、省长和省上其他领导同志的重要批示。撰写的《创建延安红云互联网平台，培育陕西红色文化产业新功能》，荣获陕西省2018年度优秀咨询建议三等奖。<\/span><\/p>\r\n","auth_key":"SOS-J_N8m6_GAGBMOu12KfHScl9RcitS","password_hash":"e10adc3949ba59abbe56e057f20f883e","password_reset_token":null,"avatar":"http://zhi.yancloud.red/admin/uploads/avatar/20190507144350_5cd12926f00b7.jpg","email":"advisor-wang@qq.com","department_id":null,"status":10,"created_at":1540871250,"updated_at":1557478224,"deleted_at":null,"title":"","desc":""}]
     * msg : success
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 21
         * username : zhangying_1
         * nickname : 姚文琦
         * introduction : <p style="text-indent:2em"><span style="font-size:16px">毕业于西北大学历史系，长期从事中共党史研究，主持编辑、撰写各类党史书籍、论文百余部（篇）。主编、参与撰写的《中国共产党陕西历史》第一卷获中央党史研究室特别奖，《中共中央在延安十三年史》《陕西省志&middot;共产党志》（上、下卷）、《陕甘宁边区史纲》获陕西省政府二等奖，《丰碑》获省政府三等奖；主编的《民主中国的模型》获中央党史研究室一等奖、陕西省政府二等奖，编写的《中共陕西简明历史》获中央党史研究室二等奖。 参与或主持了延安精神、延安革命纪念馆、照金纪念馆等大纲的撰写或审定，以及中国延安干部学院、西安政治学院的教材编写和审定工作。 姚文琦被中共中央党史研究室命名为全国党史部门党史研究领军人物。</span></p>
         * auth_key : dLWWYcsT_8PJYRdDa64_J4J_OVhZiu8O
         * password_hash : e10adc3949ba59abbe56e057f20f883e
         * password_reset_token : null
         * avatar : http://zhi.yancloud.red/admin/uploads/avatar/20181119091803_5bf20f4b27b91.jpg
         * email : yaowenqi@qq.com
         * department_id : null
         * status : 10
         * created_at : 1542590283
         * updated_at : 1557478162
         * deleted_at : null
         * title :
         * desc :
         */

        private int id;
        private String username;
        private String nickname;
        private String introduction;
        private String auth_key;
        private String password_hash;
        private Object password_reset_token;
        private String avatar;
        private String email;
        private Object department_id;
        private int status;
        private int created_at;
        private int updated_at;
        private Object deleted_at;
        private String title;
        private String desc;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getAuth_key() {
            return auth_key;
        }

        public void setAuth_key(String auth_key) {
            this.auth_key = auth_key;
        }

        public String getPassword_hash() {
            return password_hash;
        }

        public void setPassword_hash(String password_hash) {
            this.password_hash = password_hash;
        }

        public Object getPassword_reset_token() {
            return password_reset_token;
        }

        public void setPassword_reset_token(Object password_reset_token) {
            this.password_reset_token = password_reset_token;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getDepartment_id() {
            return department_id;
        }

        public void setDepartment_id(Object department_id) {
            this.department_id = department_id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getCreated_at() {
            return created_at;
        }

        public void setCreated_at(int created_at) {
            this.created_at = created_at;
        }

        public int getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(int updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", nickname='" + nickname + '\'' +
                    ", introduction='" + introduction + '\'' +
                    ", auth_key='" + auth_key + '\'' +
                    ", password_hash='" + password_hash + '\'' +
                    ", password_reset_token=" + password_reset_token +
                    ", avatar='" + avatar + '\'' +
                    ", email='" + email + '\'' +
                    ", department_id=" + department_id +
                    ", status=" + status +
                    ", created_at=" + created_at +
                    ", updated_at=" + updated_at +
                    ", deleted_at=" + deleted_at +
                    ", title='" + title + '\'' +
                    ", desc='" + desc + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ExpertTeamMore{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
