package red.yancloud.www.internet.bean;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @author：风不会停息 on 2019/8/6 21:21
 * mailbox：yh131412hys@163.com
 * project：YanAnRedCloud
 * describe：视频页面
 * change：
 * @Version：V1.0
 */
public class VideoInfo {

    /**
     * code : 0000
     * data : {"video":{"id":"661","title":"通往延安之旅","thumbnail":"/Uploads/video/15560021779241.jpg","introduce":"","content":"<p><video class=\"edui-upload-video  vjs-default-skin     video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" data-setup=\"{}\"><source src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" type=\"video/mp4\"/><\/video><\/p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。<\/p>","addtime":"2019-04-23 14:49:37","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2404","score":"2","fileaddress":"","clicknumber":"109","downnumber":"0","collectionnumber":null,"tag":"纪录片","redian":"","niandai":"0","guige":null,"jundui":"","renwu":"","daoyan":"瓦尔特·博斯哈德","daoyancon":"瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。","chupintime":"1938年","company":"瑞士理工学院","bianju":"","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":"无声","timelength":"","guojia":"","zhanzheng":"","leixing":"","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":""}}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * video : {"id":"661","title":"通往延安之旅","thumbnail":"/Uploads/video/15560021779241.jpg","introduce":"","content":"<p><video class=\"edui-upload-video  vjs-default-skin     video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" data-setup=\"{}\"><source src=\"/Uploads/videoTemp/2019423/通往延安之旅.mp4\" type=\"video/mp4\"/><\/video><\/p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。<\/p>","addtime":"2019-04-23 14:49:37","creater":"admin","status":"2","deleteflg":"1","settop":"1","type":"2404","score":"2","fileaddress":"","clicknumber":"109","downnumber":"0","collectionnumber":null,"tag":"纪录片","redian":"","niandai":"0","guige":null,"jundui":"","renwu":"","daoyan":"瓦尔特·博斯哈德","daoyancon":"瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。","chupintime":"1938年","company":"瑞士理工学院","bianju":"","zuoci":null,"zuoqu":null,"mp3file":null,"geci":null,"yuyan":"无声","timelength":"","guojia":"","zhanzheng":"","leixing":"","gequfenlei":null,"file1":"","file2":"","file3":"","file4":"","vipflg":"1","userlevel":"0","orderno":"1","playurl":"","poster":""}
         */

        private VideoBean video;

        public VideoBean getVideo() {
            return video;
        }

        public void setVideo(VideoBean video) {
            this.video = video;
        }

        public static class VideoBean {
            /**
             * id : 661
             * title : 通往延安之旅
             * thumbnail : /Uploads/video/15560021779241.jpg
             * introduce :
             * content : <p><video class="edui-upload-video  vjs-default-skin     video-js" controls="" preload="none" width="420" height="280" src="/Uploads/videoTemp/2019423/通往延安之旅.mp4" data-setup="{}"><source src="/Uploads/videoTemp/2019423/通往延安之旅.mp4" type="video/mp4"/></video></p><p>瓦尔特·博斯哈德他将1938年从西安通往延安沿途的所见所闻，拍摄制成了这段长达21分49秒的黑白无声纪录片。该视频原件现藏于瑞士苏黎世联邦理工学院现代历史档案馆。除此以外，该馆还藏有不少当时博斯哈德在华时期的档案，如博斯哈德的护照、外籍新闻记者注册证、采访毛泽东的记录稿、延安的照片等。这些首次在国内公开的珍贵档案，完整再现了博斯哈德当年的延安之旅。</p>
             * addtime : 2019-04-23 14:49:37
             * creater : admin
             * status : 2
             * deleteflg : 1
             * settop : 1
             * type : 2404
             * score : 2
             * fileaddress :
             * clicknumber : 109
             * downnumber : 0
             * collectionnumber : null
             * tag : 纪录片
             * redian :
             * niandai : 0
             * guige : null
             * jundui :
             * renwu :
             * daoyan : 瓦尔特·博斯哈德
             * daoyancon : 瑞士记者沃尔特·博斯哈德，现代新闻摄影的先驱，曾深入中国共产党领导的抗日根据地，亲自采访过毛泽东、周恩来、朱德等人，用镜头真实地记录下了那段激情燃烧的岁月中许多鲜为人知的感人故事。
             * chupintime : 1938年
             * company : 瑞士理工学院
             * bianju :
             * zuoci : null
             * zuoqu : null
             * mp3file : null
             * geci : null
             * yuyan : 无声
             * timelength :
             * guojia :
             * zhanzheng :
             * leixing :
             * gequfenlei : null
             * file1 :
             * file2 :
             * file3 :
             * file4 :
             * vipflg : 1
             * userlevel : 0
             * orderno : 1
             * playurl :
             * poster :
             */

            private String id;
            private String title;
            private String thumbnail;
            private String introduce;
            private String content;
            private String addtime;
            private String creater;
            private String status;
            private String deleteflg;
            private String settop;
            private String type;
            private String score;
            private String fileaddress;
            private String clicknumber;
            private String downnumber;
            private Object collectionnumber;
            private String tag;
            private String redian;
            private String niandai;
            private Object guige;
            private String jundui;
            private String renwu;
            private String daoyan;
            private String daoyancon;
            private String chupintime;
            private String company;
            private String bianju;
            private Object zuoci;
            private Object zuoqu;
            private Object mp3file;
            private Object geci;
            private String yuyan;
            private String timelength;
            private String guojia;
            private String zhanzheng;
            private String leixing;
            private Object gequfenlei;
            private String file1;
            private String file2;
            private String file3;
            private String file4;
            private String vipflg;
            private String userlevel;
            private String orderno;
            private String playurl;
            private String poster;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getIntroduce() {
                return introduce;
            }

            public void setIntroduce(String introduce) {
                this.introduce = introduce;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getCreater() {
                return creater;
            }

            public void setCreater(String creater) {
                this.creater = creater;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDeleteflg() {
                return deleteflg;
            }

            public void setDeleteflg(String deleteflg) {
                this.deleteflg = deleteflg;
            }

            public String getSettop() {
                return settop;
            }

            public void setSettop(String settop) {
                this.settop = settop;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getScore() {
                return score;
            }

            public void setScore(String score) {
                this.score = score;
            }

            public String getFileaddress() {
                return fileaddress;
            }

            public void setFileaddress(String fileaddress) {
                this.fileaddress = fileaddress;
            }

            public String getClicknumber() {
                return clicknumber;
            }

            public void setClicknumber(String clicknumber) {
                this.clicknumber = clicknumber;
            }

            public String getDownnumber() {
                return downnumber;
            }

            public void setDownnumber(String downnumber) {
                this.downnumber = downnumber;
            }

            public Object getCollectionnumber() {
                return collectionnumber;
            }

            public void setCollectionnumber(Object collectionnumber) {
                this.collectionnumber = collectionnumber;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public String getRedian() {
                return redian;
            }

            public void setRedian(String redian) {
                this.redian = redian;
            }

            public String getNiandai() {
                return niandai;
            }

            public void setNiandai(String niandai) {
                this.niandai = niandai;
            }

            public Object getGuige() {
                return guige;
            }

            public void setGuige(Object guige) {
                this.guige = guige;
            }

            public String getJundui() {
                return jundui;
            }

            public void setJundui(String jundui) {
                this.jundui = jundui;
            }

            public String getRenwu() {
                return renwu;
            }

            public void setRenwu(String renwu) {
                this.renwu = renwu;
            }

            public String getDaoyan() {
                return daoyan;
            }

            public void setDaoyan(String daoyan) {
                this.daoyan = daoyan;
            }

            public String getDaoyancon() {
                return daoyancon;
            }

            public void setDaoyancon(String daoyancon) {
                this.daoyancon = daoyancon;
            }

            public String getChupintime() {
                return chupintime;
            }

            public void setChupintime(String chupintime) {
                this.chupintime = chupintime;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public String getBianju() {
                return bianju;
            }

            public void setBianju(String bianju) {
                this.bianju = bianju;
            }

            public Object getZuoci() {
                return zuoci;
            }

            public void setZuoci(Object zuoci) {
                this.zuoci = zuoci;
            }

            public Object getZuoqu() {
                return zuoqu;
            }

            public void setZuoqu(Object zuoqu) {
                this.zuoqu = zuoqu;
            }

            public Object getMp3file() {
                return mp3file;
            }

            public void setMp3file(Object mp3file) {
                this.mp3file = mp3file;
            }

            public Object getGeci() {
                return geci;
            }

            public void setGeci(Object geci) {
                this.geci = geci;
            }

            public String getYuyan() {
                return yuyan;
            }

            public void setYuyan(String yuyan) {
                this.yuyan = yuyan;
            }

            public String getTimelength() {
                return timelength;
            }

            public void setTimelength(String timelength) {
                this.timelength = timelength;
            }

            public String getGuojia() {
                return guojia;
            }

            public void setGuojia(String guojia) {
                this.guojia = guojia;
            }

            public String getZhanzheng() {
                return zhanzheng;
            }

            public void setZhanzheng(String zhanzheng) {
                this.zhanzheng = zhanzheng;
            }

            public String getLeixing() {
                return leixing;
            }

            public void setLeixing(String leixing) {
                this.leixing = leixing;
            }

            public Object getGequfenlei() {
                return gequfenlei;
            }

            public void setGequfenlei(Object gequfenlei) {
                this.gequfenlei = gequfenlei;
            }

            public String getFile1() {
                return file1;
            }

            public void setFile1(String file1) {
                this.file1 = file1;
            }

            public String getFile2() {
                return file2;
            }

            public void setFile2(String file2) {
                this.file2 = file2;
            }

            public String getFile3() {
                return file3;
            }

            public void setFile3(String file3) {
                this.file3 = file3;
            }

            public String getFile4() {
                return file4;
            }

            public void setFile4(String file4) {
                this.file4 = file4;
            }

            public String getVipflg() {
                return vipflg;
            }

            public void setVipflg(String vipflg) {
                this.vipflg = vipflg;
            }

            public String getUserlevel() {
                return userlevel;
            }

            public void setUserlevel(String userlevel) {
                this.userlevel = userlevel;
            }

            public String getOrderno() {
                return orderno;
            }

            public void setOrderno(String orderno) {
                this.orderno = orderno;
            }

            public String getPlayurl() {
                return playurl;
            }

            public void setPlayurl(String playurl) {
                this.playurl = playurl;
            }

            public String getPoster() {
                return poster;
            }

            public void setPoster(String poster) {
                this.poster = poster;
            }
        }
    }
}
