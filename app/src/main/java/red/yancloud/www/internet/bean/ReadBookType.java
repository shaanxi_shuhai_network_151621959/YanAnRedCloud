package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/3 18:40
 * E_Mail：yh131412hys@163.com
 * Describe： 延云阅读的搜索条件
 * Change：
 * @Version：V1.0
 */
public class ReadBookType {

    /**
     * code : 0000
     * data : [{"id":"9964","title":"政论","status":"1"},{"id":"2210","title":"文学作品","status":"1"},{"id":"2220","title":"散文","status":"1"},{"id":"2230","title":"纪实","status":"1"},{"id":"2240","title":"报告文学","status":"1"},{"id":"2250","title":"传记","status":"1"},{"id":"2260","title":"摄影","status":"1"},{"id":"2284","title":"戏剧","status":"1"},{"id":"2270","title":"社会科学","status":"1"},{"id":"2280","title":"其他","status":"1"}]
     * msg : success
     */

    private String code;
    private String msg;
    private List<DataBean> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 9964
         * title : 政论
         * status : 1
         */

        private String id;
        private String title;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id='" + id + '\'' +
                    ", title='" + title + '\'' +
                    ", status='" + status + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ReadBookType{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
