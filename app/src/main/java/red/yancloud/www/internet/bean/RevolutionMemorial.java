package red.yancloud.www.internet.bean;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/27 20:25
 * E_Mail：yh131412hys@163.com
 * Describe：纪念馆数据列表
 * Change：
 * @Version：V1.0
 */
public class RevolutionMemorial {

    /**
     * code : 0000
     * data : {"page":{"count":"60","totalPage":6,"page":"1","limit":"10","firstRow":0},"memorials":[{"id":"2818","title":"定西精神纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538204153_1417703386.jpg"},{"id":"2816","title":"红军西征胜利纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538202382_1682524775.jpg"},{"id":"2813","title":"*屏蔽的关键字*四大会议会址纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538191284_869776708.jpg"},{"id":"2815","title":"扶眉战役纪念馆 ","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538193430_2133079003.jpg"},{"id":"2812","title":"石拐会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538190701_1937324240.jpg"},{"id":"2811","title":"八路军驻甘办事处纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538187980_388920948.jpg"},{"id":"2810","title":"小河会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538187066_1304340044.jpg"},{"id":"2809","title":"*屏蔽的关键字*中央西北局洮州会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538185656_1653571225.jpg"},{"id":"2808","title":"*屏蔽的关键字*中央西北局盐井会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538184716_136356514.jpg"},{"id":"2806","title":"杨家沟*屏蔽的关键字*历史纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-28/1538121736_634475140.jpg"}]}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * page : {"count":"60","totalPage":6,"page":"1","limit":"10","firstRow":0}
         * memorials : [{"id":"2818","title":"定西精神纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538204153_1417703386.jpg"},{"id":"2816","title":"红军西征胜利纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538202382_1682524775.jpg"},{"id":"2813","title":"*屏蔽的关键字*四大会议会址纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538191284_869776708.jpg"},{"id":"2815","title":"扶眉战役纪念馆 ","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538193430_2133079003.jpg"},{"id":"2812","title":"石拐会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538190701_1937324240.jpg"},{"id":"2811","title":"八路军驻甘办事处纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538187980_388920948.jpg"},{"id":"2810","title":"小河会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538187066_1304340044.jpg"},{"id":"2809","title":"*屏蔽的关键字*中央西北局洮州会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538185656_1653571225.jpg"},{"id":"2808","title":"*屏蔽的关键字*中央西北局盐井会议纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-29/1538184716_136356514.jpg"},{"id":"2806","title":"杨家沟*屏蔽的关键字*历史纪念馆","thumbnail":"/Uploads/newsThumbnail/2018-09-28/1538121736_634475140.jpg"}]
         */

        private PageBean page;
        private List<MemorialsBean> memorials;

        public PageBean getPage() {
            return page;
        }

        public void setPage(PageBean page) {
            this.page = page;
        }

        public List<MemorialsBean> getMemorials() {
            return memorials;
        }

        public void setMemorials(List<MemorialsBean> memorials) {
            this.memorials = memorials;
        }

        public static class PageBean {
            /**
             * count : 60
             * totalPage : 6
             * page : 1
             * limit : 10
             * firstRow : 0
             */

            private String count;
            private int totalPage;
            private String page;
            private String limit;
            private int firstRow;

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public String getPage() {
                return page;
            }

            public void setPage(String page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public int getFirstRow() {
                return firstRow;
            }

            public void setFirstRow(int firstRow) {
                this.firstRow = firstRow;
            }
        }

        public static class MemorialsBean {
            /**
             * id : 2818
             * title : 定西精神纪念馆
             * thumbnail : /Uploads/newsThumbnail/2018-09-29/1538204153_1417703386.jpg
             */

            private String id;
            private String title;
            private String thumbnail;
            private float scale;

            public MemorialsBean(String id, String title, String thumbnail, float scale) {
                this.id = id;
                this.title = title;
                this.thumbnail = thumbnail;
                this.scale = scale;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public float getScale() {
                return scale;
            }

            public void setScale(float scale) {
                this.scale = scale;
            }
        }
    }
}
