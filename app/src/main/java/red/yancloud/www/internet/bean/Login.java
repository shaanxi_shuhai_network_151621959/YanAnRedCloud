package red.yancloud.www.internet.bean;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/5 19:57
 * E_Mail：yh131412hys@163.com
 * Describe：登录
 * Change：
 * @Version：V1.0
 */
public class Login {

    /**
     * code : 0000
     * data : {"id":"137","username":"yh131412","pwd":"9415bec9bffe247fa4e7aca51c8c00f1","remark":null,"sex":"0","mobile":null,"email":"1328060338@qq.com","province":null,"city":null,"area":null,"headimgurl":null,"redstarcoin":"1000","organization":null,"addtime":"2019-06-03 09:46:44","lasttime":null,"userstatus":"1","emailstatus":"1","openid":null,"third_login_type":null,"subscribe":"1","subscribe_time":null,"name":null,"rmb":"0.00","fee_type":"1","start_time":null,"end_time":null}
     * msg : success
     */

    private String code;
    private DataBean data;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * id : 137
         * username : yh131412
         * pwd : 9415bec9bffe247fa4e7aca51c8c00f1
         * remark : null
         * sex : 0
         * mobile : null
         * email : 1328060338@qq.com
         * province : null
         * city : null
         * area : null
         * headimgurl : null
         * redstarcoin : 1000
         * organization : null
         * addtime : 2019-06-03 09:46:44
         * lasttime : null
         * userstatus : 1
         * emailstatus : 1
         * openid : null
         * third_login_type : null
         * subscribe : 1
         * subscribe_time : null
         * name : null
         * rmb : 0.00
         * fee_type : 1
         * start_time : null
         * end_time : null
         */

        /**
         * 用户id
         */
        private String id;
        /**
         * 用户名称
         */
        private String username;
        /**
         * 用户密码
         */
        private String pwd;
        /**
         * 用户备注
         */
        private Object remark;
        /**
         * 性别 0未知 1男 2女
         */
        private String sex;
        /**
         * 手机号
         */
        private String mobile;
        /**
         * 邮箱
         */
        private String email;
        /**
         * 省份
         */
        private String province;
        /**
         * 城市
         */
        private String city;
        /**
         * 区
         */
        private String area;
        /**
         * 用户头像
         */
        private String headimgurl;
        /**
         * 红星币
         */
        private String redstarcoin;
        /**
         * 所属党组织
         */
        private String organization;
        /**
         * 创建时间
         */
        private String addtime;
        /**
         * 最后登录时间
         */
        private Object lasttime;
        /**
         * 用户状态 1正常 2禁用
         */
        private String userstatus;
        /**
         * 邮箱认证状态 0未认证 2认证通过
         */
        private String emailstatus;
        /**
         * 微信openId
         */
        private Object openid;
        /**
         * 第三方登录方式 1QQ 2微博 3微信
         */
        private Object third_login_type;
        /**
         * 用户是否订阅该公众号标识 1已关注 2已取消关注
         */
        private String subscribe;
        /**
         * 关注时间
         */
        private Object subscribe_time;
        /**
         * 真实名字
         */
        private String name;
        /**
         * 人民币
         */
        private String rmb;
        /**
         * 费用类型 1免费 2点播 3包月 4包年
         */
        private String fee_type;
        /**
         * 收费有效期开始时间
         */
        private Object start_time;
        /**
         * 收费有效期结束时间
         */
        private Object end_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPwd() {
            return pwd;
        }

        public void setPwd(String pwd) {
            this.pwd = pwd;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public Object getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getHeadimgurl() {
            return headimgurl;
        }

        public void setHeadimgurl(String headimgurl) {
            this.headimgurl = headimgurl;
        }

        public String getRedstarcoin() {
            return redstarcoin;
        }

        public void setRedstarcoin(String redstarcoin) {
            this.redstarcoin = redstarcoin;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public Object getLasttime() {
            return lasttime;
        }

        public void setLasttime(Object lasttime) {
            this.lasttime = lasttime;
        }

        public String getUserstatus() {
            return userstatus;
        }

        public void setUserstatus(String userstatus) {
            this.userstatus = userstatus;
        }

        public String getEmailstatus() {
            return emailstatus;
        }

        public void setEmailstatus(String emailstatus) {
            this.emailstatus = emailstatus;
        }

        public Object getOpenid() {
            return openid;
        }

        public void setOpenid(Object openid) {
            this.openid = openid;
        }

        public Object getThird_login_type() {
            return third_login_type;
        }

        public void setThird_login_type(Object third_login_type) {
            this.third_login_type = third_login_type;
        }

        public String getSubscribe() {
            return subscribe;
        }

        public void setSubscribe(String subscribe) {
            this.subscribe = subscribe;
        }

        public Object getSubscribe_time() {
            return subscribe_time;
        }

        public void setSubscribe_time(Object subscribe_time) {
            this.subscribe_time = subscribe_time;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRmb() {
            return rmb;
        }

        public void setRmb(String rmb) {
            this.rmb = rmb;
        }

        public String getFee_type() {
            return fee_type;
        }

        public void setFee_type(String fee_type) {
            this.fee_type = fee_type;
        }

        public Object getStart_time() {
            return start_time;
        }

        public void setStart_time(Object start_time) {
            this.start_time = start_time;
        }

        public Object getEnd_time() {
            return end_time;
        }

        public void setEnd_time(Object end_time) {
            this.end_time = end_time;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id='" + id + '\'' +
                    ", username='" + username + '\'' +
                    ", pwd='" + pwd + '\'' +
                    ", remark=" + remark +
                    ", sex='" + sex + '\'' +
                    ", mobile=" + mobile +
                    ", email='" + email + '\'' +
                    ", province=" + province +
                    ", city=" + city +
                    ", area=" + area +
                    ", headimgurl=" + headimgurl +
                    ", redstarcoin='" + redstarcoin + '\'' +
                    ", organization=" + organization +
                    ", addtime='" + addtime + '\'' +
                    ", lasttime=" + lasttime +
                    ", userstatus='" + userstatus + '\'' +
                    ", emailstatus='" + emailstatus + '\'' +
                    ", openid=" + openid +
                    ", third_login_type=" + third_login_type +
                    ", subscribe='" + subscribe + '\'' +
                    ", subscribe_time=" + subscribe_time +
                    ", name=" + name +
                    ", rmb='" + rmb + '\'' +
                    ", fee_type='" + fee_type + '\'' +
                    ", start_time=" + start_time +
                    ", end_time=" + end_time +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Login{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                '}';
    }
}
