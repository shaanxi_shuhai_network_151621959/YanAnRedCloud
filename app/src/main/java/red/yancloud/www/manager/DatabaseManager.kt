package red.yancloud.www.manager

import android.database.sqlite.SQLiteDatabase
import org.greenrobot.greendao.query.Query
import red.yancloud.www.db.greendao.BookEntityDao
import red.yancloud.www.db.greendao.BookMarkEntityDao
import red.yancloud.www.db.greendao.DaoMaster
import red.yancloud.www.db.greendao.DaoSession
import red.yancloud.www.internet.bean.BookEntity
import red.yancloud.www.internet.bean.BookMarkEntity
import red.yancloud.www.util.Utils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/22 16:29
 * E_Mail：yh131412hys@163.com
 * Describe：数据库管理类
 * Change：
 * @Version：V1.0
 */
class DatabaseManager private constructor() {


    val daoMaster: DaoMaster
    /**
     * 完成对数据库的操作，只是个接口
     */
    private val daoSession: DaoSession
    private val devOpenHelper: DaoMaster.DevOpenHelper
    private val db: SQLiteDatabase

    private val bookEntityDao: BookEntityDao
    private val markEntityDao: BookMarkEntityDao

    init {
        devOpenHelper = DaoMaster.DevOpenHelper(Utils.getAppContext(), DB_NAME, null)
        db = devOpenHelper.writableDatabase
        /**
         * 判断是否存在数据库，如果没有则创建
         */
        daoMaster = DaoMaster(db)
        /**
         * 完成对数据库的操作，只是个接口
         */
        daoSession = daoMaster.newSession()

        bookEntityDao = daoSession.bookEntityDao
        markEntityDao = daoSession.bookMarkEntityDao
    }

    companion object {

        private val DB_NAME = "red_cloud.db"

        val instance = DatabaseManager()
    }

    /**
     * @Author：风不会停息 on 2019/7/22 17:20
     * E_Mail：yh131412hys@163.com
     * @param bookEntity
     * @Description：书籍基本信息插入
     * @Change：
     * @Version：V1.0
     */
    fun insertBookInfo(bookEntity: BookEntity): Long {
        return bookEntityDao.insertOrReplace(bookEntity)
    }

    /**
     * @Author：风不会停息 on 2019/7/22 17:20
     * E_Mail：yh131412hys@163.com
     * @param bookEntity
     * @Description：书籍基本信息更新
     * @Change：
     * @Version：V1.0
     */
    fun updateBookInfo(bookEntity: BookEntity) {
        bookEntityDao.update(bookEntity)
    }

    /**
     * @Author：风不会停息 on 2019/7/22 17:21
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @Description：根据bookId查询书籍信息
     * @Change：
     * @Version：V1.0
     */
    fun queryBookInfo(bookId: String): BookEntity? {
        var bookEntity: BookEntity?
        try {
            bookEntity = bookEntityDao.queryBuilder().where(BookEntityDao.Properties.BookId.eq(bookId)).build()
                .uniqueOrThrow()
        } catch (e: Exception) {
            bookEntity = null
        }

        return bookEntity
    }

    /**
     * @Author：风不会停息 on 2019/7/23 19:13
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @Description：
     * @Change：
     * @Version：V1.0
     */
    fun checkBook(bookId: String): Boolean {
        val list = bookEntityDao.queryBuilder().where(BookEntityDao.Properties.BookId.eq(bookId)).list()
        return list != null && list.size > 0
    }

    /**
     * @Author：风不会停息 on 2019/8/20 15:54
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @Description：根据书籍id，删除本地数据库中的书籍
     * @Change：
     * @Version：V1.0
     */
    fun deleteBook(bookId: String) {

        bookEntityDao.queryBuilder().where(BookEntityDao.Properties.BookId.eq(bookId)).buildDelete().executeDeleteWithoutDetachingEntities()
    }

    /**
     * @Author：风不会停息 on 2019/7/23 19:15
     * E_Mail：yh131412hys@163.com
     * @param bookId
     * @Description：获取书签列表
     * @Change：
     * @Version：V1.0
     */
    fun queryBookMarkList(bookId: String): List<BookMarkEntity>? {
        val query: Query<BookMarkEntity>
        try {
            query = markEntityDao.queryBuilder().where(BookMarkEntityDao.Properties.BookId.eq(bookId))
                .orderDesc(BookMarkEntityDao.Properties.Time).build()
        } catch (e: Exception) {
            return null
        }

        return query.list()
    }

    /**
     * @Author：风不会停息 on 2019/7/23 19:19
     * E_Mail：yh131412hys@163.com
     * @param bookMarkEntity
     * @Description：通过id和scrollY删除标签
     * @Change：
     * @Version：V1.0
     */
    fun deleteBookMaker(bookMarkEntity: BookMarkEntity) {

        val condition =
            markEntityDao.queryBuilder().where(BookMarkEntityDao.Properties.BookId.eq(bookMarkEntity.bookId))
                .and(
                    BookMarkEntityDao.Properties.ScrollY.eq(bookMarkEntity.scrollY),
                    BookMarkEntityDao.Properties.ChapterId.eq(bookMarkEntity.chapterId)
                )
        markEntityDao.delete(markEntityDao.queryBuilder().where(condition).build().uniqueOrThrow())
    }

    /**
     * @Author：风不会停息 on 2019/7/25 19:47
     * E_Mail：yh131412hys@163.com
     * @param bookMarkEntity
     * @Description：插入标签
     * @Change：
     * @Version：V1.0
     */
    fun addMaker(bookMarkEntity: BookMarkEntity) {

        markEntityDao.insert(bookMarkEntity)
    }

    /**
     * @Author：风不会停息 on 2019/8/20 15:58
     * E_Mail：yh131412hys@163.com
     * @Description：用户退出，删除本地书籍信息
     * @Change：
     * @Version：V1.0
     */
    fun deleteAll() {

        bookEntityDao.deleteAll()
        markEntityDao.deleteAll()
    }
}
