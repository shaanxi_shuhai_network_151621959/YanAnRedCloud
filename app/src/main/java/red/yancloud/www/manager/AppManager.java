package red.yancloud.www.manager;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import red.yancloud.www.R;
import red.yancloud.www.ui.activity.MainActivity;

import java.util.Stack;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/4 16:55
 * E_Mail：yh131412hys@163.com
 * Describe：应用程序Activity管理类：用于Activity管理和应用程序退出
 * Change：
 * @Version：V1.0
 */
public class AppManager {
	private static Stack<Activity> activityStack;
	private static AppManager instance;

	private AppManager() {
	}

	/**
	 * 单一实例
	 */
	public static AppManager getAppManager() {
		if (instance == null) {
			instance = new AppManager();
		}
		return instance;
	}

	public Activity getFirstActivity() {
		return activityStack.get(0);
	}

	/**
	 * 添加Activity到堆栈
	 */
	public void addActivity(Activity activity) {
		if (activityStack == null) {
			activityStack = new Stack<Activity>();
		}
		activityStack.add(activity);
	}

	/**
	 * 获取当前Activity（堆栈中最后一个压入的）
	 */
	public Activity currentActivity() {
		Activity activity = activityStack.lastElement();
		return activity;
	}

	/**
	 * 结束当前Activity（堆栈中最后一个压入的）
	 */
	public void finishLastActivity() {
		synchronized (activityStack) {
			Activity activity = activityStack.lastElement();
			if (null != activity) {
				finishActivity(activity);
				activity = null;
			}
		}
	}

	/**
	 * 结束指定的Activity
	 */
	public void finishActivity(Activity activity) {
		if (activity != null) {
			activityStack.remove(activity);
			activity.finish();
			activity = null;
		}
	}

	public void removeActivity(Activity activity) {
		if (activity != null) {
			activityStack.remove(activity);
		}
	}

	/**
	 * 结束指定类名的Activity
	 */
	public void finishActivity(Class<?> cls) {
		if (null == activityStack) {
			return;
		}
		for (Activity activity : activityStack) {
			if (activity.getClass().equals(cls)) {
				finishActivity(activity);
				return;
			}
		}
	}

	/**
	 * 查找指定类名的Activity
	 * 
	 * @param cls
	 * @return
	 */
	public boolean findActivity(Class<?> cls) {
		if (null == activityStack) {
			return false;
		}
		for (Activity activity : activityStack) {
			if (activity.getClass().equals(cls)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 查找指定类名的Activity 并且返回指定activity
	 * 
	 * @param cls
	 * @return
	 */
	public Activity getActivity(Class<?> cls) {
		if (null == activityStack) {
			return null;
		}
		for (Activity activity : activityStack) {
			if (activity.getClass().equals(cls)) {
				return activity;
			}
		}
		return null;
	}

	
	/**
	 * 除主页外，清除所有activity
	 * @return
	 */
	public boolean cleanActivity() {
		if (null == activityStack) {
			return false;
		} else {
			for (int i = 0, size = activityStack.size(); i < size; i++) {
				if (activityStack.get(i).getClass().equals(MainActivity.class)
						/*|| activityStack.get(i).getClass().equals(BKStoreActivity.class)
						|| activityStack.get(i).getClass().equals(BKShopActivity.class)*/)
				{
				}else{
					activityStack.get(i).finish();
				} 
			}
			return true;
		}
	}


	public void finishSreadActivity(Class<?> cls){

		if (null == activityStack) {
			return;
		}

		for (int i = 0; i < activityStack.size(); i++) {
//				if(activityStack.get(i).getClass().equals(SReadActivity.class)){
//					activityStack.get(i).finish();
//				}
		}
	}
	
	
	/**
	 * 返回主页
	 * @param context
	 */
	public void returnToHome(Context context) {
		if (AppManager.getAppManager().getFirstActivity() instanceof MainActivity) {
			AppManager.getAppManager().cleanActivity();
		} else {
			Intent intent = new Intent();
			intent.setClass(context, MainActivity.class);
			context.startActivity(intent);
		    AppManager.getAppManager().cleanActivity();
		}
	}
	

	/**
	 * 结束所有Activity
	 */
	public void finishAllActivity() {
		for (int i = 0, size = activityStack.size(); i < size; i++) {
			if (null != activityStack.get(i)) {

				activityStack.get(i).finish();
			}
		}
		activityStack.clear();
	}

	/**
	 * 结束所有Activity除过开始添加的的两个。
	 */
	public void finishDesignationActivity() {
		for (int i = 2, size = activityStack.size(); i < size; i++) {
			if (null != activityStack.get(i)) {
				activityStack.get(i).finish();
			}
		}
	}

	/**
	 * 退出应用程序
	 */
	@SuppressWarnings("deprecation")
	public void AppExit(Context context) {
		finishAllActivity();
//		ActivityManager activityMgr = (ActivityManager) context
//					.getSystemService(Context.ACTIVITY_SERVICE);
//		if (activityMgr != null) {
//			activityMgr.restartPackage(context.getPackageName());
//		}
//		System.exit(0);
		((Activity) context).overridePendingTransition(R.anim.out_to_right, R.anim.out_to_right);
	}

	public void clear() {
		activityStack.clear();
	}
}