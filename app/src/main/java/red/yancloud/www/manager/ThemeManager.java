package red.yancloud.www.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.content.ContextCompat;
import red.yancloud.www.R;
import red.yancloud.www.util.ScreenUtils;
import red.yancloud.www.util.Utils;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/7 9:56
 * E_Mail：yh131412hys@163.com
 * Describe：阅读页面背景
 * Change：
 * @Version：V1.0
 */
public class ThemeManager {

    public static final int READ_STYLE_THEME_00 = 0;
    public static final int READ_STYLE_THEME_01 = 1;
    public static final int READ_STYLE_THEME_02 = 2;
    public static final int READ_STYLE_THEME_03 = 3;
    public static final int READ_STYLE_THEME_04 = 4;
    public static final int READ_STYLE_THEME_05 = 5;
    public static final int READ_STYLE_THEME_06 = 6;
    public static final int READ_STYLE_THEME_07 = 7;
    public static final int READ_STYLE_THEME_08 = 8;
    public static final int READ_STYLE_THEME_09 = 9;
    public static final int READ_STYLE_THEME_10 = 10;
    public static final int READ_STYLE_THEME_11 = 11;
    public static final int READ_STYLE_THEME_DEFAULT = 12;
    public static final int READ_STYLE_THEME_NIGHT = 13;

    public static Bitmap getThemeDrawable(int theme){
        Bitmap bitmap = Bitmap.createBitmap(ScreenUtils.getScreenWidth(),ScreenUtils.getRealHeight(), Bitmap.Config.ARGB_8888);
        switch (theme) {
            case READ_STYLE_THEME_00:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(), R.color.read_style_theme_00));
                break;
            case READ_STYLE_THEME_01:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(), R.color.read_style_theme_01));
                break;
            case READ_STYLE_THEME_02:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_02));
                break;
            case READ_STYLE_THEME_03:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_03));
                break;
            case READ_STYLE_THEME_04:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_04));
                break;
            case READ_STYLE_THEME_05:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_05));
                break;
            case READ_STYLE_THEME_06:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_06));
                break;
            case READ_STYLE_THEME_07:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_07));
                break;
            case READ_STYLE_THEME_08:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_08));
                break;
            case READ_STYLE_THEME_09:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_09));
                break;
            case READ_STYLE_THEME_10:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_10));
                break;
            case READ_STYLE_THEME_11:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_11));
                break;
            case READ_STYLE_THEME_DEFAULT:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_default));
                break;
            case READ_STYLE_THEME_NIGHT:
                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(),R.color.read_style_theme_night));
                break;
            default:
                bitmap=BitmapFactory.decodeResource(Utils.getResources(), R.mipmap.read_text_bg_ic);
//                bitmap.eraseColor(ContextCompat.getColor(Utils.getAppContext(), R.color.read_theme_white));
                break;
        }
        return bitmap;

    }

}
