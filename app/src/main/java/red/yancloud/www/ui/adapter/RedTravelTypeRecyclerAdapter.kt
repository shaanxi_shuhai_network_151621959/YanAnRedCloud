package red.yancloud.www.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.internet.bean.RedTravel


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/10 15:09
 * E_Mail：yh131412hys@163.com
 * Describe：红色文旅二级分类
 * Change：
 * @Version：V1.0
 */
class RedTravelTypeRecyclerAdapter(layoutResId:Int) : BaseQuickAdapter<RedTravel.DataBean.MenusBean.ChildrensBean,BaseViewHolder>(layoutResId) {

    private var defaultSelectFirst: Boolean=true

    override fun convert(helper: BaseViewHolder, item: RedTravel.DataBean.MenusBean.ChildrensBean?) {

        if (selectPosition==helper.layoutPosition) {

            helper.setTextColor(R.id.repositorySortValueItem_tv,mContext.resources.getColor(R.color.main_color))
        }else{

            helper.setTextColor(R.id.repositorySortValueItem_tv,mContext.resources.getColor(R.color.color_99))
        }

        if (defaultSelectFirst&&helper.layoutPosition==0){

            defaultSelectFirst=false
            helper.setTextColor(R.id.repositorySortValueItem_tv,mContext.resources.getColor(R.color.main_color))
        }

        if (item != null) {
            helper.setText(R.id.repositorySortValueItem_tv,item.title)
        }
    }

    private var selectPosition = -1//用于记录用户选择的变量

    fun setSelectPosition(selectPosition: Int) {
        this.selectPosition=selectPosition
        notifyDataSetChanged()
    }
}