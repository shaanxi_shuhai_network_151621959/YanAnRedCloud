package red.yancloud.www.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_splash.*
import red.yancloud.www.R
import red.yancloud.www.db.sharedpreference.UserInfoShardPreference
import red.yancloud.www.util.AppUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/17 14:45
 * E_Mail：yh131412hys@163.com
 * Describe：导航页
 * Change：
 * @Version：V1.0
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splash.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {

                UserInfoShardPreference.instance.setISShowLead(AppUtils.getAppVersionCode(), false)

                startActivity(Intent(this@SplashActivity,MainActivity::class.java))
                finish()
            }
        })
    }
}
