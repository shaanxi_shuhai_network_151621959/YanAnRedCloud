package red.yancloud.www.ui.adapter

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import com.zhy.view.flowlayout.TagFlowLayout
import red.yancloud.www.R
import red.yancloud.www.internet.bean.RepositorySort
import red.yancloud.www.ui.activity.RepositoryEntryListActivity
import java.util.ArrayList

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/20 16:16
 * E_Mail：yh131412hys@163.com
 * Describe：知库分类
 * Change：
 * @Version：V1.0
 */
class RepositorySortRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<RepositorySort.DataBean,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: RepositorySort.DataBean?) {

        if (item!=null) {

            helper.setText(R.id.repositorySortTypeItem_tv,item.name)
            val mFlowLayout = helper.getView<TagFlowLayout>(R.id.repositorySortTypeItem_tagFlowLayout)
            mFlowLayout.adapter = object : TagAdapter<RepositorySort.DataBean.ChildrenBean>(item.children) {

                override fun getView(parent: FlowLayout?, position: Int, t: RepositorySort.DataBean.ChildrenBean?): View {
                    val tv = mLayoutInflater.inflate(R.layout.item_repository_sort_value_list, mFlowLayout, false) as AppCompatTextView
                    tv.text = t!!.name
                    return tv
                }
//                override fun getView(parent: FlowLayout, position: Int, s: String): View {
//                    val tv = mLayoutInflater.inflate(R.layout.item_repository_sort_value_list, mFlowLayout, false) as AppCompatTextView
//                    tv.text = s
//                    return tv
//                }
            }

            mFlowLayout.setOnTagClickListener(object :TagFlowLayout.OnTagClickListener{
                override fun onTagClick(view: View, position: Int, parent: FlowLayout?): Boolean {

                    (mContext as Activity).finish()
                    (mContext as Activity).startActivity(Intent(mContext,RepositoryEntryListActivity::class.java)
                        .putExtra("title",item.name)
                        .putExtra("cid",item.id)
                        .putExtra("id",item.children[position].id.toString())
                        .putParcelableArrayListExtra("SortChildrenBean",item.children as ArrayList<RepositorySort.DataBean.ChildrenBean>))
                    return true
                }
            })
//            mFlowLayout.setOnSelectListener { selectPosSet -> (mContext as Activity).title = "choose:$selectPosSet" }
        }
    }
}