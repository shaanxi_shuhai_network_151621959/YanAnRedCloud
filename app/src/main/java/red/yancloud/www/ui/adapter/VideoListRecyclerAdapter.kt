package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.VideoList

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 14:36
 * E_Mail：yh131412hys@163.com
 * Describe：延云视听
 * Change：
 * @Version：V1.0
 */
class VideoListRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<VideoList.DataBean.VideoBean,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: VideoList.DataBean.VideoBean?) {

        if (item != null) {

            Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).error(R.mipmap.ic_launcher).into(helper.getView(R.id.video_img_iv))
            helper.setText(R.id.video_title_tv,item.title)
            helper.setText(R.id.video_subTitle_tv,item.tag)
            if (item.favorite != null && item.favorite.id != null ) {

                helper.setText(R.id.video_collect_tv,"已收藏")
            }else{

                helper.setText(R.id.video_collect_tv,"收藏")
            }
            helper.addOnClickListener(R.id.video_collect_tv)
        }
    }
}