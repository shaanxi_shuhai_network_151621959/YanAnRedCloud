package red.yancloud.www.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.ui.fragment.SortFragment
import red.yancloud.www.util.ScreenUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/9 14:12
 * E_Mail：yh131412hys@163.com
 * Describe：分类
 * Change：
 * @Version：V1.0
 */
class SortRecyclerViewAdapter(layoutResId: Int) : BaseQuickAdapter<SortFragment.SortBean, BaseViewHolder>(layoutResId) {
    override fun convert(helper: BaseViewHolder, item: SortFragment.SortBean?) {

        if (item != null) {
            val layoutParams = helper.getView<CardView>(R.id.sortItem_cardView).layoutParams
////                    item宽度
            layoutParams.width = ((ScreenUtils.getScreenWidth()- ScreenUtils.dpToPx(40f)) / 2 ).toInt()
//            helper.getView<CardView>(R.id.sortItem_cardView).layoutParams = layoutParams

            helper.setText(R.id.sortItem_title_tv,item.title)
            helper.setText(R.id.sortItem_content_tv,item.content)
        }
    }
}
