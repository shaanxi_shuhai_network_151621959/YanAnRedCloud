package red.yancloud.www.ui.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.zzhoujay.richtext.RichText
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.RedBag

import java.util.ArrayList

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：红书包
 * Change：
 * @Version：V1.0
 */
class RedBagRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<RedBag.DataBean.RedsbagsBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_red_bag_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_red_bag_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_red_bag_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.redBagItemTitleTv.text=item.title
                holder.redBagItemContentTv.text=item.introduce
//                RichText.from(item.content).imageGetter(DefaultImageGetter()).into(holder.redBagItemContentTv)
                holder.redBagItemTimeTv.text=item.addtime
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.redBagItemBigImgIv)
                holder.redBagItemTitleTv.text=item.title
                holder.redBagItemTimeTv.text=item.addtime
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.redBagItemImgIv)
                holder.redBagItemTitleTv.text=item.title
//                holder.redBagItemContentTv.text=item.introduce
                RichText.from(item.introduce).into(holder.redBagItemContentTv)
                holder.redBagItemTimeTv.text=item.addtime
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

            val data = mData[position]

//            if (position==0) {
//
//                if (TextUtils.isEmpty(data.thumbnail)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }else{

                if (TextUtils.isEmpty(data.thumbnail)) {

                    return ARTICLE
                }
                return LEFT_IMG
//            }
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<RedBag.DataBean.RedsbagsBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var redBagItemTitleTv: AppCompatTextView
        var redBagItemContentTv: AppCompatTextView
        var redBagItemTimeTv: AppCompatTextView

        init {
            redBagItemTitleTv = itemView!!.findViewById(R.id.redBagItem_title_tv)
            redBagItemContentTv = itemView.findViewById(R.id.redBagItem_content_tv)
            redBagItemTimeTv = itemView.findViewById(R.id.redBagItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var redBagItemTitleTv: AppCompatTextView
        var redBagItemBigImgIv: AppCompatImageView
        var redBagItemTimeTv: AppCompatTextView

        init {
            redBagItemTitleTv = itemView.findViewById(R.id.redBagItem_title_tv)
            redBagItemBigImgIv = itemView.findViewById(R.id.redBagItem_bigImg_iv)
            redBagItemTimeTv = itemView.findViewById(R.id.redBagItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var redBagItemImgIv: AppCompatImageView
        var redBagItemTitleTv: AppCompatTextView
        var redBagItemContentTv: AppCompatTextView
        var redBagItemTimeTv: AppCompatTextView

        init {
            redBagItemImgIv = itemView.findViewById(R.id.redBagItem_img_iv)
            redBagItemTitleTv = itemView.findViewById(R.id.redBagItem_title_tv)
            redBagItemContentTv = itemView.findViewById(R.id.redBagItem_content_tv)
            redBagItemTimeTv = itemView.findViewById(R.id.redBagItem_time_tv)
        }
    }
}
