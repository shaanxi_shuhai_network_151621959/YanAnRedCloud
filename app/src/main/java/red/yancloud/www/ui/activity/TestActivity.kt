package red.yancloud.www.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.gyf.immersionbar.ImmersionBar
import kotlinx.android.synthetic.main.activity_test.*
import red.yancloud.www.R
import red.yancloud.www.internet.api.RedCloudApis


class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        ImmersionBar.with(this).statusBarView(R.id.top_view)
            .navigationBarColor(R.color.colorPrimary)
            .fullScreen(true)
            .addTag("PicAndColor")
            .init()

//        videoPlayer.setUp("http://jzvd.nathen.cn/342a5f7ef6124a4a8faf00e738b8bee4/cf6d9db0bd4d41f59d09ea0a81e918fd-5287d2089db37e62345123a1be272f8b.mp4"
//            , "饺子快长大");
//        Glide.with(this).load("http://jzvd-pic.nathen.cn/jzvd-pic/1bb2ebbe-140d-4e2e-abd2-9e7e564f71ac.png").into(videoPlayer.thumbImageView);

        /*/Uploads/videoTemp/2019423/通往延安之旅.mp4*/

//        {"code":"0000","data":[{"id":25,"parent_id":4,"sort":2,"name":"文化事件"}],"msg":"success"}
//        RedCloudApis.getInstance().getZhiKuSecondLevelSortData("1","100","11",object : Observer<RepositorySort> {
//            override fun onComplete() {
//            }
//
//            override fun onSubscribe(d: Disposable) {
//            }
//
//            override fun onNext(t: RepositorySort) {
//
////                if (t.code == Constants.SYSTEM_OK) {
////
////                }else{
////
////                }
//            }
//
//            override fun onError(e: Throwable) {
//            }
//        })

//        pwd=yh131412&email=1328060339%40qq.com&username=yh123456
//        "zid":"121"  加一个必传参数uid 没有的传0
//        id
//        fee_type 费用类型(0:免费;1:一级权限;2:二级权限;)',
//        {"code":"0000","data":{"zid":"121","pwd":"a5f06a7338dc927e7a4c8845d8f1c07a","fee_type":"0","user_type":"1","id":"224","status":"10","avatar":"","username":"YH123456"},"msg":"success"}
    }

//    override fun onBackPressed() {
//        if (Jzvd.backPress()) {
//            return
//        }
//        super.onBackPressed()
//    }
//
//    override fun onPause() {
//        super.onPause()
//        Jzvd.releaseAllVideos()
//    }
}
