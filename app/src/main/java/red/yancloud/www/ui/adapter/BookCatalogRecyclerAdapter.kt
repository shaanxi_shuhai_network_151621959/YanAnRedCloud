package red.yancloud.www.ui.adapter

import android.util.Log
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.internet.bean.BookCatalog

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 16:36
 * E_Mail：yh131412hys@163.com
 * Describe：ePup书籍目录
 * Change：
 * @Version：V1.0
 */
class BookCatalogRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<BookCatalog.DataBean.ChaptersBean,BaseViewHolder>(layoutResId){

    override fun convert(helper: BaseViewHolder, item: BookCatalog.DataBean.ChaptersBean?) {
        if (item != null) {

            helper.setText(R.id.bookCatalogItem_title_tv,item.title)
        }
    }
}
