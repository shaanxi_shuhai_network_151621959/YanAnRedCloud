package red.yancloud.www.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.internet.bean.SpecialTopicMore

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 14:36
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题内容更多
 * Change：
 * @Version：V1.0
 */
class SpecialTopicContentMoreRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<SpecialTopicMore.DataBean.ListBean,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: SpecialTopicMore.DataBean.ListBean?) {

        if (item != null) {

            helper.setText(R.id.specialTopicContentMoreItem_title_tv, item.title)
            helper.setText(R.id.specialTopicContentMoreItem_content_tv, item.introduce)
            helper.setText(R.id.specialTopicContentMoreItem_time_tv, item.addtime)
        }
    }
}