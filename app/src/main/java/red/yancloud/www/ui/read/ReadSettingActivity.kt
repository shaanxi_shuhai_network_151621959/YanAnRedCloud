package red.yancloud.www.ui.read

import android.graphics.Color
import android.view.*
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import org.greenrobot.eventbus.EventBus
import red.yancloud.www.R
import red.yancloud.www.base.BaseActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.common.MessageEvent
import red.yancloud.www.db.sharedpreference.ReadSettingShardPreference
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/11 16:26
 * E_Mail：yh131412hys@163.com
 * Describe：阅读设置页面
 * Change：
 * @Version：V1.0
 */
class ReadSettingActivity : BaseActivity(){

    private lateinit var readSetting: ReadSettingShardPreference

    private lateinit var languageId: IntArray
    private var soundSwitch: AppCompatCheckBox? = null
    private var clearCache: LinearLayoutCompat? = null
    private lateinit var languageView:Array<AppCompatTextView?>
    private var currentLanguage:Int=-1

    override val layoutId: Int
        get() = R.layout.activity_reader_setting

    override fun initData() {

        readSetting = ReadSettingShardPreference.instance
        currentLanguage=readSetting.language
    }

    /**
     * 语言模式
     */
    internal var languageClickListener: View.OnClickListener = View.OnClickListener { v ->
        for (i in languageView.indices) {
            if (v === languageView[i]) {
                languageView[i]!!.setBackgroundColor(Color.parseColor("#CFDFE5"))
                readSetting.language = i
            } else {
                languageView[i]!!.setBackgroundColor(resources.getColor(R.color.trans))
            }
        }
    }

    override fun configViews() {

        //        back = (ImageButton) findViewById(R.id.read_style_setting_btn_return);
        findViewById<View>(R.id.read_style_setting_back_iv).setOnClickListener { turnGoReadActivity() }

        languageId = intArrayOf(R.id.language_simplified_text, R.id.language_Traditional_text)

        languageView = arrayOfNulls(languageId.size)
        for (i in languageId.indices) {

            languageView[i] = findViewById<AppCompatTextView>(languageId[i])
            languageView[i]!!.setOnClickListener(languageClickListener)

            if (i == currentLanguage) {
                languageView[i]!!.setBackgroundColor(Color.parseColor("#CFDFE5"))
            } else {
                languageView[i]!!.setBackgroundColor(resources.getColor(R.color.trans))
            }
        }

        soundSwitch = findViewById<AppCompatCheckBox>(R.id.sound_flip_switch_btn)
        soundSwitch!!.setOnClickListener(SoundSwitchOnClickListener())

        clearCache = findViewById<LinearLayoutCompat>(R.id.clear_cache_layout)
        clearCache!!.setOnClickListener {
            ToastUtils.showToast("clearCache")
            //                ClearCacheDialog.getInstance(ReadSettingActivity.this, 0).showView();
        }

        soundSwitch!!.isChecked = readSetting.soundSwitch
    }

    /**
     * 音量键翻页点击事件
     *
     * @author 55345364
     */
    private inner class SoundSwitchOnClickListener : View.OnClickListener {

        override fun onClick(v: View) {
            readSetting.soundSwitch = !readSetting.soundSwitch
        }
    }

    /**
     * 监听返回
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        var flag = true
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            turnGoReadActivity()
        } else {
            flag = super.onKeyDown(keyCode, event)
        }
        return flag
    }

    /**
     * 传数据结果给ReadActivity
     */
    private fun turnGoReadActivity() {

        if (currentLanguage!=readSetting.language) {

            EventBus.getDefault().post(MessageEvent(Constants.ReadStatus.READ_LANGUANG,""))
        }
        finish()
    }
}
