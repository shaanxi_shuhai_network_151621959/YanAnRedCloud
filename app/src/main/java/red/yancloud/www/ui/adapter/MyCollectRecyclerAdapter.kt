package red.yancloud.www.ui.adapter

import android.graphics.Bitmap
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntRange
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.chad.library.adapter.base.BaseQuickAdapter
import com.zzhoujay.richtext.RichText
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.MyCollect
import java.util.*


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/14 17:17
 * E_Mail：yh131412hys@163.com
 * Describe：我的收藏、我的分享、浏览历史
 * Change：
 * @Version：V1.0
 */
class MyCollectRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val RIGHT_IMG = 1
    val VIDEO = 2

    val mData = ArrayList<MyCollect.DataBean.RecordsBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_collect_article_list,parent,false))
            VIDEO -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_collect_video_list,parent,false))
            RIGHT_IMG -> RightImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_collect_right_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.collectItemTitleTv.text=item.title
                holder.collectItemContentTv.text=item.introduce
                holder.collectItemTimeTv.text=item.time
            }
            is BigImgViewHolder -> {

                if (item.thumbnail.startsWith("http")) {

                    Glide.with(holder.itemView).load(item.thumbnail).error(R.mipmap.ic_launcher).into(holder.collectItemBigImgIv)
                }else{

                    Glide.with(holder.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.collectItemBigImgIv)
                }
                holder.collectItemTitleTv.text=item.title
                holder.collectItemTimeTv.text=item.time
            }
            is RightImgViewHolder -> {

                if (item.thumbnail.startsWith("http")) {

                    Glide.with(holder.itemView).load(item.thumbnail).error(R.mipmap.ic_launcher).into(holder.collectItemImgIv)
                }else{

                    Glide.with(holder.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.collectItemImgIv)
                }
                if (TextUtils.isEmpty(item.title)) {

                    holder.collectItemTitleTv.text=item.author
                }else{

                    holder.collectItemTitleTv.text=item.title
                }
//                holder.collectItemContentTv.text=item.introduce
                RichText.from(item.introduce).into(holder.collectItemContentTv)
                holder.collectItemTimeTv.text=item.time
            }
        }

        holder.itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv).setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemChildClickListener!!.onItemChildClick(null,holder.itemView,position)
            }
        })

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemChildClickListener: BaseQuickAdapter.OnItemChildClickListener? = null

    fun setonItemChildClickListener(onItemChildClickListener: BaseQuickAdapter.OnItemChildClickListener) {
        this.onItemChildClickListener = onItemChildClickListener
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

            when (mData[position].model) {
                Constants.CollectModel.MEDIA -> return VIDEO
                Constants.CollectModel.NEWS -> return ARTICLE
                else -> {

                    if (TextUtils.isEmpty(mData[position].thumbnail)) {

                        return ARTICLE
                    }
                    return RIGHT_IMG
                }
            }
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    /**
     * remove the item associated with the specified position of adapter
     *
     * @param position
     */
    fun remove(@IntRange(from = 0) position: Int) {
        mData.removeAt(position)
        notifyItemRemoved(position)
        compatibilityDataSizeChanged(0)
        notifyItemRangeChanged(position, mData.size - position)
    }

    /**
     * compatible getLoadMoreViewCount and getEmptyViewCount may change
     *
     * @param size Need compatible data size
     */
    private fun compatibilityDataSizeChanged(size: Int) {
        val dataSize = mData.size
        if (dataSize == size) {
            notifyDataSetChanged()
        }
    }

    fun addData(newData: List<MyCollect.DataBean.RecordsBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var collectItemTitleTv: AppCompatTextView
        var collectItemContentTv: AppCompatTextView
        var collectItemTimeTv: AppCompatTextView
//        var collectItemDeleteIv: AppCompatImageView

        init {
            collectItemTitleTv = itemView!!.findViewById(R.id.collectItem_title_tv)
            collectItemContentTv = itemView.findViewById(R.id.collectItem_content_tv)
            collectItemTimeTv = itemView.findViewById(R.id.collectItem_time_tv)
//            collectItemDeleteIv = itemView.findViewById(R.id.collectItem_delete_iv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var collectItemTitleTv: AppCompatTextView
        var collectItemBigImgIv: AppCompatImageView
        var collectItemTimeTv: AppCompatTextView
//        var collectItemDeleteIv: AppCompatImageView

        init {
            collectItemTitleTv = itemView.findViewById(R.id.collectItem_title_tv)
            collectItemBigImgIv = itemView.findViewById(R.id.collectItem_bigImg_iv)
            collectItemTimeTv = itemView.findViewById(R.id.collectItem_time_tv)
//            collectItemDeleteIv = itemView.findViewById(R.id.collectItem_delete_iv)
        }
    }

    inner class RightImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var collectItemImgIv: AppCompatImageView
        var collectItemTitleTv: AppCompatTextView
        var collectItemContentTv: AppCompatTextView
        var collectItemTimeTv: AppCompatTextView
//        var collectItemDeleteIv: AppCompatImageView

        init {
            collectItemImgIv = itemView.findViewById(R.id.collectItem_img_iv)
            collectItemTitleTv = itemView.findViewById(R.id.collectItem_title_tv)
            collectItemContentTv = itemView.findViewById(R.id.collectItem_content_tv)
            collectItemTimeTv = itemView.findViewById(R.id.collectItem_time_tv)
//            collectItemDeleteIv = itemView.findViewById(R.id.collectItem_delete_iv)
        }
    }
}
