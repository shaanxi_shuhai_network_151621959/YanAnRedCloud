package red.yancloud.www.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_expert_team_more.*
import kotlinx.android.synthetic.main.activity_expert_team_more.errorView
import kotlinx.android.synthetic.main.activity_expert_team_more.smartRefreshLayout
import kotlinx.android.synthetic.main.activity_my_collect.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseRefreshActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.ExpertTeamMore
import red.yancloud.www.internet.bean.HotSearchEntryMore
import red.yancloud.www.ui.adapter.ExpertTeamMoreRecyclerAdapter
import red.yancloud.www.ui.adapter.RecycleViewDivider
import red.yancloud.www.util.ScreenUtils
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/28 12:40
 * E_Mail：yh131412hys@163.com
 * Describe：专家团队更多
 * Change：
 * @Version：V1.0
 */
class ExpertTeamMoreActivity : BaseRefreshActivity(), View.OnClickListener {

    private val TAG = "ExpertTeamMoreActivity"

    private lateinit var mAdapter: ExpertTeamMoreRecyclerAdapter

    override val layoutId: Int
        get() = R.layout.activity_expert_team_more

    override fun configViews() {
        super.configViews()

        title_tv.text="专家团队"
        back_iv.setOnClickListener(this)
    }

    override fun initRecyclerView() {

        expertTeamMoreActivity_rV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        expertTeamMoreActivity_rV.addItemDecoration(
            RecycleViewDivider(
                this,
                DividerItemDecoration.VERTICAL,
                ScreenUtils.dpToPx(10f).toInt(),
                R.color.color_f5
            )
        )
        mAdapter= ExpertTeamMoreRecyclerAdapter()
        expertTeamMoreActivity_rV.adapter = mAdapter
        mAdapter.setOnItemClickListener(object : BaseQuickAdapter.OnItemClickListener{
            override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

                /*http://zhi.yancloud.red/index.php?r=entry/entry/view&id=*/
                startActivity(
                    Intent(mContext, NewsInfoActivity::class.java)
                        .putExtra(
                            "url",
                            "${Constants.API_BASE_RED_CLOUD_URL}News/mjentry/?id=${mAdapter.mData[position].id}")
                        .putExtra("model",Constants.CollectModel.ZHIKUMJ)
                        .putExtra("id",mAdapter.mData[position].id.toString())
                )
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {

                finish()
            }
        }
    }

    override fun getRefreshDataList() {

        loadingDialog.show()

        RedCloudApis.getInstance().getExpertTeamList(page.toString(),limit,object : Observer<ExpertTeamMore> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ExpertTeamMore) {

                Log.d(TAG,t.toString())
                if (t.code == Constants.SYSTEM_OK) {
                    if(t.data != null&& t.data.size > 0){

//                            totalPage = t.data.page.totalPage
                        errorView.visibility=View.GONE
                        smartRefreshLayout.visibility=View.VISIBLE
                        if (page==1) {

                            mAdapter.clear()
                            mAdapter.addData(t.data)
                            smartRefreshLayout.resetNoMoreData()
                            smartRefreshLayout.finishRefresh()
                        }else {

                            mAdapter.addData(t.data)
                            smartRefreshLayout.finishLoadMore()
                        }
                    }else{

                        if (page==1){

                            smartRefreshLayout.finishRefresh()

                            errorView.visibility=View.VISIBLE
                            smartRefreshLayout.visibility=View.GONE
                        }else{

                            ToastUtils.showToast(getString(R.string.data_all_load))
                            smartRefreshLayout.finishLoadMore()
                        }
                    }
                }else{

                    ToastUtils.showToast(t.msg)
                    smartRefreshLayout.finishLoadMoreWithNoMoreData()//将不会再次触发加载更多事件
                }

                loadingDialog.dismiss()
            }

            override fun onError(e: Throwable) {

                loadingDialog.dismiss()
            }
        })
    }
}
