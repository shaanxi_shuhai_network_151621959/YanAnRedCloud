package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.DiffuseYanAn
import red.yancloud.www.internet.bean.VideoList
import red.yancloud.www.util.TimeFormatUtil

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 14:36
 * E_Mail：yh131412hys@163.com
 * Describe：搜索结果列表
 * Change：
 * @Version：V1.0
 */
class SearchRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<Any,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: Any?) {

        if (item != null) {

            if (item is DiffuseYanAn.DataBean.ListBean) {

                Glide.with(helper.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(helper.getView(R.id.diffuseItem_img_iv))
                helper.setText(R.id.diffuseItem_title_tv,item.title)
                helper.setText(R.id.diffuseItem_content_tv,item.introduce)
                helper.setText(R.id.diffuseItem_time_tv,TimeFormatUtil.formatTimeDay(item.addtime.split(".")[0]))
            }
        }
    }
}