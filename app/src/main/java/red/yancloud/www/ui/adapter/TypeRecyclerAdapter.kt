package red.yancloud.www.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.internet.bean.TypeData


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/10 15:09
 * E_Mail：yh131412hys@163.com
 * Describe：页面顶部分类
 * Change：
 * @Version：V1.0
 */
class TypeRecyclerAdapter(layoutResId:Int) : BaseQuickAdapter<TypeData,BaseViewHolder>(layoutResId) {

    private var defaultSelectOne: Boolean=true

    override fun convert(helper: BaseViewHolder, item: TypeData?) {

        if (selectPosition==helper.layoutPosition) {

            helper.setTextColor(R.id.typeItem_tv,mContext.resources.getColor(R.color.main_color))
        }else{

            helper.setTextColor(R.id.typeItem_tv,mContext.resources.getColor(R.color.color_99))
        }

//        defaultSelectOne 默认选中第一项
//        selectPosition 直接刷新，导致失去默认选中
        if ((defaultSelectOne || selectPosition == -1) && helper.layoutPosition==0){

            defaultSelectOne=false
            helper.setTextColor(R.id.typeItem_tv,mContext.resources.getColor(R.color.main_color))
        }

        if (item != null) {
            helper.setText(R.id.typeItem_tv,item.type)
        }
    }

    fun setDefaultSelectOne(defaultSelectOne: Boolean) {
        this.defaultSelectOne=defaultSelectOne
        notifyDataSetChanged()
    }

    private var selectPosition = -1//用于记录用户选择的变量

    fun setSelectPosition(selectPosition: Int) {
        this.selectPosition=selectPosition
        notifyDataSetChanged()
    }
}