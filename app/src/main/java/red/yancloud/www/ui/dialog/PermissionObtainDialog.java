package red.yancloud.www.ui.dialog;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import red.yancloud.www.R;
import red.yancloud.www.manager.AppManager;
import red.yancloud.www.ui.activity.MainActivity;
import red.yancloud.www.ui.listener.OnClickPermissionListener;
import red.yancloud.www.util.ActivityUtils;


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/5 14:59
 * E_Mail：yh131412hys@163.com
 * Describe：获取权限
 * Change：
 * @Version：V1.0
 */
public class PermissionObtainDialog extends BaseDialog implements View.OnClickListener{

    private OnClickPermissionListener mClickPermissionListener;
    public PermissionObtainDialog(Context context, OnClickPermissionListener clickPermissionListener){
        super(context, R.style.dialog_from_bottom);
        mClickPermissionListener = clickPermissionListener;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_permission_obtain;
    }

    @Override
    public void initView() {
       findViewById(R.id.permission_obtain).setOnClickListener(this);
       findViewById(R.id.permission_cancel).setOnClickListener(this);
    }

    @Override
    public int gravity() {
        return Gravity.BOTTOM;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.permission_obtain:
                mClickPermissionListener.permissionObtain();
                break;

            case R.id.permission_cancel:
                ActivityUtils.finishAllActivities();
                AppManager.getAppManager().finishActivity(MainActivity.class);
                break;
        }
        dismiss();
    }
}
