package red.yancloud.www.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import androidx.annotation.NonNull;
import red.yancloud.www.ui.fragment.SortFragment;

import java.util.ArrayList;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/3 11:04
 * E_Mail：yh131412hys@163.com
 * Describe：dialog基类
 * Change：
 * @Version：V1.0
 */
public abstract class BaseDialog extends Dialog {

    public Context mContext;

    public BaseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setDialogViewLocation();
        initView();
    }

    /**
     * 设置布局位置
     */
    public void setDialogViewLocation(){
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        DisplayMetrics dms = mContext.getResources().getDisplayMetrics();
        lp.width = dms.widthPixels;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = gravity();
        window.setAttributes(lp);
    }

    public abstract int getLayoutId();
    public abstract void initView();
    public abstract int gravity();
}
