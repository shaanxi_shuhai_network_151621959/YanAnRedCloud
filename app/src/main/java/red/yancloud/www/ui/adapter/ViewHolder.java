package red.yancloud.www.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/1/18 10:05
 * E_Mail：yh131412hys@163.com
 * Describe：用于自定义的BaseCommonAdapter的ViewHolder
 * Change：
 * @Version：V1.0
 */
public class ViewHolder {

	private final SparseArray<View> mViews;

	private View mConverView;

	private ViewHolder(Context context, ViewGroup parent, int layoutId) {
		this.mViews = new SparseArray<View>();
		mConverView = LayoutInflater.from(context).inflate(layoutId, parent,false);
		mConverView.setTag(this);
	}

	public static ViewHolder get(Context context, View convertView,
			ViewGroup parent, int layoutId, int position) {
		if (null == convertView) {
			return new ViewHolder(context, parent, layoutId);
		}
		return (ViewHolder) convertView.getTag();
	}

	@SuppressWarnings("unchecked")
	public <T extends View> T getView(int viewId) {
		View view = mViews.get(viewId);
		if (null == view) {
			view = mConverView.findViewById(viewId);
			mViews.put(viewId, view);
		}
		return (T) view;
	}

	/**
	 * 获取View设置字符串内容
	 * 
	 * @param viewId
	 * @param text
	 * @return
	 */
	public ViewHolder setText(int viewId, String text) {
		TextView tv = getView(viewId);
		tv.setText(text);
		return this;
	}
	
	
	public ViewHolder setTextColor(int viewId, String text,String color) {
		TextView tv = getView(viewId);
		tv.setTextColor(Color.parseColor("#"+color));
		return this;
	}
	
	/**
	 * 设置checkBox状态
	 * @param viewId
	 * @param isCheck
	 * @param status
	 * @return
	 */
	public ViewHolder setCheckBox(int viewId, boolean isCheck, boolean status) {
		CheckBox cb = getView(viewId);
		cb.setChecked(isCheck);
		if (status) {
			cb.setVisibility(View.VISIBLE);
		} else {
			cb.setVisibility(View.GONE);
		}
		return this;
	}
	
	
	/**
	 * 是否显示图片
	 * @param viewId
	 * @param b
	 * @return
	 */
	public ViewHolder isShowImage(int viewId,boolean b){
		ImageView iv = getView(viewId);
		if(b){
			iv.setVisibility(View.VISIBLE);
		}else{
			iv.setVisibility(View.GONE);
		}
		return this;
	}
	/**
	 * 是否显示图片
	 * @param viewId
	 * @param b
	 * @return
	 */
	public ViewHolder isShowText(int viewId,boolean b){
		TextView tv = getView(viewId);
		if(b){
			tv.setVisibility(View.VISIBLE);
		}else{
			tv.setVisibility(View.GONE);
		}
		return this;
	}


	/**
	 * 是否审核
	 * @param viewId
	 * @param b
	 * @return
	 */
	public ViewHolder isShowAudit(int viewId,boolean b){
		TextView iv = getView(viewId);
		if(b){
			iv.setVisibility(View.VISIBLE);
		}else{
			iv.setVisibility(View.GONE);
		}
		return this;
	}
	
	public View getConvertView() {
		return mConverView;
	}
}
