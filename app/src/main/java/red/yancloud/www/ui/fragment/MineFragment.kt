package red.yancloud.www.ui.fragment


import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

import red.yancloud.www.R
import red.yancloud.www.base.BaseFragment
import red.yancloud.www.common.Constants
import red.yancloud.www.common.MessageEvent
import red.yancloud.www.db.sharedpreference.UserInfoShardPreference
import red.yancloud.www.internet.bean.Login
import red.yancloud.www.manager.DatabaseManager
import red.yancloud.www.ui.activity.*
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/4 17:05
 * E_Mail：yh131412hys@163.com
 * Describe：我的页面
 * Change：
 * @Version：V1.0
 */
class MineFragment : BaseFragment(), View.OnClickListener {

    private lateinit var mineFragmentUserIv: AppCompatImageView
    private lateinit var mineFragmentLoginIv: AppCompatImageView
    private lateinit var mineFragmentUserNameTv: AppCompatTextView
    private lateinit var mineFragmentSignatureTv: AppCompatTextView
    private lateinit var mineFragmentExitTv: AppCompatTextView

    override val layoutId: Int
        get() = R.layout.fragment_mine

    override fun attachView() {

    }

    override fun initData() {

        EventBus.getDefault().register(this)
    }

    override fun configView() {

        mineFragmentUserIv = parentView.findViewById<AppCompatImageView>(R.id.mineFragment_user_iv)
        mineFragmentLoginIv = parentView.findViewById<AppCompatImageView>(R.id.mineFragment_login_iv)
        mineFragmentUserNameTv = parentView.findViewById<AppCompatTextView>(R.id.mineFragment_userName_tv)
        mineFragmentSignatureTv = parentView.findViewById<AppCompatTextView>(R.id.mineFragment_signature_tv)

        mineFragmentUserIv.setOnClickListener(this)
        mineFragmentUserNameTv.setOnClickListener(this)
        parentView.findViewById<AppCompatTextView>(R.id.mineFragment_personalInformation_tv).setOnClickListener(this)
        parentView.findViewById<RelativeLayout>(R.id.mineFragment_myCollect_rl).setOnClickListener(this)
        parentView.findViewById<RelativeLayout>(R.id.mineFragment_myBookshelf_rl).setOnClickListener(this)
//        parentView.findViewById<AppCompatTextView>(R.id.mineFragment_myShare_tv).setOnClickListener(this)
//        parentView.findViewById<AppCompatTextView>(R.id.mineFragment_browseHistory_tv).setOnClickListener(this)
//        parentView.findViewById<AppCompatTextView>(R.id.mineFragment_vomit_tv).setOnClickListener(this)
        parentView.findViewById<RelativeLayout>(R.id.mineFragment_feedback_rl).setOnClickListener(this)
//        parentView.findViewById<AppCompatTextView>(R.id.mineFragment_advisoryService_tv).setOnClickListener(this)
        mineFragmentExitTv=parentView.findViewById<AppCompatTextView>(R.id.mineFragment_exit_tv)
        mineFragmentExitTv.setOnClickListener(this)

        if (UserInfoShardPreference.instance.isLogin) {

//            mContext?.let {
//                Glide.with(it).load(UserInfoShardPreference.instance.headImgUrl)
//                    .apply(RequestOptions.bitmapTransform(CircleCrop())).error(R.mipmap.mine_login_ic).into(mineFragmentUserIv)
//            }
            mineFragmentUserNameTv.text=UserInfoShardPreference.instance.userName
            mineFragmentLoginIv.visibility = View.VISIBLE
        }else{

            mineFragmentExitTv.visibility=View.GONE
            mineFragmentLoginIv.visibility = View.GONE
        }
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }

    private fun setUserInfo(loginEntity: Login.DataBean){

//        mContext?.let {
//            Glide.with(it).load(loginEntity.headimgurl)
//                .apply(RequestOptions.bitmapTransform(CircleCrop())).error(R.mipmap.mine_login_ic).into(mineFragmentUserIv)
//        }
        mineFragmentLoginIv.visibility = View.VISIBLE
        mineFragmentUserNameTv.text=loginEntity.username
        mineFragmentExitTv.visibility=View.VISIBLE
    }

    override fun onClick(v: View?) {

        if (!UserInfoShardPreference.instance.isLogin) {

            ToastUtils.showToast(R.string.not_login_prompt)
            startActivity(Intent(mContext,LoginActivity::class.java))
            return
        }
        when (v!!.id) {
//            R.id.mineFragment_user_iv-> startActivity(Intent(mContext, ChangePasswordActivity::class.java))
            R.id.mineFragment_personalInformation_tv-> startActivity(Intent(mContext,PersonalInformationActivity::class.java))
            R.id.mineFragment_myCollect_rl-> startActivity(Intent(mContext,MyCollectActivity::class.java)
                .putExtra("title","我的收藏"))
            R.id.mineFragment_myBookshelf_rl-> startActivity(Intent(mContext,MyBookshelfActivity::class.java))
//            R.id.mineFragment_myShare_tv-> startActivity(Intent(mContext,MyCollectActivity::class.java)
//                .putExtra("title","我的分享"))
//            R.id.mineFragment_browseHistory_tv-> startActivity(Intent(mContext,MyCollectActivity::class.java)
//                .putExtra("title","浏览历史"))
//            R.id.mineFragment_vomit_tv-> ToastUtils.showToast("我要吐槽")
            R.id.mineFragment_feedback_rl-> startActivity(Intent(mContext,FeedBackActivity::class.java)
                .putExtra("url","${Constants.API_BASE_RED_CLOUD_URL}user/feedback"))
//            R.id.mineFragment_advisoryService_tv-> ToastUtils.showToast("咨询客服")
            R.id.mineFragment_exit_tv-> {

                DatabaseManager.instance.deleteAll()
                UserInfoShardPreference.instance.clearUserInfo()

                mineFragmentLoginIv.visibility = View.GONE
                mineFragmentExitTv.visibility=View.GONE
                mineFragmentUserNameTv.text="登录/注册"
                mineFragmentUserIv.setImageResource(R.mipmap.mine_not_login_ic)
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent){

        when (event.type) {
            Constants.REFRESH_MINE_FRAGMENT-> {
                Log.d("REFRESH_MINE_FRAGMENT",event.type.toString())
                setUserInfo(event.result as Login.DataBean)
            }
        }
    }
}
