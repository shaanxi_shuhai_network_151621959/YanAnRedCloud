package red.yancloud.www.ui.activity

import android.text.TextUtils
import com.alibaba.fastjson.JSONObject
import com.zzhoujay.richtext.RichText
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_user_agreement.*
import kotlinx.android.synthetic.main.title_base_head.*
import okhttp3.ResponseBody

import red.yancloud.www.R
import red.yancloud.www.base.BaseActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/21 10:21
 * E_Mail：yh131412hys@163.com
 * Describe：用户协议
 * Change：
 * @Version：V1.0
 */
class UserAgreementActivity : BaseActivity() {

    override val layoutId: Int
        get() = R.layout.activity_user_agreement

    override fun initData() {

        loadingDialog.show()

        RedCloudApis.getInstance().getAbout(object : Observer<ResponseBody> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ResponseBody) {

                val result = String(t.bytes())
                if (!TextUtils.isEmpty(result)) {

                    val jsonObject = JSONObject.parseObject(result)
                    val code = jsonObject.getString("code")
                    val content = jsonObject.getJSONObject("data").getString("content")


                    if (code== Constants.SYSTEM_OK && !TextUtils.isEmpty(content)) {

                        RichText.from(content).into(userAgreementActivity_content_tv)
                    }else{

                        ToastUtils.showToast(jsonObject.getString("msg"))
                    }
                }else{

                    ToastUtils.showToast(getString(R.string.get_user_agreement))
                }

                loadingDialog.dismiss()
            }

            override fun onError(e: Throwable) {

                loadingDialog.dismiss()
            }
        })
    }

    override fun configViews() {

        title_tv.text = "用户协议"
        back_iv.setOnClickListener {

            finish()
        }
    }
}
