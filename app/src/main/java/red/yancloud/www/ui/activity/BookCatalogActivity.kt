package red.yancloud.www.ui.activity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_book_catalog.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseRefreshActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.BookCatalog
import red.yancloud.www.ui.adapter.BookCatalogRecyclerAdapter
import red.yancloud.www.ui.read.EPupReadActivity
import red.yancloud.www.util.ToastUtils


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 21:12
 * E_Mail：yh131412hys@163.com
 * Describe：书籍目录
 * Change：
 * @Version：V1.0
 */
class BookCatalogActivity : BaseRefreshActivity(),View.OnClickListener {

    private lateinit var bookId:String
    private lateinit var mAdapter:BookCatalogRecyclerAdapter

    override fun initRecyclerView() {

        bookCatalogActivity_rV.layoutManager= LinearLayoutManager(mContext, RecyclerView.VERTICAL,false)
        mAdapter = BookCatalogRecyclerAdapter(R.layout.item_book_catalog_list)
        bookCatalogActivity_rV.adapter= mAdapter
        mAdapter.onItemClickListener = object : BaseQuickAdapter.OnItemClickListener{
            override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

                startActivity(
                    Intent(mContext, EPupReadActivity::class.java)
                        .putExtra("bookId",bookId)
                        .putExtra("firstbookchapterid",(adapter!!.data[position] as BookCatalog.DataBean.ChaptersBean).id)
                )
            }
        }
    }

    override fun getRefreshDataList() {

        loadingDialog.show()

        bookId = intent.getStringExtra("bookId")
        RedCloudApis.getInstance().catalog(bookId,page.toString(), "12", object : Observer<BookCatalog> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: BookCatalog) {

                if (t.code== Constants.SYSTEM_OK) {

                    errorView.visibility= View.GONE
                    smartRefreshLayout.visibility=View.VISIBLE

                    if(t.data != null&&t.data.page!=null&& t.data.chapters != null && t.data.chapters.size > 0){

                        totalPage = t.data.page.totalPage
                        if (page==1) {

                            mAdapter.data.clear()
                            mAdapter.addData(t.data.chapters)
                            smartRefreshLayout.resetNoMoreData()
                            smartRefreshLayout.finishRefresh()
                        }else {

                            mAdapter.addData(t.data.chapters)
                            smartRefreshLayout.finishLoadMore()
                        }
                    }else{

                        if (page==1){

                            smartRefreshLayout.finishRefresh()

                            errorView.visibility=View.VISIBLE
                            smartRefreshLayout.visibility=View.GONE
                        }else{

                            ToastUtils.showToast(getString(R.string.data_all_load))
                            smartRefreshLayout.finishLoadMore()
                        }
                    }
                }else{

                    ToastUtils.showToast(t.msg)
                    errorView.visibility=View.VISIBLE
                    smartRefreshLayout.visibility=View.GONE
                }

                loadingDialog.dismiss()
            }

            override fun onError(e: Throwable) {

                loadingDialog.dismiss()
            }
        })
    }

    override val layoutId: Int
        get() = R.layout.activity_book_catalog

    override fun configViews() {
        super.configViews()

        title_tv.text="目录"
        back_iv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv-> finish()
            else -> {
            }
        }
    }
}
