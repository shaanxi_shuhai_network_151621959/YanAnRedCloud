package red.yancloud.www.ui.adapter

import android.text.TextUtils
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseSectionMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseSectionQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.SectionEntity
import com.chad.library.adapter.base.entity.SectionMultiEntity
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.SpecialTopicContent



/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/31 13:00
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题内容
 * Change：
 * @Version：V1.0
 */
class SpecialTopicContentRecyclerAdapter
/**
 * Same as QuickAdapter#QuickAdapter(Context,int) but with
 * some initialization data.
 *
 * @param sectionHeadResId The section head layout id for each item
 * @param data             A new list is created out of this one to avoid mutable list
 */
    (layoutResId: Int, data: List<MySection>) :
    BaseSectionMultiItemQuickAdapter<SpecialTopicContentRecyclerAdapter.MySection, BaseViewHolder>(
        layoutResId,
        data
    ) {

    init {

        addItemType(MySection.ARTICLE,R.layout.item_special_topic_content_article_list)
        addItemType(MySection.LEFT_IMG,R.layout.item_special_topic_content_left_img_list)
    }

    override fun convertHead(helper: BaseViewHolder, item: MySection) {

        helper.setText(R.id.specialTopicContentItem_title_tv, item.header)
        helper.addOnClickListener(R.id.specialTopicContentItem_more)
    }

    override fun convert(helper: BaseViewHolder, item: MySection) {

        val dataBean = item.t

        when (helper.itemViewType) {
            MySection.ARTICLE -> {

                helper.setText(R.id.specialTopicContentItem_title_tv, dataBean.title)
                helper.setText(R.id.specialTopicContentItem_content_tv, dataBean.introduce)
                helper.setText(R.id.specialTopicContentItem_time_tv, dataBean.addtime)
            }
            MySection.LEFT_IMG -> {

                Glide.with(helper.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL + dataBean.thumbnail)
                    .error(R.mipmap.ic_launcher)
                    .into(helper.getView(R.id.specialTopicContentItem_img_iv) as AppCompatImageView)
                helper.setText(R.id.specialTopicContentItem_title_tv, dataBean.title)
                helper.setText(R.id.specialTopicContentItem_content_tv, dataBean.introduce)
                helper.setText(R.id.specialTopicContentItem_time_tv, dataBean.addtime)
            }
        }
    }

    class MySection : SectionMultiEntity<SpecialTopicContent.DataBeanXXX.TanbensuyuanBean.DataBean> {

        var typeId: String? = null

        constructor(isHeader: Boolean, header: String,typeId: String) : super(isHeader, header) {
            this.typeId = typeId
        }

        companion object{

            val ARTICLE = 0
            val LEFT_IMG = 1
        }

        var dataBean: SpecialTopicContent.DataBeanXXX.TanbensuyuanBean.DataBean? = null

        constructor(dataBean: SpecialTopicContent.DataBeanXXX.TanbensuyuanBean.DataBean) : super(dataBean) {
            this.dataBean=dataBean
        }

        override fun getItemType(): Int {
            if (TextUtils.isEmpty(dataBean!!.thumbnail)) {

                return ARTICLE
            }
            return LEFT_IMG
        }
    }
}
