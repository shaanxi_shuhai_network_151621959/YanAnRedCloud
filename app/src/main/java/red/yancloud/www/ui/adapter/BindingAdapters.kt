package red.yancloud.www.ui.adapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import red.yancloud.www.R


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/11 16:28
 * E_Mail：yh131412hys@163.com
 * Describe：dataBinging适配器
 * Change：
 * @Version：V1.0
 */
object BindingAdapters {

    @BindingAdapter("imageUrl") //警告: Application namespace for attribute app:imageUrl will be ignored.(app:imageUrl)
    @JvmStatic//必须是静态方法
    fun bindImageUrl(view: AppCompatImageView,imageUrl: String) {
//        val options = RequestOptions()
//            .centerCrop()
//            .dontAnimate()
//
//        Glide.with(view)
//            .load(imageUrl)
//            .apply(options)
//            .into(view)
        Glide.with(view).load(imageUrl).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(view)
    }
}