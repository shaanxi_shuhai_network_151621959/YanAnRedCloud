package red.yancloud.www.ui.adapter


import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.util.MultiTypeDelegate
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.MyCollect


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/11 11:03
 * E_Mail：yh131412hys@163.com
 * Describe：我的收藏、我的分享、浏览历史
 * Change：
 * @Version：V1.0
 */
class MyCollectTRecyclerAdapter (data:List<MyCollect.DataBean.RecordsBean>): BaseQuickAdapter<MyCollect.DataBean.RecordsBean,BaseViewHolder>(data) {

    val ARTICLE = 0
    val WEB_PAGE = 1
    val VIDEO = 2

    init {
        multiTypeDelegate = object : MultiTypeDelegate<MyCollect.DataBean.RecordsBean>() {
            override fun getItemType(data: MyCollect.DataBean.RecordsBean?): Int {

                return when (data!!.model) {
                    "news" -> ARTICLE
                    "book" -> WEB_PAGE
                    "zhiku" -> VIDEO
                    else -> -1
                }
            }
        }
        multiTypeDelegate.registerItemType(ARTICLE, R.layout.item_collect_article_list)
            .registerItemType(WEB_PAGE, R.layout.item_collect_right_img_list)
            .registerItemType(VIDEO, R.layout.item_collect_video_list)
    }

    override fun convert(helper: BaseViewHolder, item: MyCollect.DataBean.RecordsBean?) {

        if (item != null) {

            when (helper.itemViewType) {
                ARTICLE -> {
    //        val collectItemTitleTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_title_tv)
    //        val collectItemContentTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_content_tv)
    //        val collectItemTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
    //        val collectItemDeleteIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
                    helper.setText(R.id.collectItem_title_tv,item.title)
                    helper.setText(R.id.collectItem_content_tv,item.introduce)
                    helper.setText(R.id.collectItem_time_tv,item.time)
                }
                WEB_PAGE -> {
//        val collectItemTitleTv = itemView!!.findViewById<AppCompatTextView>(R.id.collectItem_title_tv)
//        val collectItemImgIv = itemView!!.findViewById<AppCompatImageView>(R.id.collectItem_img_iv)
//        val collectItemTimeTv = itemView!!.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
//        val collectItemDeleteIv = itemView!!.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
                    helper.setText(R.id.collectItem_title_tv,item.title)
                    Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(helper.getView(R.id.collectItem_img_iv))
                    helper.setText(R.id.collectItem_time_tv,item.time)
                }
                VIDEO -> {
 //        val collectItemImgIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_img_iv)
//        val collectItemPlayIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_play_iv)
//        val collectItemVideoTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_videoTime_tv)
//        val collectItemTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
//        val collectItemDeleteIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
                    Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(helper.getView(R.id.collectItem_img_iv))
                    helper.setText(R.id.collectItem_videoTime_tv,item.time)
                    helper.setText(R.id.collectItem_time_tv,item.time)
                }
            }

            helper.addOnClickListener(R.id.collectItem_delete_iv)
        }
    }


//    override fun getItemViewType(position: Int): Int {
//
//        val model = data[position].model
//        if (model =="news") {
//
//            return 0
//        }else if (model=="book"){
//
//            return 1
//        }else if (model=="zhiku"){
//
//            return 2
//        }
//        return -1
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//
//        when (viewType) {
//            0 -> return ArticleViewHolder(
//                LayoutInflater.from(parent.context).inflate(
//                    R.layout.item_article_list,
//                    parent,
//                    false
//                )
//            )
//            1 -> return WebPageViewHolder(
//                LayoutInflater.from(parent.context).inflate(
//                    R.layout.item_web_page_list,
//                    parent,
//                    false
//                )
//            )
//            2 -> return VideoViewHolder(
//                LayoutInflater.from(parent.context).inflate(
//                    R.layout.item_video_list,
//                    parent,
//                    false
//                )
//            )
//            else -> return WebPageViewHolder(null)
//        }
//    }
//
//    override fun getItemCount(): Int {
//        return data.size
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//
//        val myCollectEntity = data[position]
//        when (getItemViewType(position)) {
//            0 -> {
////                if (holder is ArticleViewHolder) {
////                    holder.collectItemContentTv
////                }
//                val articleViewHolder = holder as ArticleViewHolder//强制类型转换
//                articleViewHolder.bind.ariticle=myCollectEntity
//            }
//            1 -> {
//                val webPagViewHolder = holder as WebPageViewHolder
//                webPagViewHolder.bind.webPage=myCollectEntity
//            }
//            2 -> {
//                val videoViewHolder = holder as VideoViewHolder
//                videoViewHolder.bind.video=myCollectEntity
//            }
//        }
//
//        holder.itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv).setOnClickListener(object :View.OnClickListener{
//            override fun onClick(v: View?) {
//                ToastUtils.showToast("删除")
//            }
//        })
//
//        holder.itemView.setOnClickListener(object :View.OnClickListener{
//            override fun onClick(v: View?) {
//                onItemClickListener!!.onItemClick(holder.itemView,position)
//            }
//        })
//        holder.itemView.setOnLongClickListener(object :View.OnLongClickListener{
//            override fun onLongClick(v: View?): Boolean {
//                onItemClickListener!!.onItemLongClick(holder.itemView,position)
//                return true//表示此事件已经消费，不会触发单击事件
//            }
//        })
//    }
//    internal inner class ItemsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
////        var binding: ItemBinding ? = null
////
////        init {
////            binding = DataBindingUtil.bind<ViewDataBinding>(itemView)
////        }
//    }
//    class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        var bind : ItemArticleListBinding = DataBindingUtil.bind<ViewDataBinding>(itemView) as ItemArticleListBinding
////        val collectItemTitleTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_title_tv)
////        val collectItemContentTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_content_tv)
////        val collectItemTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
////        val collectItemDeleteIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
//    }
//
//    class WebPageViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
//
//        var bind : ItemWebPageListBinding = DataBindingUtil.bind<ViewDataBinding>(itemView!!) as ItemWebPageListBinding
////        val collectItemTitleTv = itemView!!.findViewById<AppCompatTextView>(R.id.collectItem_title_tv)
////        val collectItemImgIv = itemView!!.findViewById<AppCompatImageView>(R.id.collectItem_img_iv)
////        val collectItemTimeTv = itemView!!.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
////        val collectItemDeleteIv = itemView!!.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
//    }
//
//    class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        var bind : ItemVideoListBinding = DataBindingUtil.bind<ViewDataBinding>(itemView) as ItemVideoListBinding
////        val collectItemImgIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_img_iv)
////        val collectItemPlayIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_play_iv)
////        val collectItemVideoTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_videoTime_tv)
////        val collectItemTimeTv = itemView.findViewById<AppCompatTextView>(R.id.collectItem_time_tv)
////        val collectItemDeleteIv = itemView.findViewById<AppCompatImageView>(R.id.collectItem_delete_iv)
//    }
//
//    private var onItemClickListener: OnItemClickListener? = null
//
//    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
//        this.onItemClickListener = onItemClickListener
//    }
}