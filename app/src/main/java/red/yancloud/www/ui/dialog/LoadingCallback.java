package red.yancloud.www.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import com.dyhdyh.widget.loading.factory.DialogFactory;
import red.yancloud.www.R;

import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/5 14:58
 * E_Mail：yh131412hys@163.com
 * Describe：加载框回调
 * Change：
 * @Version：V1.0
 */
public class LoadingCallback implements DialogFactory {

    @Override
    public Dialog onCreateDialog(Context context) {
        Dialog dialog = new Dialog(context, R.style.loading_dialog);
        dialog.setContentView(R.layout.layout_loading);
        return dialog;
    }

    @Override
    public void setMessage(Dialog dialog, CharSequence message) {

    }

    @Override
    public int getAnimateStyleId() {
        return R.style.dialog_fade_away;
    }
}
