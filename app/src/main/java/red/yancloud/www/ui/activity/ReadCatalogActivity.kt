package red.yancloud.www.ui.activity

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import red.yancloud.www.R

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/10 13:35
 * E_Mail：yh131412hys@163.com
 * Describe：书籍目录
 * Change：
 * @Version：V1.0
 */
class ReadCatalogActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_catalog)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        val window = window
        val lp = window.attributes
        val dms = resources.displayMetrics
        lp.width = (dms.widthPixels * 0.8).toInt()
        lp.height = dms.heightPixels
        lp.gravity = Gravity.LEFT
        window.attributes = lp
    }

    override fun onPause() {
        overridePendingTransition(R.anim.dialog_from_left_in, R.anim.dialog_from_left_out)
        super.onPause()
    }

    override fun onDestroy() {
        overridePendingTransition(R.anim.dialog_from_left_in, R.anim.dialog_from_left_out)
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            overridePendingTransition(R.anim.dialog_from_left_in, R.anim.dialog_from_left_out)
        }
        return super.onKeyDown(keyCode, event)
    }
}
