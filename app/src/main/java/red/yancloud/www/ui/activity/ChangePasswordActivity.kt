package red.yancloud.www.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.db.sharedpreference.UserInfoShardPreference
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.Default
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/22 16:34
 * E_Mail：yh131412hys@163.com
 * Describe：修改密码
 * Change：
 * @Version：V1.0
 */
class ChangePasswordActivity : BaseActivity(), View.OnClickListener {

    private val TAG = "ChangePasswordActivity"

    override val layoutId: Int
        get() = R.layout.activity_change_password

    override fun initData() {
    }

    override fun configViews() {

        title_tv.text="修改密码"

        back_iv.setOnClickListener(this)
        changePasswordActivity_sure_tv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {

                finish()
            }
            R.id.changePasswordActivity_sure_tv -> {

                val oldPassword = changePasswordActivity_oldPassword_et.text.toString().trim()
                if (oldPassword.isEmpty()) {

                    ToastUtils.showToast("请输入旧密码")
                    return
                }

                val newPassword = changePasswordActivity_newPassword_et.text.toString().trim()
                if (newPassword.isEmpty()) {

                    ToastUtils.showToast("请输入新密码")
                    return
                }
                if (newPassword.length < 6){

                    ToastUtils.showToast("请重新输入密码，长度要求不少于6位")
                    changePasswordActivity_newPassword_et.text!!.clear()
                    return
                }
                if (oldPassword==newPassword) {

                    ToastUtils.showToast("新密码与旧密码相同，请重新输入")
                    changePasswordActivity_newPassword_et.text!!.clear()
                    changePasswordActivity_repeatPassword_et.text!!.clear()
                    return
                }

                val repeatPassword = changePasswordActivity_repeatPassword_et.text.toString().trim()
                if (repeatPassword.isEmpty()) {

                    ToastUtils.showToast("请再次输入新密码")
                    return
                }

                RedCloudApis.getInstance().editPwd(UserInfoShardPreference.instance.uid,oldPassword,newPassword,repeatPassword,object : Observer<Default> {
                    override fun onComplete() {
                    }

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onNext(t: Default) {

                        Log.d(TAG,t.toString())
                        if (t.code == Constants.SYSTEM_OK) {

                            ToastUtils.showToast("密码修改成功,请重新登录")
                            UserInfoShardPreference.instance.clearUserInfo()
                            finish()
                            startActivity(Intent(this@ChangePasswordActivity,LoginActivity::class.java))
                        }else{

                            ToastUtils.showToast(t.msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                    }
                })
            }
        }
    }
}
