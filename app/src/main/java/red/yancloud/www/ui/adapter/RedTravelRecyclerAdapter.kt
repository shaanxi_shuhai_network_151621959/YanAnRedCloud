package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import android.graphics.drawable.Drawable
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.zzhoujay.richtext.RichText
import red.yancloud.www.internet.bean.RedTravel
import red.yancloud.www.util.ScreenUtils
import red.yancloud.www.util.TimeFormatUtil


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/14 16:56
 * E_Mail：yh131412hys@163.com
 * Describe：红色文旅
 * Change：
 * @Version：V1.0
 */
class RedTravelRecyclerAdapter(layoutResId: Int) : BaseQuickAdapter<RedTravel.DataBean.ListBean, BaseViewHolder>(layoutResId) {

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun convert(helper: BaseViewHolder, item: RedTravel.DataBean.ListBean?) {

        if (item != null) {

            Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).into(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {

                    val scale = resource.intrinsicWidth / resource.intrinsicHeight.toFloat()
//                    动态设置ImageView高度，防止滑动时Item中ImageView的高度为0
                    val layoutParams = helper.getView<AppCompatImageView>(R.id.redTravelItem_img_iv).layoutParams
//                    item宽度
                    layoutParams.width = (ScreenUtils.getScreenWidth() / 2 - ScreenUtils.dpToPx(10f)).toInt()
                    layoutParams.height = (layoutParams.width / scale).toInt()
                    Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).transition(DrawableTransitionOptions().crossFade()).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(helper.getView(R.id.redTravelItem_img_iv))
                }
            })

////            动态设置ImageView高度，防止滑动时Item中ImageView的高度为0
//            val layoutParams = helper.getView<AppCompatImageView>(R.id.redTravelItem_img_iv).layoutParams
//            layoutParams.width = (ScreenUtils.getScreenWidth() / 2 - ScreenUtils.dpToPx(8f)).toInt()
//            layoutParams.height = (layoutParams.width / item.scale).toInt()
//            Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).transition(DrawableTransitionOptions().crossFade()).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(helper.getView(R.id.redTravelItem_img_iv))

//            helper.setText(R.id.redTravelItem_title_tv,item.title)
            RichText.from(item.title).into(helper.getView(R.id.redTravelItem_title_tv))
            helper.setText(R.id.redTravelItem_source_tv,/*"来源 : 百度百科"*/TimeFormatUtil.formatTimeDay(item.addtime.split(".")[0]))
        }
    }
}
