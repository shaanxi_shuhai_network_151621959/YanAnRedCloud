package red.yancloud.www.ui.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import red.yancloud.www.R
import red.yancloud.www.internet.bean.BroadcastMessage

import java.util.ArrayList

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：首页广播消息列表
 * Change：
 * @Version：V1.0
 */
class BroadcastMessageRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<BroadcastMessage.DataBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_broadcast_message_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_broadcast_message_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_broadcast_message_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.broadcastMessageItemTitleTv.text=item.title
                //            holder.broadcastMessageItemContentTv.text=item.description
                holder.broadcastMessageItemTimeTv.text=item.addtime
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.thumbnail).error(R.mipmap.ic_launcher).into(holder.broadcastMessageItemBigImgIv)
                holder.broadcastMessageItemTitleTv.text=item.title
                holder.broadcastMessageItemTimeTv.text=item.addtime
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.thumbnail).error(R.mipmap.ic_launcher).into(holder.broadcastMessageItemImgIv)
                holder.broadcastMessageItemTitleTv.text=item.title
                holder.broadcastMessageItemTimeTv.text=item.addtime
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

            if (position==0) {
                val data = mData[0]

                if (TextUtils.isEmpty(data.thumbnail)) {

                    return ARTICLE
                }
                return BIG_IMG
            }
            return LEFT_IMG
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<BroadcastMessage.DataBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var broadcastMessageItemTitleTv: AppCompatTextView
        var broadcastMessageItemContentTv: AppCompatTextView
        var broadcastMessageItemTimeTv: AppCompatTextView

        init {
            broadcastMessageItemTitleTv = itemView!!.findViewById(R.id.broadcastMessageItem_title_tv)
            broadcastMessageItemContentTv = itemView.findViewById(R.id.broadcastMessageItem_content_tv)
            broadcastMessageItemTimeTv = itemView.findViewById(R.id.broadcastMessageItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var broadcastMessageItemTitleTv: AppCompatTextView
        var broadcastMessageItemBigImgIv: AppCompatImageView
        var broadcastMessageItemTimeTv: AppCompatTextView

        init {
            broadcastMessageItemTitleTv = itemView.findViewById(R.id.broadcastMessageItem_title_tv)
            broadcastMessageItemBigImgIv = itemView.findViewById(R.id.broadcastMessageItem_bigImg_iv)
            broadcastMessageItemTimeTv = itemView.findViewById(R.id.broadcastMessageItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var broadcastMessageItemImgIv: AppCompatImageView
        var broadcastMessageItemTitleTv: AppCompatTextView
        var broadcastMessageItemContentTv: AppCompatTextView
        var broadcastMessageItemTimeTv: AppCompatTextView

        init {
            broadcastMessageItemImgIv = itemView.findViewById(R.id.broadcastMessageItem_img_iv)
            broadcastMessageItemTitleTv = itemView.findViewById(R.id.broadcastMessageItem_title_tv)
            broadcastMessageItemContentTv = itemView.findViewById(R.id.broadcastMessageItem_content_tv)
            broadcastMessageItemTimeTv = itemView.findViewById(R.id.broadcastMessageItem_time_tv)
        }
    }
}
