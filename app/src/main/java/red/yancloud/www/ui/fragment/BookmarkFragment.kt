package red.yancloud.www.ui.fragment

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.ListView
import androidx.appcompat.widget.LinearLayoutCompat
import red.yancloud.www.R
import red.yancloud.www.base.BaseFragment
import red.yancloud.www.internet.bean.BookMarkEntity
import red.yancloud.www.manager.DatabaseManager
import red.yancloud.www.ui.adapter.BaseCommonAdapter
import red.yancloud.www.ui.adapter.ViewHolder
import red.yancloud.www.ui.read.EPupReadActivity
import red.yancloud.www.util.TimeFormatUtil
import red.yancloud.www.util.ToastUtils

import java.util.ArrayList
import java.util.Date

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/23 14:42
 * E_Mail：yh131412hys@163.com
 * Describe：阅读页面侧滑展示的书签页
 * Change：
 * @Version：V1.0
 */
class BookmarkFragment : BaseFragment(), AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener,
    View.OnClickListener {

    private lateinit var deleteMarkLayout: LinearLayoutCompat
    private lateinit var mListView: ListView
    private lateinit var noBookMark: ImageView

    private var bookMarkerTag = false
    private var markList: MutableList<BookMarkEntity>? = null
    private var markCommonAdapter: BaseCommonAdapter<BookMarkEntity>? = null

    private var tempList: MutableList<BookMarkEntity> = ArrayList()

    override val layoutId: Int
        get() = R.layout.fragment_bookmark

    override fun attachView() {

    }

    override fun initData() {

        deleteMarkLayout = parentView.findViewById(R.id.delete_menu)
        mListView = parentView.findViewById(R.id.mark_listView)
        noBookMark = parentView.findViewById(R.id.no_data_mark)

        parentView.findViewById<View>(R.id.delete_book).setOnClickListener(this)
        parentView.findViewById<View>(R.id.cancle_book).setOnClickListener(this)

        markList = DatabaseManager.instance.queryBookMarkList(arguments!!.getString("bookId")!!) as MutableList<BookMarkEntity>

        if (null != markList && markList!!.size > 0) {
            noBookMark.visibility = View.GONE
            markCommonAdapter = object : BaseCommonAdapter<BookMarkEntity>(
                mContext, markList,
                R.layout.item_read_mark
            ) {
                override fun convert(helper: ViewHolder, itme: BookMarkEntity, position: Int) {

                    helper.setText(R.id.mark_name, itme.chapterName)
                    helper.setText(R.id.mark_work, itme.content)
                    helper.setText(R.id.mark_time, TimeFormatUtil.dateToString(Date(itme.time)))
                    helper.setCheckBox(R.id.mark_is_delete, itme.isCheck, bookMarkerTag)
                }
            }
            mListView.adapter = markCommonAdapter
            mListView.visibility = View.VISIBLE
            mListView.onItemClickListener = this
            mListView.onItemLongClickListener = this
        } else {
            noBookMark.visibility = View.VISIBLE
            mListView.visibility = View.GONE
        }
    }

    override fun configView() {

    }

    override fun onItemClick(
        parent: AdapterView<*>, view: View, position: Int,
        id: Long
    ) {
        if (bookMarkerTag) {
            markList!![position].isCheck = !markList!![position].isCheck
            markCommonAdapter!!.notifyDataSetChanged()
        } else {
            markCurrentRead(position)
        }
    }

    /**
     * 从书签跳转到阅读页面
     *
     * @param position
     */
    private fun markCurrentRead(position: Int) {
        val intent = Intent(mContext,EPupReadActivity::class.java)
        intent.putExtra("markType", 1)
        intent.putExtra("mark", markList!![position])
//        (mContext as Activity).setResult(Activity.RESULT_OK, intent)
        startActivity(intent)
        if (mContext is Activity) {
            val activity = mContext as Activity?
            activity!!.finish()
        }
    }

    override fun onItemLongClick(
        parent: AdapterView<*>, view: View,
        position: Int, id: Long
    ): Boolean {

        bookMarkerTag = !bookMarkerTag
        if (bookMarkerTag) {
            deleteMarkLayout.visibility = View.VISIBLE
        } else {
            deleteMarkLayout.visibility = View.GONE
        }

        markCommonAdapter!!.notifyDataSetChanged()

        return false
    }

    /**
     * 删除书签
     */
    private fun deleteMark() {

        if (checkChapter.size == 0) {
            ToastUtils.showToast(R.string.delete_bookmark)
            return
        } else {
            tempList = checkChapter
            for (i in tempList.indices) {
                DatabaseManager.instance.deleteBookMaker(tempList[i])
                markList!!.remove(tempList[i])
            }
        }

        if (markList == null || markList!!.size == 0) {
            noBookMark.visibility = View.VISIBLE
            mListView.visibility = View.GONE
            deleteMarkLayout.visibility = View.GONE

        }
        markCommonAdapter!!.notifyDataSetChanged()
    }

    private val checkChapter: MutableList<BookMarkEntity>
        get() {
            tempList.clear()
            for (i in markList!!.indices) {
                if (markList!![i].isCheck) {
                    tempList.add(markList!![i])
                }
            }
            return tempList
        }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.delete_book -> deleteMark()
            R.id.cancle_book -> {
                bookMarkerTag = false
                deleteMarkLayout.visibility = View.GONE
            }
        }
    }
}
