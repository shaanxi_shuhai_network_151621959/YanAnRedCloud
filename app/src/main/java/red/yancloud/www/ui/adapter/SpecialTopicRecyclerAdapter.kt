package red.yancloud.www.ui.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.ExpertTeamMore
import red.yancloud.www.internet.bean.HotSearchEntryMore
import red.yancloud.www.internet.bean.SpecialTopic
import red.yancloud.www.util.TimeFormatUtil
import java.util.*

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题
 * Change：
 * @Version：V1.0
 */
class SpecialTopicRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<SpecialTopic.DataBean.SubjectsBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_special_topic_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_special_topic_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_special_topic_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.specialTopicItemTitleTv.text=item.name
                holder.specialTopicItemContentTv.text=item.introduce
                holder.specialTopicItemTimeTv.text=item.addtime
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.specialTopicItemBigImgIv)
                holder.specialTopicItemTitleTv.text=item.name
                holder.specialTopicItemTimeTv.text=item.addtime
            }
            is LeftImgViewHolder -> {

                val imgUrl = if (!TextUtils.isEmpty(item.thumbnail)) {

                    Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail
                }else {

                    Constants.BASE_RED_CLOUD_HOST_URL+item.img
                }
                Glide.with(holder.itemView).load(imgUrl).error(R.mipmap.ic_launcher).into(holder.specialTopicItemImgIv)
                holder.specialTopicItemTitleTv.text=item.name
                holder.specialTopicItemContentTv.text=item.introduce
                holder.specialTopicItemTimeTv.text=item.addtime
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

//            if (position==0) {
//                val data = mData[0]
//
//                if (TextUtils.isEmpty(data.thumbnail)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }
            return LEFT_IMG
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<SpecialTopic.DataBean.SubjectsBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var specialTopicItemTitleTv: AppCompatTextView
        var specialTopicItemContentTv: AppCompatTextView
        var specialTopicItemTimeTv: AppCompatTextView

        init {
            specialTopicItemTitleTv = itemView!!.findViewById(R.id.specialTopicItem_title_tv)
            specialTopicItemContentTv = itemView.findViewById(R.id.specialTopicItem_content_tv)
            specialTopicItemTimeTv = itemView.findViewById(R.id.specialTopicItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var specialTopicItemTitleTv: AppCompatTextView
        var specialTopicItemBigImgIv: AppCompatImageView
        var specialTopicItemTimeTv: AppCompatTextView

        init {
            specialTopicItemTitleTv = itemView.findViewById(R.id.specialTopicItem_title_tv)
            specialTopicItemBigImgIv = itemView.findViewById(R.id.specialTopicItem_bigImg_iv)
            specialTopicItemTimeTv = itemView.findViewById(R.id.specialTopicItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var specialTopicItemImgIv: AppCompatImageView
        var specialTopicItemTitleTv: AppCompatTextView
        var specialTopicItemContentTv: AppCompatTextView
        var specialTopicItemTimeTv: AppCompatTextView

        init {
            specialTopicItemImgIv = itemView.findViewById(R.id.specialTopicItem_img_iv)
            specialTopicItemTitleTv = itemView.findViewById(R.id.specialTopicItem_title_tv)
            specialTopicItemContentTv = itemView.findViewById(R.id.specialTopicItem_content_tv)
            specialTopicItemTimeTv = itemView.findViewById(R.id.specialTopicItem_time_tv)
        }
    }
}
