package red.yancloud.www.ui.activity

import android.view.View
import kotlinx.android.synthetic.main.activity_book_info.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseWebViewActivity

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/26 9:45
 * E_Mail：yh131412hys@163.com
 * Describe：意见反馈
 * Change：
 * @Version：V1.0
 */
class SuggestFeedbackActivity : BaseWebViewActivity() {

    override val layoutId: Int
        get() = R.layout.activity_suggest_feedback

    override fun configViews() {
        super.configViews()

        back_iv.setOnClickListener(this)

        webViewActivity_refreshLayout.setEnableLoadMore(false)
    }

    override fun addJavaScriptInterface() {

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv-> finish()
        }
    }
}
