package red.yancloud.www.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import org.greenrobot.eventbus.EventBus;
import red.yancloud.www.R;
import red.yancloud.www.common.Constants;
import red.yancloud.www.common.MessageEvent;
import red.yancloud.www.db.sharedpreference.ReadSettingShardPreference;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/7 9:57
 * E_Mail：yh131412hys@163.com
 * Describe：阅读页面更多背景设置
 * Change：
 * @Version：V1.0
 */
public class BookReadThemeSettingDialog extends Dialog{

    private Context mContext;
    private AppCompatImageView[] themeStyleViews;
    private int[] themeStyleIds;

    private BookReadThemeSettingDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
    }

    public BookReadThemeSettingDialog(Context context) {
        this(context, R.style.dialog_from_bottom);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_read_theme_setting);
        initValues();
        initView();
    }

    private void initView() {

        themeStyleIds = new int[]{
                R.id.read_style_theme_00,R.id.read_style_theme_01,R.id.read_style_theme_02,
                R.id.read_style_theme_03,R.id.read_style_theme_04,R.id.read_style_theme_05,
                R.id.read_style_theme_06,R.id.read_style_theme_07,R.id.read_style_theme_08,
                R.id.read_style_theme_09,R.id.read_style_theme_10,R.id.read_style_theme_11,
                R.id.read_style_theme_12,R.id.read_style_theme_13,R.id.read_style_theme_14,
                R.id.read_style_theme_15,R.id.read_style_theme_16,R.id.read_style_theme_17,
        };

        themeStyleViews = new AppCompatImageView[themeStyleIds.length];
        for (int i = 0; i < themeStyleIds.length; i++) {
            themeStyleViews[i] = findViewById(themeStyleIds[i]);
            themeStyleViews[i].setOnClickListener(new ReadThemeStyleOnClickListener());
        }

        for (int i = 0; i < themeStyleIds.length; i++) {
            if(ReadSettingShardPreference.Companion.getInstance().getReadStyle() == i){
                themeStyleViews[i].setSelected(true);
            }
        }

        setReadStyle(ReadSettingShardPreference.Companion.getInstance().getReadStyle());
    }


    private void initValues() {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        DisplayMetrics dms = mContext.getResources().getDisplayMetrics();
        lp.width = dms.widthPixels;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
    }


    /**
     * 主题设置按钮点击监听事件
     * @author Wangxu
     *
     */
    private class ReadThemeStyleOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < themeStyleIds.length; i++) {
                if (v == themeStyleViews[i]) {
                    themeStyleViews[i].setSelected(true);
                    ReadSettingShardPreference.Companion.getInstance().setReadStyle(i);
                } else {
                    themeStyleViews[i].setSelected(false);
                }
            }

            setReadStyle(ReadSettingShardPreference.Companion.getInstance().getReadStyle());
//            mHandler.sendEmptyMessage(Constants.ReadDialogSetting.REFRESH_ACTIVITY);
            /**
             * 设置阅读主题样式
             */
            EventBus.getDefault().post(new MessageEvent(Constants.ReadStatus.READ_STYLE_THEME,ReadSettingShardPreference.Companion.getInstance().getReadStyle()));
        }
    }

    /**
     * 设置阅读主题样式
     * @param readStyle
     */
    private void setReadStyle(int readStyle){

//        if(readStyle == ThemeManager.READ_STYLE_THEME_05 || readStyle == ThemeManager.READ_STYLE_THEME_08 || readStyle == ThemeManager.READ_STYLE_THEME_10){
//
//            pageFactory.setTextColor(Color.rgb(89, 89, 89)/*R.color.primary_text*/);
//            pageFactory.setBaseColor(R.color.color_21);
//        }else {
//
//            pageFactory.setTextColor(Color.rgb(71, 77, 91)/*R.color.title_text*/);
//            pageFactory.setBaseColor(R.color.color_33);
//        }
//        pageFactory.setBgBitmap(ThemeManager.getThemeDrawable(readStyle));
    }
}
