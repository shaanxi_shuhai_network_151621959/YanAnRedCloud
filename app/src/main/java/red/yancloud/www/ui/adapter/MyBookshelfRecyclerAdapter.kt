package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.MyBookShelf

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/11 18:32
 * E_Mail：yh131412hys@163.com
 * Describe：我的书架
 * Change：
 * @Version：V1.0
 */
class MyBookshelfRecyclerAdapter(layoutResId: Int) : BaseQuickAdapter<MyBookShelf.DataBean.RecordsBean,BaseViewHolder>(layoutResId){

    override fun convert(helper: BaseViewHolder, item: MyBookShelf.DataBean.RecordsBean?) {

        if (item != null) {

            Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).error(R.mipmap.ic_launcher)/*.thumbnail(Glide.with(view).load(R.mipmap.gilde_load_ic))*/.into(helper.getView(R.id.bookshelfItem_img_iv))
            helper.setText(R.id.bookshelfItem_title_tv,item.title)
            helper.setText(R.id.bookshelfItem_author_tv,item.author.plus("  著"))
            if (item.jindu!=null) {

                helper.setText(R.id.bookshelfItem_progress_tv, "读到 ".plus((item.jindu.toFloat()*100).toInt().toString().plus("%")))
            } else{
                helper.setText(R.id.bookshelfItem_progress_tv, "读到 0%")
            }
            helper.addOnClickListener(R.id.bookshelfItem_delete_iv)
        }
    }
}
//class MyBookshelfRecyclerAdapter(private val data: List<MyBookShelf.DataBean.RecordsBean>,private val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<MyBookshelfRecyclerAdapter.ViewHolder>(){
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_bookshelf_list,parent,false))
//    }
//
//    override fun getItemCount(): Int {
//        return data.size
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//
//        val recordsBean = data[position]
//        holder.bind.bookshelf= recordsBean
//
//        holder.itemView.findViewById<AppCompatTextView>(R.id.bookshelfItem_author_tv).text=recordsBean.author.plus("  著")
//        if (recordsBean.jindu!=null) {
//
//            holder.itemView.findViewById<AppCompatTextView>(R.id.bookshelfItem_progress_tv).text= "读到".plus((recordsBean.jindu.toFloat()*100).toInt().toString().plus("%"))
//        } else{
//
//            holder.itemView.findViewById<AppCompatTextView>(R.id.bookshelfItem_progress_tv).text="读到0%"
//        }
//
//        holder.itemView.findViewById<AppCompatImageView>(R.id.bookshelfItem_delete_iv).setOnClickListener(object :View.OnClickListener{
//            override fun onClick(v: View?) {
//                ToastUtils.showToast("删除")
//            }
//        })
//
//        holder.itemView.setOnClickListener(object :View.OnClickListener{
//            override fun onClick(v: View?) {
//                onItemClickListener.onItemClick(holder.itemView,position)
//            }
//        })
//        holder.itemView.setOnLongClickListener(object :View.OnLongClickListener{
//            override fun onLongClick(v: View?): Boolean {
//                onItemClickListener.onItemLongClick(holder.itemView,position)
//                return true//表示此事件已经消费，不会触发单击事件
//            }
//        })
//    }
//
//    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        val bind : ItemBookshelfListBinding = DataBindingUtil.bind<ViewDataBinding>(itemView) as ItemBookshelfListBinding
//    }
//}