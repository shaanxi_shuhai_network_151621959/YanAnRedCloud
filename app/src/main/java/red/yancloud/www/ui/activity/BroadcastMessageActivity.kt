package red.yancloud.www.ui.activity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_broadcast_message.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseRefreshActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.BroadcastMessage
import red.yancloud.www.ui.adapter.BroadcastMessageRecyclerAdapter
import red.yancloud.www.ui.adapter.RecycleViewDivider
import red.yancloud.www.util.ScreenUtils
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/24 19:33
 * E_Mail：yh131412hys@163.com
 * Describe：首页广播消息列表
 * Change：
 * @Version：V1.0
 */
class BroadcastMessageActivity : BaseRefreshActivity(), View.OnClickListener {

    private val TAG = "BroadcastMessageActivity"

    private lateinit var mRecyclerAdapter: BroadcastMessageRecyclerAdapter

    override val layoutId: Int
        get() = R.layout.activity_broadcast_message

    override fun configViews() {
        super.configViews()

        title_tv.text=intent.getStringExtra("type")
        back_iv.setOnClickListener(this)
    }

    override fun initRecyclerView() {

        broadcastMessageActivity_rV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        broadcastMessageActivity_rV.addItemDecoration(
            RecycleViewDivider(
                this,
                DividerItemDecoration.VERTICAL,
                ScreenUtils.dpToPx(10f).toInt(),
                R.color.color_f5
            )
        )
        mRecyclerAdapter= BroadcastMessageRecyclerAdapter()
        broadcastMessageActivity_rV.adapter = mRecyclerAdapter
        mRecyclerAdapter.setOnItemClickListener(object :BaseQuickAdapter.OnItemClickListener{
            override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

                ToastUtils.showToast(mRecyclerAdapter.mData[position].id)
                startActivity(
                    Intent(mContext, NewsInfoActivity::class.java)
                        .putExtra(
                            "url",
                            "${Constants.API_BASE_RED_CLOUD_URL}News/index?id=${mRecyclerAdapter.mData[position].id}")
                )
            }
        })
    }

    override fun getRefreshDataList() {

        getEventNews()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {

                finish()
            }
        }
    }

    private fun getEventNews() {

        loadingDialog.show()

        RedCloudApis.getInstance().getEventNews(
            page.toString(),
            limit,
            object : Observer<BroadcastMessage> {

                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: BroadcastMessage) {

                    if (t.code == Constants.SYSTEM_OK) {
                        if(t.data != null&& t.data.size > 0){

//                            totalPage = t.data.page.totalPage
                            errorView.visibility=View.GONE
                            smartRefreshLayout.visibility=View.VISIBLE
                            if (page==1) {

                                mRecyclerAdapter.clear()
                                mRecyclerAdapter.addData(t.data)
                                smartRefreshLayout.resetNoMoreData()
                                smartRefreshLayout.finishRefresh()
                            }else {

                                mRecyclerAdapter.addData(t.data)
                                smartRefreshLayout.finishLoadMore()
                            }
                        }else{

                            if (page==1){

                                smartRefreshLayout.finishRefresh()

                                errorView.visibility=View.VISIBLE
                                smartRefreshLayout.visibility=View.GONE
                            }else{

                                ToastUtils.showToast(getString(R.string.data_all_load))
                                smartRefreshLayout.finishLoadMore()
                            }
                        }
                    }

                    loadingDialog.dismiss()
                }

                override fun onError(e: Throwable) {

                    loadingDialog.dismiss()
                }
            })
    }
}
