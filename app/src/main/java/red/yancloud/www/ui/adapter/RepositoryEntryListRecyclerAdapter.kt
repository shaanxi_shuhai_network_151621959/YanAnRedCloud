package red.yancloud.www.ui.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.zzhoujay.richtext.RichText
import com.zzhoujay.richtext.ig.DefaultImageGetter
import red.yancloud.www.R
import red.yancloud.www.internet.bean.RepositoryFirstLevelData
import red.yancloud.www.util.TimeFormatUtil

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 19:25
 * E_Mail：yh131412hys@163.com
 * Describe：知库词条
 * Change：
 * @Version：V1.0
 */
class RepositoryEntryListRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<RepositoryFirstLevelData.DataBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(
                LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repository_entry_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_reposityoy_entry_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repository_entry_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.repositoryEntryItemTitleTv.text=item.title
                RichText.from(item.description).imageGetter(DefaultImageGetter()).into(holder.repositoryEntryItemContentTv)
                holder.repositoryEntryItemTimeTv.text= TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.img).error(R.mipmap.ic_launcher).into(holder.repositoryEntryItemBigImgIv)
                holder.repositoryEntryItemTitleTv.text=item.title
                holder.repositoryEntryItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.img).error(R.mipmap.ic_launcher).into(holder.repositoryEntryItemImgIv)
                holder.repositoryEntryItemTitleTv.text=item.title
                RichText.from(item.description).into(holder.repositoryEntryItemContentTv)
                holder.repositoryEntryItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

//            if (position==0) {
//                val data = mData[0]
//
//                if (TextUtils.isEmpty(data.img)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }
            return LEFT_IMG
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<RepositoryFirstLevelData.DataBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var repositoryEntryItemTitleTv: AppCompatTextView
        var repositoryEntryItemContentTv: AppCompatTextView
        var repositoryEntryItemTimeTv: AppCompatTextView

        init {
            repositoryEntryItemTitleTv = itemView!!.findViewById(R.id.repositoryEntryItem_title_tv)
            repositoryEntryItemContentTv = itemView.findViewById(R.id.repositoryEntryItem_content_tv)
            repositoryEntryItemTimeTv = itemView.findViewById(R.id.repositoryEntryItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var repositoryEntryItemTitleTv: AppCompatTextView
        var repositoryEntryItemBigImgIv: AppCompatImageView
        var repositoryEntryItemTimeTv: AppCompatTextView

        init {
            repositoryEntryItemTitleTv = itemView.findViewById(R.id.repositoryEntryItem_title_tv)
            repositoryEntryItemBigImgIv = itemView.findViewById(R.id.repositoryEntryItem_bigImg_iv)
            repositoryEntryItemTimeTv = itemView.findViewById(R.id.repositoryEntryItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var repositoryEntryItemImgIv: AppCompatImageView
        var repositoryEntryItemTitleTv: AppCompatTextView
        var repositoryEntryItemContentTv: AppCompatTextView
        var repositoryEntryItemTimeTv: AppCompatTextView

        init {
            repositoryEntryItemImgIv = itemView.findViewById(R.id.repositoryEntryItem_img_iv)
            repositoryEntryItemTitleTv = itemView.findViewById(R.id.repositoryEntryItem_title_tv)
            repositoryEntryItemContentTv = itemView.findViewById(R.id.repositoryEntryItem_content_tv)
            repositoryEntryItemTimeTv = itemView.findViewById(R.id.repositoryEntryItem_time_tv)
        }
    }
}