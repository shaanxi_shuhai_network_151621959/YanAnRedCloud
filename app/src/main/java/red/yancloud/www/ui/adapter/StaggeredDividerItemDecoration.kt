package red.yancloud.www.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import android.R.attr.bottom
import android.R.attr.left
import android.content.Context
import android.graphics.Rect
import red.yancloud.www.util.Utils.getResources
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.StaggeredGridLayoutManager




/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/17 16:14
 * E_Mail：yh131412hys@163.com
 * Describe：recyclerView间隔线
 * Change：
 * @Version：V1.0
 */
class StaggeredDividerItemDecoration(private val context: Context, private val interval: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        //        int position = parent.getChildAdapterPosition(view);
        val params = view.layoutParams as StaggeredGridLayoutManager.LayoutParams
        // 获取item在span中的下标
        val spanIndex = params.spanIndex
        val interval = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this.interval.toFloat(), context.resources.displayMetrics
        ).toInt()
        // 中间间隔
        if (spanIndex % 2 == 0) {
            outRect.left = 0
        } else {
            // item为奇数位，设置其左间隔为5dp
            outRect.left = interval
        }
        // 下方间隔
        outRect.bottom = interval
    }
}