package red.yancloud.www.ui.adapter

import android.graphics.Bitmap
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.chad.library.adapter.base.BaseQuickAdapter
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.Really
import red.yancloud.www.util.ScreenUtils

import java.util.ArrayList
import kotlin.math.roundToInt

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：去伪存真
 * Change：
 * @Version：V1.0
 */
class LookDummySeekTruthRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<Really.DataBean.ReallysBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_seek_truth_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_seek_truth_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_seek_truth_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.lookDummySeekTruthItemTitleTv.text=item.title
                holder.lookDummySeekTruthItemContentTv.text=item.introduce
                holder.lookDummySeekTruthItemTimeTv.text=item.addtime
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(holder.lookDummySeekTruthItemBigImgIv)
                holder.lookDummySeekTruthItemTitleTv.text=item.title
                holder.lookDummySeekTruthItemTimeTv.text=item.addtime
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).override(ScreenUtils.dpToPx(118f).toInt(), ScreenUtils.dpToPx(80f).toInt()).fitCenter().error(R.mipmap.ic_launcher).into(holder.lookDummySeekTruthItemImgIv)
//                Glide.with(holder.itemView.context).asBitmap().load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail)
//                    .into(object : SimpleTarget<Bitmap>() {
//                        override fun onResourceReady(
//                            resource: Bitmap,
//                            transition: Transition<in Bitmap>?
//                        ) {
//
//                            val imageWidth = resource.width
//                            val imageHeight = resource.height
//
////                            val i = imageWidth * 1.0f / imageHeight
////                            val j = 118 * 1.0f / 80
//
//                            //宽度固定,然后根据原始宽高比得到此固定宽度需要的高度
//                            val maxHeight = ScreenUtils.dpToPx(80f)
//                            val maxWidth = ScreenUtils.dpToPx(118f)
//                            var height = maxWidth * imageHeight / imageWidth
//                            if (height>maxHeight){
//
//                                height = maxHeight
//                            }
//                            var width = maxWidth * imageWidth / imageHeight
//                            if (width>maxHeight){
//
//                                width = maxWidth
//                            }
//                            val layoutParams = holder.lookDummySeekTruthItemImgIv.layoutParams
//                            layoutParams.height = height.toInt()
//                            layoutParams.width = width.toInt()
//                            holder.lookDummySeekTruthItemImgIv.setImageBitmap(resource)
//                        }
//                    })
                holder.lookDummySeekTruthItemTitleTv.text=item.title
                holder.lookDummySeekTruthItemContentTv.text=item.introduce
                holder.lookDummySeekTruthItemTimeTv.text=item.addtime
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

            val data = mData[position]

//            if (position==0) {
//
//                if (TextUtils.isEmpty(data.thumbnail)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }else{

                if (TextUtils.isEmpty(data.thumbnail)) {

                    return ARTICLE
                }
                return LEFT_IMG
//            }
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<Really.DataBean.ReallysBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var lookDummySeekTruthItemTitleTv: AppCompatTextView
        var lookDummySeekTruthItemContentTv: AppCompatTextView
        var lookDummySeekTruthItemTimeTv: AppCompatTextView

        init {
            lookDummySeekTruthItemTitleTv = itemView!!.findViewById(R.id.lookDummySeekTruthItem_title_tv)
            lookDummySeekTruthItemContentTv = itemView.findViewById(R.id.lookDummySeekTruthItem_content_tv)
            lookDummySeekTruthItemTimeTv = itemView.findViewById(R.id.lookDummySeekTruthItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var lookDummySeekTruthItemTitleTv: AppCompatTextView
        var lookDummySeekTruthItemBigImgIv: AppCompatImageView
        var lookDummySeekTruthItemTimeTv: AppCompatTextView

        init {
            lookDummySeekTruthItemTitleTv = itemView.findViewById(R.id.lookDummySeekTruthItem_title_tv)
            lookDummySeekTruthItemBigImgIv = itemView.findViewById(R.id.lookDummySeekTruthItem_bigImg_iv)
            lookDummySeekTruthItemTimeTv = itemView.findViewById(R.id.lookDummySeekTruthItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var lookDummySeekTruthItemImgIv: AppCompatImageView
        var lookDummySeekTruthItemTitleTv: AppCompatTextView
        var lookDummySeekTruthItemContentTv: AppCompatTextView
        var lookDummySeekTruthItemTimeTv: AppCompatTextView

        init {
            lookDummySeekTruthItemImgIv = itemView.findViewById(R.id.lookDummySeekTruthItem_img_iv)
            lookDummySeekTruthItemTitleTv = itemView.findViewById(R.id.lookDummySeekTruthItem_title_tv)
            lookDummySeekTruthItemContentTv = itemView.findViewById(R.id.lookDummySeekTruthItem_content_tv)
            lookDummySeekTruthItemTimeTv = itemView.findViewById(R.id.lookDummySeekTruthItem_time_tv)
        }
    }
}
