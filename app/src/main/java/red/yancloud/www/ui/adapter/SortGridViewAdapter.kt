package red.yancloud.www.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import red.yancloud.www.R
import red.yancloud.www.ui.fragment.SortFragment
import red.yancloud.www.util.ScreenUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/5/9 14:12
 * E_Mail：yh131412hys@163.com
 * Describe：分类
 * Change：
 * @Version：V1.0
 */
class SortGridViewAdapter(private val mContext: Context, private val softBeanList: List<SortFragment.SortBean>?) : BaseAdapter() {

   /* init {
        //给数组设置大小,并且全部赋值为false
        if (softBeanList != null) {
            isCheck = BooleanArray(softBeanList.size)
            for (i in softBeanList.indices) {
                isCheck!![i] = false
            }
        }

        if (item != null&&helper != null) {

            helper.setText(R.id.sortItem_title_tv,item.title)
            helper.setText(R.id.sortItem_content_tv,item.content)
        }
    }*/

    override fun getCount(): Int {
        return softBeanList?.size ?: 0
    }

    override fun getItem(position: Int): SortFragment.SortBean? {
        return if (softBeanList != null) softBeanList[position] else null
    }

    override fun getItemId(position: Int): Long {
        return (if (softBeanList != null) position else 0).toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var view = convertView
        val viewHolder: ViewHolder
        if (view == null) {

            view = View.inflate(mContext, R.layout.item_sort_list, null)
            viewHolder = ViewHolder(view!!)
            //设置gridview item宽高自动适应各种屏幕        
            val width = (ScreenUtils.getScreenWidth()-30)/2
            //5表示子元素之间的距离,3表示一排显示的个数,这样就平均排列了         
            val params = convertView?.layoutParams
            if (params != null) {
                params.width = width
                convertView.layoutParams = params
            }
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.itemTitleTv.text= softBeanList?.get(position)!!.title
        viewHolder.itemContentTv.text= softBeanList[position].content
        return view
    }

    internal class ViewHolder(view: View) {
        var itemTitleTv: AppCompatTextView = view.findViewById(R.id.sortItem_title_tv)
        var itemContentTv: AppCompatTextView = view.findViewById(R.id.sortItem_content_tv)
    }
}
