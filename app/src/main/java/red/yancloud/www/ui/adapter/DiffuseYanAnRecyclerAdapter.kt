package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.zzhoujay.richtext.RichText
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.DiffuseYanAn
import red.yancloud.www.internet.bean.VideoList
import red.yancloud.www.util.TimeFormatUtil

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/19 14:36
 * E_Mail：yh131412hys@163.com
 * Describe：漫绘延安
 * Change：
 * @Version：V1.0
 */
class DiffuseYanAnRecyclerAdapter(layoutResId:Int):BaseQuickAdapter<DiffuseYanAn.DataBean.ListBean,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: DiffuseYanAn.DataBean.ListBean?) {

        if (item != null) {

            Glide.with(helper.itemView).load(Constants.BASE_RED_CLOUD_HOST_URL+item.thumbnail).error(R.mipmap.ic_launcher).into(helper.getView(R.id.diffuseItem_img_iv))
            helper.setText(R.id.diffuseItem_title_tv,item.title)
//            helper.setText(R.id.diffuseItem_content_tv,item.introduce)
            RichText.from(item.introduce).into(helper.getView(R.id.diffuseItem_content_tv))
            helper.setText(R.id.diffuseItem_time_tv,TimeFormatUtil.formatTimeDay(item.addtime.split(".")[0]))
        }
    }
}