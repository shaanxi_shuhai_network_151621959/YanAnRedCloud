package red.yancloud.www.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/1/18 10:02
 * E_Mail：yh131412hys@163.com
 * Describe：自定义的BaseCommonAdapter
 * Change：
 * @Version：V1.0
 */
public abstract class BaseCommonAdapter<T> extends BaseAdapter {

	private List<T> mData;
	private Context mContext;
	private final int mItemLayoutId;
	
	public BaseCommonAdapter(Context context, List<T> list, int layoutId) {
		this.mData = list;
		this.mContext = context;
		this.mItemLayoutId = layoutId;
	}

	public void refresh(){
		notifyDataSetChanged();
	}

	public void setList(List<T> list){
		this.mData = list;
	    notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public T getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder = getViewHolder(convertView, parent, position);
		convert(viewHolder, getItem(position),position);
		return viewHolder.getConvertView();
	}

	public abstract void convert(ViewHolder helper,T itme,int position);
	
	private ViewHolder getViewHolder(View convertView,ViewGroup parent,int position){
		return ViewHolder.get(mContext, convertView, parent, mItemLayoutId, position);
	}
}
