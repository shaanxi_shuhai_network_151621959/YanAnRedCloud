package red.yancloud.www.ui.listener

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/3 11:03
 * E_Mail：yh131412hys@163.com
 * Describe：权限申请
 * Change：
 * @Version：V1.0
 */
interface OnClickPermissionListener {

    fun permissionObtain()
}
