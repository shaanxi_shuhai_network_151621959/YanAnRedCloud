package red.yancloud.www.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import red.yancloud.www.R
import red.yancloud.www.internet.bean.ExpertTeamMore
import red.yancloud.www.util.TimeFormatUtil
import java.util.*

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：作者团队更多
 * Change：
 * @Version：V1.0
 */
class AuthorTeamMoreRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<ExpertTeamMore.DataBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_author_team_more_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_author_team_more_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_author_team_more_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.authorTeamMoreItemTitleTv.text=item.username
                holder.authorTeamMoreItemContentTv.text=item.introduction
                holder.authorTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.avatar).error(R.mipmap.ic_launcher).into(holder.authorTeamMoreItemBigImgIv)
                holder.authorTeamMoreItemTitleTv.text=item.username
                holder.authorTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.avatar).error(R.mipmap.ic_launcher).into(holder.authorTeamMoreItemImgIv)
                holder.authorTeamMoreItemTitleTv.text=item.username
                holder.authorTeamMoreItemContentTv.text=item.introduction
                holder.authorTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

//            if (position==0) {
//                val data = mData[0]
//
//                if (TextUtils.isEmpty(data.avatar)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }
            return LEFT_IMG
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<ExpertTeamMore.DataBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var authorTeamMoreItemTitleTv: AppCompatTextView
        var authorTeamMoreItemContentTv: AppCompatTextView
        var authorTeamMoreItemTimeTv: AppCompatTextView

        init {
            authorTeamMoreItemTitleTv = itemView!!.findViewById(R.id.authorTeamMoreItem_title_tv)
            authorTeamMoreItemContentTv = itemView.findViewById(R.id.authorTeamMoreItem_content_tv)
            authorTeamMoreItemTimeTv = itemView.findViewById(R.id.authorTeamMoreItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var authorTeamMoreItemTitleTv: AppCompatTextView
        var authorTeamMoreItemBigImgIv: AppCompatImageView
        var authorTeamMoreItemTimeTv: AppCompatTextView

        init {
            authorTeamMoreItemTitleTv = itemView.findViewById(R.id.authorTeamMoreItem_title_tv)
            authorTeamMoreItemBigImgIv = itemView.findViewById(R.id.authorTeamMoreItem_bigImg_iv)
            authorTeamMoreItemTimeTv = itemView.findViewById(R.id.authorTeamMoreItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var authorTeamMoreItemImgIv: AppCompatImageView
        var authorTeamMoreItemTitleTv: AppCompatTextView
        var authorTeamMoreItemContentTv: AppCompatTextView
        var authorTeamMoreItemTimeTv: AppCompatTextView

        init {
            authorTeamMoreItemImgIv = itemView.findViewById(R.id.authorTeamMoreItem_img_iv)
            authorTeamMoreItemTitleTv = itemView.findViewById(R.id.authorTeamMoreItem_title_tv)
            authorTeamMoreItemContentTv = itemView.findViewById(R.id.authorTeamMoreItem_content_tv)
            authorTeamMoreItemTimeTv = itemView.findViewById(R.id.authorTeamMoreItem_time_tv)
        }
    }
}
