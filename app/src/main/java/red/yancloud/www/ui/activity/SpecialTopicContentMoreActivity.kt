package red.yancloud.www.ui.activity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_special_topic_content_more.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseRefreshActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.SpecialTopicMore
import red.yancloud.www.ui.adapter.RecycleViewDivider
import red.yancloud.www.ui.adapter.SpecialTopicContentMoreRecyclerAdapter
import red.yancloud.www.util.ScreenUtils
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/31 14:36
 * E_Mail：yh131412hys@163.com
 * Describe：红云专题内容更多
 * Change：
 * @Version：V1.0
 */
class SpecialTopicContentMoreActivity : BaseRefreshActivity(), View.OnClickListener {

    private lateinit var mAdapter:SpecialTopicContentMoreRecyclerAdapter

    override val layoutId: Int
        get() = R.layout.activity_special_topic_content_more

    override fun configViews() {
        super.configViews()

        title_tv.text=intent.getStringExtra("title")
        back_iv.setOnClickListener(this)
    }

    override fun initRecyclerView() {

        specialTopicContentMoreActivity_rV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        specialTopicContentMoreActivity_rV.addItemDecoration(
            RecycleViewDivider(
                this,
                DividerItemDecoration.VERTICAL,
                ScreenUtils.dpToPx(10f).toInt(),
                R.color.color_f5
            )
        )
        mAdapter= SpecialTopicContentMoreRecyclerAdapter(R.layout.item_special_topic_content_more_article_list)
        specialTopicContentMoreActivity_rV.adapter = mAdapter
        mAdapter.onItemClickListener = object : BaseQuickAdapter.OnItemClickListener{
            override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
                startActivity(
                    Intent(this@SpecialTopicContentMoreActivity, NewsInfoActivity::class.java)
                        .putExtra(
                            "url",
                            "${Constants.API_BASE_RED_CLOUD_URL}News/index?id="+ (mAdapter.data[position] as SpecialTopicMore.DataBean.ListBean).id
                        )
                        .putExtra("model",Constants.CollectModel.SUBJECT)
                        .putExtra("id",intent.getStringExtra("id"))
                )
            }
        }
    }

    override fun getRefreshDataList() {

        RedCloudApis.getInstance().getSubjectMore(page.toString(),limit,intent.getStringExtra("id"),intent.getStringExtra("typeId"),object : Observer<SpecialTopicMore> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: SpecialTopicMore) {

                if (t.code == Constants.SYSTEM_OK) {
                    if(t.data != null&&t.data.page!=null&& t.data.list != null && t.data.list.size > 0){

                        errorView.visibility=View.GONE
                        smartRefreshLayout.visibility=View.VISIBLE

                        totalPage = t.data.page.totalPage
                        if (page==1) {

                            mAdapter.data.clear()
                            mAdapter.addData(t.data.list)
                            smartRefreshLayout.resetNoMoreData()
                            smartRefreshLayout.finishRefresh()
                        }else {

                            mAdapter.addData(t.data.list)
                            smartRefreshLayout.finishLoadMore()
                        }
                    }else{

                        if (page==1){

                            smartRefreshLayout.finishRefresh()

                            errorView.visibility=View.VISIBLE
                            smartRefreshLayout.visibility=View.GONE
                        }else{

                            ToastUtils.showToast(getString(R.string.data_all_load))
                            smartRefreshLayout.finishLoadMore()
                        }
                    }
                }else{

                    errorView.visibility=View.VISIBLE
                    smartRefreshLayout.visibility=View.GONE
                }
            }

            override fun onError(e: Throwable) {

                errorView.visibility=View.VISIBLE
                smartRefreshLayout.visibility=View.GONE
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {

                finish()
            }
        }
    }
}
