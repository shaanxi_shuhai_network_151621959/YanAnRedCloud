package red.yancloud.www.ui.activity

import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.fastjson.JSONObject
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_repository_sort.*
import kotlinx.android.synthetic.main.title_base_head.*
import okhttp3.ResponseBody
import red.yancloud.www.R
import red.yancloud.www.base.BaseActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.RepositorySort
import red.yancloud.www.ui.adapter.RepositorySortRecyclerAdapter
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/20 15:04
 * E_Mail：yh131412hys@163.com
 * Describe：知库分类
 * Change：
 * @Version：V1.0
 */
class RepositorySortActivity : BaseActivity(), View.OnClickListener {

    private val TAG = "RepositorySortActivity"

    private lateinit var mAdapter:RepositorySortRecyclerAdapter

    override val layoutId: Int
        get() = R.layout.activity_repository_sort

    override fun initData() {

        loadingDialog.show()
        RedCloudApis.getInstance().getZhiKuAllSortData(object : Observer<ResponseBody> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: ResponseBody) {

                val result = String(t.bytes())

                if (!TextUtils.isEmpty(result)) {

                    val parseObject = JSONObject.parseObject(result)
                    val code = parseObject.getString("code")
                    if (code == Constants.SYSTEM_OK) {

                        val repositorySortEntity = RepositorySort()
                        repositorySortEntity.code=code

                        val dataArray = parseObject.getJSONArray("data")
                        val dataList = ArrayList<RepositorySort.DataBean>()
                        repeat(dataArray.size) {

                            val dataObject = dataArray[it] as JSONObject

                            val dataBean = RepositorySort.DataBean()
                            dataBean.id=dataObject.getIntValue("id")
                            dataBean.parent_id=dataObject.getString("parent_id")
                            dataBean.sort=dataObject.getIntValue("sort")
                            dataBean.name=dataObject.getString("name")

                            val childrenObject = dataObject.getJSONObject("children")
                            val childrenList = ArrayList<RepositorySort.DataBean.ChildrenBean>()
                            childrenObject.entries.forEach {

                                val childObject = it.value as JSONObject
                                childrenList.add(
                                    RepositorySort.DataBean.ChildrenBean(childObject.getIntValue("id"),childObject.getIntValue("parent_id"),
                                        childObject.getIntValue("sort"),childObject.getString("name")))
                            }
                            dataBean.children=childrenList

                            dataList.add(dataBean)
                        }

                        repositorySortEntity.data=dataList
                        repositorySortEntity.msg=parseObject.getString("msg")

                        Log.d(TAG,repositorySortEntity.toString())

                        if (repositorySortEntity.data!=null&&repositorySortEntity.data.size>0) {

                            mAdapter.addData(repositorySortEntity.data)
                            loadingDialog.dismiss()
                            errorView.visibility=View.GONE
                            repositorySortActivity_rV.visibility=View.VISIBLE
                        }else{

                            ToastUtils.toastServiceError()

                            loadingDialog.dismiss()
                            errorView.visibility=View.VISIBLE
                            repositorySortActivity_rV.visibility=View.GONE
                        }
                    }
                }
            }

            override fun onError(e: Throwable) {

                ToastUtils.toastServiceError()

                loadingDialog.dismiss()
                errorView.visibility=View.VISIBLE
                repositorySortActivity_rV.visibility=View.GONE
            }
        })
    }

    override fun configViews() {

        title_tv.text="知库分类"

        back_iv.setOnClickListener(this)

//        Observable.create(object :ObservableOnSubscribe<List<RepositorySort>>{
//            override fun subscribe(emitter: ObservableEmitter<List<RepositorySort>>) {
//
//                val arrayList = ArrayList<RepositorySort>()
//                lateinit var repositorySortEntity : RepositorySort
//                var valueList:ArrayList<String>
//
//                val document = Jsoup.connect("http://zhi.yancloud.red/").get()
//                val select = document.select("[class=nav-content-box]>li")
//                for (element in select) {
//
//                    val type = element.select("span").select("a").text()
//                    if (type!="首页") {
//
//                        repositorySortEntity= RepositorySort()
//                        repositorySortEntity.key=type
//                        valueList = ArrayList<String>()
//
//                        Log.d(TAG, "type:$type")
//                        for (elementValue in element.select("ul>li")) {
//
//                            val value = elementValue.select("a").text()
//                            valueList.add(value)
//                            Log.d(TAG, "value:$value")
//                        }
//                        repositorySortEntity.valueList=valueList
//
//                        arrayList.add(repositorySortEntity)
//                    }
//                }
//                emitter.onNext(arrayList)
//            }
//        }).subscribeOn(Schedulers.io())
//            .unsubscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(object :Observer<List<RepositorySort>>{
//                override fun onComplete() {
//                }
//
//                override fun onSubscribe(d: Disposable) {
//                }
//
//                override fun onNext(t: List<RepositorySort>) {
//
//                    Log.d(TAG,"$t")
//                    mAdapter.addData(t)
//                }
//
//                override fun onError(e: Throwable) {
//                }
//            })
//
        repositorySortActivity_rV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mAdapter= RepositorySortRecyclerAdapter(R.layout.item_repository_sort_type_list)
        repositorySortActivity_rV.adapter=mAdapter
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {
                finish()
            }

            else -> {
            }
        }
    }
}
