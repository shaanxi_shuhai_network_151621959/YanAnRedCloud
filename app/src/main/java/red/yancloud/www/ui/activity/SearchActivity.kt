package red.yancloud.www.ui.activity

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.title_base_head.*
import red.yancloud.www.R
import red.yancloud.www.base.BaseRefreshActivity
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.api.RedCloudApis
import red.yancloud.www.internet.bean.DiffuseYanAn
import red.yancloud.www.ui.adapter.RecycleViewDivider
import red.yancloud.www.ui.adapter.SearchRecyclerAdapter
import red.yancloud.www.util.ScreenUtils
import red.yancloud.www.util.ToastUtils

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/8/22 18:15
 * E_Mail：yh131412hys@163.com
 * Describe：搜索结果列表
 * Change：
 * @Version：V1.0
 */
class SearchActivity : BaseRefreshActivity(), View.OnClickListener {

    private lateinit var mRecyclerAdapter: SearchRecyclerAdapter

    override val layoutId: Int
        get() = R.layout.activity_search

    override fun configViews() {
        super.configViews()

        back_iv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.back_iv -> {

                finish()
            }
        }
    }

    override fun initRecyclerView() {

        searchActivity_rV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        searchActivity_rV.addItemDecoration(
            RecycleViewDivider(
                this,
                DividerItemDecoration.VERTICAL,
                ScreenUtils.dpToPx(10f).toInt(),
                R.color.color_f5
            )
        )
        mRecyclerAdapter= SearchRecyclerAdapter(R.layout.item_diffuse_list)
        searchActivity_rV.adapter = mRecyclerAdapter
        mRecyclerAdapter.setOnItemClickListener(object : BaseQuickAdapter.OnItemClickListener{
            override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

//                startActivity(
//                    Intent(mContext, NewsInfoActivity::class.java)
//                        .putExtra(
//                            "url",
//                            "http://www.yancloud.red/Yancloudapp/Article/manhuiindex/?id=${(mRecyclerAdapter.data[position] as DiffuseYanAn.DataBean.ListBean).id}")
//                        .putExtra("model",Constants.CollectModel.PAINTING)
//                        .putExtra("id",(mRecyclerAdapter.data[position] as DiffuseYanAn.DataBean.ListBean).id)
//                )
            }
        })
    }

    override fun getRefreshDataList() {

        RedCloudApis.getInstance().getPaint(page.toString(), limit, "", "", object :
            Observer<DiffuseYanAn> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: DiffuseYanAn) {

                if (t.code== Constants.SYSTEM_OK) {

                    if(t.data != null){

                        if (t.data.page!=null&& t.data.list != null && t.data.list.size > 0){

                            errorView.visibility=View.GONE
                            smartRefreshLayout.visibility=View.VISIBLE

                            totalPage = t.data.page.totalPage
                            if (page==1) {

                                searchActivity_rV.scrollToPosition(0)
                                mRecyclerAdapter.data.clear()
                                mRecyclerAdapter.addData(t.data.list)
                                smartRefreshLayout.resetNoMoreData()
                                smartRefreshLayout.finishRefresh()

                                searchActivity_rV.scrollBy(1,0)
                                searchActivity_rV.scrollBy(-1,0)
                            }else {

                                mRecyclerAdapter.addData(t.data.list)
                                smartRefreshLayout.finishLoadMore()
                            }
                        }else{

                            if (page==1) {

                                errorView.visibility=View.VISIBLE
                                smartRefreshLayout.visibility=View.GONE
                            }else{

                                ToastUtils.showToast(getString(R.string.data_all_load))
                                smartRefreshLayout.finishLoadMore()
                            }
                        }
                    }else{

                        if (page==1){

                            smartRefreshLayout.finishRefresh()

                            errorView.visibility=View.VISIBLE
                            smartRefreshLayout.visibility=View.GONE
                        }else{

                            ToastUtils.showToast(getString(R.string.data_all_load))
                            smartRefreshLayout.finishLoadMoreWithNoMoreData()
                        }
                    }
                }else{

                    ToastUtils.showToast(t.msg)
                    errorView.visibility=View.VISIBLE
                    smartRefreshLayout.visibility=View.GONE
                }

                loadingDialog.dismiss()
            }

            override fun onError(e: Throwable) {

                loadingDialog.dismiss()
            }
        })
    }
}
