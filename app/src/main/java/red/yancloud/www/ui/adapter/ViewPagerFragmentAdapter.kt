package red.yancloud.www.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import java.util.ArrayList

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/5 15:05
 * E_Mail：yh131412hys@163.com
 * Describe：viewPager适配器
 * Change：
 * @Version：V1.0
 */
class ViewPagerFragmentAdapter : FragmentPagerAdapter {

    private var mTitles: Array<String>? = null

    private val mFragmentList = ArrayList<Fragment>()

    val fragmentList: List<Fragment>
        get() = mFragmentList

    constructor(fm: FragmentManager) : super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {}

    constructor(fm: FragmentManager, mTitles: Array<String>) : super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        this.mTitles = mTitles
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: Fragment) {
        mFragmentList.add(fragment)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        //设置标题
        return mTitles?.get(position)
    }
}
