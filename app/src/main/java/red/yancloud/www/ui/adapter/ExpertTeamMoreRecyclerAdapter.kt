package red.yancloud.www.ui.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.zzhoujay.richtext.RichText
import com.zzhoujay.richtext.ig.DefaultImageGetter
import red.yancloud.www.R
import red.yancloud.www.internet.bean.ExpertTeamMore
import red.yancloud.www.internet.bean.HotSearchEntryMore
import red.yancloud.www.util.TimeFormatUtil
import java.util.*

/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/7/25 10:01
 * E_Mail：yh131412hys@163.com
 * Describe：专家团队更多
 * Change：
 * @Version：V1.0
 */
class ExpertTeamMoreRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ARTICLE = 0
    val BIG_IMG = 1
    val LEFT_IMG = 2

    val mData = ArrayList<ExpertTeamMore.DataBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            ARTICLE -> ArticleViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_expert_team_more_article_list,parent,false))
            BIG_IMG -> BigImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_expert_team_more_big_img_list,parent,false))
            LEFT_IMG -> LeftImgViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_expert_team_more_left_img_list,parent,false))
            else -> ArticleViewHolder(null)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = mData[position]

        when (holder) {
            is ArticleViewHolder -> {

                holder.expertTeamMoreItemTitleTv.text=item.nickname
                RichText.from(item.introduction).imageGetter(DefaultImageGetter()).into(holder.expertTeamMoreItemContentTv)
                holder.expertTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is BigImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.avatar).error(R.mipmap.ic_launcher).into(holder.expertTeamMoreItemBigImgIv)
                holder.expertTeamMoreItemTitleTv.text=item.nickname
                holder.expertTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
            is LeftImgViewHolder -> {

                Glide.with(holder.itemView.context).load(item.avatar).error(R.mipmap.ic_launcher).into(holder.expertTeamMoreItemImgIv)
                holder.expertTeamMoreItemTitleTv.text=item.nickname
                RichText.from(item.introduction).imageGetter(DefaultImageGetter()).into(holder.expertTeamMoreItemContentTv)
                holder.expertTeamMoreItemTimeTv.text=TimeFormatUtil.formatTimeDay(item.created_at.toLong())
            }
        }

        holder.itemView.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                onItemClickListener!!.onItemClick(null,holder.itemView,position)
            }
        })
    }

    private var onItemClickListener: BaseQuickAdapter.OnItemClickListener? = null

    fun setOnItemClickListener(onItemClickListener: BaseQuickAdapter.OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    override fun getItemViewType(position: Int): Int {

        if (mData.size>0){

//            if (position==0) {
//                val data = mData[0]
//
//                if (TextUtils.isEmpty(data.avatar)) {
//
//                    return ARTICLE
//                }
//                return BIG_IMG
//            }
            return LEFT_IMG
        }
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun clear() {
        mData.clear()
        notifyDataSetChanged()
    }

    fun addData(newData: List<ExpertTeamMore.DataBean>) {
        mData.addAll(newData)
        notifyDataSetChanged()
    }

    inner class ArticleViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){

        var expertTeamMoreItemTitleTv: AppCompatTextView
        var expertTeamMoreItemContentTv: AppCompatTextView
        var expertTeamMoreItemTimeTv: AppCompatTextView

        init {
            expertTeamMoreItemTitleTv = itemView!!.findViewById(R.id.expertTeamMoreItem_title_tv)
            expertTeamMoreItemContentTv = itemView.findViewById(R.id.expertTeamMoreItem_content_tv)
            expertTeamMoreItemTimeTv = itemView.findViewById(R.id.expertTeamMoreItem_time_tv)
        }
    }

    inner class BigImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var expertTeamMoreItemTitleTv: AppCompatTextView
        var expertTeamMoreItemBigImgIv: AppCompatImageView
        var expertTeamMoreItemTimeTv: AppCompatTextView

        init {
            expertTeamMoreItemTitleTv = itemView.findViewById(R.id.expertTeamMoreItem_title_tv)
            expertTeamMoreItemBigImgIv = itemView.findViewById(R.id.expertTeamMoreItem_bigImg_iv)
            expertTeamMoreItemTimeTv = itemView.findViewById(R.id.expertTeamMoreItem_time_tv)
        }
    }

    inner class LeftImgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var expertTeamMoreItemImgIv: AppCompatImageView
        var expertTeamMoreItemTitleTv: AppCompatTextView
        var expertTeamMoreItemContentTv: AppCompatTextView
        var expertTeamMoreItemTimeTv: AppCompatTextView

        init {
            expertTeamMoreItemImgIv = itemView.findViewById(R.id.expertTeamMoreItem_img_iv)
            expertTeamMoreItemTitleTv = itemView.findViewById(R.id.expertTeamMoreItem_title_tv)
            expertTeamMoreItemContentTv = itemView.findViewById(R.id.expertTeamMoreItem_content_tv)
            expertTeamMoreItemTimeTv = itemView.findViewById(R.id.expertTeamMoreItem_time_tv)
        }
    }
}
