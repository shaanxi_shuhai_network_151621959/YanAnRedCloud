package red.yancloud.www.ui.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import red.yancloud.www.R
import red.yancloud.www.common.Constants
import red.yancloud.www.internet.bean.ReadBook


/**
 * @Org：www.shuhai.com(陕西书海网络科技有限公司)
 * @Author：风不会停息 on 2019/6/10 15:09
 * E_Mail：yh131412hys@163.com
 * Describe：阅读
 * Change：
 * @Version：V1.0
 */
class ReadBookRecyclerAdapter(layoutResId: Int) : BaseQuickAdapter<ReadBook.DataBean.BooksBean,BaseViewHolder>(layoutResId) {

    override fun convert(helper: BaseViewHolder, item: ReadBook.DataBean.BooksBean?) {

        if (item != null) {

            Glide.with(mContext).load(Constants.BASE_RED_CLOUD_HOST_URL.plus(item.thumbnail)).error(R.mipmap.ic_launcher).into(helper.getView(R.id.readBookItem_img_iv))
            helper.setText(R.id.readBookItem_title_tv,item.title)
            helper.setText(R.id.readBookItem_author_tv,"${item.author}\t著")
        }
    }
}