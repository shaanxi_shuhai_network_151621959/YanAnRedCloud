# YanAnRedCloud

#### 介绍
延安红云

#### 软件架构
项目整体采用Java语言和Android官方Kotlin语言，以Kotlin为主，使用AndroidStudio软件开发
1、数据库框架采用GreenDao
2、网络请求框架采用Retrofit+RxJava+OkHttp
3、消息总线框架采用EventBus
4、第三方登录采用友盟SDK
5、为优化Pdf阅读效果，采用Tencent X5 WebView内核加载
6、图片加载框架采用Glide
7、刷新和加载更多框架采用SmartRefreshLayout
8、视频播放框架采用NiceVieoPlayer

#### 工程目录
1、red.yancloud.www.app Application文件，初始化
2、red.yancloud.www.base 基类
3、red.yancloud.www.common 程序常量
4、red.yancloud.www.db.greendao 数据库
5、red.yancloud.www.db.sharedpreference 用户信息存储
6、red.yancloud.www.internet.api 网络请求接口类
7、red.yancloud.www.internet.bean 实体类
8、red.yancloud.www.manager 管理类
9、red.yancloud.www.ui.activity Activity类
10、red.yancloud.www.ui.adapter 适配器类
11、red.yancloud.www.ui.dialog 弹框类
12、red.yancloud.www.ui.fragment Fragment类
13、red.yancloud.www.ui.listener 监听
14、red.yancloud.www.ui.fragment Fragment类
15、red.yancloud.www.ui.read 书籍阅读类
16、red.yancloud.www.ui.view 自定义View
17、red.yancloud.www.util 工具类
18、red.yancloud.www.wxapi 微信回调
19、libray 第三方依赖

#### Android与html交互相关方法
1、//加载数据
fun loadData()
2、//跳转相关链接
fun gotourl(url: String,model: String,id: String)
3、//跳转相关页面
fun gotopage(key: String,value: String)
4、//跳转书籍详情
fun showbook(bookId: String)
5、//跳转视频详情
showvideo(videoId: String,fileAddress: String,content: String)
6、//消息弹框
fun alert(alert: String)

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 相关账号

腾讯开放平台、QQ互联
账号:2089959592
密码:hdyl15721964878

微信开放平台
账号:2089959592@qq.com
密码:red@cloud

微博开放平台
账号:18049505550
密码:red@cloud

友盟
账号:延安红云
密码:red@cloud
